using System;
using Xunit;

using WebApp.Extensions;


namespace WebApp.Extensions.UnitTests
{
    public class ArrayExtensionsTest
    {
        [Fact]
        public void IsNullOrEmpty_Returns_True_When_Array_Null()
        {
            byte[] arr = null;

            Assert.True(arr.IsNullOrEmpty<byte>());
        }

        [Fact]
        public void IsNullOrEmpty_Returns_True_When_Array_Empty()
        {
            byte[] arr = new byte[0];

            Assert.True(arr.IsNullOrEmpty<byte>());
        }
    }
}
