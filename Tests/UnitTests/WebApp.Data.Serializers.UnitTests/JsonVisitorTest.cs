using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

using Microsoft.Extensions.Logging.Abstractions;

using Xunit;

using WebApp.Data.Serializers.Converters.Visitors;

using WebApp.Testing.Mocks.Serializers;
using WebApp.Testing.Mocks.Serializers.Converters.Visitors;

namespace WebApp.Data.Serializers.UnitTests
{
    public class JsonVisitorTest
    {
        [Fact]
        public void JsonVisitor_Constructor_Throws_ArgumentNullException_When_Logger_Null()
        {
            MockJsonParser parser = new MockJsonParser();
            MockJsonSerializerWrapper wrapper = new MockJsonSerializerWrapper();

            Assert.Throws<ArgumentNullException>(() => new JsonVisitor(wrapper, parser, null));
        }

        [Fact]
        public void JsonVisitor_Constructor_Throws_ArgumentNullException_When_Parser_Null()
        {
            MockJsonSerializerWrapper wrapper = new MockJsonSerializerWrapper();

            Assert.Throws<ArgumentNullException>(() => new JsonVisitor(wrapper, null, new NullLogger<JsonVisitor>()));
        }

        [Fact]
        public void JsonVisitor_Constructor_Throws_ArgumentNullException_When_Serializer_Null()
        {
            MockJsonParser parser = new MockJsonParser();

            Assert.Throws<ArgumentNullException>(() => new JsonVisitor(null, parser, new NullLogger<JsonVisitor>()));
        }

        [Fact]
        public void JsonVisitor_Deserializes_MotionDetection_Updates_ParseModel()
        {
            const string expectedStringValue = default(string);
            const long expectedTimeValue = default(long);

            const string json = @"{}";

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedMotionDetection detection = new ParsedMotionDetection();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                reader.Read();

                visitor.DeserializeMotionDetection(detection, ref reader, options);

                Assert.Equal(expectedStringValue, detection.group);
                Assert.Equal(expectedStringValue, detection.monitorId);
                Assert.Equal(expectedStringValue, detection.plug);
                Assert.Equal(expectedTimeValue, detection.time);
                Assert.Equal(serializer.MotionInfo, detection.details);
            }
        }

        [Fact]
        public void JsonVisitor_Deserializes_MotionDetection_Reads_Expected_Properties()
        {
            const string json = @"{}";

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedMotionDetection detection = new ParsedMotionDetection();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                reader.Read();

                visitor.DeserializeMotionDetection(detection, ref reader, options);
                var expectedStringProperties = new HashSet<string> {
                    MotionDetectionPropertyNames.Group,
                    MotionDetectionPropertyNames.MonitorId,
                    MotionDetectionPropertyNames.Plug
                };

                var expectedLongProperties = new HashSet<string> {
                    MotionDetectionPropertyNames.Time
                };

                var expectedReadProperties = new HashSet<string> {
                    MotionDetectionPropertyNames.Details
                };

                Assert.Equal(expectedStringProperties, parser.StringProperties);
                Assert.Equal(expectedLongProperties, parser.LongProperties);
                Assert.Equal(expectedReadProperties, parser.ReadProperties);
            }
        }

        [Fact]
        public void JsonVisitor_Deserialize_MotionDetection_Throws_NotSupportedException_If_No_StartObject_Token()
        {
            const string json = @"{}";

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedMotionDetection detection = new ParsedMotionDetection();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                // ref struct, cannot use Assert.Throws here 
                bool exceptionRaised = false;
                try
                {
                    visitor.DeserializeMotionDetection(detection, ref reader, options);
                }
                catch (NotSupportedException)
                {
                    exceptionRaised = true;
                }

                Assert.True(exceptionRaised);
            }
        }


        [Fact]
        public void JsonVisitor_Deserialize_MotionDetection_Throws_NotSupportedException_If_No_EndObject_Token()
        {
            const string json = @"{
                ""group"": ""myGroup""
            }";

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedMotionDetection detection = new ParsedMotionDetection();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                reader.Read();

                // ref struct, cannot use Assert.Throws here 
                bool exceptionRaised = false;
                try
                {
                    visitor.DeserializeMotionDetection(detection, ref reader, options);
                }
                catch (NotSupportedException)
                {
                    exceptionRaised = true;
                }

                Assert.True(exceptionRaised);
            }
        }

        [Fact]
        public void JsonVisitor_Deserialize_MotionDetection_Calls_JsonSerializerWrapper()
        {
            const string json = @"{}";
            const int expectedSerializerCallCount = 1;

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedMotionDetection detection = new ParsedMotionDetection();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                reader.Read();

                visitor.DeserializeMotionDetection(detection, ref reader, options);

                Assert.Equal(expectedSerializerCallCount, serializer.DeserializeWithReaderCallCount);
            }
        }

        [Fact]
        public void JsonVisitor_Deserializes_MotionInfo_Throws_JsonException_When_StartArray_NotEncountered()
        {
            const string json = @"{ ""matrices"": [] }";

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedMotionInfo detection = new ParsedMotionInfo();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                bool expectedException = false;

                var expectedStringProperties = new HashSet<string>
                {
                    MotionInfoPropertyNames.Name,
                    MotionInfoPropertyNames.Plug,
                    MotionInfoPropertyNames.Reason
                };

                var expectedReadProperties = new HashSet<string>
                { MotionInfoPropertyNames.Matrices };

                reader.Read();

                // cannot use a ref struct in anonymous function, manually verify exception
                try
                {
                    visitor.DeserializeMotionInfo(detection, ref reader, options);

                }
                catch (JsonException)
                {
                    expectedException = true;
                }

                Assert.True(expectedException);
                Assert.Equal(expectedStringProperties, parser.StringProperties);
                Assert.Equal(expectedReadProperties, parser.ReadProperties);
            }
        }

        [Fact]
        public void JsonVisitor_Deserialize_MotionInfo_Throws_NotSupportedException_If_No_StartObject_Token()
        {
            const string json = @"{}";

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedMotionInfo info = new ParsedMotionInfo();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                // ref struct, cannot use Assert.Throws here 
                bool exceptionRaised = false;
                try
                {
                    visitor.DeserializeMotionInfo(info, ref reader, options);
                }
                catch (NotSupportedException)
                {
                    exceptionRaised = true;
                }

                Assert.True(exceptionRaised);
            }
        }

        [Fact]
        public void JsonVisitor_Deserializes_MotionLocation_Updates_ParseModel()
        {
            const double expectedMockedDoubleProperty = default(double);
            const string expectedMockedStringProperty = default(string);
            const string json = @"{}";

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser(); // return fixed stubbed default values
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedLocation location = new ParsedLocation();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                reader.Read();

                visitor.DeserializeMotionLocation(location, ref reader, options);

                Assert.Equal(expectedMockedDoubleProperty, location.x);
                Assert.Equal(expectedMockedDoubleProperty, location.y);
                Assert.Equal(expectedMockedDoubleProperty, location.width);
                Assert.Equal(expectedMockedDoubleProperty, location.height);
                Assert.Equal(expectedMockedDoubleProperty, location.confidence);
                Assert.Equal(expectedMockedStringProperty, location.tag);
            }
        }


        [Fact]
        public void JsonVisitor_Deserializes_MotionLocation_Reads_Expected_Properties()
        {
            const string json = @"{}";

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedLocation location = new ParsedLocation();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                reader.Read();

                visitor.DeserializeMotionLocation(location, ref reader, options);
                var expectedStringProperties = new HashSet<string> {
                    MotionLocationPropertyNames.Tag
                };

                var expectedDoubleProperties = new HashSet<string> {
                    MotionLocationPropertyNames.X,
                    MotionLocationPropertyNames.Y,
                    MotionLocationPropertyNames.Height,
                    MotionLocationPropertyNames.Width,
                    MotionLocationPropertyNames.Confidence
                };

                Assert.Equal(expectedStringProperties, parser.StringProperties);
                Assert.Equal(expectedDoubleProperties, parser.DoubleProperties);
            }
        }


        [Fact]
        public void JsonVisitor_Deserializes_MotionLocation_Throws_NotSupportedException_When_No_StartObject_Token()
        {
            const string json = @"{}";

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedLocation location = new ParsedLocation();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                bool exception = false;

                try
                {
                    visitor.DeserializeMotionLocation(location, ref reader, options);
                }
                catch (NotSupportedException)
                {
                    exception = true;
                }

                Assert.True(exception);
            }
        }

        [Fact]
        public void JsonVisitor_Deserializes_MotionLocation_Throws_NotSupportedException_When_No_EndObject_Token()
        {
            const string json = @"{
                ""group"": ""myGroup""
            }";

            MockJsonSerializerWrapper serializer = new MockJsonSerializerWrapper();
            MockJsonParser parser = new MockJsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            ParsedLocation location = new ParsedLocation();
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                Utf8JsonReader reader = new Utf8JsonReader(stream.ToArray());
                JsonSerializerOptions options = new JsonSerializerOptions();

                bool exception = false;

                try
                {
                    reader.Read();

                    visitor.DeserializeMotionLocation(location, ref reader, options);
                }
                catch (NotSupportedException)
                {
                    exception = true;
                }

                Assert.True(exception);
            }
        }
    }
}