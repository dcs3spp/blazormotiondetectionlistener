using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Contracts;
using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Data.Serializers.Json;
using WebApp.Testing.Fixtures;


namespace WebApp.Data.Serializers.UnitTests
{
    [Collection("Json collection")]
    public class MotionDetectionSerializerTest
    {
        private const string MotionResourceFile = @"WebApp.Testing.Utils.TestData.motion.json";

        private EmbeddedJsonFileResources _fixture;
        private ITestOutputHelper _output;

        public MotionDetectionSerializerTest(EmbeddedJsonFileResources fixture, ITestOutputHelper output)
        {
            _fixture = fixture;
            _output = output;
        }

        [Fact]
        public async Task MotionDetectionSerializer_FromJSONAsync_Deserializes()
        {
            MotionDetection expected = CreateMotionDetection();

            var mockSerializer = new Mock<IJsonSerializerWrapper>();
            mockSerializer.Setup(x => x.DeserializeAsync<MotionDetection>(It.IsAny<Stream>(), It.IsAny<JsonSerializerOptions>())).ReturnsAsync(expected);

            using (Stream stream = _fixture.GetStream(MotionResourceFile))
            {
                MotionDetectionSerializer<MotionDetection> serializer = new MotionDetectionSerializer<MotionDetection>(CreateConverters(), mockSerializer.Object);
                MotionDetection actual = await serializer.FromJSONAsync(stream);

                mockSerializer.VerifyAll();
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public async Task MotionDetectionSerializer_FromJSONAsync_Throws_ArgumentNullException__For_Null_Stream_Argument()
        {
            var mockSerializer = new Mock<IJsonSerializerWrapper>();

            var m = new MotionDetectionSerializer<MotionDetection>(CreateConverters(), mockSerializer.Object);

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await m.FromJSONAsync(null));
        }

        [Fact]
        public void MotionDetectionSerializer_FromJSON_Deserializes()
        {
            MotionDetection expected = CreateMotionDetection();

            var mockSerializer = new Mock<IJsonSerializerWrapper>();
            var serializationOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };

            mockSerializer.Setup(x => x.Deserialize<MotionDetection>(It.IsAny<string>(), It.IsAny<JsonSerializerOptions>())).Returns(expected);

            using Stream stream = _fixture.GetStream(MotionResourceFile);
            MotionDetectionSerializer<MotionDetection> serializer = new MotionDetectionSerializer<MotionDetection>(CreateConverters(), mockSerializer.Object);

            MotionDetection actual = serializer.FromJSON(stream);

            mockSerializer.VerifyAll();
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MotionDetectionSerializer_FromJSON_Throws_ArgumentNullException__For_Null_Stream_Argument()
        {
            var mockSerializer = new Mock<IJsonSerializerWrapper>();

            var m = new MotionDetectionSerializer<MotionDetection>(CreateConverters(), mockSerializer.Object);

            Assert.Throws<ArgumentNullException>(() => m.FromJSON(null));
        }

        [Fact]
        public void MotionDetectionSerializer_FromJSON_Deserializes_StreamClosed()
        {
            MotionDetection expected = CreateMotionDetection();

            var mockSerializer = new Mock<JsonSerializerWrapper>();
            var serializationOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };

            mockSerializer.Setup(x => x.Deserialize<MotionDetection>(It.IsAny<string>(), serializationOptions)).Returns(expected);

            using (Stream stream = _fixture.GetStream(MotionResourceFile))
            {
                MotionDetectionSerializer<MotionDetection> serializer = new MotionDetectionSerializer<MotionDetection>(CreateConverters(), mockSerializer.Object);
                MotionDetection actual = serializer.FromJSON(stream);

                Assert.Throws<ObjectDisposedException>(() => stream.Position);
            }
        }

        private MotionDetection CreateMotionDetection()
        {
            const long Time = 840;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo expectedInfo = new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "base64", 64, 48, Time);

            return new MotionDetection("myGroup", Time, "myMonitorId", Plugin, expectedInfo);
        }

        private IList<JsonConverter> CreateConverters()
        {
            IList<JsonConverter> lst = new List<JsonConverter>();

            var parser = new JsonParser();
            var mockSerializer = Mock.Of<JsonSerializerWrapper>();
            var visitor = new JsonVisitor(mockSerializer, parser, new NullLogger<JsonVisitor>());
            var parsedModel = new ParsedMotionDetection();
            var parsedInfoModel = new ParsedMotionInfo();

            lst.Add(new MotionDetectionConverter(visitor, parsedModel, new NullLogger<MotionDetectionConverter>()));
            lst.Add(new MotionInfoConverter(visitor, parsedInfoModel, new NullLogger<MotionInfoConverter>()));

            return lst;
        }
    }
}
