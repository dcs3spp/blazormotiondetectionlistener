using System;
using System.Text;
using System.Text.Json;

using Xunit;

using WebApp.Data.Serializers.Json;


namespace WebApp.Data.Serializers.UnitTests
{
    public class JsonParserTest
    {
        [Fact]
        public void JsonParser_ReadProperty_Successfully_Reads_Matching_PropertyName()
        {
            const string json = "{\"property\": \"value\"}";
            const string expectedPropertyName = "property";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            JsonParser parser = new JsonParser();

            string actual = parser.ReadProperty(ref reader, expectedPropertyName);

            Assert.Equal(expectedPropertyName, actual);
        }

        [Fact]
        public void JsonParser_ReadProperty_Throws_JsonException_When_JsonToken_Not_PropertyName()
        {
            const string json = "{}";
            const string property = "property";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            JsonParser parser = new JsonParser();

            bool exception = false;
            try // doing this because of System.Text.Json using ref struct, Utf8JsonReader
            {
                parser.ReadProperty(ref reader, property);
            }
            catch (JsonException)
            {
                exception = true;
            }

            Assert.True(exception);
        }

        [Fact]
        public void JsonParser_ReadProperty_Throws_JsonException_When_PropertyName_Does_Not_Expected_PropertyName()
        {
            const string json = "{\"property\": \"value\"}";
            const string propertyDoesNotExist = "anotherProperty";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            JsonParser parser = new JsonParser();

            bool exception = false;
            try // doing this because of System.Text.Json using ref struct, Utf8JsonReader
            {
                parser.ReadProperty(ref reader, propertyDoesNotExist);
            }
            catch (JsonException)
            {
                exception = true;
            }

            Assert.True(exception);
        }

        [Fact]
        public void JsonParser_ReadString_Property_Reads_String_For_A_Given_PropertyName()
        {
            const string json = "{\"property\": \"value\"}";
            const string property = "property";
            const string expectedValue = "value";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            JsonParser parser = new JsonParser();

            string actual = parser.ReadStringProperty(ref reader, property);

            Assert.Equal(expectedValue, actual);
        }

        [Fact]
        public void JsonParser_ReadLongProperty_Reads_Long_For_A_Property()
        {
            const string json = "{\"property\": 3277777777777}";
            const string property = "property";
            const long expectedValue = 3277777777777;

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            JsonParser parser = new JsonParser();

            long actual = parser.ReadLongProperty(ref reader, property);

            Assert.Equal(expectedValue, actual);
        }

        [Fact]
        public void JsonParser_ReadIntProperty_Throws_JsonException_When_Fails_To_Read_Long()
        {
            const string json = "{\"property\": 32777777777777777777777}";
            const string property = "property";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            bool exception = false;
            JsonException theException = null;

            // doing this because of System.Text.Json using ref struct, Utf8JsonReader
            try
            {
                JsonParser parser = new JsonParser();
                parser.ReadLongProperty(ref reader, property);
            }
            catch (JsonException je)
            {
                theException = je;
                exception = true;
            }

            Assert.True(exception);
            Assert.True(theException != null && theException.InnerException == null);
        }

        [Fact]
        public void JsonParser_ReadLongProperty_Throws_JsonException_With_Inner_InvalidOperationException_When_Trys_To_Read_NonNumeric_Value()
        {
            const string json = "{\"property\": \"a\"}";
            const string property = "property";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            bool exception = false;
            JsonException theException = null;

            // doing this because of System.Text.Json using ref struct, Utf8JsonReader
            try
            {
                JsonParser parser = new JsonParser();
                parser.ReadLongProperty(ref reader, property);
            }
            catch (JsonException je)
            {
                theException = je;
                exception = true;
            }

            Assert.True(exception);
            Assert.True(theException.InnerException != null && (typeof(InvalidOperationException) == theException.InnerException.GetType()));


        }

        [Fact]
        public void JsonParser_ReadIntProperty_Reads_Int_For_A_Property()
        {
            const string json = "{\"property\": 32}";
            const string property = "property";
            const int expectedValue = 32;

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            JsonParser parser = new JsonParser();

            int actual = parser.ReadIntProperty(ref reader, property);

            Assert.Equal(expectedValue, actual);
        }

        [Fact]
        public void JsonParser_ReadIntProperty_Throws_JsonException_When_Fails_To_Read_Int32()
        {
            const string json = "{\"property\": 9432.0}";
            const string property = "property";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            bool exception = false;
            JsonException theException = null;

            // doing this because of System.Text.Json using ref struct, Utf8JsonReader
            try
            {
                JsonParser parser = new JsonParser();
                parser.ReadIntProperty(ref reader, property);
            }
            catch (JsonException je)
            {
                theException = je;
                exception = true;
            }

            Assert.True(exception);
            Assert.True(theException != null && theException.InnerException == null);
        }

        [Fact]
        public void JsonParser_ReadIntProperty_Throws_JsonException_With_Inner_InvalidOperationException_When_Trys_To_Read_NonNumeric_Value()
        {
            const string json = "{\"property\": \"a\"}";
            const string property = "property";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            bool exception = false;
            JsonException theException = null;

            // doing this because of System.Text.Json using ref struct, Utf8JsonReader
            try
            {
                JsonParser parser = new JsonParser();
                parser.ReadIntProperty(ref reader, property);
            }
            catch (JsonException je)
            {
                theException = je;
                exception = true;
            }

            Assert.True(exception);
            Assert.True(theException.InnerException != null && (typeof(InvalidOperationException) == theException.InnerException.GetType()));
        }

        [Fact]
        public void JsonParser_ReadDoubleProperty_Reads_Double_For_A_Property()
        {
            const string json = "{\"property\": 32.2341}";
            const string property = "property";
            const double expectedValue = 32.2341;

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            JsonParser parser = new JsonParser();

            double actual = parser.ReadDoubleProperty(ref reader, property);

            Assert.Equal(expectedValue, actual);
        }

        [Fact]
        public void JsonParser_ReadDoubleProperty_Throws_JsonException_With_Inner_InvalidOperationException_When_Trys_To_Read_NonNumeric_Value()
        {
            const string json = "{\"property\": \"a\"}";
            const string property = "property";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            bool exception = false;
            JsonException theException = null;

            // doing this because of System.Text.Json using ref struct, Utf8JsonReader
            try
            {
                JsonParser parser = new JsonParser();
                parser.ReadDoubleProperty(ref reader, property);
            }
            catch (JsonException je)
            {
                theException = je;
                exception = true;
            }

            Assert.True(exception);
            Assert.True(theException.InnerException != null && (typeof(InvalidOperationException) == theException.InnerException.GetType()));
        }

        [Fact(Skip = "How to trigger Utf8JsonReader TryGetDouble() to return false")]
        public void JsonParser_ReadDoubleProperty_Throws_JsonException_When_Fails_To_Read_Double()
        {
            const string json = "{\"property\": 2.79769313486232E+308}";
            const string property = "property";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            bool exception = false;
            JsonException theException = null;

            // doing this because of System.Text.Json using ref struct, Utf8JsonReader
            try
            {
                JsonParser parser = new JsonParser();
                double result = parser.ReadDoubleProperty(ref reader, property);
                double x = result;
            }
            catch (JsonException je)
            {
                theException = je;
                exception = true;
            }

            Assert.True(exception);
            Assert.True(theException != null && theException.InnerException == null);
        }

        [Fact]
        public void JsonParser_ReadDateTimeOffsetProperty_Reads_DateTimeOffset_For_A_Property()
        {
            const string json = "{\"property\": \"2020-04-24T13:49:16+00:00\"}";
            const string property = "property";
            DateTimeOffset expectedValue = DateTimeOffset.Parse("2020-04-24T13:49:16+00:00");

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            JsonParser parser = new JsonParser();

            DateTimeOffset actual = parser.ReadDateTimeOffsetProperty(ref reader, property);

            Assert.Equal(expectedValue, actual);
        }


        [Fact]
        public void JsonParser_ReadDateTimeOffsetProperty_Throws_JsonException_When_Fails_To_Read_ISO8601_1_Format()
        {
            const string json = "{\"property\": \"26/07/2019\"}";
            const string property = "property";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            bool exception = false;
            JsonException theException = null;

            // doing this because of System.Text.Json using ref struct, Utf8JsonReader
            try
            {
                JsonParser parser = new JsonParser();
                parser.ReadDateTimeOffsetProperty(ref reader, property);
            }
            catch (JsonException je)
            {
                theException = je;
                exception = true;
            }

            Assert.True(exception);
            Assert.True(theException != null && theException.InnerException == null);
        }


        [Fact]
        public void JsonParser_ReadDateTimeOffsetProperty_Throws_JsonException_With_Inner_InvalidOperationException_When_Trys_To_Read_NonString_Value()
        {
            const string json = "{\"property\": 1}";
            const string property = "property";

            byte[] bytes = Encoding.UTF8.GetBytes(json);
            Utf8JsonReader reader = new Utf8JsonReader(bytes);
            reader.Read();

            bool exception = false;
            JsonException theException = null;

            // doing this because of System.Text.Json using ref struct, Utf8JsonReader
            try
            {
                JsonParser parser = new JsonParser();
                parser.ReadDateTimeOffsetProperty(ref reader, property);
            }
            catch (JsonException je)
            {
                theException = je;
                exception = true;
            }

            Assert.True(exception);
            Assert.True(theException.InnerException != null && (typeof(InvalidOperationException) == theException.InnerException.GetType()));
        }

        [Fact]
        public void JsonParser_ThrowFailedConversion_Throws_JsonException()
        {
            JsonParser parser = new JsonParser();
            Assert.Throws<JsonException>(() => parser.ThrowFailedConversion("Property"));
        }

        [Fact]
        public void JsonParser_ThrowUnrecognisedProperty_Throws_JsonException()
        {
            JsonParser parser = new JsonParser();
            Assert.Throws<JsonException>(() => parser.ThrowUnrecognisedProperty("Property"));
        }
    }
}