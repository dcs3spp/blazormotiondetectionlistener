using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Contracts;
using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Data.Serializers.Json;

using WebApp.Testing.Fixtures;
using WebApp.Testing.Mocks.Serializers.Converters.Visitors;


namespace WebApp.Data.Serializers.UnitTests
{
    [Collection("Json collection")]
    public class MotionInfoConverterTest
    {
        private const string MotionInfoResourceFile = @"WebApp.Testing.Utils.TestData.motioninfo.json";

        private EmbeddedJsonFileResources _fixture;
        private ITestOutputHelper _output;

        public MotionInfoConverterTest(EmbeddedJsonFileResources fixture, ITestOutputHelper output)
        {
            _fixture = fixture;
            _output = output;
        }



        [Fact]
        public void MotionInfoConverter_Deserialize()
        {
            /**

            Test that:
            1. accept has been called on the mocked ParsedInfo instance
            2. The ToModel method has been called on ParsedInfo object
            3. The actual MotionInfo object matches that returned by ParsedInfo.ToModel return value
            */

            MotionInfo expected = CreateMotionInfo();
            Utf8JsonReader reader = new Utf8JsonReader(new MemoryStream().ToArray());

            var parsedInfoModelMock = new MockParsedInfo();
            var visitorMock = new Mock<IVisitor>();

            var converter = new MotionInfoConverter(visitorMock.Object, parsedInfoModelMock, new NullLogger<MotionInfoConverter>());

            JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            serializeOptions.Converters.Add(converter);

            MotionInfo actual = converter.Read(ref reader, typeof(MotionInfo), serializeOptions);

            // System.Text.Json enforces the use of Utf8JsonReader for custom converters within the Read and Write methods
            // Due to the presence of a ref struct parameter being present in accept (enforced as a result of using System.Text.Json)
            // need to implement a manual mock to test that the accept method has been called with the expected arguments
            // and produced the expected return value. 
            Assert.Equal(1, parsedInfoModelMock.AcceptCallCount);
            Assert.Same(parsedInfoModelMock.AcceptArgs["options"], serializeOptions);
            Assert.Same(parsedInfoModelMock.AcceptArgs["visitor"], visitorMock.Object);
            Assert.Equal((JsonReaderState)parsedInfoModelMock.AcceptArgs["reader"], reader.CurrentState);
            Assert.Equal(1, parsedInfoModelMock.ToModelCallCount);
            Assert.Equal(expected, parsedInfoModelMock.ToModelReturnValue);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MotionInfoConverter_Serializes_ThrowsNotImplementedException()
        {
            MotionInfo expected = CreateMotionInfo();

            var parser = Mock.Of<IJsonParser>();
            var serializationWrapper = Mock.Of<JsonSerializerWrapper>();
            var visitor = new JsonVisitor(serializationWrapper, parser, new NullLogger<JsonVisitor>());
            var parsedInfoModel = new ParsedMotionInfo();

            var converter = new MotionInfoConverter(visitor, parsedInfoModel, new NullLogger<MotionInfoConverter>());

            JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            serializeOptions.Converters.Add(converter);

            Assert.Throws<NotImplementedException>(() => converter.Write(new Utf8JsonWriter(new MemoryStream()), expected, serializeOptions));
        }

        private MotionInfo CreateMotionInfo()
        {
            const long Time = 820;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            return new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "base64", 64, 48, Time);
        }

        /// <summary>
        /// Create a Utf8 reader for motion detection json file with first token read
        /// Throws JsonException if Read operation failed
        /// </summary>
        private Utf8JsonReader CreateUtf8JsonReader()
        {
            Utf8JsonReader reader;

            using (Stream stream = _fixture.GetStream(MotionInfoResourceFile))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    stream.CopyTo(m);
                    byte[] bytes = m.ToArray();

                    reader = new Utf8JsonReader(bytes, true, new JsonReaderState());
                    reader.Read();
                }
            }

            return reader;
        }
    }
}
