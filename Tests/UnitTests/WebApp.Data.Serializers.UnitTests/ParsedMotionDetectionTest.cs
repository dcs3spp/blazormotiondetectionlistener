using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

using Microsoft.Extensions.Logging.Abstractions;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Contracts;
using WebApp.Data.Serializers.UnitTests.Mocks.Converters.Visitors;

using WebApp.Testing.Mocks.Serializers;


namespace WebApp.Data.Serializers.Converters.Visitors
{

    public class ParsedMotionDetectionTest
    {
        private ITestOutputHelper _output;

        public ParsedMotionDetectionTest(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void ParsedMotionDetection_ToString_Reflects_State()
        {
            ParsedMotionDetection obj = CreateParsedMotionDetection();

            const string Header = "Parsed Motion Detection\n-----------------------\n";
            string expectedString = string.Format(
                "\n{0}\ndetails: {1}\ngroup: {2}\nmonitorId: {3}\nplugin: {4}\ntime: {5}\n",
                Header, obj.details, obj.group, obj.monitorId, obj.plug, obj.time.ToString());

            Assert.Equal(expectedString, obj.ToString());
        }

        [Fact]
        public void ParsedMotionDetection_ToModel_Returns_MotionDetection_Matching_State()
        {
            ParsedMotionDetection obj = CreateParsedMotionDetection();
            MotionDetection expected = CreateMotionDetection();

            Assert.Equal(expected, obj.ToModel());
        }

        [Fact]
        public void ParsedMotionDetection_Accept_Calls_IVisitor_DeserializeMotionDetection()
        {
            ParsedMotionDetection obj = new ParsedMotionDetection();

            MockJsonSerializerWrapper mockJsonSerializerWrapper = new MockJsonSerializerWrapper();

            MockJsonVisitor mockVisitor = new MockJsonVisitor(mockJsonSerializerWrapper, new NullLogger<IVisitor>());
            Utf8JsonReader reader = new Utf8JsonReader(new MemoryStream().ToArray());
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            obj.accept(mockVisitor, ref reader, options);

            Assert.Equal(1, mockVisitor.DeserializeMotionDetectionCallCount);
            Assert.Same(mockVisitor.DeserializeMotionDetectionArgs["target"], obj);
            Assert.Same(mockVisitor.DeserializeMotionDetectionArgs["options"], options);
            Assert.Equal((JsonReaderState)mockVisitor.DeserializeMotionDetectionArgs["reader"], reader.CurrentState);
        }

        [Fact]
        public void ParsedMotionDetection_Accept_Updates_State_Via_IVisitor_DeserializeMotionDetection()
        {
            ParsedMotionDetection obj = new ParsedMotionDetection();

            var mockJsonSerializer = new MockJsonSerializerWrapper();
            MockJsonVisitor mockVisitor = new MockJsonVisitor(mockJsonSerializer, new NullLogger<IVisitor>());

            Utf8JsonReader reader = new Utf8JsonReader(new MemoryStream().ToArray());
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            IList<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            obj.accept(mockVisitor, ref reader, options);

            Assert.Equal("myGroup", obj.group);
            Assert.Equal("myMonitorId", obj.monitorId);
            Assert.Equal("TensorFlow-WithFiltering-And-MQTT", obj.plug);
            Assert.Equal(840, obj.time);
            Assert.Equal("base64", obj.details.Img);
            Assert.Equal(64, obj.details.ImgHeight);
            Assert.Equal(48, obj.details.ImgWidth);
            Assert.Equal(840, obj.details.Time);
            Assert.Equal("person", obj.details.Reason);
            Assert.Equal("Tensorflow", obj.details.Name);
            Assert.Equal("TensorFlow-WithFiltering-And-MQTT", obj.details.Plug);
            Assert.True(expectedMatrices.SequenceEqual<MotionLocation>(obj.details.Matrices));
        }





        private ParsedMotionDetection CreateParsedMotionDetection()
        {
            const long Time = 840;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo info = new MotionInfo(Plugin, "Tensorflow", "person", expectedMatrices, "base64", 64, 48, Time);

            ParsedMotionDetection obj = new ParsedMotionDetection();
            obj.group = "myGroup";
            obj.monitorId = "myMonitorId";
            obj.plug = Plugin;
            obj.time = Time;
            obj.details = info;

            return obj;
        }

        private MotionDetection CreateMotionDetection()
        {
            const long Time = 840;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo info = new MotionInfo(Plugin, "Tensorflow", "person", expectedMatrices, "base64", 64, 48, Time);
            MotionDetection obj = new MotionDetection("myGroup", Time, "myMonitorId", Plugin, info);

            return obj;
        }
    }
}