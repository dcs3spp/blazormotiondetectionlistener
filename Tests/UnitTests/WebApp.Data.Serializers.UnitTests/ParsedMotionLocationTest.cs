using System.IO;
using System.Text.Json;

using Microsoft.Extensions.Logging.Abstractions;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Contracts;
using WebApp.Data.Serializers.UnitTests.Mocks.Converters.Visitors;

using WebApp.Testing.Mocks.Serializers;


namespace WebApp.Data.Serializers.Converters.Visitors
{

    public class ParsedMotionLocationTest
    {
        private ITestOutputHelper _output;

        public ParsedMotionLocationTest(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void ParsedLocation_ToString_Reflects_State()
        {
            const string Header = "Parsed Location\n---------------\n";

            ParsedLocation obj = CreateParsedLocation();

            string expectedString = string.Format("\n{0}\nx: {1}\ny: {2}\nwidth: {3}\nheight: {4}\ntag: {5}\nconfidence: {6}\n",
                       Header,
                       obj.x,
                       obj.y,
                       obj.width,
                       obj.height,
                       obj.tag,
                       obj.confidence
                   );

            Assert.Equal(expectedString, obj.ToString());
        }

        [Fact]
        public void ParsedLocation_ToModel_Returns_MotionLocation_Matching_State()
        {
            ParsedLocation obj = CreateParsedLocation();
            MotionLocation expected = CreateMotionLocation();

            Assert.Equal(expected, obj.ToModel());
        }

        [Fact]
        public void ParsedLocation_Accept_Calls_IVisitor_DeserializeMotionLocation()
        {
            ParsedLocation obj = new ParsedLocation();

            MockJsonSerializerWrapper mockJsonSerializerWrapper = new MockJsonSerializerWrapper();

            MockJsonVisitor mockVisitor = new MockJsonVisitor(mockJsonSerializerWrapper, new NullLogger<IVisitor>());
            Utf8JsonReader reader = new Utf8JsonReader(new MemoryStream().ToArray());
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            obj.accept(mockVisitor, ref reader, options);

            var locations = mockVisitor.DeserializeMotionLocationArgs[0];

            Assert.Equal(1, mockVisitor.DeserializeMotionLocationCallCount);
            Assert.Equal(1, mockVisitor.DeserializeMotionLocationArgs.Count);
            Assert.Same(locations["target"], obj);
            Assert.Same(locations["options"], options);
            Assert.Equal((JsonReaderState)locations["reader"], reader.CurrentState);
        }

        [Fact]
        public void ParsedLocation_Accept_Updates_State_Via_IVisitor_DeserializeMotionLocation()
        {
            ParsedLocation obj = new ParsedLocation();

            var mockJsonSerializer = new MockJsonSerializerWrapper();
            MockJsonVisitor mockVisitor = new MockJsonVisitor(mockJsonSerializer, new NullLogger<IVisitor>());

            Utf8JsonReader reader = new Utf8JsonReader(new MemoryStream().ToArray());
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            obj.accept(mockVisitor, ref reader, options);

            Assert.Equal(2.313079833984375, obj.x);
            Assert.Equal(1.0182666778564453, obj.y);
            Assert.Equal(373.25050354003906, obj.width);
            Assert.Equal(476.9341278076172, obj.height);
            Assert.Equal("person", obj.tag);
            Assert.Equal(0.7375929355621338, obj.confidence);
        }





        private ParsedLocation CreateParsedLocation()
        {
            ParsedLocation obj = new ParsedLocation();

            obj.x = 2.313079833984375;
            obj.y = 1.0182666778564453;
            obj.width = 373.25050354003906;
            obj.height = 476.9341278076172;
            obj.tag = "person";
            obj.confidence = 0.7375929355621338;

            return obj;
        }

        private MotionLocation CreateMotionLocation()
        {
            MotionLocation location = new MotionLocation(
                2.313079833984375,
                1.0182666778564453,
                373.25050354003906,
                476.9341278076172,
                "person",
                0.7375929355621338);

            return location;
        }
    }
}