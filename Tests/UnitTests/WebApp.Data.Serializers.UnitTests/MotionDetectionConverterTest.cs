using System.Collections.Generic;
using System.IO;
using System.Text.Json;

using Microsoft.Extensions.Logging.Abstractions;

using Moq;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Contracts;
using WebApp.Data.Serializers.Converters;

using WebApp.Testing.Fixtures;
using WebApp.Testing.Mocks.Serializers.Converters.Visitors;


namespace WebApp.Data.Serializers.UnitTests
{
    [Collection("Json collection")]
    public class MotionDetectionConverterTest
    {
        private const string MotionResourceFile = @"WebApp.Testing.Utils.TestData.motion.json";
        private const string MotionPascalCaseResourceFile = @"WebApp.Testing.Utils.TestData.motionPascalCase.json";
        private EmbeddedJsonFileResources _fixture;
        private ITestOutputHelper _output;

        public MotionDetectionConverterTest(EmbeddedJsonFileResources fixture, ITestOutputHelper output)
        {
            _fixture = fixture;
            _output = output;
        }

        [Fact]
        public void MotionDetectionConverter_Deserialize()
        {
            MotionDetection expected = CreateMotionDetection();
            Utf8JsonReader reader = CreateUtf8JsonReader();

            var parsedDetectionModelMock = new MockParsedDetection();
            var visitorMock = new Mock<IVisitor>();

            var converter = new MotionDetectionConverter(visitorMock.Object, parsedDetectionModelMock, new NullLogger<MotionDetectionConverter>());

            JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            serializeOptions.Converters.Add(converter);

            MotionDetection actual = converter.Read(ref reader, typeof(MotionDetection), serializeOptions);

            // System.Text.Json enforces the use of Utf8JsonReader for custom converters within the Read and Write methods
            // Due to the presence of a ref struct parameter being present in accept (enforced as a result of using System.Text.Json)
            // need to implement a manual mock to test that the accept method has been called with the expected arguments
            // and produced the expected return value. 
            Assert.Equal(1, parsedDetectionModelMock.AcceptCallCount);
            Assert.Same(parsedDetectionModelMock.AcceptArgs["options"], serializeOptions);
            Assert.Same(parsedDetectionModelMock.AcceptArgs["visitor"], visitorMock.Object);
            Assert.Equal((JsonReaderState)parsedDetectionModelMock.AcceptArgs["reader"], reader.CurrentState);
            Assert.Equal(1, parsedDetectionModelMock.ToModelCallCount);
            Assert.Equal(expected, parsedDetectionModelMock.ToModelReturnValue);
        }

        [Fact]
        public void MotionDetectionConverter_Serialize_CamelCase()
        {
            string expected = string.Empty;

            MotionDetection obj = CreateMotionDetection();
            using (StreamReader reader = new StreamReader(_fixture.GetStream(MotionResourceFile)))
            {
                expected = reader.ReadToEnd();
            }

            using (MemoryStream stream = new MemoryStream(1024))
            using (Utf8JsonWriter writer = new Utf8JsonWriter(stream, new JsonWriterOptions { Indented = true }))
            {
                var parsedDetectionModelMock = new MockParsedDetection();
                var visitorMock = new Mock<IVisitor>();
                var converter = new MotionDetectionConverter(visitorMock.Object, parsedDetectionModelMock, new NullLogger<MotionDetectionConverter>());

                JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
                serializeOptions.Converters.Add(converter);

                converter.Write(writer, obj, serializeOptions);
                writer.Flush();

                string json = System.Text.Encoding.UTF8.GetString(stream.ToArray());
                Assert.Equal(expected, json);
            }
        }


        [Fact]
        public void MotionDetectionConverter_Serialize_Null_JsonNamingPolicy()
        {
            string expected = string.Empty;

            MotionDetection obj = CreateMotionDetection();
            using (StreamReader reader = new StreamReader(_fixture.GetStream(MotionPascalCaseResourceFile)))
            {
                expected = reader.ReadToEnd();
            }

            using (MemoryStream stream = new MemoryStream(1024))
            using (Utf8JsonWriter writer = new Utf8JsonWriter(stream, new JsonWriterOptions { Indented = true }))
            {
                var parsedDetectionModelMock = new MockParsedDetection();
                var visitorMock = new Mock<IVisitor>();
                var converter = new MotionDetectionConverter(visitorMock.Object, parsedDetectionModelMock, new NullLogger<MotionDetectionConverter>());

                JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = null };
                serializeOptions.Converters.Add(converter);

                converter.Write(writer, obj, serializeOptions);
                writer.Flush();

                string json = System.Text.Encoding.UTF8.GetString(stream.ToArray());

                Assert.Equal(expected, json);
            }
        }


        private MotionDetection CreateMotionDetection()
        {
            const long Time = 820;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo expectedInfo = new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "YmFzZTY0", 64, 48, Time);

            return new MotionDetection("myGroup", Time, "myMonitorId", Plugin, expectedInfo);
        }

        /// <summary>
        /// Create a Utf8 reader for motion detection json file with first token read
        /// Throws JsonException if Read operation failed
        /// </summary>
        private Utf8JsonReader CreateUtf8JsonReader()
        {
            Utf8JsonReader reader;

            using (Stream stream = _fixture.GetStream(MotionResourceFile))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    stream.CopyTo(m);
                    byte[] bytes = m.ToArray();

                    reader = new Utf8JsonReader(bytes, true, new JsonReaderState());
                    reader.Read();
                }
            }

            return reader;
        }
    }
}
