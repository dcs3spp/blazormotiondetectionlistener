using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

using Microsoft.Extensions.Logging.Abstractions;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Contracts;
using WebApp.Data.Serializers.UnitTests.Mocks.Converters.Visitors;

using WebApp.Testing.Mocks.Serializers;


namespace WebApp.Data.Serializers.Converters.Visitors
{

    public class ParsedMotionInfoTest
    {
        private ITestOutputHelper _output;

        public ParsedMotionInfoTest(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void ParsedMotionInfo_ToString_Reflects_State()
        {
            const string Header = "Parsed Motion Info\n------------------\n";

            ParsedMotionInfo obj = CreateParsedMotionInfo();

            string matricesString = string.Join("\n", obj.matrices.Select(item => item.ToString()).ToArray());
            string expectedString = string.Format("{0}\nimg: {1}\nimgHeight: {2:0}\nimgWidth: {3:0}\nmatrices:\n {4}\nname: {5}\nplug: {6}\nreason: {7}\ntime: {8}",
               Header,
               obj.img,
               obj.imgHeight,
               obj.imgWidth,
               matricesString,
               obj.name,
               obj.plug,
               obj.reason,
               obj.time.ToString());

            Assert.Equal(expectedString, obj.ToString());
        }

        [Fact]
        public void ParsedMotionInfo_ToModel_Returns_MotionInfo_Matching_State()
        {
            ParsedMotionInfo obj = CreateParsedMotionInfo();
            MotionInfo expected = CreateMotionInfo();

            Assert.Equal(expected, obj.ToModel());
        }

        [Fact]
        public void ParsedMotionInfo_Accept_Calls_IVisitor_DeserializeMotionInfo()
        {
            ParsedMotionInfo obj = new ParsedMotionInfo();

            MockJsonSerializerWrapper mockJsonSerializerWrapper = new MockJsonSerializerWrapper();

            MockJsonVisitor mockVisitor = new MockJsonVisitor(mockJsonSerializerWrapper, new NullLogger<IVisitor>());
            Utf8JsonReader reader = new Utf8JsonReader(new MemoryStream().ToArray());
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            obj.accept(mockVisitor, ref reader, options);

            Assert.Equal(1, mockVisitor.DeserializeMotionInfoCallCount);
            Assert.Same(mockVisitor.DeserializeMotionInfoArgs["target"], obj);
            Assert.Same(mockVisitor.DeserializeMotionInfoArgs["options"], options);
            Assert.Equal((JsonReaderState)mockVisitor.DeserializeMotionInfoArgs["reader"], reader.CurrentState);
        }

        [Fact]
        public void ParsedMotionInfo_Accept_Updates_State_Via_IVisitor_DeserializeMotionInfo()
        {
            ParsedMotionInfo obj = new ParsedMotionInfo();

            var mockJsonSerializer = new MockJsonSerializerWrapper();
            MockJsonVisitor mockVisitor = new MockJsonVisitor(mockJsonSerializer, new NullLogger<IVisitor>());

            Utf8JsonReader reader = new Utf8JsonReader(new MemoryStream().ToArray());
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            IList<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            obj.accept(mockVisitor, ref reader, options);

            Assert.Equal("base64", obj.img);
            Assert.Equal(64, obj.imgHeight);
            Assert.Equal(48, obj.imgWidth);
            Assert.Equal(840, obj.time);
            Assert.Equal("person", obj.reason);
            Assert.Equal("Tensorflow", obj.name);
            Assert.Equal("TensorFlow-WithFiltering-And-MQTT", obj.plug);
            Assert.True(expectedMatrices.SequenceEqual<MotionLocation>(obj.matrices));
        }





        private ParsedMotionInfo CreateParsedMotionInfo()
        {
            const long Time = 840;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo info = new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "base64", 64, 48, Time);

            ParsedMotionInfo obj = new ParsedMotionInfo();
            obj.img = "base64";
            obj.imgHeight = 64;
            obj.imgWidth = 48;
            obj.matrices = expectedMatrices;
            obj.name = "Tensorflow";
            obj.plug = Plugin;
            obj.reason = "object";
            obj.time = Time;

            return obj;
        }

        private MotionInfo CreateMotionInfo()
        {
            const long Time = 840;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo info = new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "base64", 64, 48, Time);

            return info;
        }
    }
}