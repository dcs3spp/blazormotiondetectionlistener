using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data;
using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;


namespace WebApp.Data.Serializers.UnitTests
{
    public class JsonConverterFactoryFixture
    {
        public JsonSerializerOptions _Serializers { get; }

        public JsonConverterFactoryFixture()
        {
            _Serializers = JsonConvertersFactory.CreateDefaultJsonConverters(
                new NullLogger<MotionDetectionConverter>(),
                new NullLogger<MotionInfoConverter>(),
                new NullLogger<JsonVisitor>()
            );
        }
    }

    public class JsonConverterFactoryTest : IClassFixture<JsonConverterFactoryFixture>
    {
        public JsonConverterFactoryFixture _Factory { get; }
        public ITestOutputHelper _Output { get; }

        public JsonConverterFactoryTest(JsonConverterFactoryFixture factory, ITestOutputHelper output)
        {
            _Factory = factory;
            _Output = output;
        }


        [Fact]
        public void JsonConverterFactoryTest_CreateDefaultJsonSerializer_Creates_JsonSerializerOptions_with_Correct_Count_Of_Converters()
        {
            const int expectedConverterCount = 2;

            Assert.Equal(expectedConverterCount, _Factory._Serializers.Converters.Count);
        }

        [Fact]
        public void JsonConverterFactoryTest_CreateDefaultJsonSerializer_Creates_MotionDetectionConverter()
        {
            const int Index = 0;

            Assert.IsType<MotionDetectionConverter>(_Factory._Serializers.Converters[Index]);
        }

        [Fact]
        public void JsonConverterFactoryTest_CreateDefaultJsonSerializer_Creates_MotionInfoConverter()
        {
            const int Index = 1;

            Assert.IsType<MotionInfoConverter>(_Factory._Serializers.Converters[Index]);
        }

        [Fact]
        public void JsonConverterFactoryTest_CreateDefaultJsonSerializer_Creates_JsonSerializerOptions_Using_CamelCase_JsonNamingPolicy()
        {
            Assert.Equal(JsonNamingPolicy.CamelCase, _Factory._Serializers.PropertyNamingPolicy);
        }
    }
}