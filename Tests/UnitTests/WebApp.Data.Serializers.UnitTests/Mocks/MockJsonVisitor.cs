using System;
using System.Collections.Generic;
using System.Text.Json;

using Microsoft.Extensions.Logging;

using WebApp.Data.Serializers.Contracts;
using WebApp.Data.Serializers.Converters.Visitors;


namespace WebApp.Data.Serializers.UnitTests.Mocks.Converters.Visitors
{
    /** 
     * Have had to implement a manual mock due to limitation of mocking frameworks
     * such as Moq that currently do not allow testing that a method call has been
     * triggered that accepts a ref struct parameter
     *
     * Have also had to implement a custom wrapper for JsonSerializer since it
     * appears that Microsoft Fakes and other libraries are not available for .NET
     * core unless go down commercial pade solutions.
     */
    public class MockJsonVisitor : IVisitor
    {
        #region Data Members
        private ILogger _log;
        private IJsonSerializerWrapper _jsonSerializer;
        #endregion

        #region Test run state tracking - Needed due to lacking ability to test ref utf8JsonReader in Moq
        public IDictionary<string, Object> DeserializeMotionDetectionArgs { get; private set; }
        public IDictionary<string, Object> DeserializeMotionInfoArgs { get; private set; }
        public IList<IDictionary<string, Object>> DeserializeMotionLocationArgs { get; private set; }
        public int DeserializeMotionDetectionCallCount { get; private set; }
        public int DeserializeMotionInfoCallCount { get; private set; }
        public int DeserializeMotionLocationCallCount { get; private set; }
        #endregion

        #region Constructor

        public MockJsonVisitor(IJsonSerializerWrapper jsonSerializer, ILogger<IVisitor> log)
        {
            _log = log ?? throw new ArgumentNullException(nameof(log));
            _jsonSerializer = jsonSerializer ?? throw new ArgumentNullException(nameof(jsonSerializer));

            DeserializeMotionDetectionArgs = new Dictionary<string, Object>();
            DeserializeMotionInfoArgs = new Dictionary<string, Object>();
            DeserializeMotionLocationArgs = new List<IDictionary<string, Object>>();
        }
        #endregion

        #region Visitor Methods
        public void DeserializeMotionDetection(IParsedMotionDetection target, ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            this.DeserializeMotionDetectionCallCount++;
            DeserializeMotionDetectionArgs["target"] = target;
            DeserializeMotionDetectionArgs["reader"] = reader.CurrentState;
            DeserializeMotionDetectionArgs["options"] = options;

            target.group = "myGroup";
            target.time = 840;
            target.monitorId = "myMonitorId";
            target.plug = "TensorFlow-WithFiltering-And-MQTT";

            target.details = _jsonSerializer.Deserialize<MotionInfo>(ref reader, options);
        }

        public void DeserializeMotionInfo(IParsedMotionInfo target, ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            this.DeserializeMotionInfoCallCount++;
            DeserializeMotionInfoArgs["target"] = target;
            DeserializeMotionInfoArgs["reader"] = reader.CurrentState;
            DeserializeMotionInfoArgs["options"] = options;

            target.plug = "TensorFlow-WithFiltering-And-MQTT";
            target.name = "Tensorflow";
            target.reason = "person";
            target.matrices = ReadMatrices(ref reader, options);
            target.img = "base64";
            target.imgHeight = 64;
            target.imgWidth = 48;
            target.time = 840;

            _log.LogTrace(string.Format("Completed Read operation for MotionInfo object, token type is now => {0}", reader.TokenType));
        }
        public void DeserializeMotionLocation(IParsedLocation target, ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            this.DeserializeMotionLocationCallCount++;
            Dictionary<string, Object> dict = new Dictionary<string, Object>();
            dict["target"] = target;
            dict["reader"] = reader.CurrentState;
            dict["options"] = options;
            this.DeserializeMotionLocationArgs.Add(dict);

            target.x = 2.313079833984375;
            target.y = 1.0182666778564453;
            target.width = 373.25050354003906;
            target.height = 476.9341278076172;
            target.tag = "person";
            target.confidence = 0.7375929355621338;
        }
        #endregion

        private List<MotionLocation> ReadMatrices(ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            List<MotionLocation> matrices = new List<MotionLocation>();

            for (int count = 0; count < 1; count++)
            {
                ParsedLocation location = new ParsedLocation();
                location.accept(this, ref reader, options);
                matrices.Add(location.ToModel());
            }

            return matrices;
        }
    }
}
