using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using System.Text.Json.Serialization;

using Microsoft.Extensions.Logging.Abstractions;

using Moq;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data;
using WebApp.Data.Serializers;
using WebApp.Data.Serializers.Contracts;
using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Data.Serializers.Json;
using WebApp.Repository;
using WebApp.Testing.Fixtures;


namespace WebApp.UnitTests
{
    [Collection("Json collection")]
    public class MotionDetectionRepositoryTest
    {
        private ITestOutputHelper _output;
        private EmbeddedJsonFileResources _jsonResources;
        private const string JsonFileName = "WebApp.Testing.Utils.TestData.motion.json";

        public MotionDetectionRepositoryTest(EmbeddedJsonFileResources fixture, ITestOutputHelper output)
        {
            _output = output;
            _jsonResources = fixture;
        }

        [Fact]
        public async Task FromJSONAsync_Deserializes_Stream_Async()
        {
            MotionDetection expected = CreateMotionDetection();

            using (Stream stream = _jsonResources.GetStream(JsonFileName))
            {
                var mockSerializer = new Mock<IMotionDetectionSerializer<MotionDetection>>();
                mockSerializer.Setup(x => x.FromJSONAsync(stream)).ReturnsAsync(expected);

                MotionDetectionRepository repository = new MotionDetectionRepository(mockSerializer.Object, new NullLogger<MotionDetectionRepository>());

                MotionDetection actual = await repository.FromJSONAsync(stream);

                mockSerializer.VerifyAll();
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public async Task FromJSONAsync_Deserializes_Stream_Async_Throws_ArgumentNullException_When_Stream_Null()
        {
            var mockSerializer = new Mock<IMotionDetectionSerializer<MotionDetection>>();

            MotionDetectionRepository repository = new MotionDetectionRepository(mockSerializer.Object, new NullLogger<MotionDetectionRepository>());

            await Assert.ThrowsAsync<ArgumentNullException>(() => repository.FromJSONAsync(null));
        }

        [Fact]
        public void FromJSON_Deserializes_Stream_Sync()
        {
            MotionDetection expected = CreateMotionDetection();

            using (Stream stream = _jsonResources.GetStream(JsonFileName))
            {
                var mockSerializer = new Mock<IMotionDetectionSerializer<MotionDetection>>();
                mockSerializer.Setup(x => x.FromJSON(stream)).Returns(expected);

                MotionDetectionRepository repository = new MotionDetectionRepository(mockSerializer.Object, new NullLogger<MotionDetectionRepository>());

                MotionDetection actual = repository.FromJSON(stream);

                mockSerializer.VerifyAll();
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void FromJSONAsync_Deserializes_Stream_Sync_Throws_ArgumentNullException_When_Stream_Null()
        {
            var mockSerializer = new Mock<IMotionDetectionSerializer<MotionDetection>>();

            MotionDetectionRepository repository = new MotionDetectionRepository(mockSerializer.Object, new NullLogger<MotionDetectionRepository>());

            Assert.Throws<ArgumentNullException>(() => repository.FromJSON(null));
        }

        [Fact]
        public void FromJSON_Deserializes_Stream_Sync_Deserializes()
        {
            MotionDetection expected = CreateMotionDetection();

            using (Stream stream = _jsonResources.GetStream(JsonFileName))
            {
                IList<JsonConverter> converters = new List<JsonConverter>();

                JsonSerializerWrapper jsonSerializerWrapper = new JsonSerializerWrapper();
                JsonParser parser = new JsonParser();
                JsonVisitor visitor = new JsonVisitor(jsonSerializerWrapper, parser, new NullLogger<JsonVisitor>());
                ParsedMotionDetection parsedModel = new ParsedMotionDetection();
                ParsedMotionInfo parsedInfoModel = new ParsedMotionInfo();

                converters.Add(new MotionDetectionConverter(visitor, parsedModel, new NullLogger<MotionDetectionConverter>()));
                converters.Add(new MotionInfoConverter(visitor, parsedInfoModel, new NullLogger<MotionInfoConverter>()));

                MotionDetectionSerializer<MotionDetection> s = new MotionDetectionSerializer<MotionDetection>(converters, new JsonSerializerWrapper());

                MotionDetectionRepository repository = new MotionDetectionRepository(s, new NullLogger<MotionDetectionRepository>());
                MotionDetection actual = repository.FromJSON(stream);

                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void AddItem_Throws_ArgumentNullException_When_Item_Parameter_Null()
        {
            var mockSerializer = new Mock<IMotionDetectionSerializer<MotionDetection>>();

            MotionDetectionRepository repository = new MotionDetectionRepository(mockSerializer.Object, new NullLogger<MotionDetectionRepository>());

            Assert.Throws<ArgumentNullException>(() => repository.AddItem(null));
        }

        [Fact]
        public void Add_Item_Adds_Item_To_Buffer()
        {
            var item = CreateMotionDetection();
            var mockSerializer = new Mock<IMotionDetectionSerializer<MotionDetection>>();

            MotionDetectionRepository repository = new MotionDetectionRepository(mockSerializer.Object, new NullLogger<MotionDetectionRepository>());

            repository.AddItem(item);

            var enumerator = repository.GetEnumerator();
            enumerator.MoveNext();

            Assert.Equal(item, enumerator.Current);
        }

        [Fact]
        public void Get_Enumerator_Empty_When_No_Items()
        {
            var mockSerializer = new Mock<IMotionDetectionSerializer<MotionDetection>>();

            MotionDetectionRepository repository = new MotionDetectionRepository(mockSerializer.Object, new NullLogger<MotionDetectionRepository>());

            var enumerator = repository.GetEnumerator();

            Assert.Null(enumerator.Current);
            Assert.False(enumerator.MoveNext());
        }

        [Fact]
        public void Get_Enumerator_Iterates_All_Buffer_Items()
        {
            const short ExpectedCount = 2;

            var item = CreateMotionDetection();
            var item2 = new MotionDetection(item);
            var mockSerializer = new Mock<IMotionDetectionSerializer<MotionDetection>>();

            MotionDetectionRepository repository = new MotionDetectionRepository(mockSerializer.Object, new NullLogger<MotionDetectionRepository>());

            repository.AddItem(item);
            repository.AddItem(item2);

            var enumerator = repository.GetEnumerator();
            var count = 0;
            while (enumerator.MoveNext())
            {
                count++;
            }

            Assert.Equal(ExpectedCount, count);
        }

        private MotionDetection CreateMotionDetection()
        {
            const long Time = 820;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo expectedInfo = new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "YmFzZTY0", 64, 48, Time);

            return new MotionDetection("myGroup", Time, "myMonitorId", Plugin, expectedInfo);
        }
    }
}
