using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Minio;
using Moq;
using Xunit;


namespace WebApp.S3.UnitTests
{
    public class MinioClientWrapperTest
    {
        private const string AccessKey = "accesskey";
        private const string BucketName = "Bucket";
        private const string ObjectName = "ObjectName";

        private const string EndPoint = "endpoint";
        private const string Password = "password";


        [Fact]
        public void MinioClientWrapperTest_Constructor_Throws_ArgumentNull_Exception_When_EndPoint_Parameter_Null()
        {
            Assert.Throws<ArgumentNullException>(() => new MinioClientWrapper(null, AccessKey, Password));
        }

        [Fact]
        public void MinioClientWrapperTest_Constructor_Throws_ArgumentNull_Exception_When_AccessKey_Parameter_Null()
        {
            Assert.Throws<ArgumentNullException>(() => new MinioClientWrapper(EndPoint, null, Password));
        }

        [Fact]
        public void MinioClientWrapperTest_Constructor_Throws_ArgumentNull_Exception_When_Password_Parameter_Null()
        {
            Assert.Throws<ArgumentNullException>(() => new MinioClientWrapper(EndPoint, AccessKey, null));
        }

        [Fact]
        public async Task MinioClientWrapperTest_GetObjectAsync_Calls_GetObjectAsync_With_Matching_Parameters()
        {
            var mockClient = new Mock<IObjectOperations>();
            var mockAction = new Action<Stream>((Stream s) => { });
            var mockStream = new MemoryStream();

            mockClient.Setup(x => x.GetObjectAsync(BucketName, ObjectName, mockAction, null, default(CancellationToken)))
                .Returns(Task.CompletedTask).Verifiable();

            var wrapper = new MinioClientWrapper(mockClient.Object);

            await wrapper.GetObjectAsync(BucketName, ObjectName, mockAction);

            mockClient.Verify();
        }

        [Fact]
        public async Task MinioClientWrapperTest_GetObjectAsync_Calls_PutObjectAsync_With_Matching_Parameters()
        {
            var mockClient = new Mock<IObjectOperations>();
            var mockAction = new Action<Stream>((Stream s) => { });
            var mockSize = 100;
            var mockStream = new MemoryStream();


            mockClient.Setup(x => x.PutObjectAsync(BucketName, ObjectName, mockStream, mockSize, null, null, null, default(CancellationToken)))
                .Returns(Task.CompletedTask).Verifiable();

            var wrapper = new MinioClientWrapper(mockClient.Object);

            await wrapper.PutObjectAsync(BucketName, ObjectName, mockStream, mockSize, null, null, null, default(CancellationToken));

            mockClient.Verify();
        }
    }
}