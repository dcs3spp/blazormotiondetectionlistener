using Xunit;

using WebApp.Testing.Fixtures;


namespace WebApp.S3.UnitTests.TestCollections
{
    [CollectionDefinition("Json collection")]
    public class JsonCollection : ICollectionFixture<EmbeddedJsonFileResources>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces
    }
}
