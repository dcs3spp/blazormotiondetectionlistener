using Xunit;

using WebApp.S3.Config;
using WebApp.Testing.Fixtures;

namespace WebApp.S3.UnitTests
{
    public class S3ValidationTest
    {
        [Fact]
        public void S3ConfigValidation_Returns_ValidateOptionsResult_Success()
        {
            S3Config config = S3TestUtils.CreateDefaultS3Config();

            S3ConfigValidation obj = new S3ConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Succeeded);
        }

        [Fact]
        public void S3ConfigValidation_Returns_ValidateOptionsResult_Failed_Config_Null()
        {
            S3ConfigValidation obj = new S3ConfigValidation();

            var result = obj.Validate(string.Empty, null);

            Assert.True(result.Failed);
        }

        [Fact]
        public void S3ConfigValidation_Returns_ValidateOptionsResult_Failed_AccessKey_Null()
        {
            S3Config config = S3TestUtils.CreateDefaultS3Config();
            config.AccessKey = null;

            S3ConfigValidation obj = new S3ConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void S3ConfigValidation_Returns_ValidateOptionsResult_Failed_Bucket_Null()
        {
            S3Config config = S3TestUtils.CreateDefaultS3Config();
            config.Bucket = null;

            S3ConfigValidation obj = new S3ConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void S3ConfigValidation_Returns_ValidateOptionsResult_Failed_Invalid_Bucket_Name_Policy()
        {
            S3Config config = S3TestUtils.CreateDefaultS3Config();
            config.Bucket = "-";

            S3ConfigValidation obj = new S3ConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void S3ConfigValidation_Returns_ValidateOptionsResult_Failed_Endpoint_Null()
        {
            S3Config config = S3TestUtils.CreateDefaultS3Config();
            config.Endpoint = null;

            S3ConfigValidation obj = new S3ConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void S3ConfigValidation_Returns_ValidateOptionsResult_Failed_SecretKey_Null()
        {
            S3Config config = S3TestUtils.CreateDefaultS3Config();
            config.SecretKey = null;

            S3ConfigValidation obj = new S3ConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }
    }
}
