using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Minio.Exceptions;
using Moq;
using Newtonsoft.Json.Linq;
using RestSharp;
using Xunit;
using Xunit.Abstractions;

using WebApp.S3.Config;
using WebApp.S3.Contracts;
using WebApp.Testing.Fixtures;


namespace WebApp.S3.UnitTests
{
    [Collection("Json collection")]
    public class S3ServiceTest
    {
        private const string MotionResourceFile = @"WebApp.Testing.Utils.TestData.motionImg.json";

        const string JpegContentType = "image/jpeg";
        private string _Json { get; set; }

        public S3ServiceTest(EmbeddedJsonFileResources fixture, ITestOutputHelper output)
        {
            using (var stream = new StreamReader(fixture.GetStream(MotionResourceFile)))
            {
                _Json = stream.ReadToEnd();
            }
        }

        [Fact]
        public void S3Service_Constructor_Factory_Creates_MinioClient()
        {
            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);

            var called = false;
            var mockClient = new Mock<IMinioClient>();

            MinioFactory factory = (endpoint, login, passwd) =>
                {
                    called = true;
                    return mockClient.Object;
                };

            var svc = new S3Service(options, factory, new NullLogger<S3Service>());

            Assert.True(called);
        }

        [Fact]
        public void S3Service_Constructor_Throws_ArgumentNullException_When_Options_Null()
        {
            var mockClient = new Mock<IMinioClient>();

            Assert.Throws<ArgumentNullException>(() =>
            {
                return new S3Service(
                        null, (endpoint, login, passwd) => mockClient.Object, new NullLogger<S3Service>()
                );
            });
        }

        [Fact]
        public void S3Service_Constructor_Throws_ArgumentNullException_When_Logger_Null()
        {
            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);
            var mockClient = new Mock<IMinioClient>();

            Assert.Throws<ArgumentNullException>(() =>
            {
                return new S3Service(
                        options, (endpoint, login, passwd) => mockClient.Object, null
                );
            });
        }

        [Fact]
        public void S3Service_Constructor_Throws_ArgumentNullException_When_FactoryMethod_Null()
        {
            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);
            var mockClient = new Mock<IMinioClient>();

            Assert.Throws<ArgumentNullException>(() =>
            {
                return new S3Service(
                        options, null, new NullLogger<S3Service>()
                );
            });
        }

        [Fact]
        public async Task S3Service_UploadImage_Extracts_Image_From_Payload_And_Replaces_With_ID_After_Upload()
        {
            const string objName = "objName";
            string guid = Guid.NewGuid().ToString();

            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);

            var mockClient = new Mock<IMinioClient>();
            mockClient.Setup(m =>
                m.PutObjectAsync(
                    config.Bucket,
                    objName,
                    It.IsAny<BufferedStream>(),
                    It.IsAny<long>(),
                    JpegContentType,
                    null, null, default(CancellationToken)
                )
            );

            S3Service svc = new S3Service(
                    options, (endpoint, login, passwd) => mockClient.Object, new NullLogger<S3Service>()
            );
            var modifiedPayload = await svc.UploadImage(_Json, guid);

            JObject rootElement = JObject.Parse(Encoding.UTF8.GetString(modifiedPayload));
            JObject details = (JObject)rootElement["details"];
            string img = details["img"].Value<string>();

            Assert.Equal(img, guid);
        }

        [Fact]
        public async Task S3Service_UploadAsync_Calls_PutObjectAsync_With_Expected_Args()
        {
            const string objName = "objName";
            const string testData = "testData";

            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);

            using (var buffer = new BufferedStream(new MemoryStream(Encoding.UTF8.GetBytes(testData))))
            {
                var mockClient = new Mock<IMinioClient>();
                mockClient.Setup(m =>
                    m.PutObjectAsync(
                        config.Bucket,
                        objName,
                        buffer,
                        buffer.Length,
                        JpegContentType,
                        null, null, default(CancellationToken)
                    )
                ).Verifiable();

                S3Service svc = new S3Service(
                        options, (endpoint, login, passwd) => mockClient.Object, new NullLogger<S3Service>()
                );

                await svc.UploadAsync(buffer, objName);

                mockClient.Verify(m =>
                    m.PutObjectAsync(
                        config.Bucket,
                        objName,
                        buffer,
                        buffer.Length,
                        JpegContentType,
                        null, null, default(CancellationToken)),
                        Times.Once()
                );
            }
        }

        [Fact]
        public async Task S3Service_UploadAsync_Throws_ArgumentNullException_When_Buffer_Null()
        {
            const string objectName = "testData";

            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);

            var mockClient = new Mock<IMinioClient>();

            S3Service svc = new S3Service(
                    options, (endpoint, login, passwd) => mockClient.Object, new NullLogger<S3Service>()
            );

            await Assert.ThrowsAsync<ArgumentNullException>(() => svc.UploadAsync(null, objectName));
        }

        [Fact]
        public async Task S3Service_UploadAsync_Throws_ArgumentNullException_When_ObjectName_Null()
        {
            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);

            var mockClient = new Mock<IMinioClient>();

            S3Service svc = new S3Service(
                    options, (endpoint, login, passwd) => mockClient.Object, new NullLogger<S3Service>()
            );

            await Assert.ThrowsAsync<ArgumentNullException>(() =>
                svc.UploadAsync(new BufferedStream(new MemoryStream()), null));
        }

        [Fact]
        public async Task S3Service_UploadAsync_Returns_When_MinioException_Caught()
        {
            const string objName = "objName";
            const string testData = "testData";

            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);

            using (var buffer = new BufferedStream(new MemoryStream(Encoding.UTF8.GetBytes(testData))))
            {
                var mockResponse = new Mock<IRestResponse>();
                var mockClient = new Mock<IMinioClient>();
                mockClient.Setup(m =>
                    m.PutObjectAsync(
                        config.Bucket,
                        objName,
                        buffer,
                        buffer.Length,
                        JpegContentType,
                        null, null, default(CancellationToken)
                    )
                ).Throws(new MinioException(mockResponse.Object));

                S3Service svc = new S3Service(
                        options, (endpoint, login, passwd) => mockClient.Object, new NullLogger<S3Service>()
                );

                await Assert.ThrowsAsync<MinioException>(() => svc.UploadAsync(buffer, objName));
            }
        }

        [Fact]
        public async Task S3Service_DownloadObject_Throws_ArgumentNullException_When_ObjectName_Null()
        {
            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);

            var mockClient = new Mock<IMinioClient>();

            S3Service svc = new S3Service(
                    options, (endpoint, login, passwd) => mockClient.Object, new NullLogger<S3Service>()
            );

            await Assert.ThrowsAsync<ArgumentNullException>(() =>
                svc.DownloadObject(null));
        }

        [Fact]
        public async Task S3Service_DownloadObject_Calls_GetObjectAsync_With_Expected_Args()
        {
            const string objName = "objName";
            const string testData = "testData";

            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);

            Action<Stream> action = delegate (Stream stream) { };

            var mockClient = new Mock<IMinioClient>();
            mockClient.Setup(m =>
                m.GetObjectAsync(
                    config.Bucket,
                    objName,
                    It.IsAny<Action<Stream>>(),
                    null,
                    default(CancellationToken)
                )
            ).Returns(Task.FromResult<BufferedStream>(new BufferedStream(new MemoryStream(Encoding.UTF8.GetBytes(testData))))).Verifiable(); //.Returns<Task<BufferedStream>>(x => x).Verifiable();

            S3Service svc = new S3Service(
                    options, (endpoint, login, passwd) => mockClient.Object, new NullLogger<S3Service>()
            );

            using (var result = await svc.DownloadObject(objName))
            {
                mockClient.Verify(m =>
                    m.GetObjectAsync(
                        config.Bucket,
                        objName,
                        It.IsAny<Action<Stream>>(),
                        null,
                        default(CancellationToken)
                    )
                );
            }
        }

        [Fact]
        public async Task S3Service_DownloadObject_Throws_MinioException_When_Error_Encountered()
        {
            const string objName = "objName";

            var config = S3TestUtils.CreateDefaultS3Config();
            var options = Options.Create<S3Config>(config);

            Action<Stream> action = delegate (Stream stream) { };

            var mockClient = new Mock<IMinioClient>();
            var mockResponse = new Mock<IRestResponse>();

            mockClient.Setup(m =>
                m.GetObjectAsync(
                    config.Bucket,
                    objName,
                    It.IsAny<Action<Stream>>(),
                    null,
                    default(CancellationToken)
                )
            ).Throws(new MinioException(mockResponse.Object));

            S3Service svc = new S3Service(
                    options, (endpoint, login, passwd) => mockClient.Object, new NullLogger<S3Service>()
            );

            await Assert.ThrowsAsync<MinioException>(() => svc.DownloadObject(objName));
        }
    }
}
