using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;


namespace WebApp.Realtime.SignalR.UnitTests
{
    public class MotionHubTest
    {
        [Fact]
        public void MotionHub_Constructor_Sets_Logger()
        {
            MotionHub hub = new MotionHub(new NullLogger<MotionHub>());

            Assert.Equal(typeof(NullLogger<MotionHub>), hub.Logger.GetType());
        }

        [Fact]
        public async Task MotionHub_OnConnectedAsync()
        {
            var mockClientContext = new Mock<HubCallerContext>();

            MotionHub hub = new MotionHub(new NullLogger<MotionHub>())
            {
                Context = mockClientContext.Object
            };
            mockClientContext.SetupGet(x => x.ConnectionId).Returns("1054");
            mockClientContext.SetupGet(x => x.User.Identity.Name).Returns("User");

            await hub.OnConnectedAsync();

            mockClientContext.Verify();
        }

        [Fact]
        public async Task MotionHub_OnDisconnectedAsync_Exception_Null()
        {
            var mockClientContext = new Mock<HubCallerContext>();

            MotionHub hub = new MotionHub(new NullLogger<MotionHub>())
            {
                Context = mockClientContext.Object
            };

            mockClientContext.SetupGet(x => x.ConnectionId).Returns("1054");
            mockClientContext.SetupGet(x => x.User.Identity.Name).Returns("User");

            await hub.OnDisconnectedAsync(null);

            mockClientContext.VerifyAll();
        }

        [Fact]
        public async Task MotionHub_OnDisconnectedAsync_Exception()
        {
            var mockClientContext = new Mock<HubCallerContext>();
            var mockException = new Mock<ArgumentNullException>();

            MotionHub hub = new MotionHub(new NullLogger<MotionHub>())
            {
                Context = mockClientContext.Object
            };

            mockClientContext.SetupGet(x => x.ConnectionId).Returns("1054");
            mockClientContext.SetupGet(x => x.User.Identity.Name).Returns("User");
            mockException.SetupGet(x => x.Message).Returns("Exception Message");

            await hub.OnDisconnectedAsync(mockException.Object);

            mockClientContext.VerifyAll();
            mockException.VerifyGet(x => x.Message);
        }
    }
}
