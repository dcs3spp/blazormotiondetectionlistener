using System.Threading.Tasks;

using Microsoft.Extensions.Logging.Abstractions;
using Xunit;
using Xunit.Abstractions;


using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Realtime.SignalR.Client;


namespace WebApp.Realtime.SignalR.UnitTests
{
    public class HubProxyFactoryTest
    {
        private ITestOutputHelper _Output { get; }
        private IHubProxy _Client { get; }
        private IHubProxyFactory _Factory { get; }


        public HubProxyFactoryTest(ITestOutputHelper output)
        {
            _Factory = new HubProxyFactory();
            _Client = _Factory.Create("http://localhost:4443", JsonConvertersFactory.CreateDefaultJsonConverters(
                new NullLogger<MotionDetectionConverter>(),
                new NullLogger<MotionInfoConverter>(),
                new NullLogger<JsonVisitor>()
            ));

            _Output = output;
        }

        [Fact]
        public async Task HubProxyFactory_Create_Constructs_IHubProxy()
        {
            try
            {
                Assert.IsAssignableFrom<IHubProxy>(_Client);
            }
            finally
            {
                await _Client.DisposeAsync();
            }
        }
    }
}
