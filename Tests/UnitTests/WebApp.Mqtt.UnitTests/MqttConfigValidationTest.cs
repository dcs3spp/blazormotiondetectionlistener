using Xunit;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Testing.Helpers;


namespace WebApp.Mqtt.UnitTests
{
    public class MqttConfigValidationTest
    {
        [Fact]
        public void MqttConfigValidation_Returns_ValidateOptionsResult_Success_WithTls_CACerts()
        {
            const string caPath = "caCert.pem";

            MqttConfig config = MqttTestUtils.CreateMqttConfigWithTLS(caPath);
            MqttConfigValidation obj = new MqttConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Succeeded);
        }

        [Fact]
        public void MqttConfigValidation_Returns_ValidateOptionsResult_Failed_WithTls_CACerts_Empty()
        {
            MqttConfig config = MqttTestUtils.CreateMqttConfigWithTLS(string.Empty);

            MqttConfigValidation obj = new MqttConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void MqttConfigValidation_Returns_ValidateOptionsResult_Success_WithoutTls()
        {
            MqttConfig config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            MqttConfigValidation obj = new MqttConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Succeeded);
        }

        [Fact]
        public void MqttConfigValidation_Returns_ValidateOptionsResult_Failed_Config_Null()
        {
            MqttConfigValidation obj = new MqttConfigValidation();

            var result = obj.Validate(string.Empty, null);

            Assert.True(result.Failed);
        }

        [Fact]
        public void MqttConfigValidation_Returns_ValidateOptionsResult_Failed_Host_Null()
        {
            MqttConfig config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            config.Host = null;

            MqttConfigValidation obj = new MqttConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void MqttConfigValidation_Returns_ValidateOptionsResult_Failed_Password_Null()
        {
            MqttConfig config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            config.Password = null;

            MqttConfigValidation obj = new MqttConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void MqttConfigValidation_Returns_ValidateOptionsResultFailed_Topic_Null()
        {
            MqttConfig config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            config.Topic = null;

            MqttConfigValidation obj = new MqttConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void MqttConfigValidation_Returns_ValidateOptionsResultFailed_UserName_Null()
        {
            MqttConfig config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            config.UserName = null;

            MqttConfigValidation obj = new MqttConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }
    }
}