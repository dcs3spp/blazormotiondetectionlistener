using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

using Moq;
using MQTTnet.Client.Options;
using Xunit;

using WebApp.Mqtt.Contracts;
using WebApp.Mqtt.Factory;
using WebApp.Mqtt.Testing.Helpers;


namespace WebApp.Mqtt.UnitTests
{
    public class MQTTnetManagedClientTlsOptionsBuilderTest
    {
        [Fact]
        public void MQTTnetManagedClientTlsOptionsBuilder_WithOptions_Returns_Builder()
        {
            const string certPath = "Certs/digiCert.pem";

            var builder = new MQTTnetManagedClientTlsOptionsBuilder();
            var mockFactory = new Mock<IX509CertificateWrapper>();

            using (var cert = X509Certificate.CreateFromCertFile(certPath))
            {
                mockFactory.Setup(x => x.CreateFromCertFile(certPath)).Returns(cert);

                var config = MqttTestUtils.CreateMqttConfigWithTLS(certPath);

                var result = builder.WithOptions(config, (context) => true, mockFactory.Object);

                mockFactory.Verify(x => x.CreateFromCertFile(certPath), Times.Once());

                Assert.Same(builder, result);
            }
        }

        [Fact]
        public void MQTTnetManagedClientTlsOptionsBuilder_WithOptions_Throws_ArgumentNullException_When_Config_Parameter_Null()
        {
            var builder = new MQTTnetManagedClientTlsOptionsBuilder();
            var mockFactory = new Mock<IX509CertificateWrapper>();

            Assert.Throws<ArgumentNullException>(() => builder.WithOptions(null, (context) => true, mockFactory.Object));
        }

        [Fact]
        public void MQTTnetManagedClientTlsOptionsBuilder_WithOptions_Throws_ArgumentNullException_When_CertValidationHandler_Parameter_Null()
        {
            var builder = new MQTTnetManagedClientTlsOptionsBuilder();
            var mqttConfig = new Config.MqttConfig();
            var mockFactory = new Mock<IX509CertificateWrapper>();

            Assert.Throws<ArgumentNullException>(() => builder.WithOptions(mqttConfig, null, mockFactory.Object));
        }

        [Fact]
        public void MQTTnetManagedClientTlsOptionsBuilder_WithOptions_Throws_ArgumentNullException_When_X509Factory_Parameter_Null()
        {
            var builder = new MQTTnetManagedClientTlsOptionsBuilder();
            var mqttConfig = new Config.MqttConfig();

            Assert.Throws<ArgumentNullException>(() => builder.WithOptions(mqttConfig, (context) => true, null));
        }


        [Fact]
        public void MqttnetManagedClientTlsOptionsBuilder_WithOptions_Builds_Options()
        {
            const string certPath = "Certs/digiCert.pem";

            var builder = new MQTTnetManagedClientTlsOptionsBuilder();
            var config = MqttTestUtils.CreateMqttConfigWithTLS(certPath);
            var expectedOptions = MqttTestUtils.ToManagedClientOptions(config);
            var expectedTls = expectedOptions.ClientOptions.ChannelOptions.TlsOptions;

            MqttClientTlsOptions actualTls = null;

            var mockFactory = new Mock<IX509CertificateWrapper>();

            try
            {
                using (var cert = X509Certificate.CreateFromCertFile(certPath))
                {
                    mockFactory.Setup(x => x.CreateFromCertFile(certPath)).Returns(cert);

                    builder.WithOptions(config, (context) => true, mockFactory.Object);
                    var options = builder.Build();

                    actualTls = options.ClientOptions.ChannelOptions.TlsOptions;

                    Assert.Equal(expectedTls.AllowUntrustedCertificates, actualTls.AllowUntrustedCertificates);
                    Assert.Equal(expectedTls.UseTls, actualTls.UseTls);
                    Assert.NotNull(actualTls.CertificateValidationHandler);
#if WINDOWS_UWP
                    Assert.True(expectedTls.Certificates.SequenceEqual<byte[]>(actualTls.Certificates));
#else
                    Assert.True(expectedTls.Certificates.SequenceEqual<X509Certificate>(actualTls.Certificates));
#endif
                }
            }
            finally
            {
#if !WINDOWS_UWP
                if (expectedTls != null)
                    MqttTestUtils.DisposeCertificates(expectedTls.Certificates);

                if (actualTls != null)
                    MqttTestUtils.DisposeCertificates(expectedTls.Certificates);
#endif
            }

        }
    }
}
