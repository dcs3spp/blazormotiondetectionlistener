using System;
using Xunit;

using Microsoft.Extensions.Logging.Abstractions;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;
using Moq;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Contracts;
using WebApp.Mqtt.Factory;
using WebApp.Mqtt.Testing.Helpers;

namespace WebApp.Mqtt.UnitTests
{
    public class MqttClientFactoryTest
    {
        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClient_Throw_ArgumentNullException_When_Config_Null()
        {
            var builder = new MQTTnetManagedClientOptionsBuilder();

            Assert.Throws<ArgumentNullException>(() =>
                new MQTTClientFactory(new NullLogger<MQTTClientFactory>()).CreateMQTTnetManagedClient(null, builder));
        }

        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClient_Throw_ArgumentNullException_When_Builder_Null()
        {
            var config = new MqttConfig();

            Assert.Throws<ArgumentNullException>(() =>
                new MQTTClientFactory(new NullLogger<MQTTClientFactory>()).CreateMQTTnetManagedClient(config, null));
        }

        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClient_Creates_Tuple_With_Client_And_Options()
        {
            var mockOpts = new ManagedMqttClientOptions();

            var builderMock = new Mock<IMQTTnetManagedClientOptionsBuilder>();

            builderMock.Setup(m => m.WithOptions(It.IsAny<MqttConfig>())).Returns(builderMock.Object);
            builderMock.Setup(m => m.Build()).Returns(mockOpts);

            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var factory = new MQTTClientFactory(new NullLogger<MQTTClientFactory>());

            var result = factory.CreateMQTTnetManagedClient(config, builderMock.Object);

            try
            {
                builderMock.VerifyAll();

                Assert.Equal(mockOpts, result.options);
                Assert.NotNull(result.client);
            }
            finally
            {
                if (result.client != null)
                    result.client.Dispose();
            }
        }


        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClientTls_Throws_ArgumentNullException_When_Config_Null()
        {
            var builder = new MQTTnetManagedClientTlsOptionsBuilder();

            Assert.Throws<ArgumentNullException>(() =>
                new MQTTClientFactory(new NullLogger<MQTTClientFactory>()).CreateMQTTnetManagedClientTls(null, builder));
        }

        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClientTls_Throws_ArgumentNullException_When_Builder_Null()
        {
            var config = new MqttConfig();
            config.Tls = new MqttTls();
            config.Tls.CACerts = String.Empty;

            Assert.Throws<ArgumentNullException>(() =>
                new MQTTClientFactory(new NullLogger<MQTTClientFactory>()).CreateMQTTnetManagedClientTls(config, null));
        }

        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClientTls_Throws_ArgumentNullException_When_Config_Tls_Null()
        {
            const string caPath = "test.pem";

            var builder = new MQTTnetManagedClientTlsOptionsBuilder();
            var config = MqttTestUtils.CreateMqttConfigWithTLS(caPath);
            config.Tls = null;

            Assert.Throws<ArgumentNullException>(() =>
                new MQTTClientFactory(new NullLogger<MQTTClientFactory>()).CreateMQTTnetManagedClientTls(config, builder));
        }

        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClientTls_Creates_Tuple_With_Client_And_Options()
        {
            const string caPath = "ca.pem";

            var mockOpts = new ManagedMqttClientOptions();

            var builderMock = new Mock<IMQTTnetManagedClientTlsOptionsBuilder>();

            builderMock.Setup(m => m.WithOptions(
                It.IsAny<MqttConfig>(),
                It.IsAny<Func<MqttClientCertificateValidationCallbackContext, bool>>(),
                It.IsAny<X509CertificateWrapper>())).Returns(builderMock.Object);

            builderMock.Setup(m => m.Build()).Returns(mockOpts);

            var config = MqttTestUtils.CreateMqttConfigWithTLS(caPath);
            var factory = new MQTTClientFactory(new NullLogger<MQTTClientFactory>());

            var result = factory.CreateMQTTnetManagedClientTls(config, builderMock.Object);

            try
            {
                builderMock.VerifyAll();

                Assert.Equal(mockOpts, result.options);
                Assert.NotNull(result.client);
            }
            finally
            {
                if (result.client != null)
                    result.client.Dispose();
            }
        }

        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClient_Throws_ArgumentNullException_When_Config_Null()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new MQTTClientFactory(new NullLogger<MQTTClientFactory>()).CreateMQTTnetManagedClient(null));
        }
    }
}