using System;
using Xunit;

using WebApp.Mqtt.Factory;
using WebApp.Mqtt.Testing.Helpers;


namespace WebApp.Mqtt.UnitTests
{
    public class MQTTnetManagedClientOptionsBuilderTest
    {
        [Fact]
        public void MQTTnetManagedClientOptionsBuilder_WithOptions_Returns_Builder()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();

            var builder = new MQTTnetManagedClientOptionsBuilder();
            var result = builder.WithOptions(config);

            Assert.Same(builder, result);
        }

        [Fact]
        public void MQTTnetManagedClientOptionsBuilder_WithOptions_Builds_Options()
        {
            // dispose of the certs remember
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var configExpected = MqttTestUtils.ToManagedClientOptions(config);

            MQTTnetManagedClientOptionsBuilder builder = new MQTTnetManagedClientOptionsBuilder();
            builder.WithOptions(config);

            var result = builder.Build();

            Assert.Equal(configExpected.AutoReconnectDelay, result.AutoReconnectDelay);
            Assert.Equal(configExpected.ClientOptions.Credentials.Username, result.ClientOptions.Credentials.Username);
            Assert.Equal(configExpected.ClientOptions.ChannelOptions.ToString(), result.ClientOptions.ChannelOptions.ToString());
        }

        [Fact]
        public void MQTTnetManagedClientOptionsBuilder_WithOptions_Throws_ArgumentNullException_When_Config_Parameter_Is_Null()
        {

            MQTTnetManagedClientOptionsBuilder builder = new MQTTnetManagedClientOptionsBuilder();

            Assert.Throws<ArgumentNullException>(() => builder.WithOptions(null));
        }
    }
}
