using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using MQTTnet;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Extensions.ManagedClient;
using Moq;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Contracts;
using WebApp.Mqtt.Testing.Helpers;


namespace WebApp.Mqtt.UnitTests
{
    public class MqttServiceTest
    {
        [Fact]
        public void MqttService_Constructor_Initialises_Client()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);
            IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

            var factoryMock = new Mock<IMQTTClientFactory>();
            var clientMock = new Mock<IManagedMqttClient>();
            factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                .Returns((clientMock.Object, clientOptions)).Verifiable();

            MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());

            factoryMock.Verify();

            Assert.Equal(clientMock.Object, svc.Client);
        }

        [Fact]
        public void MqttService_Constructor_Configures_Client_ConnectingFailedHandler()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);
            IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

            var factoryMock = new Mock<IMQTTClientFactory>();
            var clientMock = new Mock<IManagedMqttClient>();
            factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                .Returns((clientMock.Object, clientOptions));

            MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());
            clientMock.VerifySet(x => x.ConnectingFailedHandler = svc);
        }

        [Fact]
        public void MqttService_Constructor_Configures_Client_Configures_Client_DisconnectedHandler()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);

            using (var client = new MqttFactory().CreateManagedMqttClient())
            {
                IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

                var clientMock = new Mock<IManagedMqttClient>();
                var factoryMock = new Mock<IMQTTClientFactory>();
                factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                    .Returns((client, clientOptions));

                MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());

                Assert.NotNull(svc.Client.DisconnectedHandler);
            }
        }

        [Fact]
        public void MqttService_Constructor_Configure_Client_Configures_Client_ApplicationMessageReceived_Handler()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);

            using (var client = new MqttFactory().CreateManagedMqttClient())
            {
                IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

                var clientMock = new Mock<IManagedMqttClient>();
                var factoryMock = new Mock<IMQTTClientFactory>();
                factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                    .Returns((client, clientOptions));

                MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());

                Assert.NotNull(svc.Client.ApplicationMessageReceivedHandler);
            }
        }

        [Fact]
        public void MqttService_HandleConnectingFailedAsync_Returns_Completed_Task()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);
            IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

            var factoryMock = new Mock<IMQTTClientFactory>();
            var clientMock = new Mock<IManagedMqttClient>();
            factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                .Returns((clientMock.Object, clientOptions)).Verifiable();

            MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());

            var mockArgs = new ManagedProcessFailedEventArgs(new ArgumentNullException());
            Task task = svc.HandleConnectingFailedAsync(mockArgs);

            Assert.Equal(Task.CompletedTask.Status, task.Status);
        }

        [Fact]
        public void MqttService_HandleConnectedAsync_Returns_Completed_Task()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);
            IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

            var factoryMock = new Mock<IMQTTClientFactory>();
            var clientMock = new Mock<IManagedMqttClient>();
            factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                .Returns((clientMock.Object, clientOptions)).Verifiable();

            MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());

            var mockArgs = new MqttClientConnectedEventArgs(new MqttClientAuthenticateResult());
            Task task = svc.HandleConnectedAsync(mockArgs);

            Assert.Equal(Task.CompletedTask.Status, task.Status);
        }

        [Fact]
        public void MqttService_HandleDisconnectedAsync_Returns_Completed_Task()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);
            IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

            var factoryMock = new Mock<IMQTTClientFactory>();
            var clientMock = new Mock<IManagedMqttClient>();
            factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                .Returns((clientMock.Object, clientOptions)).Verifiable();

            MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());

            var mockArgs = new MqttClientDisconnectedEventArgs(
                false,
                new ArgumentNullException(),
                new MqttClientAuthenticateResult(),
                MqttClientDisconnectReason.KeepaliveTimeout);

            Task task = svc.HandleDisconnectedAsync(mockArgs);

            Assert.Equal(Task.CompletedTask.Status, task.Status);
        }

        [Fact]
        public async Task MqttService_StartAsync_Calls_Client_StartAsync()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);
            IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

            var factoryMock = new Mock<IMQTTClientFactory>();
            var clientMock = new Mock<IManagedMqttClient>();
            factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                .Returns((clientMock.Object, clientOptions)).Verifiable();

            MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());
            await svc.StartAsync();

            clientMock.Verify(m => m.StartAsync(clientOptions), Times.Once());
        }

        [Fact]
        public async Task MqttService_StartAsync_Calls_Client_SubscribeAsync()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);
            IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

            var factoryMock = new Mock<IMQTTClientFactory>();
            var clientMock = new Mock<IManagedMqttClient>();
            factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                .Returns((clientMock.Object, clientOptions)).Verifiable();

            var filters = new List<MqttTopicFilter>()
                {
                    new MqttTopicFilterBuilder().WithTopic(config.Topic).Build()
                };

            MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());
            await svc.StartAsync();

            clientMock.Verify(m => m.SubscribeAsync(
                It.Is<IEnumerable<MqttTopicFilter>>(lst => VerifySubscriptionTopics(lst, config.Topic))),
                Times.Once()
            );
        }

        [Fact]
        public async Task MqttService_StopAsync_Calls_Client_StopAsync()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);
            IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

            var factoryMock = new Mock<IMQTTClientFactory>();
            var clientMock = new Mock<IManagedMqttClient>();
            factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                .Returns((clientMock.Object, clientOptions)).Verifiable();

            MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());
            await svc.StopAsync();

            clientMock.Verify(m => m.StopAsync(), Times.Once());
        }

        [Fact]
        public void MqttService_Dispose_Disposes_MQTTnetManagedClient()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);
            IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

            var factoryMock = new Mock<IMQTTClientFactory>();
            var clientMock = new Mock<IManagedMqttClient>();
            factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                .Returns((clientMock.Object, clientOptions)).Verifiable();

            MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());
            svc.Dispose();

            clientMock.Verify(client => client.Dispose(), Times.Once());
        }

        [Fact]
        public void MqttService_Dipose_Disposes_Certificates_For_MQTTnetManagedClient()
        {
            const string caPath = "Certs/digiCert.pem";

            var config = MqttTestUtils.CreateMqttConfigWithTLS(caPath);
            var clientOptions = MqttTestUtils.ToManagedClientOptions(config);
            IOptions<MqttConfig> opts = Options.Create<MqttConfig>(config);

            var factoryMock = new Mock<IMQTTClientFactory>();
            var clientMock = new Mock<IManagedMqttClient>();
            factoryMock.Setup(x => x.CreateMQTTnetManagedClient(config))
                .Returns((clientMock.Object, clientOptions));

            MqttService svc = new MqttService(opts, factoryMock.Object, new NullLogger<MqttService>());
            svc.Dispose();

            Assert.True(clientOptions.ClientOptions.ChannelOptions.TlsOptions.Certificates.Count == 0);
        }


        /// <summary>
        /// Verify that list of <see cref="MqttTopicFilter"/> instances contains at least one
        /// instance with a filter that matches a given topic
        /// </summary>
        /// <param name="lst">List of topic filters</param>
        /// <param name="topic">Verification topic</param>
        /// <returns>ture if topic is in the list, otherwise false</returns>
        private bool VerifySubscriptionTopics(IEnumerable<MqttTopicFilter> lst, string topic)
        {
            var items = from item in lst
                        where item.Topic == topic
                        select item;

            return items.ToList().Count > 0;
        }
    }
}