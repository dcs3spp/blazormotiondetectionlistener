using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;


namespace WebApp.Data.UnitTests
{
    public class MotionDetectionTest
    {
        private ITestOutputHelper _output;

        public MotionDetectionTest(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void MotionDetection_Property_Constructor()
        {
            const string ExpectedGroup = "mygroup";
            const string ExpectedMonitorId = "monitorId";
            const string ExpectedPlug = "plug";

            long expectedTime = 830;

            IList<MotionLocation> lst = new List<MotionLocation>();
            lst.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo info = new MotionInfo("plug", "name", "reason", lst, "img", 1, 2, expectedTime);
            MotionDetection motion = new MotionDetection(ExpectedGroup, expectedTime, ExpectedMonitorId, ExpectedPlug, info);

            Assert.Equal(motion.Group, ExpectedGroup);
            Assert.Equal(motion.MonitorId, ExpectedMonitorId);
            Assert.Equal(motion.Plug, ExpectedPlug);
            Assert.Equal(motion.Time, expectedTime);
            Assert.Equal(motion.Details, info);
            Assert.False(ReferenceEquals(motion.Details, info));
            Assert.False(ReferenceEquals(motion.Time, expectedTime));
        }

        [Fact]
        public void MotionDetection_Property_Constructor_Throws_ArgumentNullException_When_Group_null()
        {
            long expectedTime = 820;

            MotionInfo info = new MotionInfo("plug", "name", "reason", new List<MotionLocation>(), "img", 1, 2, expectedTime);
            Assert.Throws<ArgumentNullException>(() => new MotionDetection(null, expectedTime, "monitorId", "plug", info));
        }

        [Fact]
        public void MotionDetection_Property_Constructor_Throws_ArgumentNullException_When_MonitorId_null()
        {
            long expectedTime = 820;

            MotionInfo info = new MotionInfo("plug", "name", "reason", new List<MotionLocation>(), "img", 1, 2, expectedTime);
            Assert.Throws<ArgumentNullException>(() => new MotionDetection("myGroup", expectedTime, null, "plug", info));
        }

        [Fact]
        public void MotionDetection_Property_Constructor_Throws_ArgumentNullException_When_Plugin_null()
        {
            long expectedTime = 820;

            MotionInfo info = new MotionInfo("plug", "name", "reason", new List<MotionLocation>(), "img", 1, 2, expectedTime);
            Assert.Throws<ArgumentNullException>(() => new MotionDetection("myGroup", expectedTime, "monitorId", null, info));
        }

        [Fact]
        public void MotionDetection_Property_Constructor_Throws_ArgumentNullException_When_Details_null()
        {
            long expectedTime = 820;

            MotionInfo info = new MotionInfo("plug", "name", "reason", new List<MotionLocation>(), "img", 1, 2, expectedTime);
            Assert.Throws<ArgumentNullException>(() => new MotionDetection("myGroup", expectedTime, "monitorId", "plug", null));
        }

        [Fact]
        public void MotionDetection_Copy_Constructor_Deep_Copy_Source_Object()
        {
            const string ExpectedGroup = "mygroup";
            const string ExpectedMonitorId = "monitorId";
            const string ExpectedPlug = "plug";

            long expectedTime = 820;

            IList<MotionLocation> lst = new List<MotionLocation>();
            lst.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo info = new MotionInfo("plug", "name", "reason", lst, "img", 1, 2, expectedTime);
            MotionDetection motionA = new MotionDetection(ExpectedGroup, expectedTime, ExpectedMonitorId, ExpectedPlug, info);
            MotionDetection motionB = new MotionDetection(motionA);

            Assert.Equal(motionB.Group, ExpectedGroup);
            Assert.Equal(motionB.MonitorId, ExpectedMonitorId);
            Assert.Equal(motionB.Plug, ExpectedPlug);
            Assert.Equal(motionB.Time, expectedTime);
            Assert.Equal(motionB.Details, info);
            Assert.False(ReferenceEquals(motionB.Details, info));
            Assert.False(ReferenceEquals(motionB.Time, expectedTime));
        }

        [Fact]
        public void MotionDetection_Copy_Constructor_Throws_ArgumentNullException_When_Source_Object_null()
        {
            MotionDetection source = null;

            Assert.Throws<ArgumentNullException>(() => new MotionDetection(source));
        }

        [Fact]
        public void MotionDetection_Equals_Object_Equal()
        {
            const long Time = 820;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("mygroup", Time, "monitorId", "plug", infoB);

            Object objA = motionA;
            Object objB = motionB;

            Assert.True(objA.Equals(objB));
        }

        [Fact]
        public void MotionDetection_Equals_Object_Unequal()
        {
            const long Time = 1020;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("myothergroup", Time, "monitorId", "plug", infoB);

            Object objA = motionA;
            Object objB = motionB;

            Assert.False(objA.Equals(objB));
        }

        [Fact]
        public void MotionDetection_Equals_Object_Null()
        {
            const long Time = 1020;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("myothergroup", Time, "monitorId", "plug", infoB);

            Object objA = motionA;
            Object objB = null;

            Assert.False(objA.Equals(objB));
        }

        [Fact]
        public void MotionDetection_Equals_Object_MismatchedType()
        {
            const long Time = 1020;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("myothergroup", Time, "monitorId", "plug", infoB);

            Object objA = motionA;
            Object objB = 1;

            Assert.False(object.Equals(objA, objB));
        }

        [Fact]
        public void MotionDetection_Equals_IEquatable_Equal()
        {
            const long Time = 1020;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("mygroup", Time, "monitorId", "plug", infoB);

            Assert.True(motionA.Equals(motionB));
        }

        [Fact]
        public void MotionDetection_Equals_IEquatable_Equal_Reference()
        {
            const long Time = 1020;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionDetection motionB = motionA;

            Assert.True(motionA.Equals(motionB));
        }

        [Fact]
        public void MotionDetection_Equals_IEquatable_Unequal_NullReference()
        {
            const long Time = 1020;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);
            MotionDetection motionB = null;

            Assert.False(motionA.Equals(motionB));
        }

        [Fact]
        public void MotionDetection_Equals_IEquatable_Unequal_Group()
        {
            const long Time = 1020;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("myothergroup", Time, "monitorId", "plug", infoB);

            Assert.False(motionA.Equals(motionB));
        }

        [Fact]
        public void MotionDetection_Equals_IEquatable_Unequal_Time()
        {
            const long TimeA = 2021;
            const long TimeB = 2022;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, TimeA);
            MotionDetection motionA = new MotionDetection("mygroup", TimeA, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, TimeB);
            MotionDetection motionB = new MotionDetection("mygroup", TimeB, "monitorId", "plug", infoB);

            Assert.False(motionA.Equals(motionB));
        }

        [Fact]
        public void MotionDetection_Equals_IEquatable_Unequal_MonitorId()
        {
            const long Time = 4040;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("mygroup", Time, "AnotherMonitorId", "plug", infoB);

            Assert.False(motionA.Equals(motionB));
        }

        [Fact]
        public void MotionDetection_Equals_IEquatable_Unequal_Plugin()
        {
            const long Time = 4040;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("mygroup", Time, "monitorId", "AnotherPlug", infoB);

            Assert.False(motionA.Equals(motionB));
        }

        [Fact]
        public void MotionDetection_Equals_IEquatable_Unequal_Details()
        {
            const long Time = 4040;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("unequalplug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("mygroup", Time, "monitorId", "plug", infoB);

            Assert.False(motionA.Equals(motionB));
        }

        [Fact]
        public void MotionDetection_Equals_Operator_Returns_True_When_Objects_Equal()
        {
            const long Time = 4040;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("mygroup", Time, "monitorId", "plug", infoB);

            Assert.True(motionA == motionB);
        }

        [Fact]
        public void MotionDetection_Not_Equals_Operator_Returns_False_When_Objects_Equal()
        {
            const long Time = 4040;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("mygroup", Time, "monitorId", "plug", infoB);

            Assert.False(motionA != motionB);
        }

        [Fact]
        public void MotionDetection_Equals_Operator_Returns_False_When_Objects_Unequal()
        {
            const long Time = 4040;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("anotherGroup", Time, "monitorId", "plug", infoB);

            Assert.False(motionA == motionB);
        }

        [Fact]
        public void MotionDetection_Equals_Operator_Returns_False_When_Target_Object_Null()
        {
            const long Time = 9009;

            IList<MotionLocation> lst = new List<MotionLocation>();
            lst.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo info = new MotionInfo("plug", "name", "reason", lst, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", info);
            MotionDetection motionB = null;

            Assert.False(motionA == motionB);
        }

        [Fact]
        public void MotionDetection_Equals_Operator_Returns_False_When_Destination_Object_Null()
        {
            const long Time = 9009;

            IList<MotionLocation> lst = new List<MotionLocation>();
            lst.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionDetection motionA = null;
            MotionInfo info = new MotionInfo("plug", "name", "reason", lst, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("mygroup", Time, "monitorId", "plug", info);

            Assert.False(motionA == motionB);
        }

        [Fact]
        public void MotionDetection_Not_Equals_Operator_Returns_True_When_Objects_Unequal()
        {
            const long Time = 9009;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("anotherGroup", Time, "monitorId", "plug", infoB);

            Assert.True(motionA != motionB);
        }

        [Fact]
        public void MotionDetection_Not_Equals_Operator_Returns_True_When_Target_Object_Null()
        {
            const long Time = 9009;

            IList<MotionLocation> lst = new List<MotionLocation>();
            lst.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo info = new MotionInfo("plug", "name", "reason", lst, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", info);
            MotionDetection motionB = null;

            Assert.True(motionA != motionB);
        }

        [Fact]
        public void MotionDetection_Not_Equals_Operator_Returns_True_When_Destination_Object_Null()
        {
            const long Time = 9009;

            IList<MotionLocation> lst = new List<MotionLocation>();
            lst.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionDetection motionA = null;
            MotionInfo info = new MotionInfo("plug", "name", "reason", lst, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("mygroup", Time, "monitorId", "plug", info);

            Assert.True(motionA != motionB);
        }

        [Fact]
        public void MotionDetection_GetHashCode_Calculates_Expected_HashCode()
        {
            const long Time = 9009;

            int monitorIdHashCode = "monitorId".GetHashCode();
            int groupHashCode = "mygroup".GetHashCode();
            int timeHashCode = Time.GetHashCode();

            const int Prime = 397;
            int expectedHashCode = 37;

            unchecked
            {
                expectedHashCode = Prime * expectedHashCode;
                expectedHashCode = groupHashCode + expectedHashCode;
                expectedHashCode = Prime * expectedHashCode;
                expectedHashCode = monitorIdHashCode + expectedHashCode;
                expectedHashCode = Prime * expectedHashCode;
                expectedHashCode = timeHashCode + expectedHashCode;
            }

            IList<MotionLocation> lst = new List<MotionLocation>();
            lst.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo info = new MotionInfo("plug", "name", "reason", lst, "img", 1, 2, Time);
            MotionDetection motion = new MotionDetection("mygroup", Time, "monitorId", "plug", info);

            Assert.Equal(expectedHashCode, motion.GetHashCode());
        }

        /**
            Official stance on the behaviour of GetHashCode => https://docs.microsoft.com/en-us/dotnet/api/system.object.gethashcode?view=netcore-3.1

            A hash function must have the following properties: - If two objects compare as equal, the GetHashCode() method for each object must return the same value. 
            However, if two objects do not compare as equal, the GetHashCode() methods for the two objects do not have to return different values. - The GetHashCode() 
            method for an object must consistently return the same hash code as long as there is no modification to the object state that determines the return value
            of the object's method. Note that this is true only for the current execution of an application, and that a different hash code can be returned if the
            application is run again.
        */
        [Fact]
        public void MotionDetection_GetHashCode_Same_For_Equal_Objects()
        {
            const long Time = 9009;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionDetection motionA = new MotionDetection("mygroup", Time, "monitorId", "plug", infoA);

            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);
            MotionDetection motionB = new MotionDetection("mygroup", Time, "monitorId", "plug", infoB);

            int motionAHashCode = motionA.GetHashCode();
            int motionBHashCode = motionB.GetHashCode();

            Assert.Equal(motionA, motionB);
            Assert.Equal(motionAHashCode, motionBHashCode);
            Assert.Equal(motionAHashCode, motionA.GetHashCode());
            Assert.Equal(motionBHashCode, motionB.GetHashCode());
        }

        [Fact]
        public void MotionDetection_ToString_Reflects_Object_State()
        {
            const long Time = 9009;

            IList<MotionLocation> matrices = new List<MotionLocation>();
            matrices.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo info = new MotionInfo("plug", "name", "reason", matrices, "img", 1, 2, Time);
            MotionDetection obj = new MotionDetection("mygroup", Time, "monitorId", "plug", info);

            string matricesString = "\n" + string.Join("\n", obj.Details.Matrices.Select(item => item.ToString()).ToArray());

            string infoString = string.Format
                ("\nimg: {0}\nimgHeight: {1:0}\nimgWidth: {2:0}\nmatrices:\n {3}\nname: {4}\nplug: {5}\nreason: {6}\ntime: {7}\n",
                obj.Details.Img,
                obj.Details.ImgHeight,
                obj.Details.ImgWidth,
                matricesString,
                obj.Details.Name,
                obj.Details.Plug,
                obj.Details.Reason,
                obj.Time
            );

            string expectedString = string.Format(
                "\ndetails: {0}\ngroup: {1}\nmonitorId: {2}\nplugin: {3}\ntime: {4}\n",
                infoString,
                obj.Group,
                obj.MonitorId,
                obj.Plug,
                Time);

            Assert.Equal(expectedString, obj.ToString());
        }
    }
}
