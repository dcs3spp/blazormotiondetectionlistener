using System;
using Xunit;
using Xunit.Abstractions;


namespace WebApp.Data.UnitTests
{
    public class MotionLocationTest
    {
        private ITestOutputHelper _output;

        public MotionLocationTest(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void MotionLocation_Property_Constructor()
        {
            const double ExpectedConfidence = 0.75;
            const double ExpectedHeight = 2.5;
            const string ExpectedTag = "person";
            const double ExpectedWidth = 2.5;
            const double ExpectedX = 1.2;
            const double ExpectedY = 1.3;

            MotionLocation location = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            Assert.Equal(location.Confidence, ExpectedConfidence);
            Assert.Equal(location.Height, ExpectedHeight);
            Assert.Equal(location.Tag, ExpectedTag);
            Assert.Equal(location.Width, ExpectedWidth);
            Assert.Equal(location.X, ExpectedX);
            Assert.Equal(location.Y, ExpectedY);
        }

        [Fact]
        public void MotionLocation_Property_Constructor_Throws_ArgumentNullException_When_Tag_Null()
        {
            Assert.Throws<ArgumentNullException>(() => new MotionLocation(1.2, 1.3, 2.5, 2.5, null, 0.75));
        }

        [Fact]
        public void MotionLocation_Property_Constructor_Initialises_Deep_Copy_Of_Source_Object()
        {
            const double ExpectedConfidence = 0.75;
            const double ExpectedHeight = 2.5;
            const string ExpectedTag = "person";
            const double ExpectedWidth = 2.5;
            const double ExpectedX = 1.2;
            const double ExpectedY = 1.3;

            MotionLocation location = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation copy = new MotionLocation(location);

            Assert.Equal(copy.Confidence, ExpectedConfidence);
            Assert.Equal(copy.Height, ExpectedHeight);
            Assert.Equal(copy.Tag, ExpectedTag);
            Assert.Equal(copy.Width, ExpectedWidth);
            Assert.Equal(copy.X, ExpectedX);
            Assert.Equal(copy.Y, ExpectedY);
        }

        [Fact]
        public void MotionLocation_Copy_Constructor_Throws_ArgumentNullException_When_Source_Null()
        {
            Assert.Throws<ArgumentNullException>(() => new MotionLocation(null));
        }

        [Fact]
        public void MotionLocation_Equals_Object_Returns_True_When_Equal()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            Object objA = motionLocationA;
            Object objB = motionLocationB;

            Assert.True(objA.Equals(objB));
        }

        [Fact]
        public void MotionLocation_Equals_Object_Returns_False_When_Unequal()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "personB", 0.75);

            Object objA = motionLocationA;
            Object objB = motionLocationB;

            Assert.False(objA.Equals(objB));
        }

        [Fact]
        public void MotionLocation_Equals_Object_Returns_False_When_Target_Object_Is_Null()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            Object objA = motionLocationA;
            Object objB = null;

            Assert.False(objA.Equals(objB));
        }

        [Fact]
        public void MotionLocation_Equals_Object_Returns_False_When_Target_Type_Mismatch()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            Object objA = motionLocationA;
            Object objB = "mismatch";

            Assert.False(objA.Equals(objB));
        }

        [Fact]
        public void MotionLocation_IEquatable_Equals_Returns_True_When_Equal()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            Assert.True(motionLocationA.Equals(motionLocationB));
        }

        [Fact]
        public void MotionLocation_IEquatable_Equals_Returns_True_When_Same()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = motionLocationA;

            Assert.True(motionLocationA.Equals(motionLocationB));
        }

        [Fact]
        public void MotionLocation_IEquatable_Equals_Returns_False_When_Target_Object_Null()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = null;

            Assert.False(motionLocationA.Equals(motionLocationB));
        }

        [Fact]
        public void MotionLocation_IEquatable_Equals_Returns_False_When_X_Property_Different()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.3, 1.3, 2.5, 2.5, "person", 0.75);

            Assert.False(motionLocationA.Equals(motionLocationB));
        }

        [Fact]
        public void MotionLocation_IEquatable_Equals_Returns_False_When_Y_Property_Different()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.4, 2.5, 2.5, "person", 0.75);

            Assert.False(motionLocationA.Equals(motionLocationB));
        }

        [Fact]
        public void MotionLocation_IEquatable_Equals_Returns_False_When_Width_Property_Different()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.6, 2.5, "person", 0.75);

            Assert.False(motionLocationA.Equals(motionLocationB));
        }

        [Fact]
        public void MotionLocation_IEquatable_Equals_Returns_False_When_Height_Property_Different()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.6, "person", 0.75);

            Assert.False(motionLocationA.Equals(motionLocationB));
        }

        [Fact]
        public void MotionLocation_IEquatable_Equals_Returns_False_When_Tag_Property_Different()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "personB", 0.75);

            Assert.False(motionLocationA.Equals(motionLocationB));
        }

        [Fact]
        public void MotionLocation_IEquatable_Equals_Returns_False_When_Confidence_Property_Different()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.76);

            Assert.False(motionLocationA.Equals(motionLocationB));
        }

        [Fact]
        public void MotionLocation_Equals_Operator_Returns_True_When_Objects_Equal()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            Assert.True(motionLocationA == motionLocationB);
        }

        [Fact]
        public void MotionLocation_Equals_Operator_Returns_False_When_Objects_Unequal()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.3, 1.3, 2.5, 2.5, "person", 0.75);

            Assert.False(motionLocationA == motionLocationB);
        }

        [Fact]
        public void MotionLocation_Equals_Operator_Returns_False_When_Target_Object_Null()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = null;

            Assert.False(motionLocationA == motionLocationB);
        }

        [Fact]
        public void MotionLocation_Equals_Operator_Returns_False_When_Source_Object_Null()
        {
            MotionLocation motionLocationA = null;
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            Assert.False(motionLocationA == motionLocationB);
        }

        [Fact]
        public void MotionLocation_Not_Equals_Operator_Returns_False_When_Objects_Equal()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            Assert.False(motionLocationA != motionLocationB);
        }

        [Fact]
        public void MotionLocation_Not_Equals_Operator_Returns_True_When_Objects_Unequal()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "personB", 0.75);

            Assert.True(motionLocationA != motionLocationB);
        }

        [Fact]
        public void MotionLocation_Not_Equals_Operator_Returns_True_When_Target_Object_Null()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = null;

            Assert.True(motionLocationA != motionLocationB);
        }

        [Fact]
        public void MotionLocation_Not_Equals_Operator_Returns_True_When_Source_Object_Null()
        {
            MotionLocation motionLocationA = null;
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            Assert.True(motionLocationA != motionLocationB);
        }


        [Fact]
        public void MotionLocation_GetHashCode_Calculates_Expected_HashCode()
        {
            MotionLocation motionLocation = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            int expectedHashCode = new { motionLocation.Confidence, motionLocation.Height, motionLocation.Tag, motionLocation.Width, motionLocation.X, motionLocation.Y }.GetHashCode();

            Assert.Equal(expectedHashCode, motionLocation.GetHashCode());
        }

        /**
            Official stance on the behaviour of GetHashCode => https://docs.microsoft.com/en-us/dotnet/api/system.object.gethashcode?view=netcore-3.1

            A hash function must have the following properties: - If two objects compare as equal, the GetHashCode() method for each object must return the same value. 
            However, if two objects do not compare as equal, the GetHashCode() methods for the two objects do not have to return different values. - The GetHashCode() 
            method for an object must consistently return the same hash code as long as there is no modification to the object state that determines the return value
            of the object's method. Note that this is true only for the current execution of an application, and that a different hash code can be returned if the
            application is run again.
        */
        [Fact]
        public void MotionLocation_GetHashCode_Same_For_Equal_Objects()
        {
            MotionLocation motionLocationA = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);
            MotionLocation motionLocationB = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            int motionLocationAHashCode = motionLocationA.GetHashCode();
            int motionLocationBHashCode = motionLocationB.GetHashCode();

            Assert.Equal(motionLocationA, motionLocationB);
            Assert.Equal(motionLocationAHashCode, motionLocationBHashCode);
            Assert.Equal(motionLocationAHashCode, motionLocationA.GetHashCode());
            Assert.Equal(motionLocationBHashCode, motionLocationB.GetHashCode());
        }

        [Fact]
        public void MotionLocation_ToString_Reflects_Object_State()
        {
            MotionLocation obj = new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75);

            string expected = string.Format(
              "x: {0}\ny: {1}\nwidth: {2}\nheight: {3}\ntag: {4}\nconfidence: {5}",
              obj.X,
              obj.Y,
              obj.Width,
              obj.Height,
              obj.Tag,
              obj.Confidence);

            Assert.Equal(expected, obj.ToString());
        }
    }
}
