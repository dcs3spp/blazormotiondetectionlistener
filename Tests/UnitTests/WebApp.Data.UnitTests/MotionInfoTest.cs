using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;


namespace WebApp.Data.UnitTests
{
    public class MotionInfoTest
    {
        private ITestOutputHelper _output;

        public MotionInfoTest(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void MotionInfo_Property_Constructor()
        {
            const int ExpectedHeight = 1;
            const string ExpectedImg = "img";
            const string ExpectedName = "name";
            const string ExpectedPlug = "plug";
            const string ExpectedReason = "reason";
            const int ExpectedWidth = 2;

            long expectedTime = 832;

            IList<MotionLocation> expectedMatricesList = new List<MotionLocation>();
            expectedMatricesList.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo info = new MotionInfo("plug", "name", "reason", expectedMatricesList, "img", 1, 2, expectedTime);

            Assert.Equal(info.Img, ExpectedImg);
            Assert.Equal(info.Name, ExpectedName);
            Assert.Equal(info.Plug, ExpectedPlug);
            Assert.Equal(info.Reason, ExpectedReason);
            Assert.Equal(info.Time, expectedTime);
            Assert.Equal(info.ImgHeight, ExpectedHeight);
            Assert.Equal(info.ImgWidth, ExpectedWidth);
            Assert.Equal(info.Matrices, expectedMatricesList);
        }

        [Fact]
        public void MotionInfo_Property_Constructor_Throws_ArgumentNullException_When_Plugin_null()
        {
            long expectedTime = 876;

            Assert.Throws<ArgumentNullException>(() => new MotionInfo(null, "name", "reason", new List<MotionLocation>(), "img", 1, 2, expectedTime));
        }

        [Fact]
        public void MotionInfo_Property_Constructor_Throws_ArgumentNullException_When_Name_null()
        {
            long expectedTime = 1000;

            Assert.Throws<ArgumentNullException>(() => new MotionInfo("plug", null, "reason", new List<MotionLocation>(), "img", 1, 2, expectedTime));
        }

        [Fact]
        public void MotionInfo_Property_Constructor_Throws_ArgumentNullException_When_Reason_null()
        {
            long expectedTime = 10001;

            Assert.Throws<ArgumentNullException>(() => new MotionInfo("plug", "name", null, new List<MotionLocation>(), "img", 1, 2, expectedTime));
        }

        [Fact]
        public void MotionInfo_Property_Constructor_Throws_ArgumentNullException_When_Img_null()
        {
            long expectedTime = 101;

            Assert.Throws<ArgumentNullException>(() => new MotionInfo("plug", "name", "reason", new List<MotionLocation>(), null, 1, 2, expectedTime));
        }

        [Fact]
        public void MotionInfo_Property_Constructor_Throws_ArgumentNullException_When_Matrices_null()
        {
            long expectedTime = 202;

            Assert.Throws<ArgumentNullException>(() => new MotionInfo("plug", "name", "reason", null, "img", 1, 2, expectedTime));
        }

        [Fact]
        public void MotionInfo_Copy_Constructor_Throws_ArgumentNullException_When_Source_Object_null()
        {
            Assert.Throws<ArgumentNullException>(() => new MotionInfo(null));
        }

        [Fact]
        public void MotionInfo_Copy_Constructor_Initialises_Deep_Copy_from_Source_Object()
        {
            const int ExpectedHeight = 1;
            const string ExpectedImg = "img";
            const string ExpectedName = "name";
            const string ExpectedPlug = "plug";
            const string ExpectedReason = "reason";
            const int ExpectedWidth = 2;

            long expectedTime = 2020;

            IList<MotionLocation> expectedMatricesList = new List<MotionLocation>();
            expectedMatricesList.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo source = new MotionInfo("plug", "name", "reason", expectedMatricesList, "img", 1, 2, expectedTime);
            MotionInfo copy = new MotionInfo(source);

            Assert.Equal(copy.Img, ExpectedImg);
            Assert.Equal(copy.ImgHeight, ExpectedHeight);
            Assert.Equal(copy.ImgWidth, ExpectedWidth);
            Assert.Equal(copy.Name, ExpectedName);
            Assert.Equal(copy.Plug, ExpectedPlug);
            Assert.Equal(copy.Reason, ExpectedReason);
            Assert.False(ReferenceEquals(source, copy));
        }

        [Fact]
        public void MotionInfo_Equals_Object_Returns_True_When_Equal()
        {
            const long Time = 35034;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);

            Object objA = infoA;
            Object objB = infoB;

            Assert.True(objA.Equals(objB));
        }

        [Fact]
        public void MotionInfo_Equals_Object_Returns_False_When_Unequal()
        {
            const long Time = 45034;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionInfo infoB = new MotionInfo("AnottherPlug", "name", "reason", lstB, "img", 1, 2, Time);

            Object objA = infoA;
            Object objB = infoB;

            Assert.False(objA.Equals(objB));
        }

        [Fact]
        public void MotionInfo_Equals_Object_Returns_False_When_Target_Object_Null()
        {
            const long Time = 35034;

            IList<MotionLocation> lstMatrices = new List<MotionLocation>();
            lstMatrices.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatrices, "img", 1, 2, Time);

            Object objA = infoA;
            Object objB = null;

            Assert.False(objA.Equals(objB));
        }

        [Fact]
        public void MotionInfo_Equals_Object_Returns_False_When_Target_Type_Mismatch()
        {
            const long Time = 35034;

            IList<MotionLocation> lstMatrices = new List<MotionLocation>();
            lstMatrices.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatrices, "img", 1, 2, Time);

            Object objA = infoA;
            Object objB = "mismath";

            Assert.False(objA.Equals(objB));
        }

        [Fact]
        public void MotionInfo_IEquatable_Equals_Returns_True_When_Objects_Equal()
        {
            const long Time = 35034;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);

            Assert.True(infoA.Equals(infoB));
        }

        [Fact]
        public void MotionInfo_IEquatable_Equals_Returns_True_When_Objects_Same()
        {
            const long Time = 35034; ;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionInfo infoB = infoA;

            Assert.True(infoA.Equals(infoB));
        }

        [Fact]
        public void MotionInfo_IEquatable_Equals_Returns_False_When_Target_Object_Null()
        {
            const long Time = 35034;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionInfo infoB = null;

            Assert.False(infoA.Equals(infoB));
        }

        [Fact]
        public void MotionInfo_IEquatable_Equals_Returns_False_When_Plug_Property_Different()
        {
            const long Time = 35034;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionInfo infoB = new MotionInfo("plugB", "name", "reason", lstB, "img", 1, 2, Time);

            Assert.False(infoA.Equals(infoB));
        }

        [Fact]
        public void MotionInfo_IEquatable_Equals_Returns_False_When_Name_Property_Different()
        {
            const long Time = 35034; ;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionInfo infoB = new MotionInfo("plug", "nameB", "reason", lstB, "img", 1, 2, Time);

            Assert.False(infoA.Equals(infoB));
        }

        [Fact]
        public void MotionInfo_IEquatable_Equals_Returns_False_When_Reason_Property_Different()
        {
            const long Time = 35034;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionInfo infoB = new MotionInfo("plug", "name", "reasonB", lstB, "img", 1, 2, Time);

            Assert.False(infoA.Equals(infoB));
        }

        [Fact]
        public void MotionInfo_IEquatable_Equals_Returns_False_When_Matrices_Property_Different()
        {
            const long Time = 35034;

            IList<MotionLocation> lstMatricesA = new List<MotionLocation>();
            lstMatricesA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstMatricesB = new List<MotionLocation>();
            lstMatricesB.Add(new MotionLocation(1.4, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatricesA, "img", 1, 2, Time);
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstMatricesB, "img", 1, 2, Time);

            Assert.False(infoA.Equals(infoB));
        }

        [Fact]
        public void MotionInfo_IEquatable_Equals_Returns_False_When_Img_Property_Different()
        {
            const long Time = 35034;

            IList<MotionLocation> lstMatricesA = new List<MotionLocation>();
            lstMatricesA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstMatricesB = new List<MotionLocation>();
            lstMatricesB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatricesA, "img", 1, 2, Time);
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstMatricesB, "imgB", 1, 2, Time);

            Assert.False(infoA.Equals(infoB));
        }

        [Fact]
        public void MotionInfo_IEquatable_Equals_Returns_False_When_ImgHeight_Property_Different()
        {
            const long Time = 35034;

            IList<MotionLocation> lstMatricesA = new List<MotionLocation>();
            lstMatricesA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstMatricesB = new List<MotionLocation>();
            lstMatricesB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatricesA, "img", 1, 2, Time);
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstMatricesB, "img", 2, 2, Time);

            Assert.False(infoA.Equals(infoB));
        }

        [Fact]
        public void MotionInfo_IEquatable_Equals_Returns_False_When_ImgWidth_Property_Different()
        {
            const long TimeA = 35034;
            const long TimeB = 4567;

            IList<MotionLocation> lstMatricesA = new List<MotionLocation>();
            lstMatricesA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstMatricesB = new List<MotionLocation>();
            lstMatricesB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatricesA, "img", 1, 2, TimeA);
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstMatricesB, "img", 1, 2, TimeB);

            Assert.False(infoA.Equals(infoB));
        }

        [Fact]
        public void MotionInfo_Equals_Operator_Returns_True_When_Objects_Equal()
        {
            const long TimeA = 35034;
            const long TimeB = 35034;

            IList<MotionLocation> lstMatricesA = new List<MotionLocation>();
            lstMatricesA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstMatricesB = new List<MotionLocation>();
            lstMatricesB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatricesA, "img", 1, 2, TimeA);
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstMatricesB, "img", 1, 2, TimeB);

            Assert.True(infoA == infoB);
        }

        [Fact]
        public void MotionInfo_Equals_Operator_Returns_False_When_Objects_Unequal()
        {
            const long TimeA = 35034;
            const long TimeB = 4567;

            IList<MotionLocation> lstMatricesA = new List<MotionLocation>();
            lstMatricesA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstMatricesB = new List<MotionLocation>();
            lstMatricesB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatricesA, "img", 1, 2, TimeA);
            MotionInfo infoB = new MotionInfo("plugB", "name", "reason", lstMatricesB, "img", 1, 2, TimeB);

            Assert.False(infoA == infoB);
        }

        [Fact]
        public void MotionInfo_Equals_Operator_Returns_False_When_Target_Object_Null()
        {
            const long TimeA = 35034;

            IList<MotionLocation> lstMatrices = new List<MotionLocation>();
            lstMatrices.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatrices, "img", 1, 2, TimeA);
            MotionInfo infoB = null;

            Assert.False(infoA == infoB);
        }

        [Fact]
        public void MotionInfo_Equals_Operator_Returns_False_When_Source_Object_Null()
        {
            const long TimeA = 35034;

            IList<MotionLocation> lstMatrices = new List<MotionLocation>();
            lstMatrices.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = null;
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstMatrices, "img", 1, 2, TimeA);

            Assert.False(infoA == infoB);
        }

        [Fact]
        public void MotionInfo_Not_Equals_Operator_Returns_False_When_Objects_Equal()
        {
            const long TimeA = 35034;
            const long TimeB = 35034;

            IList<MotionLocation> lstMatricesA = new List<MotionLocation>();
            lstMatricesA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstMatricesB = new List<MotionLocation>();
            lstMatricesB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatricesA, "img", 1, 2, TimeA);
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstMatricesB, "img", 1, 2, TimeB);

            Assert.False(infoA != infoB);
        }

        [Fact]
        public void MotionInfo_Not_Equals_Operator_Returns_True_When_Objects_Unequal()
        {
            const long TimeA = 35034;
            const long TimeB = 4567;

            IList<MotionLocation> lstMatricesA = new List<MotionLocation>();
            lstMatricesA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstMatricesB = new List<MotionLocation>();
            lstMatricesB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatricesA, "img", 1, 2, TimeA);
            MotionInfo infoB = new MotionInfo("plug", "nameB", "reason", lstMatricesB, "img", 1, 2, TimeB);

            Assert.True(infoA != infoB);
        }

        [Fact]
        public void MotionInfo_Not_Equals_Operator_Returns_True_When_Target_Object_Null()
        {
            const long TimeA = 35034;

            IList<MotionLocation> lstMatricesA = new List<MotionLocation>();
            lstMatricesA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));


            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstMatricesA, "img", 1, 2, TimeA);
            MotionInfo infoB = null;

            Assert.True(infoA != infoB);
        }

        [Fact]
        public void MotionInfo_Not_Equals_Operator_Returns_True_When_Source_Object_Null()
        {
            const long TimeA = 35034;

            IList<MotionLocation> lstMatricesA = new List<MotionLocation>();
            lstMatricesA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = null;
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstMatricesA, "img", 1, 2, TimeA);

            Assert.True(infoA != infoB);
        }


        [Fact]
        public void MotionInfo_GetHashCode_Calculates_Expected_HashCode()
        {
            long Time = 1001;

            IList<MotionLocation> lst = new List<MotionLocation>();
            lst.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo info = new MotionInfo("plug", "name", "reason", lst, "img", 1, 2, Time);

            int timeHashCode = Time.GetHashCode();

            const int Prime = 397;
            int expectedHashCode = 37;

            unchecked
            {
                expectedHashCode = Prime * expectedHashCode;
                expectedHashCode = timeHashCode + expectedHashCode;
            }


            Assert.Equal(expectedHashCode, info.GetHashCode());
        }

        /**
            Official stance on the behaviour of GetHashCode => https://docs.microsoft.com/en-us/dotnet/api/system.object.gethashcode?view=netcore-3.1

            A hash function must have the following properties: - If two objects compare as equal, the GetHashCode() method for each object must return the same value. 
            However, if two objects do not compare as equal, the GetHashCode() methods for the two objects do not have to return different values. - The GetHashCode() 
            method for an object must consistently return the same hash code as long as there is no modification to the object state that determines the return value
            of the object's method. Note that this is true only for the current execution of an application, and that a different hash code can be returned if the
            application is run again.
        */
        [Fact]
        public void MotionInfo_GetHashCode_Same_For_Equal_Objects()
        {
            long Time = 1001;

            IList<MotionLocation> lstA = new List<MotionLocation>();
            lstA.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            IList<MotionLocation> lstB = new List<MotionLocation>();
            lstB.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo infoA = new MotionInfo("plug", "name", "reason", lstA, "img", 1, 2, Time);
            MotionInfo infoB = new MotionInfo("plug", "name", "reason", lstB, "img", 1, 2, Time);

            int infoAHashCode = infoA.GetHashCode();
            int infoBHashCode = infoB.GetHashCode();

            Assert.Equal(infoA, infoB);
            Assert.Equal(infoAHashCode, infoBHashCode);
            Assert.Equal(infoAHashCode, infoA.GetHashCode());
            Assert.Equal(infoBHashCode, infoB.GetHashCode());
        }

        [Fact]
        public void MotionInfo_ToString_Reflects_Object_State()
        {
            long Time = 1001;

            IList<MotionLocation> matrices = new List<MotionLocation>();
            matrices.Add(new MotionLocation(1.2, 1.3, 2.5, 2.5, "person", 0.75));

            MotionInfo obj = new MotionInfo("plug", "name", "reason", matrices, "img", 1, 2, Time);

            string matricesString = "\n" + string.Join("\n", obj.Matrices.Select(item => item.ToString()).ToArray());

            string expectedString = string.Format
                ("\nimg: {0}\nimgHeight: {1:0}\nimgWidth: {2:0}\nmatrices:\n {3}\nname: {4}\nplug: {5}\nreason: {6}\ntime: {7}\n",
                obj.Img,
                obj.ImgHeight,
                obj.ImgWidth,
                matricesString,
                obj.Name,
                obj.Plug,
                obj.Reason,
                obj.Time.ToString()
            );

            Assert.Equal(expectedString, obj.ToString());
        }
    }
}