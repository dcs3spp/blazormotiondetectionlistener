using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

using WebApp.Controllers;
using WebApp.S3.Contracts;


namespace WebApp.UnitTests
{
    public class ApiControllerTest
    {
        [Fact]
        public void ApiController_Constructor()
        {
            var s3Svc = Mock.Of<IS3Service>();
            var obj = new ApiController(new NullLogger<ApiController>(), s3Svc);

            Assert.NotNull(obj);
            Assert.Equal(typeof(ApiController), obj.GetType());
        }

        [Fact]
        public async Task ApiController_DownloadImage_Calls_S3_Service_GetObject()
        {
            const string Data = "test";
            const string ObjectName = "testObject";

            var rawData = Encoding.UTF8.GetBytes(Data);
            using (var bufferedData = new BufferedStream(new MemoryStream(rawData)))
            {
                var s3Svc = new Mock<IS3Service>();
                s3Svc.Setup(x => x.DownloadObject(ObjectName)).Returns(Task.FromResult(bufferedData)).Verifiable();

                var controller = new ApiController(new NullLogger<ApiController>(), s3Svc.Object);
                var result = await controller.DownloadImageAsync(ObjectName);

                s3Svc.Verify();
            }
        }

        [Fact]
        public async Task ApiController_DownloadImage_Prepares_FileStream()
        {
            const string Data = "test";
            const string ObjectName = "testObject";

            var rawData = Encoding.UTF8.GetBytes(Data);
            using (var bufferedData = new BufferedStream(new MemoryStream(rawData)))
            {
                var s3Svc = new Mock<IS3Service>();
                s3Svc.Setup(x => x.DownloadObject(ObjectName)).Returns(Task.FromResult(bufferedData));

                var controller = new ApiController(new NullLogger<ApiController>(), s3Svc.Object);
                var result = await controller.DownloadImageAsync(ObjectName);

                var fileResult = Assert.IsType<FileStreamResult>(result);
                var fileStream = Assert.IsAssignableFrom<BufferedStream>(fileResult.FileStream);

                using (var memory = new MemoryStream())
                {
                    fileStream.CopyTo(memory);

                    var bytes = memory.ToArray();
                    var actualData = Encoding.UTF8.GetString(bytes);

                    Assert.Equal(Data, actualData);
                }
            }
        }

        [Fact]
        public async Task ApiController_DownloadImage_Returns_FileNotFound_When_Exception_Thrown()
        {
            const string Data = "test";
            const string ObjectName = "testObject";

            var rawData = Encoding.UTF8.GetBytes(Data);
            using (var bufferedData = new BufferedStream(new MemoryStream(rawData)))
            {
                var s3Svc = new Mock<IS3Service>();
                s3Svc.Setup(x => x.DownloadObject(ObjectName)).Throws(new ArgumentNullException()).Verifiable();

                var controller = new ApiController(new NullLogger<ApiController>(), s3Svc.Object);
                var result = await controller.DownloadImageAsync(ObjectName);

                s3Svc.Verify();

                Assert.IsType<NotFoundResult>(result);
            }
        }
    }
}
