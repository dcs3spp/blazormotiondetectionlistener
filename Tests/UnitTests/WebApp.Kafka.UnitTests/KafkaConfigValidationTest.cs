using System.Collections.Generic;
using Xunit;

using WebApp.Kafka.Config;
using WebApp.Kafka.UnitTests.Helpers;


namespace WebApp.Kafka.UnitTests
{
    public class KafkaConfigValidationTest
    {
        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Success()
        {
            IList<string> consumerBrokers = new List<string>() { "127.0.0.1:9092", "myhost" };
            IList<string> producerBrokers = new List<string>() { "localhost:9092,example.com:9092", "myhost" };

            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, producerBrokers);
            KafkaConfigValidation obj = new KafkaConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Succeeded);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_BootstrapServersInvalid()
        {
            IList<string> consumerBrokers = new List<string>() { "[example]:9092", "myhost" };
            IList<string> producerBrokers = new List<string>() { "localhost:9092,..", "myhost" };

            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, producerBrokers);
            KafkaConfigValidation obj = new KafkaConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_ReplicationCount_GreaterThan_Broker_Count()
        {
            IList<string> consumerBrokers = new List<string>() { "localhost:9092", "example.com:9092" };
            IList<string> producerBrokers = new List<string>() { "localhost:9092", "example.com:9092" };

            // Set the replication count > Broker Count (2)
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, producerBrokers);
            config.Topic.PartitionCount = 3;
            config.Topic.ReplicationCount = 3;

            KafkaConfigValidation obj = new KafkaConfigValidation();

            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_Config_Null()
        {
            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, null);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_Missing_Consumer()
        {
            IList<string> brokers = new List<string>() { "localhost:9092", "example.com:9092" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            config.Consumer = null;

            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_Missing_Producer()
        {
            IList<string> brokers = new List<string>() { "localhost:9092", "example.com:9092" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            config.Producer = null;

            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_CameraTopics_Null()
        {
            IList<string> brokers = new List<string>() { "localhost:9092", "example.com:9092" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            config.MqttCameraTopics = null;

            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_CameraTopics_Empty()
        {
            IList<string> brokers = new List<string>() { "localhost:9092", "example.com:9092" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            config.MqttCameraTopics = new HashSet<string>();

            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_Topic_Null()
        {
            IList<string> brokers = new List<string>() { "localhost:9092", "example.com:9092" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            config.Topic = null;

            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_Topic_Name_Null()
        {
            IList<string> brokers = new List<string>() { "localhost:9092", "example.com:9092" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            config.Topic.Name = null;

            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_Producer_Bootstrap_Servers_Empty()
        {
            IList<string> brokers = new List<string>() { "localhost:9092", "example.com:9092" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            config.Producer.BootstrapServers = null;

            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_Producer_Bootstrap_Server_Empty()
        {
            IList<string> brokers = new List<string>() { "localhost:9092", "" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);

            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }


        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_Consumer_Bootstrap_Servers_Empty()
        {
            IList<string> brokers = new List<string>() { "localhost:9092", "example.com:9092" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            config.Consumer.BootstrapServers = null;

            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }

        [Fact]
        public void KafkaConfigValidation_Returns_ValidateOptionsResult_Failed_Consumer_GroupId_Empty()
        {
            IList<string> brokers = new List<string>() { "localhost:9092", "example.com:9092" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            config.Consumer.GroupId = null;

            KafkaConfigValidation obj = new KafkaConfigValidation();
            var result = obj.Validate(string.Empty, config);

            Assert.True(result.Failed);
        }
    }
}

