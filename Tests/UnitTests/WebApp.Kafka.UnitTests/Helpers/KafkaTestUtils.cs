using System.Collections.Generic;

using WebApp.Kafka.Config;


namespace WebApp.Kafka.UnitTests.Helpers
{
    public class KafkaTestUtils
    {
        /// <summary>
        /// Create a KafkaConfig object with BootstrapServers property for consumer and producer
        /// populated from a list
        /// </summary>
        /// <param name="consumerBootstrapServers">List of servers for consumer config section</param>
        /// <param name="producerBootstrapServers">List of servers for producer config section</param>
        /// <returns>Valid KafkaConfig</returns>
        public static KafkaConfig CreateKafkaConfig(IList<string> consumerBootstrapServers, IList<string> producerBootstrapServers)
        {
            KafkaConfig config = new KafkaConfig();
            config.Consumer = new Confluent.Kafka.ConsumerConfig()
            {
                GroupId = "Test-Consumer-Group",
                BootstrapServers = string.Join(',', consumerBootstrapServers)
            };
            config.Producer = new Confluent.Kafka.ProducerConfig()
            {
                BootstrapServers = string.Join(',', producerBootstrapServers)
            };
            config.MqttCameraTopics = new HashSet<string>() { "shinobi/group/monitor/trigger" };
            config.Topic = new KafkaTopic()
            {
                Name = "eventbus"
            };

            return config;
        }
    }
}