using System;
using System.Collections.Generic;

using Confluent.Kafka;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Xunit;

using WebApp.Kafka.Config;
using WebApp.Kafka.ProducerProvider;
using WebApp.Kafka.UnitTests.Helpers;


namespace WebApp.Kafka.UnitTests
{
    public class KafkaProducerProviderTest
    {
        [Fact]
        public void KafkaProducerProvider_Constructor_Sets_KafkaConfig()
        {
            IList<string> consumerBrokers = new List<string>() { "localhost" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, consumerBrokers);

            var options = Options.Create(config);

            KafkaProducerProvider prov = new KafkaProducerProvider(options);

            Assert.Same(options.Value, prov.Config);
        }

        [Fact]
        public void KafkaProducerProvider_Creates_Aggregate_With_Topic_Matching_Config_Topic()
        {
            IList<string> consumerBrokers = new List<string>() { "localhost" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, consumerBrokers);

            var options = Options.Create(config);

            KafkaProducerProvider prov = new KafkaProducerProvider(options);
            IKafkaProducerAggregate<string, byte[]> aggregate =
                prov.CreateProducerAggregate<string, byte[]>(NullLogger.Instance);

            Assert.Equal(config.Topic.Name, aggregate.Topic);
        }

        [Fact]
        public void KafkaProducerProvider_Creates_Aggregate_With_Typed_Producer_Property()
        {
            IList<string> consumerBrokers = new List<string>() { "localhost" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, consumerBrokers);

            var options = Options.Create(config);

            KafkaProducerProvider prov = new KafkaProducerProvider(options);
            IKafkaProducerAggregate<string, byte[]> aggregate =
                prov.CreateProducerAggregate<string, byte[]>(NullLogger.Instance);

            Assert.True(typeof(IProducer<string, byte[]>).IsAssignableFrom(aggregate.Producer.GetType()));
        }

        [Fact]
        public void KafkaProducerProvider_Creates_Aggregate_With_Typed_DeliveryReportAction()
        {
            Type expectedType = typeof(Action<DeliveryReport<string, byte[]>>);

            IList<string> consumerBrokers = new List<string>() { "localhost" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, consumerBrokers);

            var options = Options.Create(config);

            KafkaProducerProvider prov = new KafkaProducerProvider(options);
            IKafkaProducerAggregate<string, byte[]> aggregate =
                prov.CreateProducerAggregate<string, byte[]>(NullLogger.Instance);

            Assert.Equal(expectedType, aggregate.DeliveryReportAction.GetType());
        }

        [Fact]
        public void KafkaProducerProvider_Create_Aggregate_Raises_ArgumentNullException_For_Null_ILogger_Parameter()
        {
            IList<string> consumerBrokers = new List<string>() { "localhost" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, consumerBrokers);

            var options = Options.Create(config);

            KafkaProducerProvider prov = new KafkaProducerProvider(options);
            Assert.Throws<ArgumentNullException>(() => prov.CreateProducerAggregate<string, byte[]>(null));
        }
    }
}
