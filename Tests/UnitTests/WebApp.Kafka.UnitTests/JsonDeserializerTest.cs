using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

using Confluent.Kafka;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

using WebApp.Data.Serializers.Contracts;
using WebApp.Kafka.SchemaRegistry.Serdes;


namespace WebApp.Kafka.UnitTests
{
    public class JsonDeserializerTest
    {
        [Fact]
        public async Task JsonDeserializer_Deserialize_KafkaMessageFormat_Success()
        {
            const string message = "deserialized";

            ReadOnlyMemory<byte> kafkaMessage = CreateKafkaMessage(message, 5);

            // setup the mock to return expected message value
            var mockDeserialiser = new Mock<IMotionDetectionSerializer<string>>();
            mockDeserialiser.Setup(m => m.FromJSON(It.IsAny<Stream>())).Returns(message);

            JsonDeserializer<string> deserializer = new JsonDeserializer<string>(mockDeserialiser.Object, new NullLogger<JsonDeserializer<string>>());
            string result = await deserializer.DeserializeAsync(kafkaMessage, false, new SerializationContext());

            Assert.Equal(message, result);
        }

        [Fact]
        public async Task JsonDeserializer_Deserialize_MotionDetection_Catch_Deserialization_Failure()
        {
            const string message = "deserialized";

            ReadOnlyMemory<byte> kafkaMessage = CreateKafkaMessage(message, 5);

            // setup the mock deserializer to throw JsonException
            var mockDeserialiser = new Mock<IMotionDetectionSerializer<string>>();
            mockDeserialiser.Setup(m => m.FromJSON(It.IsAny<Stream>())).Throws<JsonException>();

            JsonDeserializer<string> deserializer = new JsonDeserializer<string>(mockDeserialiser.Object, new NullLogger<JsonDeserializer<string>>());

            await Assert.ThrowsAsync<JsonException>(async () => await deserializer.DeserializeAsync(kafkaMessage, false, new SerializationContext()));
        }

        [Fact]
        public async Task JsonDeserializer_Deserialize_IsNull()
        {
            var mockDeserialiser = new Mock<IMotionDetectionSerializer<string>>();

            SerializationContext ctxt = new SerializationContext();

            JsonDeserializer<string> deserializer = new JsonDeserializer<string>(mockDeserialiser.Object, new NullLogger<JsonDeserializer<string>>());
            string result = await deserializer.DeserializeAsync(null, true, ctxt);

            Assert.Null(result);
        }

        [Fact]
        public async Task JsonDeserializer_Deserialize_MotionDetection_Throws_InvalidDataException_Invalid_Data_Length_LessThan_5()
        {
            ReadOnlyMemory<byte> kafkaMessage = new ReadOnlyMemory<byte>(new byte[] { 0 });

            var mockDeserialiser = new Mock<IMotionDetectionSerializer<string>>();
            JsonDeserializer<string> deserializer = new JsonDeserializer<string>(mockDeserialiser.Object, new NullLogger<JsonDeserializer<string>>());

            await Assert.ThrowsAsync<InvalidDataException>(async () => await deserializer.DeserializeAsync(kafkaMessage, false, new SerializationContext()));
        }


        [Fact]
        public async Task JsonDeserializer_Deserialize_MotionDetection__Throws_InvalidDataException_Missing_FirstElement_MagicByte()
        {
            ReadOnlyMemory<byte> kafkaMessage = new ReadOnlyMemory<byte>(ToUTF8("testwithnomagicbyte"));

            var mockDeserialiser = new Mock<IMotionDetectionSerializer<string>>();
            JsonDeserializer<string> deserializer = new JsonDeserializer<string>(mockDeserialiser.Object, new NullLogger<JsonDeserializer<string>>());

            await Assert.ThrowsAsync<InvalidDataException>(async () => await deserializer.DeserializeAsync(kafkaMessage, false, new SerializationContext()));
        }


        /// <summary>Encode message as UTF8</summary>
        /// <returns>Byte array</returns>
        private byte[] ToUTF8(string message)
        {
            return Encoding.UTF8.GetBytes(message);
        }

        /// <summary>
        /// Modify message payload for Kafka Confluent compatibility:
        /// Prepend magic byte 0 to head of message
        /// Prepend a 4 byte schema id to allow JSON schema validation
        /// </summary>
        private ReadOnlyMemory<byte> CreateKafkaMessage(string payload, int schemaId, int initialBufferSize = 1024)
        {
            const byte magicByte = 0;

            using (MemoryStream stream = new MemoryStream(initialBufferSize))
            using (BinaryWriter writer = new BinaryWriter(stream, Encoding.UTF8))
            {
                writer.Write(magicByte);
                writer.Write(IPAddress.HostToNetworkOrder(schemaId));
                writer.Write(ToUTF8(payload));

                writer.Flush();

                return new ReadOnlyMemory<byte>(stream.ToArray());
            }
        }
    }
}