using Xunit;

using WebApp.Kafka.Config;


namespace WebApp.Kafka.UnitTests
{
    public class KafkaTopicTest
    {
        [Fact]
        public void KafkaTopic_Constructor_Sets_Default_Partition_Count()
        {
            const int expectedPartitionCount = 3;

            KafkaTopic config = new KafkaTopic();

            Assert.Equal(config.PartitionCount, expectedPartitionCount);
        }

        [Fact]
        public void KafkaTopic_Constructor_Sets_Default_Replication_Count()
        {
            const int expectedReplicationCount = 1;

            KafkaTopic config = new KafkaTopic();

            Assert.Equal(config.ReplicationCount, expectedReplicationCount);
        }
    }
}