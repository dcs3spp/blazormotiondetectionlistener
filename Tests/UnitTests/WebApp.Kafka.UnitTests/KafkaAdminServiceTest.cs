using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Confluent.Kafka;
using Confluent.Kafka.Admin;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

using WebApp.Kafka.Admin;
using WebApp.Kafka.Config;
using WebApp.Kafka.Testing.Helpers;


namespace WebApp.Kafka.UnitTests
{
    public class KafkaAdminServiceTest
    {
        [Fact]
        public void KafkaAdminService_Constructor_Throws_ArgumentNullException_When_Config_Null()
        {
            var mockFactory = Mock.Of<IKafkaAdminFactory>();

            Assert.Throws<ArgumentNullException>(() =>
            {
                var svc = new KafkaAdminService(
                    null,
                    mockFactory,
                    new NullLogger<KafkaAdminService>());
            });
        }

        [Fact]
        public void KafkaAdminService_Constructor_Throws_ArgumentNullException_When_Factory_Null()
        {
            IList<string> brokers = new List<string>() { "127.0.0.1:9092", "myhost" };

            var config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            Assert.Throws<ArgumentNullException>(() =>
            {
                var svc = new KafkaAdminService(
                    opts,
                    null,
                    new NullLogger<KafkaAdminService>());
            });
        }

        [Fact]
        public void KafkaAdminService_Constructor_Throws_ArgumentNullException_When_Logger_Null()
        {
            IList<string> brokers = new List<string>() { "127.0.0.1:9092", "myhost" };

            var config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var mockFactory = Mock.Of<IKafkaAdminFactory>();

            Assert.Throws<ArgumentNullException>(() =>
            {
                var svc = new KafkaAdminService(
                    opts,
                    mockFactory,
                    null);
            });
        }

        [Fact]
        public async Task KafkaAdminService_InitializeAsync_Calls_KafkaAdminClient_CreateTopicsAsync()
        {
            IList<string> brokers = new List<string>() { "127.0.0.1:9092", "myhost" };

            var config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var mockClient = new Mock<IAdminClient>();
            mockClient.Setup(x => x.CreateTopicsAsync(It.IsAny<IEnumerable<TopicSpecification>>(), null));

            var mockFactory = new Mock<IKafkaAdminFactory>();
            mockFactory.Setup(x => x.CreateAdminClient()).Returns(mockClient.Object);

            var topic = CreateTopicSpecificationFromConfig(config);

            var svc = new KafkaAdminService(
                opts,
                mockFactory.Object,
                new NullLogger<KafkaAdminService>());

            await svc.InitializeAsync();

            mockClient.Verify(x =>
                x.CreateTopicsAsync(
                    It.Is<IEnumerable<TopicSpecification>>(lst => VerifyConfigTopic(lst, config)),
                    null
                )
            );
        }

        [Fact]
        public async Task KafkaAdminService_InitializeAsync_Throws_CreateTopicsException_TopicAlreadyExists_ForDuplicate_Topics()
        {
            IList<string> brokers = new List<string>() { "127.0.0.1:9092", "myhost" };

            var config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var mockClient = new Mock<IAdminClient>();
            var mockFactory = new Mock<IKafkaAdminFactory>();

            var topic = CreateTopicSpecificationFromConfig(config);

            mockFactory.Setup(x => x.CreateAdminClient()).Returns(mockClient.Object);
            mockClient.Setup(x =>
                x.CreateTopicsAsync(
                    It.Is<IEnumerable<TopicSpecification>>(lst => VerifyConfigTopic(lst, config)),
                    null
                )
            ).ThrowsAsync(
                new CreateTopicsException(CreateTopicErrorReport(config.Topic.Name, ErrorCode.TopicAlreadyExists))
            ).Verifiable();

            var svc = new KafkaAdminService(
                opts,
                mockFactory.Object,
                new NullLogger<KafkaAdminService>());

            await svc.InitializeAsync();

            mockClient.Verify();
        }

        [Fact]
        public async Task KafkaAdminService_InitializeAsync_Rethrows_CreateTopicsException_For_ErrorCode_Not_TopicAlreadyExists()
        {
            IList<string> brokers = new List<string>() { "127.0.0.1:9092", "myhost" };

            var config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var mockClient = new Mock<IAdminClient>();
            var mockFactory = new Mock<IKafkaAdminFactory>();

            var topic = CreateTopicSpecificationFromConfig(config);

            mockFactory.Setup(x => x.CreateAdminClient()).Returns(mockClient.Object);
            mockClient.Setup(x =>
                x.CreateTopicsAsync(
                    It.IsAny<IEnumerable<TopicSpecification>>(),
                    null
                )
            ).ThrowsAsync(
                new CreateTopicsException(CreateTopicErrorReport(config.Topic.Name, ErrorCode.RequestTimedOut))
            );

            var svc = new KafkaAdminService(
                opts,
                mockFactory.Object,
                new NullLogger<KafkaAdminService>());

            await Assert.ThrowsAsync<CreateTopicsException>(() => svc.InitializeAsync());
        }


        /// <summary>
        /// Create an array with one <see cref="TopicSpecification"/>
        /// from a <see cref="KafkaConfig"/> instance
        /// </summary>
        /// <param name="config">Options from config file</param>
        /// <returns>
        /// List containing one <see cref="TopicSpecification"/> item
        /// derived from config options
        /// </returns>
        private IList<TopicSpecification> CreateTopicSpecificationFromConfig(KafkaConfig config)
        {
            var topic = new TopicSpecification
            {
                Name = config.Topic.Name,
                NumPartitions = config.Topic.PartitionCount,
                ReplicationFactor = config.Topic.ReplicationCount
            };

            return new List<TopicSpecification>(new TopicSpecification[] { topic });
        }


        /// <summary>Verify that src and destination list contain topic specification</summary>
        /// <param name="expected">List topics</param>
        /// <param name="config">Configuration containing Kafka topic</param>
        /// <returns>True if list contains topic specification from config</returns>
        private bool VerifyConfigTopic(IEnumerable<TopicSpecification> expected, KafkaConfig config)
        {
            bool found = false;
            foreach (var item in expected)
            {
                if (item.Name == config.Topic.Name
                    && item.NumPartitions == config.Topic.PartitionCount
                    && item.ReplicationFactor == config.Topic.ReplicationCount)
                {
                    found = true;
                }

            }

            return found;
        }


        /// <summary>Create Kafka topic error report for a duplicate topic</summary>
        /// <param name="topic">Kafka topic that raised duplicate error report</param>
        /// <param name="error"><see cref="ErrorCode"/> for the report"</param>
        /// <returns>
        /// <see cref="IList"/> containing <see cref="CreateTopicReport"/> for topic
        /// </returns>
        private List<CreateTopicReport> CreateTopicErrorReport(string topic, ErrorCode error)
        {
            var report = new CreateTopicReport();

            report.Error = new Error(error);
            report.Topic = topic;

            return new List<CreateTopicReport>() { report };
        }
    }
}