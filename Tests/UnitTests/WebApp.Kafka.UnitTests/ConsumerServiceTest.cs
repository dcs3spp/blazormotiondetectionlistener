using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Xunit;

using Confluent.Kafka;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Moq;
using WebApp.Data;
using WebApp.Kafka.Config;
using WebApp.Kafka.Testing.Helpers;
using WebApp.Realtime.SignalR;


namespace WebApp.Kafka.UnitTests
{
    public class ConsumerServiceTest
    {
        [Fact]
        public void ConsumerService_Constructor_Preconfigures_Consumer_Confg()
        {
            var config = CreateKafkaConfig();

            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var mockHub = Mock.Of<IHubContext<MotionHub, IMotion>>();
            var mockDeserializer = Mock.Of<IAsyncDeserializer<MotionDetection>>();
            var mockFactory = new Mock<ConsumerFactory>();

            // setup the factory to return Kafka consumer mock
            var mockConsumer = new Mock<IConsumer<string, MotionDetection>>();
            mockFactory.Setup(_ => _(
                It.Is<KafkaConfig>(c => c == config),
                It.Is<IAsyncDeserializer<MotionDetection>>(x => x == mockDeserializer)
            )).Returns(mockConsumer.Object).Verifiable();

            var logger = new NullLogger<ConsumerService>();
            var svc = new ConsumerService(opts, mockFactory.Object, mockHub, mockDeserializer, logger);

            Assert.Equal(true, config.Consumer.EnableAutoCommit);
            Assert.Equal(false, config.Consumer.EnableAutoOffsetStore);
            Assert.Equal(AutoOffsetReset.Latest, config.Consumer.AutoOffsetReset);
        }

        [Fact]
        public void ConsumerService_Constructor_Throws_ArgumentNullException_When_Config_Null()
        {
            var mockHub = Mock.Of<IHubContext<MotionHub, IMotion>>();
            var mockDeserializer = Mock.Of<IAsyncDeserializer<MotionDetection>>();
            var mockFactory = Mock.Of<ConsumerFactory>();

            var logger = new NullLogger<ConsumerService>();

            Assert.Throws<ArgumentNullException>(() =>
                new ConsumerService(null, mockFactory, mockHub, mockDeserializer, logger)
            );
        }

        [Fact]
        public void ConsumerService_Constructor_Throws_ArgumentNullException_When_Factory_Null()
        {
            var config = CreateKafkaConfig();
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var logger = new NullLogger<ConsumerService>();
            var mockHub = Mock.Of<IHubContext<MotionHub, IMotion>>();
            var mockDeserializer = Mock.Of<IAsyncDeserializer<MotionDetection>>();

            Assert.Throws<ArgumentNullException>(() =>
                new ConsumerService(opts, null, mockHub, mockDeserializer, logger)
            );
        }

        [Fact]
        public void ConsumerService_Constructor_Throws_ArgumentNullException_When_Logger_Null()
        {
            var config = CreateKafkaConfig();
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var mockFactory = Mock.Of<ConsumerFactory>();
            var mockHub = Mock.Of<IHubContext<MotionHub, IMotion>>();
            var mockDeserializer = Mock.Of<IAsyncDeserializer<MotionDetection>>();

            Assert.Throws<ArgumentNullException>(() =>
                new ConsumerService(opts, mockFactory, mockHub, mockDeserializer, null)
            );
        }

        [Fact]
        public void ConsumerService_Constructor_Throws_ArgumentNullException_When_SignalRHub_Null()
        {
            var config = CreateKafkaConfig();
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var logger = new NullLogger<ConsumerService>();
            var mockFactory = Mock.Of<ConsumerFactory>();
            var mockDeserializer = Mock.Of<IAsyncDeserializer<MotionDetection>>();

            Assert.Throws<ArgumentNullException>(() =>
                new ConsumerService(opts, mockFactory, null, mockDeserializer, logger)
            );
        }

        [Fact]
        public void ConsumerService_Constructor_Throws_ArgumentNullException_When_Deserializer_Null()
        {
            var config = CreateKafkaConfig();
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var logger = new NullLogger<ConsumerService>();
            var mockHub = Mock.Of<IHubContext<MotionHub, IMotion>>();
            var mockFactory = Mock.Of<ConsumerFactory>();

            Assert.Throws<ArgumentNullException>(() =>
                new ConsumerService(opts, mockFactory, mockHub, null, logger)
            );
        }

        [Fact]
        public async Task ConsumerService_ExecAsync_Takes_No_Further_Action_When_An_Unrecognised_Topic_Is_Consumed()
        {
            const string Key = "key";

            var config = CreateKafkaConfig();
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var logger = new NullLogger<ConsumerService>();
            var hub = Mock.Of<IHubContext<MotionHub, IMotion>>(h =>
                h.Clients == Mock.Of<IHubClients<IMotion>>(clients =>
                    clients.All == Mock.Of<IMotion>()
                )
            );
            var hubMock = Mock.Get<IHubContext<MotionHub, IMotion>>(hub);
            var mockConsumer = new Mock<IConsumer<string, MotionDetection>>();
            var mockFactory = new Mock<ConsumerFactory>();
            var deserializer = Mock.Of<IAsyncDeserializer<MotionDetection>>();

            // mock consumed messaged, containing a key that is not a recognised camera device
            var message = Mock.Of<ConsumeResult<string, MotionDetection>>(obj =>
                    obj.Message == Mock.Of<Message<string, MotionDetection>>(msg =>
                      msg.Key == Key
                      && msg.Value == CreateMotionDetection()
                    )
            );

            // setup the factory to return Kafka consumer mock
            mockFactory.Setup(_ => _(
                It.Is<KafkaConfig>(c => c == config),
                It.Is<IAsyncDeserializer<MotionDetection>>(x => x == deserializer)
            )).Returns(mockConsumer.Object).Verifiable();

            // setup kafka consumer to subscribe, consume mock message from topic and close
            mockConsumer.Setup(c => c.Subscribe(It.Is<string>(c => c == config.Topic.Name))).Verifiable();
            mockConsumer.Setup(c => c.Consume(It.IsAny<CancellationToken>())).Returns(message).Verifiable();
            //mockConsumer.Setup(c => c.Close()).Verifiable();

            // create and start the consumer service
            var svc = new ConsumerService(opts, mockFactory.Object, hub, deserializer, logger);
            await svc.StartAsync(CancellationToken.None);
            await Task.Delay(10);
            await svc.StopAsync(CancellationToken.None);

            // verify that consumer factory was called and consumer subscribed, consumed and closed
            mockFactory.Verify();
            mockConsumer.Verify();

            // verify that signal r clients were not notified and the offset for the message was not stored
            hubMock.Verify(x => x.Clients.All.ReceiveMotionDetection(message.Message.Value), Times.Never());
            mockConsumer.Verify(x => x.StoreOffset(message), Times.Never());
        }

        [Fact]
        public async Task ConsumerService_StartAsync_Dispatches_To_SignalR_Clients_Storing_Kafka_Offset_For_Message()
        {
            const string Key = "shinobi/group/monitor/trigger";

            var config = CreateKafkaConfig();
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var logger = new NullLogger<ConsumerService>();
            var hub = Mock.Of<IHubContext<MotionHub, IMotion>>(h =>
                h.Clients == Mock.Of<IHubClients<IMotion>>(clients =>
                    clients.All == Mock.Of<IMotion>()
                )
            );
            var mockConsumer = new Mock<IConsumer<string, MotionDetection>>();
            var mockFactory = new Mock<ConsumerFactory>();
            var mockHub = Mock.Get<IHubContext<MotionHub, IMotion>>(hub);
            var deserializer = Mock.Of<IAsyncDeserializer<MotionDetection>>();

            // mock consumed messaged, containing a key that is not a recognised camera device
            var message = Mock.Of<ConsumeResult<string, MotionDetection>>(obj =>
                    obj.Message == Mock.Of<Message<string, MotionDetection>>(msg =>
                      msg.Key == Key
                      && msg.Value == CreateMotionDetection()
                    )
            );

            // setup the factory to return Kafka consumer mock
            mockFactory.Setup(_ => _(
                It.Is<KafkaConfig>(c => c == config),
                It.Is<IAsyncDeserializer<MotionDetection>>(x => x == deserializer)
            )).Returns(mockConsumer.Object).Verifiable();

            // setup kafka consumer to subscribe, consume mock message from topic and close
            mockConsumer.Setup(c => c.Subscribe(It.Is<string>(c => c == config.Topic.Name))).Verifiable();
            mockConsumer.Setup(c => c.Consume(It.IsAny<CancellationToken>())).Returns(message).Verifiable();
            // mockConsumer.Setup(c => c.Close()).Verifiable();
            mockConsumer.Setup(x => x.StoreOffset(message)).Verifiable();
            mockHub.Setup(hub => hub.Clients.All.ReceiveMotionDetection(message.Message.Value)).Verifiable();

            // create and start the consumer service
            var svc = new ConsumerService(opts, mockFactory.Object, hub, deserializer, logger);
            await svc.StartAsync(CancellationToken.None);
            await Task.Delay(10);
            await svc.StopAsync(CancellationToken.None);

            // verify that consumer factory was called and consumer subscribed, consumed and closed
            mockFactory.Verify();
            mockConsumer.Verify();
            mockHub.Verify();
        }

        [Fact]
        public async Task ConsumerService_Consumer_Closed_When_Consumer_Throws_Exception()
        {
            const string Key = "shinobi/group/monitor/trigger";

            var config = CreateKafkaConfig();
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var logger = new NullLogger<ConsumerService>();
            var hub = Mock.Of<IHubContext<MotionHub, IMotion>>(h =>
                h.Clients == Mock.Of<IHubClients<IMotion>>(clients =>
                    clients.All == Mock.Of<IMotion>()
                )
            );
            var mockConsumer = new Mock<IConsumer<string, MotionDetection>>();
            var mockFactory = new Mock<ConsumerFactory>();
            var mockHub = Mock.Get<IHubContext<MotionHub, IMotion>>(hub);
            var deserializer = Mock.Of<IAsyncDeserializer<MotionDetection>>();

            // mock consumed messaged, containing a key that is not a recognised camera device
            var message = Mock.Of<ConsumeResult<string, MotionDetection>>(obj =>
                    obj.Message == Mock.Of<Message<string, MotionDetection>>(msg =>
                      msg.Key == Key
                      && msg.Value == CreateMotionDetection()
                    )
            );

            // setup the factory to return Kafka consumer mock
            mockFactory.Setup(_ => _(
                It.Is<KafkaConfig>(c => c == config),
                It.Is<IAsyncDeserializer<MotionDetection>>(x => x == deserializer)
            )).Returns(mockConsumer.Object).Verifiable();

            // setup kafka consumer to expect subscribe, throw a ConsumeException and close
            mockConsumer.Setup(c => c.Subscribe(It.Is<string>(c => c == config.Topic.Name))).Verifiable();
            mockConsumer.Setup(c => c.Consume(It.IsAny<CancellationToken>()))
                .Throws(CreateFatalConsumeException())
                .Verifiable();

            // create and start the consumer service
            var svc = new ConsumerService(opts, mockFactory.Object, hub, deserializer, logger);
            await svc.StartAsync(CancellationToken.None);
            await Task.Delay(10);
            await svc.StopAsync(CancellationToken.None);

            // verify that consumer factory was called and consumer threw an exception
            mockFactory.Verify();
            mockConsumer.Verify();

            // verify that signal r clients were not notified and the offset for the message was not stored
            mockHub.Verify(x => x.Clients.All.ReceiveMotionDetection(message.Message.Value), Times.Never());
            mockConsumer.Verify(x => x.StoreOffset(message), Times.Never());
        }


        [Fact]
        public void ConsumerService_Consumer_Closed_By_ConsumerService_Disposed()
        {
            var config = CreateKafkaConfig();
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var logger = new NullLogger<ConsumerService>();
            var hub = Mock.Of<IHubContext<MotionHub, IMotion>>(h =>
                h.Clients == Mock.Of<IHubClients<IMotion>>(clients =>
                    clients.All == Mock.Of<IMotion>()
                )
            );
            var mockConsumer = new Mock<IConsumer<string, MotionDetection>>();
            var mockFactory = new Mock<ConsumerFactory>();
            var mockHub = Mock.Get<IHubContext<MotionHub, IMotion>>(hub);
            var deserializer = Mock.Of<IAsyncDeserializer<MotionDetection>>();

            // setup the factory to return Kafka consumer mock
            mockFactory.Setup(_ => _(
                It.Is<KafkaConfig>(c => c == config),
                It.Is<IAsyncDeserializer<MotionDetection>>(x => x == deserializer)
            )).Returns(mockConsumer.Object).Verifiable();

            // create the consumer service
            var svc = new ConsumerService(opts, mockFactory.Object, hub, deserializer, logger);

            svc.Dispose();

            mockConsumer.Verify(x => x.Close(), Times.Once());
        }

        [Fact]
        public void ConsumerService_Consumer_Disposed_By_ConsumerService_Disposed()
        {
            var config = CreateKafkaConfig();
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var logger = new NullLogger<ConsumerService>();
            var hub = Mock.Of<IHubContext<MotionHub, IMotion>>(h =>
                h.Clients == Mock.Of<IHubClients<IMotion>>(clients =>
                    clients.All == Mock.Of<IMotion>()
                )
            );
            var mockConsumer = new Mock<IConsumer<string, MotionDetection>>();
            var mockFactory = new Mock<ConsumerFactory>();
            var mockHub = Mock.Get<IHubContext<MotionHub, IMotion>>(hub);
            var deserializer = Mock.Of<IAsyncDeserializer<MotionDetection>>();

            // setup the factory to return Kafka consumer mock
            mockFactory.Setup(_ => _(
                It.Is<KafkaConfig>(c => c == config),
                It.Is<IAsyncDeserializer<MotionDetection>>(x => x == deserializer)
            )).Returns(mockConsumer.Object).Verifiable();

            // create the consumer service
            var svc = new ConsumerService(opts, mockFactory.Object, hub, deserializer, logger);

            svc.Dispose();

            mockConsumer.Verify(x => x.Dispose(), Times.Once());
        }


        /// <summary>Create KafkaConfig with list of brokers for consumer and produer</summary>
        /// <returns>KafkaConfig</returns>
        private KafkaConfig CreateKafkaConfig()
        {
            IList<string> consumerBrokers = new List<string>() { "127.0.0.1:9092", "myhost" };
            IList<string> producerBrokers = new List<string>() { "localhost:9092,example.com:9092", "myhost" };

            return KafkaTestUtils.CreateKafkaConfig(consumerBrokers, producerBrokers);
        }

        /// <summary>Create a default Motion detection obect</summary>
        /// <returns>Default motion detection object</returns>
        private MotionDetection CreateMotionDetection()
        {
            const long Time = 820;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo expectedInfo = new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "YmFzZTY0", 64, 48, Time);

            return new MotionDetection("myGroup", Time, "myMonitorId", Plugin, expectedInfo);
        }

        /// <summary>Create a ConsumeException object</summary>
        /// <returns>Consume Exception with error code for BrokerNotAvailable</returns>
        private ConsumeException CreateFatalConsumeException()
        {
            var result = new ConsumeResult<byte[], byte[]>();

            return new ConsumeException(
                new ConsumeResult<byte[], byte[]>(),
                ErrorCode.Local_Fatal,
                new ArgumentNullException()
            );
        }
    }
}