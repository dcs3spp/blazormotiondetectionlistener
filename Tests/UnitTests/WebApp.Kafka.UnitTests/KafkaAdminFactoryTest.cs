using System;
using System.Collections.Generic;

using Confluent.Kafka;
using Microsoft.Extensions.Options;
using Xunit;

using WebApp.Kafka.Admin;
using WebApp.Kafka.Config;
using WebApp.Kafka.UnitTests.Helpers;


namespace WebApp.Kafka.UnitTests
{
    public class KafkaAdminFactoryTest
    {
        [Fact]
        public void KafkaAdminFactory_Constructor_Throws_ArgumentNullException_When_Config_Null()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                var svc = new KafkaAdminFactory(null);
            });
        }

        [Fact]
        public void KafkaAdminFactory_CreateAdminClient_Creates_IAdminClient()
        {
            IList<string> brokers = new List<string>() { "127.0.0.1:9092", "myhost" };
            var config = KafkaTestUtils.CreateKafkaConfig(brokers, brokers);
            IOptions<KafkaConfig> opts = Options.Create<KafkaConfig>(config);

            var svc = new KafkaAdminFactory(opts);

            using (var client = svc.CreateAdminClient())
            {
                Assert.IsAssignableFrom<IAdminClient>(client);
            }
        }
    }
}