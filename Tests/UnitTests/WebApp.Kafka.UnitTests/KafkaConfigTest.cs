using System.Collections.Generic;
using System.Linq;
using Xunit;

using WebApp.Kafka.Config;
using WebApp.Kafka.UnitTests.Helpers;


namespace WebApp.Kafka.UnitTests
{
    public class KafkaConfigTest
    {
        [Fact]
        public void KafkaConfig_GetBrokers_Returns_Consumer_Producer_Set_Union()
        {
            IList<string> consumers = new List<string>() { "broker1:9092", "broker2:9092" };
            IList<string> producers = new List<string>() { "broker1:9092", "broker2:9092" };
            HashSet<string> expectedBrokers = consumers.Concat<string>(producers).ToHashSet<string>();

            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumers, producers);

            Assert.Equal(expectedBrokers, config.GetBrokers());
        }
    }
}