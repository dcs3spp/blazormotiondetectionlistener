using System;
using System.Collections.Generic;

using Confluent.Kafka;
using Moq;
using Microsoft.Extensions.Options;
using Xunit;

using WebApp.Kafka.Config;
using WebApp.Kafka.ProducerProvider;
using WebApp.Kafka.UnitTests.Helpers;


namespace WebApp.Kafka.UnitTests
{
    public class KafkaProducerAggregateTest
    {
        [Fact]
        public void KafkaProducerAggregateTest_Constructor_Sets_Producer_Property()
        {
            var mockProducer = new Mock<IProducer<string, byte[]>>();
            var mockDeliveryReport = new Mock<Action<DeliveryReport<string, byte[]>>>();

            IList<string> consumerBrokers = new List<string>() { "localhost" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, consumerBrokers);

            var options = Options.Create(config);

            KafkaProducerAggregate<string, byte[]> obj =
                new KafkaProducerAggregate<string, byte[]>(mockProducer.Object, mockDeliveryReport.Object, options.Value.Topic.Name);

            Assert.Equal(mockProducer.Object, obj.Producer);
        }

        [Fact]
        public void KafkaProducerAggregateTest_Constructor_Sets_DeliveryReportAction_Property()
        {
            var mockProducer = new Mock<IProducer<string, byte[]>>();
            var mockDeliveryReportAction = new Mock<Action<DeliveryReport<string, byte[]>>>();

            IList<string> consumerBrokers = new List<string>() { "localhost" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, consumerBrokers);

            var options = Options.Create(config);

            KafkaProducerAggregate<string, byte[]> obj =
                new KafkaProducerAggregate<string, byte[]>(mockProducer.Object, mockDeliveryReportAction.Object, options.Value.Topic.Name);

            Assert.Equal(mockDeliveryReportAction.Object, obj.DeliveryReportAction);
        }

        [Fact]
        public void KafkaProducerAggregateTest_Constructor_Sets_Topic_Property()
        {
            var mockProducer = new Mock<IProducer<string, byte[]>>();
            var mockDeliveryReportAction = new Mock<Action<DeliveryReport<string, byte[]>>>();

            IList<string> consumerBrokers = new List<string>() { "localhost" };
            KafkaConfig config = KafkaTestUtils.CreateKafkaConfig(consumerBrokers, consumerBrokers);

            var options = Options.Create(config);

            KafkaProducerAggregate<string, byte[]> obj =
                new KafkaProducerAggregate<string, byte[]>(mockProducer.Object, mockDeliveryReportAction.Object, options.Value.Topic.Name);

            Assert.Equal(config.Topic.Name, obj.Topic);
        }
    }
}
