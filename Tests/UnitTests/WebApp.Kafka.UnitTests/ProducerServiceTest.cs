using System;
using System.IO;
using System.Text;

using Confluent.Kafka;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

using WebApp.Kafka.ProducerProvider;


namespace WebApp.Kafka.UnitTests
{
    public class ProducerServiceTest
    {
        [Fact]
        public void ProducerService_Constructor_Throws_ArgumentNullException_When_Factory_Null()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new ProducerService<string, byte[]>(null, new NullLogger<ProducerService<string, byte[]>>())
            );
        }

        [Fact]
        public void ProducerService_Constructor_Throws_ArgumentNullException_When_Logger_Null()
        {
            var mockFactory = new Mock<IKafkaProducerProvider>();

            Assert.Throws<ArgumentNullException>(() =>
                new ProducerService<string, byte[]>(mockFactory.Object, null)
            );
        }

        [Fact]
        public void ProducerService_Constructor_Calls_Factory_To_Create_Aggregate()
        {
            var logger = new NullLogger<ProducerService<string, byte[]>>();
            var mockFactory = new Mock<IKafkaProducerProvider>();

            mockFactory.Setup(x => x.CreateProducerAggregate<string, byte[]>(logger))
                .Verifiable();

            var svc = new ProducerService<string, byte[]>(mockFactory.Object, logger);

            mockFactory.Verify();
        }

        [Fact]
        public void ProducerService_Produce_Calls_Agreggate_IProducer_Produce_With_Expected_Args()
        {
            const string key = "key";
            byte[] value = new byte[] { 1, 2, 3, 4 };
            const string topic = "topic";

            Action<DeliveryReport<string, byte[]>> deliveryReport = delegate (DeliveryReport<string, byte[]> report) { };

            var logger = new NullLogger<ProducerService<string, byte[]>>();
            var mockFactory = new Mock<IKafkaProducerProvider>();

            var mockProducer = new Mock<IProducer<string, byte[]>>();
            mockProducer.Setup(x =>
                x.Produce(
                    topic,
                    It.Is<Message<string, byte[]>>(
                        x => x.Value == value && x.Key == key),
                        deliveryReport
                    ))
            .Verifiable();

            var aggregate = new KafkaProducerAggregate<string, byte[]>(
                mockProducer.Object, deliveryReport, topic
            );

            mockFactory.Setup(x => x.CreateProducerAggregate<string, byte[]>(logger))
                .Returns(aggregate);

            var svc = new ProducerService<string, byte[]>(mockFactory.Object, logger);
            svc.Produce(key, value);

            mockProducer.Verify();
        }

        [Fact]
        public void ProducerService_PrepareMessage_Creates_Kafka_Compliant_Message_Payload()
        {
            const int headerSize = sizeof(int) + sizeof(byte);
            const int magicByte = 0;
            const int bufferSize = 1024;
            const int schemaId = 5;
            const string payload = "payload";

            byte[] raw = Encoding.UTF8.GetBytes(payload);

            var logger = new NullLogger<ProducerService<string, byte[]>>();
            var mockFactory = new Mock<IKafkaProducerProvider>();

            var svc = new ProducerService<string, byte[]>(mockFactory.Object, logger);
            byte[] kafkaMessage = svc.PrepareMessage(raw, schemaId, bufferSize);

            Assert.True(kafkaMessage.Length > 5);
            Assert.Equal(magicByte, kafkaMessage[0]);
            using (var stream = new MemoryStream(kafkaMessage, headerSize, kafkaMessage.Length - headerSize))
            {
                Assert.Equal(raw, stream.ToArray());
            }
        }


        [Fact]
        public void ProducerService_Dispose_Calls_Agreggate_IProducer_Dispose()
        {
            const string topic = "topic";

            Action<DeliveryReport<string, byte[]>> deliveryReport = delegate (DeliveryReport<string, byte[]> report) { };

            var logger = new NullLogger<ProducerService<string, byte[]>>();
            var mockFactory = new Mock<IKafkaProducerProvider>();

            var mockProducer = new Mock<IProducer<string, byte[]>>();
            mockProducer.Setup(x => x.Dispose()).Verifiable();

            var aggregate = new KafkaProducerAggregate<string, byte[]>(
                mockProducer.Object, deliveryReport, topic
            );

            mockFactory.Setup(x => x.CreateProducerAggregate<string, byte[]>(logger)).Returns(aggregate);

            var svc = new ProducerService<string, byte[]>(mockFactory.Object, logger);
            svc.Dispose();

            mockProducer.Verify();
        }

        [Fact]
        public void ProducerService_Dispose_Does_Not_Call_Agreggate_IProducer_Dispose_When_ProducerService_Already_Disposed()
        {
            const string topic = "topic";

            Action<DeliveryReport<string, byte[]>> deliveryReport = delegate (DeliveryReport<string, byte[]> report) { };

            var logger = new NullLogger<ProducerService<string, byte[]>>();
            var mockFactory = new Mock<IKafkaProducerProvider>();

            var mockProducer = new Mock<IProducer<string, byte[]>>();

            var aggregate = new KafkaProducerAggregate<string, byte[]>(
                mockProducer.Object, deliveryReport, topic
            );

            mockFactory.Setup(x => x.CreateProducerAggregate<string, byte[]>(logger)).Returns(aggregate);

            var svc = new ProducerService<string, byte[]>(mockFactory.Object, logger);
            svc.Dispose();
            svc.Dispose();

            mockProducer.Verify(x => x.Dispose(), Times.Once());
        }

    }
}