using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using MQTTnet;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

using WebApp.Kafka.Contracts;
using WebApp.Mqtt.Contracts;
using WebApp.S3.Contracts;
using WebApp.Testing.Fixtures;


namespace WebApp.BackgroundServices.UnitTests
{
    [Collection("Json collection")]
    public class MqttKafkaBridgeTest
    {
        private const int DefaultBufferSize = 1024;
        private const string MotionResourceFile = @"WebApp.Testing.Utils.TestData.motion.json";
        private const short SchemaId = 5;

        private string guid = Guid.NewGuid().ToString();

        private byte[] _Payload { get; set; }
        private byte[] _KafkaPayload { get; set; }
        private byte[] _UploadedImagePayload { get; set; }

        public MqttKafkaBridgeTest(EmbeddedJsonFileResources fixture)
        {
            using (var stream = new StreamReader(fixture.GetStream(MotionResourceFile)))
            {
                string json = stream.ReadToEnd();
                _Payload = Encoding.UTF8.GetBytes(json);

                JObject rootElement = JObject.Parse(json);
                JObject details = (JObject)rootElement["details"];
                byte[] fromBase64 = Convert.FromBase64String(details["img"].Value<string>());
                details["img"] = guid;

                _UploadedImagePayload = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(rootElement));
                _KafkaPayload = ToKafkaPayload(_UploadedImagePayload, SchemaId);
            }
        }

        [Fact]
        public void MqttKafkaBridge_Constructor_Throws_ArgumentNullException_When_IS3Service_Null()
        {
            var producerSvc = new Mock<IProducerService<string, byte[]>>();
            var mockMqttSvc = new Mock<IMqttService>();
            var logger = new NullLogger<MqttKafkaBridge>();

            Assert.Throws<ArgumentNullException>(() =>
            {
                var mqttKafkaBridge = new MqttKafkaBridge(
                    null,
                    mockMqttSvc.Object,
                    producerSvc.Object,
                    logger
                );
            });
        }

        [Fact]
        public void MqttKafkaBridge_Constructor_Throws_ArgumentNullException_When_IMqttService_Null()
        {
            var producerSvc = new Mock<IProducerService<string, byte[]>>();
            var mockS3Svc = new Mock<IS3Service>();
            var logger = new NullLogger<MqttKafkaBridge>();

            Assert.Throws<ArgumentNullException>(() =>
            {
                var mqttKafkaBridge = new MqttKafkaBridge(
                    mockS3Svc.Object,
                    null,
                    producerSvc.Object,
                    logger
                );
            });
        }

        [Fact]
        public void MqttKafkaBridge_Constructor_Throws_ArgumentNullException_When_IProducerService_Null()
        {
            var mockMqttSvc = new Mock<IMqttService>();
            var mockS3Svc = new Mock<IS3Service>();
            var logger = new NullLogger<MqttKafkaBridge>();

            Assert.Throws<ArgumentNullException>(() =>
            {
                var mqttKafkaBridge = new MqttKafkaBridge(
                    mockS3Svc.Object,
                    mockMqttSvc.Object,
                    null,
                    logger
                );
            });
        }

        [Fact]
        public void MqttKafkaBridge_Constructor_Throws_ArgumentNullException_When_ILogger_Null()
        {
            var mockMqttSvc = new Mock<IMqttService>();
            var mockS3Svc = new Mock<IS3Service>();
            var producerSvc = new Mock<IProducerService<string, byte[]>>();

            Assert.Throws<ArgumentNullException>(() =>
            {
                var mqttKafkaBridge = new MqttKafkaBridge(
                    mockS3Svc.Object,
                    mockMqttSvc.Object,
                    producerSvc.Object,
                    null
                );
            });
        }

        [Fact]
        public async Task MqttKafkaBridge_ExecuteAsync_Triggers_IMqttService_StartAsync_And_StopAsync()
        {
            var mockMqttSvc = new Mock<IMqttService>();
            var mockS3Svc = new Mock<IS3Service>();
            var producerSvc = new Mock<IProducerService<string, byte[]>>();
            var logger = new NullLogger<MqttKafkaBridge>();
            var cancellationToken = new CancellationTokenSource(1000);

            var mqttKafkaBridge = new MqttKafkaBridge(
                mockS3Svc.Object,
                mockMqttSvc.Object,
                producerSvc.Object,
                logger
            );

            await mqttKafkaBridge.StartAsync(CancellationToken.None);
            await Task.Delay(10);
            await mqttKafkaBridge.StopAsync(CancellationToken.None);

            mockMqttSvc.Verify(x => x.StartAsync());
            mockMqttSvc.Verify(x => x.StopAsync());
        }

        [Fact]
        public async Task MqttKafkaBridge_OnMqttMessageReceivedAsync_Transforms_Message_To_Link_To_Uploaded_Image()
        {
            const string clientId = "clientID";
            const string topic = "Topic";

            var message = Mock.Of<MqttApplicationMessage>(x =>
                                x.Topic == topic && x.Payload == _Payload);
            var mqttSvc = Mock.Of<IMqttService>();
            var producerSvc = Mock.Of<IProducerService<string, byte[]>>();
            var s3Svc = Mock.Of<IS3Service>();
            var logger = new NullLogger<MqttKafkaBridge>();
            var cancellationToken = new CancellationTokenSource(1000);
            var eventArgs = new Mock<MqttApplicationMessageReceivedEventArgs>(clientId, message);

            var s3Mock = Mock.Get<IS3Service>(s3Svc);

            var mqttKafkaBridge = new MqttKafkaBridge(s3Svc, mqttSvc, producerSvc, logger);
            await mqttKafkaBridge.OnMqttMessageReceivedAsync(eventArgs.Object);

            s3Mock.Verify(x => x.UploadImage(
                Encoding.UTF8.GetString(message.Payload),
                It.IsAny<string>(), // guid
                DefaultBufferSize),
                Times.Once()
            );
        }

        [Fact]
        public async Task MqttKafkaBridge_OnMqttMessageReceivedAsync_Transforms_Message_To_Kafka_Payload()
        {
            const string ClientId = "clientID";
            const string Topic = "Topic";

            var message = Mock.Of<MqttApplicationMessage>(x =>
                                x.Topic == Topic && x.Payload == _Payload);
            var mqttSvc = Mock.Of<IMqttService>();
            var producerSvc = Mock.Of<IProducerService<string, byte[]>>();
            var s3Svc = Mock.Of<IS3Service>();
            var logger = new NullLogger<MqttKafkaBridge>();
            var cancellationToken = new CancellationTokenSource(1000);
            var eventArgs = new Mock<MqttApplicationMessageReceivedEventArgs>(ClientId, message);

            var s3Mock = Mock.Get<IS3Service>(s3Svc);
            s3Mock.Setup(x => x.UploadImage(
                Encoding.UTF8.GetString(message.Payload),
                It.IsAny<string>(),
                DefaultBufferSize
            )).ReturnsAsync(_UploadedImagePayload).Verifiable();

            var producerMock = Mock.Get<IProducerService<string, byte[]>>(producerSvc);
            producerMock.Setup(x => x.PrepareMessage(_UploadedImagePayload, SchemaId, DefaultBufferSize)).Returns(_KafkaPayload);

            var mqttKafkaBridge = new MqttKafkaBridge(s3Svc, mqttSvc, producerSvc, logger);
            await mqttKafkaBridge.OnMqttMessageReceivedAsync(eventArgs.Object);

            await Task.Run(() => producerMock.Verify(x => x.PrepareMessage(_UploadedImagePayload, SchemaId, DefaultBufferSize), Times.Once()));
        }


        [Fact]
        public async Task MqttKafkaBridge_OnMqttMessageReceivedAsync_Produces_To_Kafka_Topic()
        {
            const string ClientId = "clientID";
            const int FlushTime = 10;
            const string Topic = "Topic";

            var message = Mock.Of<MqttApplicationMessage>(x =>
                                x.Topic == Topic && x.Payload == _Payload);
            var mqttSvc = Mock.Of<IMqttService>();
            var producerSvc = Mock.Of<IProducerService<string, byte[]>>();
            var s3Svc = Mock.Of<IS3Service>();
            var logger = new NullLogger<MqttKafkaBridge>();
            var cancellationToken = new CancellationTokenSource(1000);
            var eventArgs = new Mock<MqttApplicationMessageReceivedEventArgs>(ClientId, message);

            var s3Mock = Mock.Get<IS3Service>(s3Svc);
            s3Mock.Setup(x => x.UploadImage(
                Encoding.UTF8.GetString(message.Payload),
                It.IsAny<string>(),
                DefaultBufferSize
            )).ReturnsAsync(_UploadedImagePayload).Verifiable();

            var producerMock = Mock.Get<IProducerService<string, byte[]>>(producerSvc);
            producerMock.Setup(x => x.PrepareMessage(_UploadedImagePayload, SchemaId, DefaultBufferSize)).Returns(_KafkaPayload);

            var mqttKafkaBridge = new MqttKafkaBridge(s3Svc, mqttSvc, producerSvc, logger);
            await mqttKafkaBridge.OnMqttMessageReceivedAsync(eventArgs.Object);

            await Task.Run(() => producerMock.Verify(x => x.Produce(Topic, _KafkaPayload, FlushTime), Times.Once()));
        }

        [Fact]
        public async Task MqttKafkaBridge_OnMqttMessageReceivedAsync_Takes_No_Action_When_Topic_Null()
        {
            const int BufferSize = 1024;
            const string ClientId = "clientID";
            const int FlushTime = 10;
            const string Topic = null;

            var message = Mock.Of<MqttApplicationMessage>(x =>
                                x.Topic == Topic && x.Payload == _Payload);
            var mqttSvc = Mock.Of<IMqttService>();
            var producerSvc = Mock.Of<IProducerService<string, byte[]>>();
            var s3Svc = Mock.Of<IS3Service>();
            var logger = new NullLogger<MqttKafkaBridge>();
            var cancellationToken = new CancellationTokenSource(1000);
            var eventArgs = new Mock<MqttApplicationMessageReceivedEventArgs>(ClientId, message);

            var s3Mock = Mock.Get<IS3Service>(s3Svc);
            s3Mock.Setup(x => x.UploadImage(
                Encoding.UTF8.GetString(message.Payload),
                It.IsAny<string>(),
                BufferSize
            )).ReturnsAsync(_UploadedImagePayload).Verifiable();

            var producerMock = Mock.Get<IProducerService<string, byte[]>>(producerSvc);
            producerMock.Setup(x => x.PrepareMessage(_UploadedImagePayload, SchemaId, 1024)).Returns(_KafkaPayload);

            var mqttKafkaBridge = new MqttKafkaBridge(s3Svc, mqttSvc, producerSvc, logger);
            await mqttKafkaBridge.OnMqttMessageReceivedAsync(eventArgs.Object);

            s3Mock.Verify(x => x.UploadImage(Encoding.UTF8.GetString(_Payload), It.IsAny<string>(), DefaultBufferSize), Times.Never());
            producerMock.Verify(x => x.PrepareMessage(_UploadedImagePayload, SchemaId, DefaultBufferSize), Times.Never());
            producerMock.Verify(x => x.Produce(Topic, _KafkaPayload, FlushTime), Times.Never());
        }

        [Fact]
        public async Task MqttKafkaBridge_OnMqttMessageReceivedAsync_Takes_No_Action_When_Payload_Null()
        {
            const int BufferSize = 1024;
            const string ClientId = "clientID";
            const int FlushTime = 10;
            const string Topic = "Topic";

            var message = Mock.Of<MqttApplicationMessage>(x =>
                                x.Topic == Topic && x.Payload == null);
            var mqttSvc = Mock.Of<IMqttService>();
            var producerSvc = Mock.Of<IProducerService<string, byte[]>>();
            var s3Svc = Mock.Of<IS3Service>();
            var logger = new NullLogger<MqttKafkaBridge>();
            var cancellationToken = new CancellationTokenSource(1000);
            var eventArgs = new Mock<MqttApplicationMessageReceivedEventArgs>(ClientId, message);

            var s3Mock = Mock.Get<IS3Service>(s3Svc);
            s3Mock.Setup(x => x.UploadImage(
                It.IsAny<string>(),
                It.IsAny<string>(),
                BufferSize
            ));

            var producerMock = Mock.Get<IProducerService<string, byte[]>>(producerSvc);
            producerMock.Setup(x => x.PrepareMessage(It.IsAny<byte[]>(), SchemaId, DefaultBufferSize));

            var mqttKafkaBridge = new MqttKafkaBridge(s3Svc, mqttSvc, producerSvc, logger);
            await mqttKafkaBridge.OnMqttMessageReceivedAsync(eventArgs.Object);

            s3Mock.Verify(x => x.UploadImage(It.IsAny<string>(), It.IsAny<string>(), DefaultBufferSize), Times.Never());
            producerMock.Verify(x => x.PrepareMessage(It.IsAny<byte[]>(), SchemaId, DefaultBufferSize), Times.Never());
            producerMock.Verify(x => x.Produce(Topic, It.IsAny<byte[]>(), FlushTime), Times.Never());
        }

        [Fact]
        public async Task MqttKafkaBridge_OnMqttMessageReceivedAsync_Takes_No_Action_When_Payload_Empty()
        {
            const int BufferSize = 1024;
            const string ClientId = "clientID";
            const int FlushTime = 10;
            const string Topic = "Topic";

            var message = Mock.Of<MqttApplicationMessage>(x =>
                                x.Topic == Topic && x.Payload == new byte[0]);
            var mqttSvc = Mock.Of<IMqttService>();
            var producerSvc = Mock.Of<IProducerService<string, byte[]>>();
            var s3Svc = Mock.Of<IS3Service>();
            var logger = new NullLogger<MqttKafkaBridge>();
            var cancellationToken = new CancellationTokenSource(1000);
            var eventArgs = new Mock<MqttApplicationMessageReceivedEventArgs>(ClientId, message);

            var s3Mock = Mock.Get<IS3Service>(s3Svc);
            s3Mock.Setup(x => x.UploadImage(
                It.IsAny<string>(),
                It.IsAny<string>(),
                BufferSize
            ));

            var producerMock = Mock.Get<IProducerService<string, byte[]>>(producerSvc);
            producerMock.Setup(x => x.PrepareMessage(It.IsAny<byte[]>(), SchemaId, DefaultBufferSize));

            var mqttKafkaBridge = new MqttKafkaBridge(s3Svc, mqttSvc, producerSvc, logger);
            await mqttKafkaBridge.OnMqttMessageReceivedAsync(eventArgs.Object);

            s3Mock.Verify(x => x.UploadImage(It.IsAny<string>(), It.IsAny<string>(), DefaultBufferSize), Times.Never());
            producerMock.Verify(x => x.PrepareMessage(It.IsAny<byte[]>(), SchemaId, DefaultBufferSize), Times.Never());
            producerMock.Verify(x => x.Produce(Topic, It.IsAny<byte[]>(), FlushTime), Times.Never());
        }

        [Fact]
        public async Task MqttKafkaBridge_OnMqttMessageReceivedAsync_Throws_Exception()
        {
            const int BufferSize = 1024;
            const string ClientId = "ClientID";
            const string Topic = "Topic";

            var message = Mock.Of<MqttApplicationMessage>(x =>
                                x.Topic == Topic && x.Payload == _Payload);
            var mqttSvc = Mock.Of<IMqttService>();
            var producerSvc = Mock.Of<IProducerService<string, byte[]>>();
            var s3Svc = Mock.Of<IS3Service>();
            var eventArgs = new Mock<MqttApplicationMessageReceivedEventArgs>(ClientId, message);
            var logger = new NullLogger<MqttKafkaBridge>();

            var s3Mock = Mock.Get<IS3Service>(s3Svc);
            s3Mock.Setup(x => x.UploadImage(
                It.IsAny<string>(),
                It.IsAny<string>(),
                BufferSize
            )).Throws(new ArgumentNullException());

            var mqttKafkaBridge = new MqttKafkaBridge(s3Svc, mqttSvc, producerSvc, logger);

            Task task = mqttKafkaBridge.OnMqttMessageReceivedAsync(eventArgs.Object);
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await task);
        }

        [Fact]
        public void MqttKafkaBridge_Dispose_Disposes_Mqtt_Service()
        {
            const string Topic = "Topic";

            var message = Mock.Of<MqttApplicationMessage>(x =>
                                x.Topic == Topic && x.Payload == new byte[0]);
            var mqttSvc = Mock.Of<IMqttService>();
            var producerSvc = Mock.Of<IProducerService<string, byte[]>>();
            var s3Svc = Mock.Of<IS3Service>();
            var logger = new NullLogger<MqttKafkaBridge>();

            var mqttMock = Mock.Get<IMqttService>(mqttSvc);
            mqttMock.Setup(x => x.Dispose()).Verifiable();

            var mqttKafkaBridge = new MqttKafkaBridge(s3Svc, mqttSvc, producerSvc, logger);
            mqttKafkaBridge.Dispose();

            mqttMock.Verify();
        }

        /// <summary>
        /// Transform payload to Kafka format, containing magic prepend and schema id
        /// </summary>
        /// <param name="payload">Message body</param>
        /// <param name="schemaId">Schema identifer for validating message from schema registry</param>
        /// <returns>Transformed message represented as bytes</returns>
        private byte[] ToKafkaPayload(byte[] payload, short schemaId)
        {
            const int initialBufferSize = 1024;
            const byte magicByte = 0;

            using (MemoryStream stream = new MemoryStream(initialBufferSize))
            using (BinaryWriter writer = new BinaryWriter(stream, Encoding.UTF8))
            {
                writer.Write(magicByte);
                writer.Write(IPAddress.HostToNetworkOrder(schemaId));
                writer.Write(_Payload);

                writer.Flush();

                var message = stream.ToArray();

                return message;
            }
        }
    }
}

