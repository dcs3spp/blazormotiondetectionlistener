using System.Collections.Generic;
using System.IO;
using System.Text.Json;

using Microsoft.Extensions.Logging;

using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Json;
using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;


using WebApp.Testing.Fixtures;


namespace WebApp.Data.Serializers.IntegrationTests
{
    [Collection("Json collection")]
    public class MotionDetectionConverterTest
    {
        private const string MotionResourceFile = @"WebApp.Testing.Utils.TestData.motion.json";

        private EmbeddedJsonFileResources _fixture;
        private ITestOutputHelper _output;

        public MotionDetectionConverterTest(EmbeddedJsonFileResources fixture, ITestOutputHelper output)
        {
            _fixture = fixture;
            _output = output;
        }


        [Fact]
        public void MotionDetectionConverter_Deserializes()
        {
            MotionDetection expectedMotionDetection = CreateMotionDetection();
            JsonSerializerWrapper jsonSerializerWrapper = new JsonSerializerWrapper();

            using var logFactory = LoggerFactory.Create(builder => builder.AddConsole());
            var logger = logFactory.CreateLogger<MotionDetectionConverter>();
            var loggerInfo = logFactory.CreateLogger<MotionInfoConverter>();
            var loggerVisitor = logFactory.CreateLogger<JsonVisitor>();

            var parsedModel = new ParsedMotionDetection();
            var parsedInfoModel = new ParsedMotionInfo();
            var parser = new JsonParser();
            var visitor = new JsonVisitor(jsonSerializerWrapper, parser, loggerVisitor);
            var converter = new MotionDetectionConverter(visitor, parsedModel, logger);

            JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            serializeOptions.Converters.Add(converter);
            serializeOptions.Converters.Add(new MotionInfoConverter(visitor, parsedInfoModel, loggerInfo));

            using (Stream stream = _fixture.GetStream(MotionResourceFile))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    stream.CopyTo(m);
                    byte[] bytes = m.ToArray();

                    Utf8JsonReader reader = new Utf8JsonReader(bytes, true, new JsonReaderState());
                    reader.Read();
                    MotionDetection motion = converter.Read(ref reader, typeof(MotionDetection), serializeOptions);

                    Assert.Equal(expectedMotionDetection, motion);
                }
            }
        }

        private MotionDetection CreateMotionDetection()
        {
            const long Time = 820;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo expectedInfo = new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "YmFzZTY0", 64, 48, Time);

            return new MotionDetection("myGroup", Time, "myMonitorId", Plugin, expectedInfo);
        }
    }
}