using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging.Abstractions;

using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Data.Serializers.Json;
using WebApp.Testing.Fixtures;


namespace WebApp.Data.Serializer.IntegrationTests
{
    [Collection("Json collection")]
    public class JsonSerializerWrapperTest
    {
        private EmbeddedJsonFileResources _fixture;
        private ITestOutputHelper _output;


        public JsonSerializerWrapperTest(EmbeddedJsonFileResources fixture, ITestOutputHelper output)
        {
            _fixture = fixture;
            _output = output;
        }

        private const string MotionResourceFile = @"WebApp.Testing.Utils.TestData.motioninfo.json";

        [Fact]
        public async Task JsonSerializerWrapper_DeserializeAsync_Deserializes()
        {
            ParsedMotionInfo parsedInfoModel = new ParsedMotionInfo();
            JsonParser parser = new JsonParser();
            JsonVisitor visitor = new JsonVisitor(new JsonSerializerWrapper(), parser, new NullLogger<JsonVisitor>());

            MotionInfoConverter converter = new MotionInfoConverter(visitor, parsedInfoModel, new NullLogger<MotionInfoConverter>());

            JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            serializeOptions.Converters.Add(converter);

            JsonSerializerWrapper serializer = new JsonSerializerWrapper();

            MotionInfo expectedInfo = CreateMotionInfo();
            MotionInfo actualInfo = null;

            using (Stream stream = _fixture.GetStream(MotionResourceFile))
            {
                actualInfo = await serializer.DeserializeAsync<MotionInfo>(stream, serializeOptions);
            }

            Assert.Equal(expectedInfo, actualInfo);
        }


        [Fact]
        public void JsonSerializerWrapper_Deserialize_From_Reader_Deserializes()
        {
            ParsedMotionInfo parsedInfoModel = new ParsedMotionInfo();
            JsonParser parser = new JsonParser();
            JsonVisitor visitor = new JsonVisitor(new JsonSerializerWrapper(), parser, new NullLogger<JsonVisitor>());

            MotionInfoConverter converter = new MotionInfoConverter(visitor, parsedInfoModel, new NullLogger<MotionInfoConverter>());

            JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            serializeOptions.Converters.Add(converter);

            JsonSerializerWrapper serializer = new JsonSerializerWrapper();

            MotionInfo expectedInfo = CreateMotionInfo();
            MotionInfo actualInfo = null;

            using (Stream stream = _fixture.GetStream(MotionResourceFile))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    stream.CopyTo(m);
                    byte[] bytes = m.ToArray();

                    Utf8JsonReader reader = new Utf8JsonReader(bytes);

                    actualInfo = serializer.Deserialize<MotionInfo>(ref reader, serializeOptions);
                }
            }

            Assert.Equal(expectedInfo, actualInfo);
        }


        [Fact]
        public void JsonSerializerWrapper_Deserialize_From_String_Deserializes()
        {
            ParsedMotionInfo parsedInfoModel = new ParsedMotionInfo();
            JsonParser parser = new JsonParser();
            JsonVisitor visitor = new JsonVisitor(new JsonSerializerWrapper(), parser, new NullLogger<JsonVisitor>());

            MotionInfoConverter converter = new MotionInfoConverter(visitor, parsedInfoModel, new NullLogger<MotionInfoConverter>());

            JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            serializeOptions.Converters.Add(converter);

            JsonSerializerWrapper serializer = new JsonSerializerWrapper();

            MotionInfo expectedInfo = CreateMotionInfo();
            MotionInfo actualInfo = null;

            using (Stream stream = _fixture.GetStream(MotionResourceFile))
            {
                using (StreamReader m = new StreamReader(stream))
                {
                    string json = m.ReadToEnd();
                    actualInfo = serializer.Deserialize<MotionInfo>(json, serializeOptions);
                }
            }

            Assert.Equal(expectedInfo, actualInfo);
        }

        private MotionInfo CreateMotionInfo()
        {
            const long Time = 820;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            return new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "base64", 64, 48, Time);
        }
    }
}
