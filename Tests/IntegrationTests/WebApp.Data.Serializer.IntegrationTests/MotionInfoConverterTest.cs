using System.Collections.Generic;
using System.IO;
using System.Text.Json;

using Microsoft.Extensions.Logging;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Json;
using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;

using WebApp.Testing.Fixtures;


namespace WebApp.Data.Serializers.IntegrationTests
{
    [Collection("Json collection")]
    public class MotionInfoConverterTest
    {
        private const string MotionInfoResourceFile = @"WebApp.Testing.Utils.TestData.motioninfo.json";

        private EmbeddedJsonFileResources _fixture;
        private ITestOutputHelper _output;

        public MotionInfoConverterTest(EmbeddedJsonFileResources fixture, ITestOutputHelper output)
        {
            _fixture = fixture;
            _output = output;
        }

        [Fact]
        public void MotionInfoConverter_Deserializes()
        {
            MotionInfo expectedMotionInfo = CreateMotionInfo();

            using var logFactory = LoggerFactory.Create(builder => builder.AddConsole());
            var logger = logFactory.CreateLogger<MotionInfoConverter>();
            var loggerVisitor = logFactory.CreateLogger<JsonVisitor>();
            var parser = new JsonParser();
            var serializationWrapper = new JsonSerializerWrapper();

            var visitor = new JsonVisitor(serializationWrapper, parser, loggerVisitor);
            var parsedInfoModel = new ParsedMotionInfo();

            var converter = new MotionInfoConverter(visitor, parsedInfoModel, logger);

            JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            serializeOptions.Converters.Add(converter);

            using (Stream stream = _fixture.GetStream(MotionInfoResourceFile))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    stream.CopyTo(m);
                    byte[] bytes = m.ToArray();

                    Utf8JsonReader reader = new Utf8JsonReader(bytes, true, new JsonReaderState());
                    reader.Read();

                    MotionInfo actual = converter.Read(ref reader, typeof(MotionInfo), serializeOptions);

                    Assert.Equal(expectedMotionInfo, actual);
                }
            }
        }

        private MotionInfo CreateMotionInfo()
        {
            const long Time = 820;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            return new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "base64", 64, 48, Time);
        }
    }
}