using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

using Microsoft.Extensions.Logging.Abstractions;

using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Data.Serializers.Json;
using WebApp.Testing.Fixtures;

using WebApp.Testing.Mocks.Serializers.Converters.Visitors;


namespace WebApp.Data.Serializer.IntegrationTests
{
    [Collection("Json collection")]
    public class JsonVisitorTest
    {
        private EmbeddedJsonFileResources _fixture;
        private ITestOutputHelper _output;

        public JsonVisitorTest(EmbeddedJsonFileResources fixture, ITestOutputHelper output)
        {
            _fixture = fixture;
            _output = output;
        }

        private const string MotionResourceFile = @"WebApp.Testing.Utils.TestData.motioninfo.json";
        private const string InvalidMotionInfo = @"WebApp.Testing.Utils.TestData.motioninfo_extraAttribute.json";

        [Fact]
        public void JsonVisitor_Deserialize_MotionInfo()
        {
            MotionInfo expectedInfo = CreateMotionInfo();

            JsonSerializerWrapper serializer = new JsonSerializerWrapper();
            JsonParser parser = new JsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            JsonSerializerOptions options = new JsonSerializerOptions();

            ParsedMotionInfo actualInfo = new ParsedMotionInfo();

            using (Stream stream = _fixture.GetStream(MotionResourceFile))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    stream.CopyTo(m);
                    byte[] bytes = m.ToArray();

                    Utf8JsonReader reader = new Utf8JsonReader(bytes, true, new JsonReaderState());
                    reader.Read();

                    visitor.DeserializeMotionInfo(actualInfo, ref reader, options);
                }
            }

            MotionInfo actual = actualInfo.ToModel();

            Assert.Equal(expectedInfo, actual);
        }

        [Fact]
        public void JsonVisitor_DeserializeMotionInfo_Throws_Exception_When_Fails_To_Encounter_Expected_EndObject_Token()
        {
            MotionInfo expectedInfo = CreateMotionInfo();
            bool exception = false;

            JsonSerializerWrapper serializer = new JsonSerializerWrapper();
            JsonParser parser = new JsonParser();
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parser, logger);

            JsonSerializerOptions options = new JsonSerializerOptions();

            ParsedMotionInfo actualInfo = new ParsedMotionInfo();

            using (Stream stream = _fixture.GetStream(InvalidMotionInfo))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    stream.CopyTo(m);
                    byte[] bytes = m.ToArray();

                    Utf8JsonReader reader = new Utf8JsonReader(bytes, true, new JsonReaderState());
                    reader.Read();

                    try
                    {
                        visitor.DeserializeMotionInfo(actualInfo, ref reader, options);
                    }
                    catch (NotSupportedException)
                    {
                        exception = true;
                    }
                }
            }

            Assert.True(exception);
        }

        [Fact]
        public void JsonVisitor_DeserializeMotionInfo_Reads_Expected_Properties()
        {
            MotionInfo expectedInfo = CreateMotionInfo();

            JsonSerializerWrapper serializer = new JsonSerializerWrapper();
            MockJsonParser parserMock = new MockJsonParser(new JsonParser());
            NullLogger<JsonVisitor> logger = new NullLogger<JsonVisitor>();
            JsonVisitor visitor = new JsonVisitor(serializer, parserMock, logger);

            JsonSerializerOptions options = new JsonSerializerOptions();

            ParsedMotionInfo actualInfo = new ParsedMotionInfo();

            using (Stream stream = _fixture.GetStream(MotionResourceFile))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    stream.CopyTo(m);
                    byte[] bytes = m.ToArray();
                    this._output.WriteLine(System.Text.Encoding.UTF8.GetString(bytes));

                    Utf8JsonReader reader = new Utf8JsonReader(bytes, true, new JsonReaderState());
                    reader.Read();

                    visitor.DeserializeMotionInfo(actualInfo, ref reader, options);
                }
            }

            // test expected properties have been set for info and location
            var expectedStringProperties = new HashSet<string> {
                    MotionInfoPropertyNames.Img,
                    MotionInfoPropertyNames.Name,
                    MotionInfoPropertyNames.Plug,
                    MotionLocationPropertyNames.Tag,
                    MotionInfoPropertyNames.Reason
                };

            var expectedLongProperties = new HashSet<string> {
                    MotionInfoPropertyNames.Time
                };

            var expectedDoubleProperties = new HashSet<string> {
                    MotionLocationPropertyNames.Confidence,
                    MotionLocationPropertyNames.Height,
                    MotionLocationPropertyNames.Width,
                    MotionLocationPropertyNames.X,
                    MotionLocationPropertyNames.Y
                };

            var expectedIntProperties = new HashSet<string> {
                    MotionInfoPropertyNames.ImgWidth,
                    MotionInfoPropertyNames.ImgHeight
                };

            var expectedProperties = new HashSet<string> {
                    MotionInfoPropertyNames.Matrices
            };

            Assert.Equal(expectedStringProperties, parserMock.StringProperties);
            Assert.Equal(expectedLongProperties, parserMock.LongProperties);
            Assert.Equal(expectedIntProperties, parserMock.IntProperties);
            Assert.Equal(expectedProperties, parserMock.ReadProperties);
        }

        private MotionInfo CreateMotionInfo()
        {
            const long Time = 820;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            return new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "base64", 64, 48, Time);
        }
    }
}
