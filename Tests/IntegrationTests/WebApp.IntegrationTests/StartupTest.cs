using System;
using System.Collections.Generic;

using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Xunit;

using WebApp.BackgroundServices;
using WebApp.Kafka;


namespace WebApp.IntegrationTests
{
    public class StartupTest
    {
        [Fact]
        public void Startup_ConfigureServices_Resolves_MqttKafkaBroker()
        {
            const string file = "appsettings.test.json";

            var config = ReadConfiguration(file);
            var services = new ServiceCollection();
            var startup = new Startup(config);

            startup.ConfigureServices(services);

            ContainerBuilder containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            startup.ConfigureContainer(containerBuilder);

            using (var container = containerBuilder.Build())
            using (var scope = container.BeginLifetimeScope())
            {
                var matches = scope.Resolve<IEnumerable<IHostedService>>();

                Assert.True(ContainsService(matches, typeof(MqttKafkaBridge)));
            }
        }

        [Fact]
        public void Startup_ConfigureServices_Resolves_KafkaAdmin()
        {
            const string file = "appsettings.test.json";

            var config = ReadConfiguration(file);
            var services = new ServiceCollection();
            var startup = new Startup(config);

            startup.ConfigureServices(services);

            ContainerBuilder containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            startup.ConfigureContainer(containerBuilder);

            using (var container = containerBuilder.Build())
            using (var scope = container.BeginLifetimeScope())
            {
                var matches = scope.Resolve<IEnumerable<IHostedService>>();

                Assert.True(ContainsService(matches, typeof(MqttKafkaBridge)));
            }
        }

        [Fact]
        public void Startup_ConfigureServices_Resolves_ConsumerService()
        {
            const string file = "appsettings.test.json";

            var config = ReadConfiguration(file);
            var services = new ServiceCollection();
            var startup = new Startup(config);

            startup.ConfigureServices(services);

            ContainerBuilder containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            startup.ConfigureContainer(containerBuilder);

            using (var container = containerBuilder.Build())
            using (var scope = container.BeginLifetimeScope())
            {
                var matches = scope.Resolve<IEnumerable<IHostedService>>();

                Assert.True(ContainsService(matches, typeof(ConsumerService)));
            }
        }


        /// <summary>Read configuration from json settings file</summary>
        /// <param name="filePath">Path to json file</param>
        /// <returns><see cref="IConfiguration"/> derived from configuration</returns>
        private IConfiguration ReadConfiguration(string filePath)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile(filePath)
                .Build();

            return config;
        }

        /// <summary>Check if a type of service is contained within a list</summary>
        /// <param name="lst">List containing <see cref="IHostedService"/> instances</param>
        /// <param name="svc">The type of service to search for</param>
        /// <returns>True if the service is contained in the list, otherwise false</returns>
        private bool ContainsService(IEnumerable<IHostedService> lst, Type svc)
        {
            bool found = false;

            var cursor = lst.GetEnumerator();

            while (cursor.MoveNext() && !found)
            {
                found = cursor.Current.GetType() == svc;
            }

            return found;
        }
    }
}
