using System.IO;
using System.Reflection;

using Autofac;
using Microsoft.Extensions.Options;
using Xunit;
using Xunit.Abstractions;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Contracts;
using WebApp.Testing.Fixtures;


namespace WebApp.Mqtt.AutofacModule.IntegrationTests
{
    public class MqttModuleTest
    {
        private readonly string _configPath;
        private ITestOutputHelper _output { get; set; }


        public MqttModuleTest(ITestOutputHelper output)
        {
            _output = output;
            _configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        [Fact]
        public void MqttModule_Resolves_MQTTClientFactory()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new MqttModule()
            }))
            {

                Assert.True(container.IsRegistered(typeof(IMQTTClientFactory)));

                using (var scope = container.BeginLifetimeScope())
                {
                    IMQTTClientFactory factory = scope.Resolve<IMQTTClientFactory>();
                    Assert.True(factory != null);
                }
            }
        }

        [Fact]
        public void MqttModule_Resolves_MqttService()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new MqttModule()
            }))
            {

                Assert.True(container.IsRegistered(typeof(IMqttService)));

                using (var scope = container.BeginLifetimeScope())
                {
                    IMqttService consumer = scope.Resolve<IMqttService>();
                    Assert.True(consumer != null);
                }
            }
        }

        [Fact]
        public void MqttModule_Resolves_Validation()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new MqttModule()
            }))
            {

                Assert.True(container.IsRegistered(typeof(IValidateOptions<MqttConfig>)));

                using (var scope = container.BeginLifetimeScope())
                {
                    IValidateOptions<MqttConfig> validator = scope.Resolve<IValidateOptions<MqttConfig>>();
                    Assert.True(validator != null);
                }
            }
        }
    }
}
