using System.IO;
using System.Reflection;

using Autofac;
using Microsoft.Extensions.Options;
using Xunit;
using Xunit.Abstractions;

using WebApp.S3.Config;
using WebApp.S3.Contracts;
using WebApp.Testing.Fixtures;


namespace WebApp.S3.AutofacModule.IntegrationTests
{
    public class S3ModuleTest
    {
        private readonly string _configPath;
        private ITestOutputHelper _output { get; set; }


        public S3ModuleTest(ITestOutputHelper output)
        {
            _output = output;
            _configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }


        [Fact]
        public void S3Module_Resolves_S3Service()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);

            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new S3Module()
            }))
            {
                Assert.True(container.IsRegistered(typeof(IS3Service)));

                using (var scope = container.BeginLifetimeScope())
                {
                    IS3Service s3Service = scope.Resolve<IS3Service>();
                    IS3Service s3Service2 = scope.Resolve<IS3Service>();

                    Assert.NotNull(s3Service);
                    Assert.Same(s3Service, s3Service2);
                }
            }
        }

        [Fact]
        public void S3Module_Resolves_Validation()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new S3Module()
            }))
            {
                Assert.True(container.IsRegistered(typeof(IValidateOptions<S3Config>)));

                using (var scope = container.BeginLifetimeScope())
                {
                    IValidateOptions<S3Config> validation = scope.Resolve<IValidateOptions<S3Config>>();
                    Assert.True(validation != null);
                }
            }
        }
    }
}
