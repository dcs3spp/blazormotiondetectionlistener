using System.IO;
using System.Reflection;

using Autofac;
using Confluent.Kafka;
using Microsoft.Extensions.Options;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.AutofacModule;
using WebApp.Kafka.Config;
using WebApp.Kafka.Contracts;
using WebApp.Kafka.ProducerProvider;
using WebApp.Kafka.SchemaRegistry.Serdes;
using WebApp.Testing.Fixtures;


namespace WebApp.Kafka.AutofacModule.UnitTests
{
    public class KafkaModuleTest
    {
        /// <summary>Helper provided by Xunit for writing to console, if needed</summary>
        private ITestOutputHelper _output { get; set; }

        ///<summary>Path for appsettings.json config file</summary>
        private readonly string _configPath;


        /// <summary>Initiase config file and Xunit console writer</summary>
        public KafkaModuleTest(ITestOutputHelper output)
        {
            _output = output;
            _configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        [Fact]
        public void KafkaModule_Resolves_ConsumerService()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);

            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new SerializerModule(),
                new KafkaModule()
            }))
            {
                Assert.True(container.IsRegistered(typeof(ConsumerService)));

                using (var scope = container.BeginLifetimeScope())
                {
                    ConsumerService consumer = scope.Resolve<ConsumerService>();
                    Assert.True(consumer != null);
                }
            }
        }

        [Fact]
        public void KafkaModule_Resolves_ProducerService()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] { new KafkaModule() }))
            {

                Assert.True(container.IsRegistered(typeof(IProducerService<string, byte[]>)));
                using (var scope = container.BeginLifetimeScope())
                {
                    IProducerService<string, byte[]> producer = scope.Resolve<IProducerService<string, byte[]>>();
                    Assert.True(producer != null);
                }
            }
        }

        [Fact]
        public void KafkaModule_Resolves_KafkaProducerProvider()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] { new KafkaModule() }))
            {

                Assert.True(container.IsRegistered(typeof(IKafkaProducerProvider)));
                using (var scope = container.BeginLifetimeScope())
                {
                    IKafkaProducerProvider producer = scope.Resolve<IKafkaProducerProvider>();
                    Assert.True(producer != null);
                }
            }
        }

        [Fact]
        public void KafkaModule_Resolves_JsonDeserializer()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new KafkaModule(),
                new SerializerModule()
            }))
            {

                Assert.True(container.IsRegistered(typeof(IAsyncDeserializer<string>)));
                using (var scope = container.BeginLifetimeScope())
                {
                    IAsyncDeserializer<string> deserializer = scope.Resolve<IAsyncDeserializer<string>>();
                    Assert.True(deserializer != null);
                    Assert.Equal(typeof(JsonDeserializer<string>), deserializer.GetType());
                }
            }
        }

        [Fact]
        public void KafkaModule_Resolve_Validator()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new KafkaModule()
            }))
            {

                Assert.True(container.IsRegistered(typeof(IValidateOptions<KafkaConfig>)));

                using (var scope = container.BeginLifetimeScope())
                {
                    IValidateOptions<KafkaConfig> validator = scope.Resolve<IValidateOptions<KafkaConfig>>();
                    Assert.True(validator != null);
                }
            }
        }

        [Fact]
        public void KafkaModule_Resolve_ProducerService()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);

            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new KafkaModule()
            }))
            {
                Assert.True(container.IsRegistered(typeof(IProducerService<string, byte[]>)));

                using (var scope = container.BeginLifetimeScope())
                {
                    var svc = scope.Resolve<IProducerService<string, byte[]>>();

                    Assert.True(svc != null);
                    Assert.Equal(typeof(ProducerService<string, byte[]>), svc.GetType());
                }
            }
        }

        [Fact]
        public void KafkaModule_Resolve_ConsumerFactory_Resolves_As_Singleton()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);

            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new KafkaModule()
            }))
            {
                Assert.True(container.IsRegistered(typeof(ConsumerFactory)));

                using (var scope = container.BeginLifetimeScope())
                {
                    var factory = scope.Resolve<ConsumerFactory>();
                    var factory2 = scope.Resolve<ConsumerFactory>();

                    Assert.Same(factory, factory2);
                }
            }
        }
    }
}
