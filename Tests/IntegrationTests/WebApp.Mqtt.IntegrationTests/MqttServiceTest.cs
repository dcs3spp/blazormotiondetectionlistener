using System.IO;
using System.Reflection;

using Autofac;
using Xunit;
using Xunit.Abstractions;

using WebApp.Mqtt.Contracts;
using WebApp.Mqtt.AutofacModule;
using WebApp.Testing.Fixtures;


namespace WebApp.Mqtt.IntegrationTests
{
    public class MqttServiceTest
    {
        private readonly string _configPath;
        private ITestOutputHelper _output { get; set; }


        public MqttServiceTest(ITestOutputHelper output)
        {
            _output = output;
            _configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }


        [Fact]
        public void MqttService_Constructor_Resolves_Dependencies()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new MqttModule()
            }))
            {

                using (var scope = container.BeginLifetimeScope())
                {
                    var service = scope.Resolve<IMqttService>();

                    Assert.NotNull(service);
                }
            }
        }
    }
}
