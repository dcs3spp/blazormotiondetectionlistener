using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Xunit;

using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Factory;
using WebApp.Mqtt.Testing.Helpers;


namespace WebApp.Mqtt
{
    public class MQTTnetManagedClientTlsOptionsBuilderTest
    {
        [Fact]
        public void MQTTnetManagedClientTlsOptionsBuilder_Builds_Options_Derived_From_Config()
        {
            const string certPath = "Certs/digiCert.pem";

            var builder = new MQTTnetManagedClientTlsOptionsBuilder();
            var config = MqttTestUtils.CreateMqttConfigWithTLS(certPath);

            MqttClientTlsOptions actualTls = null;
            MqttClientTlsOptions expectedTls = null;

            try
            {
                var expectedOptions = MqttTestUtils.ToManagedClientOptions(config);
                expectedTls = expectedOptions.ClientOptions.ChannelOptions.TlsOptions;

                var options = builder.WithOptions(
                    config,
                    expectedOptions.ClientOptions.ChannelOptions.TlsOptions.CertificateValidationHandler,
                    new X509CertificateWrapper()
                )
                .Build();

                actualTls = options.ClientOptions.ChannelOptions.TlsOptions;

                Assert.Equal(expectedTls.AllowUntrustedCertificates, actualTls.AllowUntrustedCertificates);
                Assert.Equal(expectedTls.UseTls, actualTls.UseTls);
                Assert.Equal(expectedTls.CertificateValidationHandler, actualTls.CertificateValidationHandler);

#if WINDOWS_UWP
                Assert.True(expectedTls.Certificates.SequenceEqual<byte[]>(actualTls.Certificates));
#else
                Assert.True(expectedTls.Certificates.SequenceEqual<X509Certificate>(actualTls.Certificates));
#endif
            }
            finally
            {
#if !WINDOWS_UWP
                if (expectedTls != null)
                    MqttTestUtils.DisposeCertificates(expectedTls.Certificates);

                if (actualTls != null)
                    MqttTestUtils.DisposeCertificates(actualTls.Certificates);
#endif
            }
        }
    }
}
