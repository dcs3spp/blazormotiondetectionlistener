using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Xunit;

using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Factory;
using WebApp.Mqtt.Testing.Helpers;


namespace WebApp.Mqtt
{
    public class MQTTnetManagedClientOptionsBuilderTest
    {
        [Fact]
        public void MQTTnetManagedClientOptionsBuilder_Builds_Options_Derived_From_Config()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var expectedOptions = MqttTestUtils.ToManagedClientOptions(config);

            var options = new MQTTnetManagedClientOptionsBuilder().WithOptions(config).Build();

            Assert.Equal(expectedOptions.AutoReconnectDelay, options.AutoReconnectDelay);
            Assert.Equal(expectedOptions.ClientOptions.Credentials.Username, options.ClientOptions.Credentials.Username);
            Assert.Equal(expectedOptions.ClientOptions.Credentials.Password, options.ClientOptions.Credentials.Password);
            Assert.Equal(expectedOptions.ClientOptions.ChannelOptions.ToString(), options.ClientOptions.ChannelOptions.ToString());
        }
    }
}
