using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Xunit;

using Microsoft.Extensions.Logging.Abstractions;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Contracts;
using WebApp.Mqtt.Testing.Helpers;
using WebApp.Mqtt.Factory;

namespace WebApp.Mqtt.IntegrationTests
{
    public class MqttClientFactoryTest
    {
        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClient_Creates_Tuple_With_Client_And_Options()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var expectedOptions = MqttTestUtils.ToManagedClientOptions(config);
            var factory = new MQTTClientFactory(new NullLogger<MQTTClientFactory>());

            var result = factory.CreateMQTTnetManagedClient(config, new MQTTnetManagedClientOptionsBuilder());

            using (var client = result.client)
                Assert.NotNull(client);

            Assert.Equal(expectedOptions.AutoReconnectDelay, result.options.AutoReconnectDelay);
            Assert.Equal(expectedOptions.ClientOptions.Credentials.Username, result.options.ClientOptions.Credentials.Username);
            Assert.Equal(expectedOptions.ClientOptions.Credentials.Password, result.options.ClientOptions.Credentials.Password);
            Assert.Equal(expectedOptions.ClientOptions.ChannelOptions.ToString(), result.options.ClientOptions.ChannelOptions.ToString());
        }

        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClientTls_Creates_Tuple_With_Client_And_Options()
        {
            const string certPath = "Certs/digiCert.pem";

            var config = MqttTestUtils.CreateMqttConfigWithTLS(certPath);
            var expectedOptions = MqttTestUtils.ToManagedClientOptions(config);
            var expectedTls = expectedOptions.ClientOptions.ChannelOptions.TlsOptions;

            MqttClientTlsOptions actualTls = null;

            try
            {
                var factory = new MQTTClientFactory(new NullLogger<MQTTClientFactory>());
                var result = factory.CreateMQTTnetManagedClientTls(config, new MQTTnetManagedClientTlsOptionsBuilder());

                actualTls = result.options.ClientOptions.ChannelOptions.TlsOptions;
                using (var client = result.client)
                    Assert.NotNull(client);

                Assert.Equal(expectedTls.AllowUntrustedCertificates, actualTls.AllowUntrustedCertificates);
                Assert.Equal(expectedTls.UseTls, actualTls.UseTls);
                Assert.NotNull(actualTls.CertificateValidationHandler);
#if WINDOWS_UWP
                Assert.True(expectedTls.Certificates.SequenceEqual<byte[]>(actualTls.Certificates));
#else
                Assert.True(expectedTls.Certificates.SequenceEqual<X509Certificate>(actualTls.Certificates));
#endif
            }
            finally
            {
                if (actualTls != null)
                    MqttTestUtils.DisposeCertificates(actualTls.Certificates);

                if (expectedTls != null)
                    MqttTestUtils.DisposeCertificates(expectedTls.Certificates);
            }
        }

        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClient_Creates_Client_Without_Tls_When_Config_Has_No_Tls()
        {
            var config = MqttTestUtils.CreateMqttConfigWithoutTLS();
            var expectedOptions = MqttTestUtils.ToManagedClientOptions(config);

            var factory = new MQTTClientFactory(new NullLogger<MQTTClientFactory>());

            var result = factory.CreateMQTTnetManagedClient(config);

            try
            {
                var options = result.options;

                Assert.Equal(expectedOptions.AutoReconnectDelay, options.AutoReconnectDelay);
                Assert.Equal(expectedOptions.ClientOptions.Credentials.Username, options.ClientOptions.Credentials.Username);
                Assert.Equal(expectedOptions.ClientOptions.Credentials.Password, options.ClientOptions.Credentials.Password);
                Assert.Equal(expectedOptions.ClientOptions.ChannelOptions.ToString(), options.ClientOptions.ChannelOptions.ToString());
            }
            finally
            {
                if (result.client != null)
                    result.client.Dispose();
            }
        }

        [Fact]
        public void MqttClientFactory_CreateMQTTnetManagedClient_Creates_Tls_When_Config_Has_Tls()
        {
            const string caPath = "Certs/digiCert.pem";

            var config = MqttTestUtils.CreateMqttConfigWithTLS(caPath);
            var expectedOptions = MqttTestUtils.ToManagedClientOptions(config);
            var factory = new MQTTClientFactory(new NullLogger<MQTTClientFactory>());

            MqttClientTlsOptions actualTls = null;
            MqttClientTlsOptions expectedTls = expectedOptions.ClientOptions.ChannelOptions.TlsOptions;
            IManagedMqttClient client = null;

            try
            {
                var result = factory.CreateMQTTnetManagedClient(config);
                actualTls = result.options.ClientOptions.ChannelOptions.TlsOptions;
                client = result.client;

                Assert.Equal(expectedTls.AllowUntrustedCertificates, actualTls.AllowUntrustedCertificates);
                Assert.Equal(expectedTls.UseTls, actualTls.UseTls);
                Assert.NotNull(actualTls.CertificateValidationHandler);

#if WINDOWS_UWP
                Assert.True(expectedTls.Certificates.SequenceEqual<byte[]>(actualTls.Certificates));
#else
                Assert.True(expectedTls.Certificates.SequenceEqual<X509Certificate>(actualTls.Certificates));
#endif
            }
            finally
            {
                if (client != null)
                    client.Dispose();
#if !WINDOWS_UWP
                if (actualTls != null)
                    MqttTestUtils.DisposeCertificates(actualTls.Certificates);
#endif
            }
        }
    }
}
