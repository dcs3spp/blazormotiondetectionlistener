using System.IO;
using System.Reflection;
using System.Text.Json.Serialization;

using Autofac;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data.Serializers.Contracts;
using WebApp.Testing.Fixtures;


namespace WebApp.Data.Serializers.AutofacModule.UnitTests
{
    public class SerializerModuleTest
    {
        private ITestOutputHelper _output { get; set; }
        private string _configPath;


        public SerializerModuleTest(ITestOutputHelper output)
        {
            _configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            _output = output;
        }

        [Fact]
        public void SerializerModule_Resolves_IMotionDetectionSerializer()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new SerializerModule()
            });

            Assert.True(container.IsRegistered(typeof(IMotionDetectionSerializer<MotionDetection>)));

            using (var scope = container.BeginLifetimeScope())
            {
                IMotionDetectionSerializer<MotionDetection> serializer = scope.Resolve<IMotionDetectionSerializer<MotionDetection>>();
                Assert.NotNull(serializer);
            }
        }

        [Fact]
        public void SerializerModule_Resolves_JsonConverter()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new SerializerModule()
            });

            Assert.True(container.IsRegistered(typeof(JsonConverter)));

            using (var scope = container.BeginLifetimeScope())
            {
                var convertor = scope.Resolve<JsonConverter>();
                Assert.NotNull(convertor);
            }
        }

        [Fact]
        public void SerializerModule_Resolves_IParsedLocation()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new SerializerModule()
            });

            Assert.True(container.IsRegistered(typeof(IParsedLocation)));

            using (var scope = container.BeginLifetimeScope())
            {
                var obj = scope.Resolve<IParsedLocation>();
                Assert.NotNull(obj);
            }
        }

        [Fact]
        public void SerializerModule_Resolves_IParsedMotionDetection()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new SerializerModule()
            });

            Assert.True(container.IsRegistered(typeof(IParsedMotionDetection)));

            using (var scope = container.BeginLifetimeScope())
            {
                var obj = scope.Resolve<IParsedMotionDetection>();
                Assert.NotNull(obj);
            }
        }

        [Fact]
        public void SerializerModule_Resolves_IParsedMotionInfo()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new SerializerModule()
            });

            Assert.True(container.IsRegistered(typeof(IParsedMotionInfo)));

            using (var scope = container.BeginLifetimeScope())
            {
                var obj = scope.Resolve<IParsedMotionInfo>();
                Assert.NotNull(obj);
            }
        }

        [Fact]
        public void SerializerModule_Resolves_IJsonParser()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new SerializerModule()
            });

            Assert.True(container.IsRegistered(typeof(IJsonParser)));

            using (var scope = container.BeginLifetimeScope())
            {
                var obj = scope.Resolve<IJsonParser>();
                Assert.NotNull(obj);
            }
        }

        [Fact]
        public void SerializerModule_Resolves_IJsonSerializerWrapper()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new SerializerModule()
            });

            Assert.True(container.IsRegistered(typeof(IJsonSerializerWrapper)));

            using (var scope = container.BeginLifetimeScope())
            {
                var obj = scope.Resolve<IJsonSerializerWrapper>();
                Assert.NotNull(obj);
            }
        }

        [Fact]
        public void SerializerModule_Resolves_IVisitor()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new SerializerModule()
            });

            Assert.True(container.IsRegistered(typeof(IVisitor)));

            using (var scope = container.BeginLifetimeScope())
            {
                var obj = scope.Resolve<IVisitor>();
                Assert.NotNull(obj);
            }
        }

        [Fact]
        public void SerializerModule_Resolves_IVisitorElement()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new SerializerModule()
            });

            Assert.True(container.IsRegistered(typeof(IVisitorElement<MotionDetection>)));

            using (var scope = container.BeginLifetimeScope())
            {
                var obj = scope.Resolve<IVisitorElement<MotionDetection>>();
                Assert.NotNull(obj);
            }
        }

    }
}
