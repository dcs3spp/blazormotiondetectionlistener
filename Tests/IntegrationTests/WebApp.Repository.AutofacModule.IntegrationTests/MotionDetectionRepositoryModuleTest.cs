using System.IO;
using System.Reflection;

using Autofac;
using Xunit;
using Xunit.Abstractions;

using WebApp.Repository.AutofacModule;
using WebApp.Repository.Contracts;
using WebApp.Testing.Fixtures;


namespace WebApp.Data.Serializers.AutofacModule
{
    public class MotionDetectionRepositoryModuleTest
    {
        private ITestOutputHelper _output { get; set; }
        private readonly string _configPath;

        public MotionDetectionRepositoryModuleTest(ITestOutputHelper output)
        {
            _output = output;
            _configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }


        [Fact]
        public void MotionDetectionRepositoryModule_Resolves_IMotionDetectionRepository()
        {
            var services = CoreTestUtils.ConfigureServices(_configPath);
            var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] {
                new MotionDetectionRepositoryModule()
            });

            Assert.True(container.IsRegistered(typeof(IMotionDetectionRepository)));

            using (var scope = container.BeginLifetimeScope())
            {
                IMotionDetectionRepository consumer = scope.Resolve<IMotionDetectionRepository>();
                Assert.True(consumer != null);
            }
        }
    }
}
