using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

using Bunit;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;
using Xunit.Abstractions;

using WebApp.Data;
using WebApp.Data.Serializers.Contracts;
using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Repository;
using WebApp.Repository.Contracts;
using WebApp.Realtime.SignalR.Client;
using WebApp.Testing.Mocks.Blazor;


namespace WebApp.Blazor.UnitTests
{
    public class IndexTest : TestContext, IDisposable
    {
        private MotionDetectionRepository _Repository { get; }
        private MotionDetection _MotionDetectionItem { get; }
        private ITestOutputHelper _Output { get; }

        #region Mocks
        private Mock<IHubProxyFactory> _FactoryHubMock { get; }
        private Mock<IHubProxy> _HubProxyMock { get; }
        #endregion


        /// <summary>
        /// Test setup, runs before every test
        /// Setup injected services
        /// Mock repository and signalR hub connection
        /// </summary>
        public IndexTest(ITestOutputHelper output)
        {
            _Output = output;

            _FactoryHubMock = new Mock<IHubProxyFactory>();
            _HubProxyMock = new Mock<IHubProxy>();
            _HubProxyMock.Setup(x => x.StartAsync(default(CancellationToken))).Returns(Task.CompletedTask);
            _HubProxyMock.Setup(x => x.StopAsync(default(CancellationToken))).Returns(Task.CompletedTask);
            _FactoryHubMock.Setup(x => x.Create(It.IsAny<string>(), It.IsAny<JsonSerializerOptions>()))
                           .Returns(_HubProxyMock.Object).Verifiable();

            var mock = new Mock<IMotionDetectionSerializer<MotionDetection>>();
            _MotionDetectionItem = CreateMotionDetection();
            _Repository = new MotionDetectionRepository(mock.Object, new NullLogger<MotionDetectionRepository>());

            Services.AddScoped<ILogger<MotionDetectionRepository>, NullLogger<MotionDetectionRepository>>();
            Services.AddScoped<ILogger<MotionInfoConverter>, NullLogger<MotionInfoConverter>>();
            Services.AddScoped<ILogger<MotionDetectionConverter>, NullLogger<MotionDetectionConverter>>();
            Services.AddScoped<ILogger<JsonVisitor>, NullLogger<JsonVisitor>>();
            Services.AddScoped<ILogger<WebApp.Pages.Index>, NullLogger<WebApp.Pages.Index>>();
            Services.AddScoped<IMotionDetectionRepository>(sp => _Repository);
            Services.AddScoped<MockNavigationManager>();
            Services.AddSingleton(typeof(IHubProxyFactory), _FactoryHubMock.Object);
        }

        [Fact]
        public void Index_Page_MotionDetection_Event_Renders_Group()
        {
            string expectedGroup = $"Group: {_MotionDetectionItem.Group}";

            _Repository.AddItem(_MotionDetectionItem);

            var cut = RenderComponent<WebApp.Pages.Index>();
            var cardContent = cut.Find("[class=mat-card-content]");

            Assert.Equal(expectedGroup, cardContent.Children[0].TextContent.Trim());
        }

        [Fact]
        public void Index_Page_MotionDetection_Event_Renders_Monitor()
        {
            string expectedGroup = $"Group: {_MotionDetectionItem.Group}";
            string expectedMonitor = $"Monitor: {_MotionDetectionItem.MonitorId}";
            string expectedTime = $"Time Received UTC: {_MotionDetectionItem.TimeReceivedUTC}";

            _Repository.AddItem(_MotionDetectionItem);

            var cut = RenderComponent<WebApp.Pages.Index>();
            var cardContent = cut.Find("[class=mat-card-content]");

            Assert.Equal(expectedGroup, cardContent.Children[0].TextContent.Trim());
            Assert.Equal(expectedMonitor, cardContent.Children[1].TextContent.Trim());
            Assert.Equal(expectedTime, cardContent.NextElementSibling.TextContent.Trim());
        }

        [Fact]
        public void Index_Page_MotionDetection_Event_Renders_Time()
        {
            string expectedTime = $"Time Received UTC: {_MotionDetectionItem.TimeReceivedUTC}";

            _Repository.AddItem(_MotionDetectionItem);

            var cut = RenderComponent<WebApp.Pages.Index>();
            var cardContent = cut.Find("[class=mat-card-content]");

            Assert.Equal(expectedTime, cardContent.NextElementSibling.TextContent.Trim());
        }

        [Fact]
        public void Index_Page_No_MotionDetection_Events_Renders_Empty()
        {
            var cut = RenderComponent<WebApp.Pages.Index>();
            var cardContent = cut.Find("[class=mat-layout-grid-inner]");

            Assert.Equal(0, cardContent.Children.Length);
        }

        [Fact]
        public void Index_Page_OnAfterRenderAsync_Creates_HubConnection()
        {
            var cut = RenderComponent<WebApp.Pages.Index>();

            _FactoryHubMock.Verify(x =>
                x.Create(It.IsAny<string>(),
                It.IsAny<JsonSerializerOptions>()), Times.Once()
            );
        }

        [Fact]
        public void Index_Page_OnAfterRenderAsync_Starts_SignalR_Hub_Connection()
        {
            var cut = RenderComponent<WebApp.Pages.Index>();

            _HubProxyMock.Verify(x =>
                x.StartAsync(default),
                Times.Once()
            );
        }

        [Fact]
        public async Task Index_Page_DisposeAsync_Calls_Proxy_DisposeAsync()
        {
            var indexPage = RenderComponent<WebApp.Pages.Index>().Instance;

            await indexPage.DisposeAsync();

            _HubProxyMock.Verify(x => x.DisposeAsync(), Times.Once());
        }

        [Fact]
        public async Task Index_Page_DisposeAsync_Calls_Proxy_StopAsync()
        {
            var indexPage = RenderComponent<WebApp.Pages.Index>().Instance;

            await indexPage.DisposeAsync();

            _HubProxyMock.Verify(x => x.StopAsync(default(CancellationToken)), Times.Once());
        }

        [Fact]
        public void Index_Page_Dispose_Calls_Proxy_DisposeAync()
        {
            var indexPage = RenderComponent<WebApp.Pages.Index>().Instance;

            indexPage.Dispose();

            _HubProxyMock.Verify(x => x.DisposeAsync(), Times.Once());
        }

        [Fact]
        public void Index_Page_After_Disposed_Does_Not_Call_Proxy_DisposeAsync()
        {
            var indexPage = RenderComponent<WebApp.Pages.Index>().Instance;

            indexPage.Dispose();
            indexPage.Dispose();

            _HubProxyMock.Verify(x => x.DisposeAsync(), Times.Once());
        }

        private MotionDetection CreateMotionDetection()
        {
            const long Time = 820;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo expectedInfo = new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "YmFzZTY0", 64, 48, Time);

            return new MotionDetection("myGroup", Time, "myMonitorId", Plugin, expectedInfo);
        }
    }
}