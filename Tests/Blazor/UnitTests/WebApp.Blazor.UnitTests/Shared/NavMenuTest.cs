using System;

using Bunit;
using Xunit;

using WebApp.Shared;


namespace WebApp.Blazor.UnitTests
{
    public class NavMenuTest : TestContext
    {
        [Fact]
        public void NavMenu_Renders_Brand()
        {
            const string ExpectedBrand = "Live Cam Motion Detection";

            var cut = RenderComponent<NavMenu>();
            var brand = cut.Find("[class=navbar-brand]");

            Assert.Equal(ExpectedBrand, brand.TextContent);
        }

        [Fact]
        public void NavMenu_Renders_HomeLink()
        {
            const string ExpectedLinkName = "Home";

            var cut = RenderComponent<NavMenu>();
            var linkHome = cut.Find("[class~=nav-link]");

            Assert.Equal(ExpectedLinkName, linkHome.TextContent.Trim());
        }

        [Fact]
        public void NavMenu_ToggleNavMenu_Hides_HomeButton()
        {
            var cut = RenderComponent<NavMenu>();
            cut.Find("[class=navbar-toggler]").Click();

            Assert.Throws<Bunit.ElementNotFoundException>(() => cut.Find("[class=collapse]"));
        }
    }
}
