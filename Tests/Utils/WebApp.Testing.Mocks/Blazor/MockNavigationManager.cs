using Microsoft.AspNetCore.Components;


namespace WebApp.Testing.Mocks.Blazor
{
    public sealed class MockNavigationManager : NavigationManager
    {
        public MockNavigationManager(string baseUri, string uri) : base() =>
          this.Initialize(baseUri, uri);

        protected override void NavigateToCore(string uri, bool forceLoad) =>
          this.WasNavigateInvoked = true;

        public bool WasNavigateInvoked { get; private set; }
    }
}