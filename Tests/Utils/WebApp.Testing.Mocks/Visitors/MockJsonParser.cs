using System;
using System.Collections.Generic;
using System.Text.Json;

using WebApp.Data.Serializers.Contracts;


/// <summary>
/// This class tracks properties read for each type. It acts as an optional proxy, forwarding
/// method calls to an actual IJsonParser instance injected during creation.
/// It adds properties to track the set of property names for each type, e.g. string, int, 
/// double, datetimeoffset etc.
/// 
/// Used in tests for JsonVisitor.
///
/// Why do this? At the time of writing mocking frameworks do not appear to allow tracing
/// or mocking method calls that have ref struct parameters.
/// System.Text.Json uses Utf8JsonReader, a ref struct.
/// </summary>

namespace WebApp.Testing.Mocks.Serializers.Converters.Visitors
{
    public class MockJsonParser : IJsonParser
    {
        #region Track properties that have been read
        public HashSet<string> DoubleProperties { get; set; }
        public HashSet<string> DateTimeOffsetProperties { get; set; }
        public HashSet<string> IntProperties { get; set; }
        public HashSet<string> LongProperties { get; set; }
        public HashSet<string> ReadProperties { get; set; }
        public HashSet<string> StringProperties { get; set; }
        public HashSet<string> FailedConversionProperties { get; set; }
        public HashSet<string> UnrecognisedProperties { get; set; }
        #endregion

        private IJsonParser Parser { get; }

        public MockJsonParser()
        {
            DateTimeOffsetProperties = new HashSet<string>();
            DoubleProperties = new HashSet<string>();
            IntProperties = new HashSet<string>();
            LongProperties = new HashSet<string>();
            StringProperties = new HashSet<string>();
            ReadProperties = new HashSet<string>();
        }

        public MockJsonParser(IJsonParser actual) : this()
        {
            Parser = actual ?? throw new ArgumentNullException(nameof(actual));
        }

        public string ReadProperty(ref Utf8JsonReader reader, string propertyName)
        {
            ReadProperties.Add(propertyName);

            if (Parser == null)
                return propertyName;

            return Parser.ReadProperty(ref reader, propertyName);
        }

        public string ReadStringProperty(ref Utf8JsonReader reader, string propertyName)
        {
            StringProperties.Add(propertyName);

            if (Parser == null)
                return default(string);

            return Parser.ReadStringProperty(ref reader, propertyName);
        }

        public int ReadIntProperty(ref Utf8JsonReader reader, string propertyName)
        {
            IntProperties.Add(propertyName);

            if (Parser == null)
                return default(int);
            else
                return Parser.ReadIntProperty(ref reader, propertyName);
        }

        public long ReadLongProperty(ref Utf8JsonReader reader, string propertyName)
        {
            LongProperties.Add(propertyName);

            if (Parser == null)
                return default(long);
            else
                return Parser.ReadLongProperty(ref reader, propertyName);
        }


        public double ReadDoubleProperty(ref Utf8JsonReader reader, string propertyName)
        {
            DoubleProperties.Add(propertyName);

            if (Parser == null)
                return default(double);
            else
                return Parser.ReadDoubleProperty(ref reader, propertyName);
        }

        public DateTimeOffset ReadDateTimeOffsetProperty(ref Utf8JsonReader reader, string propertyName)
        {
            DateTimeOffsetProperties.Add(propertyName);

            if (Parser == null)
                return default(DateTimeOffset);
            else
                return Parser.ReadDateTimeOffsetProperty(ref reader, propertyName);
        }


        public void ThrowFailedConversion(string propertyName)
        {
            FailedConversionProperties.Add(propertyName);
        }

        public void ThrowUnrecognisedProperty(string propertyName)
        {
            UnrecognisedProperties.Add(propertyName);
        }
    }
}
