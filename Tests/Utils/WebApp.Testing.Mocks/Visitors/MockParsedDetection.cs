using System.Collections.Generic;
using System.Text.Json;

using WebApp.Data;
using WebApp.Data.Serializers.Contracts;


namespace WebApp.Testing.Mocks.Serializers.Converters.Visitors
{
    public class MockParsedDetection : IVisitorElement<MotionDetection>
    {
        public IDictionary<string, object> AcceptArgs { get; private set; }
        public int AcceptCallCount { get; private set; }
        public int ToModelCallCount { get; private set; }
        public MotionDetection ToModelReturnValue { get; private set; }

        public MockParsedDetection()
        {
            AcceptArgs = new Dictionary<string, object>();

            const long Time = 820;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo expectedInfo = new MotionInfo(Plugin, "Tensorflow", "object", expectedMatrices, "YmFzZTY0", 64, 48, Time);


            ToModelReturnValue = new MotionDetection("myGroup", Time, "myMonitorId", Plugin, expectedInfo);
        }

        public void accept(IVisitor visitor, ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            AcceptCallCount++;
            AcceptArgs["visitor"] = visitor;
            AcceptArgs["reader"] = reader.CurrentState;
            AcceptArgs["options"] = options;
        }
        public MotionDetection ToModel()
        {
            ToModelCallCount++;

            return ToModelReturnValue;
        }
    }
}