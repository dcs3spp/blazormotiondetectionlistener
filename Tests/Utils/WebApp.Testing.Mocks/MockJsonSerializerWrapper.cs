using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

using WebApp.Data;
using WebApp.Data.Serializers.Contracts;


namespace WebApp.Testing.Mocks.Serializers
{
    /** 
     * Have had to implement a manual mock due to limitation of mocking frameworks
     * such as Moq that currently do not allow testing that a method call has been
     * triggered that accepts a ref struct parameter
     *
     * Have also had to implement a custom wrapper for JsonSerializer since it
     * appears that Microsoft Fakes and other libraries are not available for .NET
     * core unless go down commercial paid solutions.
     *
     * The classes uses properties to set output state for 
     * MotionInfo and MotionDetection models
     */
    public class MockJsonSerializerWrapper : IJsonSerializerWrapper
    {
        public MotionInfo MotionInfo { get; set; }
        public MotionDetection MotionDetection { get; set; }

        public int DeserializeWithReaderCallCount { get; set; }
        public int DeserializeWithStringCallCount { get; set; }

        public MockJsonSerializerWrapper()
        {
            const long Time = 840;
            const string Plugin = "TensorFlow-WithFiltering-And-MQTT";

            List<MotionLocation> expectedMatrices = new List<MotionLocation>();
            expectedMatrices.Add(new MotionLocation(2.313079833984375, 1.0182666778564453, 373.25050354003906, 476.9341278076172, "person", 0.7375929355621338));

            MotionInfo = new MotionInfo(Plugin, "Tensorflow", "person", expectedMatrices, "base64", 64, 48, Time);

            MotionDetection = new MotionDetection("myGroup", Time, "myMonitorId", Plugin, MotionInfo);
        }


        public MockJsonSerializerWrapper(MotionDetection detection, MotionInfo info)
        {
            MotionDetection = detection ?? throw new ArgumentNullException();
            MotionInfo = info ?? throw new ArgumentNullException();
        }

        public T Deserialize<T>(string json, JsonSerializerOptions options)
        {
            DeserializeWithStringCallCount++;

            if (typeof(T).Equals(MotionInfo))
            {
                return (T)(object)this.MotionInfo;
            }
            else if (typeof(T).Equals(MotionDetection))
            {
                return (T)(object)this.MotionDetection;
            }
            else
            {
                string message = string.Format("Mock unsupported for type {}", typeof(T).ToString());
                throw new NotSupportedException(message: message);
            }
        }

        public T Deserialize<T>(ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            DeserializeWithReaderCallCount++;

            if (typeof(T).Equals(MotionInfo.GetType()))
            {
                return (T)(object)this.MotionInfo;
            }
            else if (typeof(T).Equals(MotionDetection.GetType()))
            {
                return (T)(object)this.MotionDetection;
            }
            else
            {
                string message = string.Format("Mock unsupported for type");
                throw new NotSupportedException(message: message);
            }
        }

        public Task<T> DeserializeAsync<T>(Stream stream, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }
    }
}