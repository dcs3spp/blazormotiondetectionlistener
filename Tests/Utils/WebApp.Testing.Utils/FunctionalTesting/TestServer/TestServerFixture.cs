using System;
using System.Threading.Tasks;

using Xunit;

using WebApp.Testing.Fixtures;
using WebApp.Testing.Functional.SystemEnvironment;


namespace WebApp.Testing.Functional.TestServers
{
    public class TestServerFixture<TStartup> : IDisposable, IAsyncLifetime where TStartup : class
    {
        /// <summary>True when fixture has been disposed</summary>
        private bool _Disposed { get; set; }

        /// <summary>Metadata relating to appsettings file used</summary>
        public ConfigMetaData ConfigMetaData { get; }

        /// <summary>TestServer provided by <see cref="WebApplicationFactory"/> subclass</summary>
        public WebAppTestFactory<TStartup> Factory { get; }


        /// <summary>
        /// Set environment variables for secrets from .env file
        /// if not running in a container
        /// </summary>
        public TestServerFixture()
        {
            ConfigMetaData = SettingsFile.GetConfigMetaData();

            _Disposed = false;

            WebAppSystemEnvironmentFactory envFactory = new WebAppSystemEnvironmentFactory();
            envFactory.LoadEnvironmentVariables();

            Factory = new WebAppTestFactory<TStartup>(ConfigMetaData.SettingsFile);
        }

        #region IAsyncLifetime
        public Task InitializeAsync()
        {
            return CoreTestUtils.CreateKafkaTopic(ConfigMetaData.ConfigBasePath, ConfigMetaData.ConfigFileName);
        }

        public Task DisposeAsync() => Task.CompletedTask;
        #endregion

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose the WebApplicationFactory
        /// </summary>
        /// <remarks>
        /// For further info see https://docs.microsoft.com/en-us/dotnet/api/system.idisposable?view=netcore-3.1
        /// </remarks>
        protected virtual void Dispose(bool disposing)
        {
            if (_Disposed)
                return;

            if (disposing)
            {
                Factory.Dispose();
            }

            _Disposed = true;
        }

        /// <summary>
        /// Finalizer
        /// </summary>
        ~TestServerFixture()
        {
            Dispose(false);
        }
        #endregion
    }
}