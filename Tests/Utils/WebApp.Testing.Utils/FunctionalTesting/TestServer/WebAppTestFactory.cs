using System;

using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;



namespace WebApp.Testing.Functional.TestServers
{

    /// <summary>
    /// WebApplicationFactory that uses a test server for SUT
    /// and loads configuration from specified settings superceded
    /// by environment variables. Generic host is configured to 
    /// use Autofac service provider factory.
    /// </summary>
    /// <remarks>
    /// In ConfigureWebHost an alternative option to using subclass of TStartup for testing is
    /// to use ConfigureTestServices. This will allow overriding services after TStartup
    /// to limit and issue having two startup classes running, i.e. live and test:
    /// https://github.com/dotnet/aspnetcore/issues/20307
    /// https://github.com/dotnet/aspnetcore/issues/19404
    /// https://github.com/dotnet/aspnetcore/issues/12360
    /// https://github.com/dotnet/aspnetcore/issues/9218#issuecomment-553406831
    /// However, at the time of writing, ConfigureTestServices also has a bug in .Net Core 3+ 
    /// that is waiting to be addressed relating to registered HttpClients 
    /// https://github.com/dotnet/aspnetcore/issues/17937
    /// </remarks>
    public class WebAppTestFactory<TStartup> : WebApplicationFactory<TStartup>, IDisposable
        where TStartup : class
    {
        const string AspNetCoreEnvironment = "ASPNETCORE_ENVIRONMENT";
        const string DefaultSettingsFile = "appsettings.json";

        private bool _Disposed { get; set; }
        private string _SettingsFile { get; }


        /// <summary>
        /// Create WebApplicationFactory using specified settings file.
        /// </summary>
        /// <param name="settingsFile">Absolute path to settings file</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown when settingsFile parameter is null or string.Empty
        /// </exception>
        public WebAppTestFactory(string settingsFile) : base()
        {
            _Disposed = false;
            _SettingsFile = settingsFile;

            if (string.IsNullOrEmpty(_SettingsFile))
                throw new ArgumentNullException(nameof(settingsFile));
        }

        /// <summary>
        /// Configure web host with config from settings file
        /// Use to configure services for initialising services before TStartup
        /// Use ConfigureTestServices for initialising services after TStartup
        /// </summary>
        /// <seealso cref="Microsoft.AspNetCore.Mvc.Testing.ConfigureWebHost(IWebHostBuilder)"/></seealso>
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.UseStartup<TStartup>()
                .ConfigureAppConfiguration((context, cb) =>
                {
                    cb.AddJsonFile(_SettingsFile, optional: false)
                    .AddEnvironmentVariables();
                })
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddConsole();
                });
        }

        /// <summary>Use generic host with autofac service provider</summary>
        protected override IHostBuilder CreateHostBuilder()
        {
            var builder = Host.CreateDefaultBuilder()
                .UseServiceProviderFactory(new AutofacServiceProviderFactory());

            return builder;
        }


        #region IDisposable
        /// <summary>
        /// Subclasses only provide Dispose(bool) and must call base.Dispose(disposing)
        /// </summary>
        /// <remarks>
        /// For further info see https://docs.microsoft.com/en-us/dotnet/api/system.idisposable?view=netcore-3.1
        /// </remarks>
        protected override void Dispose(bool disposing)
        {
            if (_Disposed)
                return;

            _Disposed = true;

            base.Dispose(disposing);
        }

        /// <summary>
        /// Finalizer
        /// </summary>
        ~WebAppTestFactory()
        {
            Dispose(false);
        }
        #endregion
    }
}