using System.IO;
using System.Reflection;


namespace WebApp.Testing.Functional.SystemEnvironment
{
    public struct ConfigMetaData
    {
        public string SettingsFile { get; }
        public string ConfigBasePath { get; }
        public string ConfigFileName { get; }

        public ConfigMetaData(string file, string basePath, string fileName)
        {
            SettingsFile = file;
            ConfigBasePath = basePath;
            ConfigFileName = fileName;
        }
    }

    public static class SettingsFile
    {
        private const string AspNetCoreEnvironment = "ASPNETCORE_ENVIRONMENT";
        public static ConfigMetaData GetConfigMetaData(string defaultSettingsFile = "appsettings.Local.json")
        {
            System.Console.WriteLine($"SettingsFile::GetConfigMetaData ::: Directory for executing assembly :: {Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}");
            System.Console.WriteLine($"SettingsFile::GetConfigMetaData ::: Executing assembly :: {Assembly.GetExecutingAssembly()}");
            var envVar = System.Environment.GetEnvironmentVariable(AspNetCoreEnvironment);
            var ConfigFileName = defaultSettingsFile;

            if (envVar != null)
                ConfigFileName = $"appsettings.{envVar}.json";

            var ConfigBasePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var SettingsFile = $"{ConfigBasePath}/{ConfigFileName}";

            return new ConfigMetaData(SettingsFile, ConfigBasePath, ConfigFileName);
        }
    }
}