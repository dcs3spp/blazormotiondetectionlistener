using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;



namespace WebApp.Testing.Functional.SystemEnvironment
{
    /// <summary>
    /// Dictionary of variables stored in .env file
    /// </summary>
    /// <remarks>
    /// This does not set the environment variables. It only
    /// loads them from the .env file.
    /// </remarks>
    public class DotEnvFile
    {
        public Vars EnvVars { get; }


        /// <summary>Store environment variables from .env into dictionary</summary>
        /// <param name="paths">The absolute path for .env file</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref=""></exception>
        public DotEnvFile(string path)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentNullException(nameof(path));

            if (!File.Exists(path))
                throw new FileNotFoundException($"Could not find {path}");

            EnvVars = EnvFileParser.Load(path);
        }


        /// <summary>
        /// Ensure that the variables loaded from .env file match the required set
        /// </summary>
        /// <param name="expectedDotEnvVars">
        /// Set containing required environment variables that should be set
        /// </param>
        /// <exception cref="InvalidOperationException">
        /// Thrown when a required environment variable is missing from the set of
        /// expectedDotEnvVars
        /// </exception>
        public void EnsureInitialised(HashSet<string> expectedDotEnvVars)
        {
            var actualEnvVars = EnvVars.Keys.ToHashSet<string>();

            if (!expectedDotEnvVars.IsSubsetOf(actualEnvVars))
                throw new InvalidOperationException("Missing required variables in .env file");
        }
    }
}