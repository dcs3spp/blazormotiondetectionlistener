using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using WebApp.Testing.Functional.SystemEnvironment;


namespace WebApp.Testing.Functional.Abstractions
{
    /// <summary>
    /// Abstract class that provides Autofac IOC in self hosting environment
    /// Intended for use in functional test environments.
    /// An Autofac <see cref="ILIfetimeScope"/> is created for the lifetime
    /// of this host and tagged with <see cref="Autofac.TransactionScope"/>.
    /// </summary>
    /// <remarks>
    /// The base class implements <see cref="TestHost"/> starts and stops
    /// the server for us. It also disposes of the host for us and all 
    /// associated services.
    /// </remarks>
    public abstract class AutofacTestHost<TStartup> : TestHost
        where TStartup : class
    {
        public const string TransactionScope = "transaction";

        public ILifetimeScope BaseScope { get; private set; }

        /// <summary>
        /// Construct individual service properties
        /// </summary>
        public AutofacTestHost() : base()
        {
            BaseScope = Server.Services.GetAutofacRoot().BeginLifetimeScope(TransactionScope);
        }

        /// <summary>
        /// Create a tagged <see cref="ILifetimeScope"/>
        /// </summary>
        /// <param name="tag">
        /// Tag for the scope 
        /// </param>
        /// <remarks>
        /// The client should dispose of the <see cref="ILifetimeScope"/> instance
        /// </remarks>
        public ILifetimeScope CreateScope(string tag)
        {
            var scope = Server.Services.GetAutofacRoot().BeginLifetimeScope(tag);

            return scope;
        }

        /// <summary>
        /// Build the server, with Autofac IOC.
        /// </summary>
        protected override IHost BuildServer(HostBuilder builder)
        {
            // build the host instance
            return new HostBuilder()
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddConsole();
                    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Information);
                })
                .ConfigureWebHost(webBuilder =>
                {
                    webBuilder.ConfigureAppConfiguration((context, cb) =>
                    {
                        cb.AddJsonFile(ConfigMetaData.SettingsFile, optional: false)
                        .AddEnvironmentVariables();
                    })
                    .ConfigureServices(services =>
                    {
                        services.AddHttpClient();
                    })
                    .UseStartup<TStartup>()
                    .UseKestrel()
                    .UseUrls("http://127.0.0.1:0");
                }).Build();
        }

        #region Abstractions
        /// <summary>
        /// Subclasses must override to initialise system environment
        /// variables from .env file or system environment variables
        /// if running within a container
        /// </summary>
        protected override abstract void LoadSystemEnvironment();
        #endregion

        #region Dispose
        private bool _disposed = false;

        /// <summary>
        /// Dispose of scopes that have been created
        /// </summary>
        /// <param name="disposing">True if triggered from <see cref="TestHost.Dispose"/></param>
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                BaseScope.Dispose();

            _disposed = true;
            base.Dispose(disposing);
        }
        #endregion
    }
}