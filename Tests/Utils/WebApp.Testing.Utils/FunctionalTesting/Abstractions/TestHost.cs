using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Xunit;

using WebApp.Testing.Functional.SystemEnvironment;


namespace WebApp.Testing.Functional.Abstractions
{
    /// <summary> 
    /// An abstract representation of a self hosted ASP.NET Core 3.1 instance
    /// Use xunit IAsyncLifeTime to start and stop it in test fixtures
    /// Override <see cref="BuildServer"/> method to customise building the self hosted test server
    /// </summary>
    /// <remarks>
    /// Subclasses implement <see cref="BuildServer"/> to customise host environment.
    /// Subclasses could add attributes to required services, e.g. MQTT clients, GRPC clients etc.
    /// </remarks>
    public abstract class TestHost : IAsyncLifetime, IDisposable
    {
        /// <summary>
        /// Timeout period to wait for server startup, expressed in seconds
        /// Once elapsed, if the server has not started a <see cref="TimeoutException"/> is thrown
        /// </summary>
        private const short StartupTimeOutSeconds = 30;

        /// <summary>
        /// Configuration metadata, determines settings file based on ASPNETCORE_ENVIRONMENT variable
        /// Allows for retrieving absolute path to settings file, base path and filename
        /// </summary>
        public ConfigMetaData ConfigMetaData { get; private set; }

        /// <summary>
        /// Self hosted test server
        /// </summary>
        public IHost Server { get; private set; }

        /// <summary>
        /// Url of server
        /// Initialised after server has been started via IAsyncLifetime
        /// </summary>
        public Uri ServerUrl { get; private set; }



        #region Constructor
        /// <summary>
        /// Constructor that:
        /// 1. Loads secrets from .env file or load from env vars if running in container
        /// 2. Creates settings file meta data based on executing assembly location and ASPNETCORE_ENVIRONMENT
        /// 3. Builds the server via abstract method <see cref="BuildServer"/>
        /// </summary>
        public TestHost()
        {
            // load secrets from .env file, or load them from env vars if running in a container
            LoadSystemEnvironment();

            // based upon the ASPNETCORE_ENVIRONMENT get the config meta data
            ConfigMetaData = SettingsFile.GetConfigMetaData();

            Server = BuildServer(new HostBuilder());
        }
        #endregion

        #region Abstractions
        /// <summary>
        /// Abstract method to implement building the server
        /// </summary>
        protected abstract IHost BuildServer(HostBuilder builder);

        /// <summary>
        /// Abstract method to load system environment from .env file or
        /// system environment variables
        /// </summary>
        protected abstract void LoadSystemEnvironment();
        #endregion


        #region Xunit equivalent of IDisposeAsync, until upcoming version of Xunit provides IDisposeAsync
        /// <summary>
        /// Perform initialisation task(s), such as database migrations, populating a cache and 
        /// then start the server. Wait for <see cref="StartupTimeOutSeconds"/> before throwing a TimeoutException
        /// </summary>
        /// <exception cref="TimeoutException">
        /// Thrown if server did not start after see cref="StartupTimeOutSeconds"/> seconds
        /// </exception>
        /// <remarks>
        /// Using helper library provided by 
        /// https://github.com/thomaslevesque/Extensions.Hosting.AsyncInitialization to
        /// perform asynchronous initialisation tasks before server starts.
        /// Why use the helper library? Found out the hard way, when had async task set up as 
        /// IHostedService. This was runing fine on development environment but other IHostedServices
        /// were dependent on it executing and completing first. Also, it started to block when used with 
        /// TestServer in CI environment. The Startup class config method is synchronous, as is IStartupFilter
        /// Read more here:
        /// https://github.com/thomaslevesque/Extensions.Hosting.AsyncInitialization
        /// https://andrewlock.net/running-async-tasks-on-app-startup-in-asp-net-core-part-1/
        /// https://andrewlock.net/running-async-tasks-on-app-startup-in-asp-net-core-part-2/
        /// https://andrewlock.net/running-async-tasks-on-app-startup-in-asp-net-core-3/
        /// https://andrewlock.net/introducing-ihostlifetime-and-untangling-the-generic-host-startup-interactions/
        /// </remarks>
        public virtual async Task InitializeAsync()
        {
            var cts = new CancellationTokenSource(TimeSpan.FromSeconds(StartupTimeOutSeconds));
            try
            {
                await Server?.InitAsync();
                await Server?.StartAsync(cts.Token);

                ServerUrl = new Uri(Server?.Services.GetService<IServer>().Features.Get<IServerAddressesFeature>().Addresses.Single());
            }
            catch (OperationCanceledException)
            {
                throw new TimeoutException($"Timed out waiting for server to start");
            }
        }
        public virtual async Task DisposeAsync()
        {
            await Server?.StopAsync();
            Dispose();
        }
        #endregion


        #region Dispose
        private bool _disposed = false;
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                Server?.Dispose();
            }

            Server = null;
            _disposed = true;
        }
        #endregion
    }
}