using System;
using System.Collections.Generic;


namespace WebApp.Testing.Functional.Abstractions
{
    /// <summary>
    /// Metadata structure for system environment
    /// Used for initialising environment variables when testing locally
    /// with docker-compose (.env) and testing within a 
    /// docker-compose stack (EnvironmentVariables)
    /// </summary>
    public struct EnvironmentContext
    {
        public EnvironmentContext(
            string dotEnvFilePath,
            HashSet<string> dotenvVariables,
            HashSet<string> environmentVariables,
            Action<string, IDictionary<string, string>> mappingDelegate
        )
        {
            DotEnvFilePath = dotEnvFilePath;
            DotEnvVariables = dotenvVariables;
            EnvironmentVariables = environmentVariables;
            MappingDelegate = mappingDelegate;
        }

        /// <summary>Set of .dotenv environment variable names </summary>
        public readonly HashSet<string> DotEnvVariables;

        /// <summary>Absolute path to .env file</summary>
        public readonly string DotEnvFilePath;

        /// <summary>Set of environment variable names</summary>
        public readonly HashSet<string> EnvironmentVariables;

        /// <summary>
        /// Delegate used to map a dotenv variable to an environment variable
        /// value held in a dictionary
        /// </summary>
        public readonly Action<string, IDictionary<string, string>> MappingDelegate;
    }
}