using System;
using Microsoft.Extensions.DependencyInjection;
using Xunit;


namespace WebApp.Testing.Functional.Abstractions
{
    /// <summary>
    /// Abstract test class that spawns an in-process Kestrel host. 
    /// It creates a BUnit test context before each test for a Blazor Server page. 
    /// Subclasses must override <see cref="CreatePageDependencies"/> method to 
    /// allow services to be initialised for the Page Under Test (PUT). 
    /// </summary>
    /// <typeparam name="THost">
    /// In-process host, derived from <see cref="TestHost"/>
    /// </typeparam>
    /// <typeparam name="TPage">
    /// Blazor Server Page <see cref="BlazorServerPage"/>
    /// </typeparam>
    /// <typeparam name="THostFixture">
    /// <see cref="BlazorServerTestFixture"/> that provides access to the in-proces
    /// host and a factory method to create service dependencies for the 
    /// Page Under Test (PUT)
    /// </typeparam>
    /// <remarks>
    /// The PUT is initialised before each test is run.
    /// The test host is spawned before all tests are run in the test collection.
    /// </remarks>
    public abstract class BlazorServerTest<THost, TPage, THostFixture> : IClassFixture<THostFixture>, IDisposable
        where THost : TestHost, new()
        where TPage : BlazorServerPage, new()
        where THostFixture : BlazorServerFixture<THost, TPage>
    {
        /// <summary>The Page Under Test (PUT)</summary>
        protected BlazorServerPage _Page;

        /// <summary>The in-process test host</summary>
        protected THostFixture _Fixture;

        /// <summary>
        /// Factory method to allow service dependencies to be injected into the Page Under Test (PUT)
        /// </summary>
        protected abstract IServiceCollection CreatePageDependencies();

        #region Test Lifetime
        #region Setup
        /// <summary>
        /// Initialise the dependencies for the Page Under Test (PUT)
        /// </summary>
        public BlazorServerTest(THostFixture fixture)
        {
            _Fixture = fixture;
            _Page = _Fixture.CreatePageFromServices(CreatePageDependencies);
        }
        #endregion

        #region Teardown
        private bool _disposed = false;

        /// <summary>
        /// Dispose the Page Under Test (PUT)
        /// Triggered after every test
        /// </summary>
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _Page.Dispose();
            }

            _disposed = true;
        }
        #endregion
        #endregion
    }
}