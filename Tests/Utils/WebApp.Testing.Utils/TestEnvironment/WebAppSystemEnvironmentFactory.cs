using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using WebApp.Testing.Functional.Abstractions;


namespace WebApp.Testing.Functional.SystemEnvironment
{
    /// <summary>
    /// Helper class to set secret environment variables from .env file
    /// </summary>
    /// <remarks>
    /// Considered using user-secrets, however would like to run tests 
    /// in containers for use in CI pipelines, e.g. GitLab, Azure etc.
    /// </remarks>
    public class WebAppSystemEnvironmentFactory : SystemEnvironmentFactory
    {
        protected override EnvironmentContext EnvironmentContext { get; set; }
        public WebAppSystemEnvironmentFactory() : base()
        {
            // determine .env file path based upon the location of this assembly
            string location = Path.GetDirectoryName(Assembly.GetAssembly(typeof(WebAppSystemEnvironmentFactory)).Location);
            string dotEnvFilePath = Path.Combine(location, @"../../../../../../Docker/.env");

            EnvironmentContext = new EnvironmentContext(
                dotEnvFilePath,
                new HashSet<string> {
                    "MQTT_USER",
                    "MQTT_PASSWORD",
                    "MINIO_USER",
                    "MINIO_PASSWORD"
                },
                new HashSet<string> {
                    "S3Settings__AccessKey",
                    "S3Settings__SecretKey",
                    "MqttSettings__Password",
                    "MqttSettings__UserName"
                },
                SetupEnvironmentVariable
            );
        }

        /// <summary>
        /// Delegate used to set an environment variable to a variable loaded from .env file
        /// </summary>
        /// <param name="">Variable from .env file</param>
        /// <param name="">Key value dictionary of environment variables</param>
        protected void SetupEnvironmentVariable(string envVar, IDictionary<string, string> dotEnvVars)
        {
            const string MqttUserDotEnv = "MQTT_USER";
            const string MqttUserEnv = "MqttSettings__UserName";
            const string MqttPasswordDotEnv = "MQTT_PASSWORD";
            const string MqttPasswordEnv = "MqttSettings__Password";
            const string MinioUserDotEnv = "MINIO_USER";
            const string MinioUserEnv = "S3Settings__AccessKey";
            const string MinioPasswordEnv = "S3Settings__SecretKey";
            const string MinioPasswordDotEnv = "MINIO_PASSWORD";

            switch (envVar)
            {
                case MinioPasswordEnv:
                    {
                        Environment.SetEnvironmentVariable(MinioPasswordEnv, dotEnvVars[MinioPasswordDotEnv]);
                        break;
                    }
                case MinioUserEnv:
                    {
                        Environment.SetEnvironmentVariable(MinioUserEnv, dotEnvVars[MinioUserDotEnv]);
                        break;
                    }
                case MqttPasswordEnv:
                    {
                        Environment.SetEnvironmentVariable(MqttPasswordEnv, dotEnvVars[MqttPasswordDotEnv]);
                        break;
                    }
                case MqttUserEnv:
                    {
                        Environment.SetEnvironmentVariable(MqttUserEnv, dotEnvVars[MqttUserDotEnv]);
                        break;
                    }
            }
        }

    }

}