﻿using System;
using System.IO;
using System.Reflection;


namespace WebApp.Testing.Fixtures
{
    /// <summary>
    /// Allow json files to be read from an assembly
    /// </summary>
    public class EmbeddedJsonFileResources
    {
        /// <summary>
        /// This Assembly
        /// </summary>
        public Assembly Assembly { get; set; }


        /// <summary>
        /// Initialise Assembly to default to this assembly
        /// </summary>
        public EmbeddedJsonFileResources()
        {
            Assembly = Assembly.GetAssembly(typeof(EmbeddedJsonFileResources));
        }



        /// <summary>
        /// Try and retrieve json file as stream from WebApp.Testing.Utils assembly
        /// </summary>
        /// <param name="resourceFileName">The path to the json resource</param>
        /// <exception cref="ArgumentException">
        /// Thrown when failed to locate the resource within the currently exeucting 
        /// assembly
        /// </exception>
        /// <remarks>The client is reponsible for disposing and closing the stream</remarks>
        public Stream GetStream(string resourceFileName)
        {
            Stream stream = Assembly.GetManifestResourceStream(resourceFileName);

            if (stream is null)
            {
                throw new ArgumentException("Failed to find " + resourceFileName + " in test assembly");
            }

            return stream;
        }


        /// <summary>
        /// Retrieve a json file resource from an assembly
        /// </summary>
        /// <param name="resourceFileName">Full path to resource in stream</param>
        /// <param name="assembly">Assembly that holds the resource</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown when <paramref name="resourceFileName"/> or <paramref name="assembly"/>
        /// is null
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Thrown when failed to find the json resource within the assembly
        /// </exception>
        /// <remarks>The client is reponsible for disposing and closing the stream</remarks>
        public Stream GetStream(string resourceFileName, Assembly assembly)
        {
            Assembly source = assembly ?? throw new ArgumentNullException(nameof(assembly));
            string jsonFile = resourceFileName ?? throw new ArgumentNullException(nameof(resourceFileName));

            Stream stream = source.GetManifestResourceStream(jsonFile);

            if (stream is null)
            {
                throw new ArgumentException("Failed to find " + resourceFileName + " in test assembly");
            }

            return stream;
        }
    }
}

