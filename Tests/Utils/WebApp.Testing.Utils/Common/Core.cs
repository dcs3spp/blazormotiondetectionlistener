using System;
using System.IO;
using System.Threading.Tasks;

using Autofac;
using Autofac.Builder;
using Autofac.Extensions.DependencyInjection;
using Extensions.Hosting.AsyncInitialization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging.Abstractions;

using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Kafka.AutofacModule;
using WebApp.Kafka.Admin;
using WebApp.Kafka.Config;
using WebApp.Mqtt.Config;
using WebApp.Realtime.SignalR.Client;
using WebApp.S3.Config;


namespace WebApp.Testing.Fixtures
{
    /// <summary>
    /// Core helper testing utilities
    /// </summary>
    public class CoreTestUtils
    {
        /// <summary>
        /// Create a configuration object from appsettings.json file and environmental variables
        /// </summary>
        /// <param name="basePath">Base path for json file</param>
        /// <param name="fileName">Name of the json file</param>
        /// <param name="optional">True when json config file is mandatory</param>
        public static IConfigurationRoot GetIConfigurationRoot(string basePath, string fileName = "appsettings.json", bool optional = true)
        {
            if (basePath == null)
                throw new ArgumentNullException(nameof(basePath));

            string fullPath = Path.Combine(basePath, $"{fileName}");
            return new ConfigurationBuilder()
                .AddJsonFile(fullPath, optional: optional)
                .AddEnvironmentVariables()
                .Build();
        }

        /// <summary>
        /// Configure services for logging, signalR and config options
        /// </summary>
        /// <param name="configBasePath">Base path for json file</param>
        /// <param name="fileName">Filename for json file, defaults to 'appsettings'</param>
        /// <param name="optional">True when json config file is mandatory</param>
        /// <returns>Populated service collection</returns>
        public static IServiceCollection ConfigureServices(string configBasePath, string fileName = "appsettings.json", bool optional = true)
        {
            if (configBasePath == null)
                throw new ArgumentNullException(nameof(configBasePath));

            var services = new ServiceCollection();

            // Add configuration from appsettings.json
            IConfigurationRoot _configuration = CoreTestUtils.GetIConfigurationRoot(configBasePath, fileName, optional);
            services.Configure<KafkaConfig>(options =>
                _configuration.GetSection(KafkaConfig.SectionName).Bind(options));
            services.Configure<S3Config>(options =>
                _configuration.GetSection(S3Config.SectionName).Bind(options));
            services.Configure<MqttConfig>(options =>
                _configuration.GetSection(MqttConfig.SectionName).Bind(options));

            services.AddSignalR(o => o.EnableDetailedErrors = true)
               .AddJsonProtocol(options =>
               {
                   options.PayloadSerializerOptions = JsonConvertersFactory.CreateDefaultJsonConverters(
                       new NullLogger<MotionDetectionConverter>(),
                       new NullLogger<MotionInfoConverter>(),
                       new NullLogger<JsonVisitor>());
               });

            services.AddSingleton<IHubProxyFactory, HubProxyFactory>();

            // Populate service provider with usual services
            services.AddLogging();
            // services.AddSignalR();
            services.AddOptions();
            services.AddSingleton<IKafkaAdminFactory, KafkaAdminFactory>();
            services.AddAsyncInitializer<KafkaAdminService>();

            return services;
        }

        /// <summary>
        /// Create topic on Kafka cluster based on settings in config file
        /// </summary>
        /// <param name="configBasePath">Base path where config file exists</param>
        /// <param name="fileName">The config file that exists in the base path</param>
        /// <param name="optional">True when json config file is mandatory</param>
        /// <exception cref="ArgumentNullException">Thrown if KafkaAdmin service failed to be resolved</exception>
        public static async Task CreateKafkaTopic(string configBasePath, string fileName = "appsettings.json", bool optional = true)
        {
            var services = ConfigureServices(configBasePath, fileName, optional);

            using (var container = CoreTestUtils.ConfigureContainer(services, new Autofac.Module[] { new KafkaModule() }))
            {
                using (var scope = container.BeginLifetimeScope())
                {
                    var kafkaSvc = scope.Resolve<IAsyncInitializer>() ?? throw new ArgumentNullException("Failed to resolve KafkaAdminService");
                    await kafkaSvc.InitializeAsync();
                }
            }
        }

        /// <summary>
        /// Build Autofac container with the modules and services provided
        /// </summary>
        /// <param name="services">Services</param>
        /// <param name="modules">List of Autofac modules</param>
        /// <returns>Populated service collection</returns>
        public static IContainer ConfigureContainer(IServiceCollection services, Autofac.Module[] modules)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (modules == null)
                throw new ArgumentNullException(nameof(modules));

            var containerBuilder = new ContainerBuilder();

            foreach (var module in modules)
            {
                containerBuilder.RegisterModule(module);
            }

            containerBuilder.Populate(services);

            return containerBuilder.Build(ContainerBuildOptions.IgnoreStartableComponents);
        }
    }
}
