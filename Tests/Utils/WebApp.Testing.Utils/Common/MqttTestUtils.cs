using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;


namespace WebApp.Mqtt.Testing.Helpers
{
    public class MqttTestUtils
    {
        /// <summary>Create a MqttConfig object</summary>
        /// <returns>Valid MqttConfig</returns>
        public static MqttConfig CreateMqttConfigWithoutTLS()
        {
            MqttConfig config = new MqttConfig();

            config.Host = "localhost";
            config.Port = 1883;
            config.Topic = "shinobi/group/monitor/trigger";
            config.Password = "Pa55w0rd";
            config.UserName = "abcdefg12345";

            return config;
        }

        /// <summary>Create a MqttConfig object with Tls property set</summary>
        /// <param name="caPath">Path to X509 certificate</param>
        /// <returns>Valid MqttConfig with Tls property set</returns>
        public static MqttConfig CreateMqttConfigWithTLS(string caPath)
        {
            MqttConfig config = CreateMqttConfigWithoutTLS();
            config.Tls = new MqttTls();
            config.Tls.CACerts = caPath;

            return config;
        }

        /// <summary>Convert MqttConfig to ManagedMqttClient options</summary>
        /// <param name="config">Config for Mqtt service</param>
        /// <returns>MQTTnet configuration options <see cref="IManagedMqttClientOptions"/></returns>
        public static IManagedMqttClientOptions ToManagedClientOptions(MqttConfig config)
        {
            var opts = new ManagedMqttClientOptions()
            {
                AutoReconnectDelay = TimeSpan.FromSeconds(5),
                ClientOptions = new MqttClientOptions()
                {
                    Credentials = new MqttClientCredentials() { Username = config.UserName, Password = Encoding.UTF8.GetBytes(config.Password) },
                    ChannelOptions = new MqttClientTcpOptions()
                    {
                        Server = config.Host,
                        Port = config.Port,
                        TlsOptions = new MqttClientTlsOptions()
                    }
                }
            };

            if (config.Tls != null)
            {
                using (var cert = X509Certificate.CreateFromCertFile(config.Tls.CACerts))
                    ToTlsOptions(opts.ClientOptions.ChannelOptions.TlsOptions, config, cert);
            }
            return opts;
        }

        /// <summary>Update MqttClientTlsOptions with properties from Mqtt Config</summary>
        /// <param name="options">Mqttnet Tls options</param>
        /// <param name="config">Config for Mqtt service</param>
        /// <param name="cert">X509 certificate to use with Tls</param>
        public static void ToTlsOptions(MqttClientTlsOptions options, MqttConfig config, X509Certificate cert)
        {
            options.AllowUntrustedCertificates = true;
            options.CertificateValidationHandler = (context) => true;
            options.UseTls = true;

#if WINDOWS_UWP
            options.Certificates = new List<byte[]>
            { cert.Export(X509ContentType.Cert) };
#else
            options.Certificates = new List<X509Certificate>
            {
                new X509Certificate(cert.Export(X509ContentType.Cert))
            };
#endif
        }

        /// <summary>Dispose and remove each certificate from list</summary>
        /// <param name="certificates">List containing certificates</param>
        public static void DisposeCertificates(IList<X509Certificate> certificates)
        {
            while (certificates.Count > 0)
            {
                X509Certificate cert = certificates[0];
                certificates.RemoveAt(0);
                cert.Dispose();
            }
        }
    }
}
