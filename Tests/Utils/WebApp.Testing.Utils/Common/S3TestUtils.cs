using WebApp.S3.Config;


namespace WebApp.Testing.Fixtures
{
    public class S3TestUtils
    {
        /// <summary>Create a default S3Config object</summary>
        /// <returns>Default S3Config</returns>
        /// <code>
        /// var s3Config = new S3Config() {
        ///     AccessKey = "accesskey",
        ///     Bucket = "bucket",
        ///     Endpoint = "localhost:9000",
        ///     SecretKey = "Pa55w0rd"   
        /// }
        /// </code>
        public static S3Config CreateDefaultS3Config()
        {
            S3Config config = new S3Config();

            config.AccessKey = "accessKey";
            config.Bucket = "bucket";
            config.Endpoint = "localhost:9000";
            config.SecretKey = "Pa55w0rd";

            return config;
        }
    }
}