using System;

using WebApp.Testing.Functional.Abstractions;


namespace WebApp.FunctionalTests.Pages
{
    /// <summary> 
    /// BUnit test context for Index Blazor Server page
    /// Most Blazor server pages have injected dependencies.
    /// This class allows those dependencies to be registered. 
    /// </summary>
    /// <remarks>
    /// Injecting the dependencies from a class derived from <see cref="TestHost"/>
    /// </remarks>
    public class IndexPage : BlazorServerPage, IDisposable
    {
        #region Constructor
        /// <summary>
        /// Constructor that initialise Blazor server page test context
        /// </summary>
        public IndexPage() : base()
        {
        }
        #endregion


        #region Dispose
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        #endregion
    }
}
