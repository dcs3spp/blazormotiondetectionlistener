namespace WebApp.FunctionalTests
{
    public static class ApiRoutes 
    {
        public static class Get
        {
            public static string Images = "api";

            public static string Image(string id)
            {
                return $"api/{id}";
            }
        }
    }
}
