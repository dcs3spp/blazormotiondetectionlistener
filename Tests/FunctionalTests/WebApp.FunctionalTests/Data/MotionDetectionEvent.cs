using System.Reflection;
using System.IO;

using Microsoft.Extensions.Logging.Abstractions;
using MQTTnet;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Data;
using WebApp.Data.Serializers;
using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Data.Serializers.Json;
using WebApp.Testing.Fixtures;


namespace WebApp.FunctionalTests.Data
{
    /// <summary>
    /// Helper class that loads a motion detection event from test fixture
    /// and deserializes into POCO <see cref="MotionDetection"/> class
    /// for test assertions
    /// </summary>
    public sealed class MotionDetectionEvent
    {
        private const string MotionResourceFile = @"WebApp.Testing.Utils.TestData.motionImg.json";

        public ManagedMqttApplicationMessage Message { get; private set; }
        public MotionDetection MotionDetection { get; private set; }

        #region Constructor
        public MotionDetectionEvent()
        {
            LoadMotionDetectionEvent();
        }
        #endregion


        /// <summary>Create an mqtt message from stream</summary>
        private ManagedMqttApplicationMessage CreateMessageFromStream(MemoryStream stream)
        {
            const string Topic = "shinobi/group/monitor/trigger";

            var message = new MqttApplicationMessageBuilder()
                .WithTopic(Topic)
                .WithPayload(stream.ToArray())
                .Build();

            return new ManagedMqttApplicationMessageBuilder()
                .WithApplicationMessage(message)
                .Build();
        }

        private void LoadMotionDetectionEvent()
        {
            const int StreamBufferSize = 2048;

            var assemblyResource = new EmbeddedJsonFileResources();
            var opts = JsonConvertersFactory.CreateDefaultJsonConverters(new NullLogger<MotionDetectionConverter>(), new NullLogger<MotionInfoConverter>(), new NullLogger<JsonVisitor>());
            var deserializer = new MotionDetectionSerializer<MotionDetection>(opts.Converters, new JsonSerializerWrapper());

            using (var stream = assemblyResource.GetStream(MotionResourceFile, Assembly.GetAssembly(typeof(EmbeddedJsonFileResources))))
            {
                using (var memory = new MemoryStream(StreamBufferSize))
                {
                    stream.CopyTo(memory);
                    Message = CreateMessageFromStream(memory);
                    RewindStream(memory);
                    MotionDetection = deserializer.FromJSON(memory);
                }
            }
        }

        private void RewindStream(Stream stream)
        {
            stream.Position = 0;
        }
    }
}