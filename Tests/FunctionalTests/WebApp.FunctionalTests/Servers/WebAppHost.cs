using System.Net.Http;

using Autofac;
using Autofac.Extensions.DependencyInjection;
using MQTTnet.Extensions.ManagedClient;
using WebApp.Testing.Functional.Abstractions;
using WebApp.Testing.Functional.SystemEnvironment;
using WebApp.Mqtt.Contracts;
using WebApp.S3.Contracts;


namespace WebApp.FunctionalTests.Server
{
    /// <summary>
    /// Concrete class for WebApp self hosting environment
    /// Intended for use in functional test environments
    /// Provides properties for accessing Mqtt and S3 clients
    /// </summary
    /// <remarks>
    /// The base class starts and stops the server for us.
    /// The base class disposes of the host for us and all 
    /// associated services
    /// </remarks>
    public class WebAppHost : AutofacTestHost<Startup>
    {
        /// <summary>
        /// Client for MQTT broker
        /// </summary>
        public IManagedMqttClient MqttClient { get; private set; }

        /// <summary>
        /// S3 Service
        /// </summary>
        public IS3Service S3Client { get; private set; }


        /// <summary>
        /// HttpClientFactory
        /// </summary>
        public IHttpClientFactory HttpClientFactory { get; private set; }


        /// <summary>
        /// Construct individual service properties
        /// </summary>
        public WebAppHost() : base()
        {
            var root = this.Server.Services.GetAutofacRoot();

            MqttClient = BaseScope.Resolve<IMqttService>().Client;
            S3Client = BaseScope.Resolve<IS3Service>();
            HttpClientFactory = BaseScope.Resolve<IHttpClientFactory>();
        }

        /// <summary>
        /// Subclasses override to initialise system environment
        /// variables from .env file or system environment variables
        /// if running within a container
        /// </summary>
        protected sealed override void LoadSystemEnvironment()
        {
            WebAppSystemEnvironmentFactory factory = new WebAppSystemEnvironmentFactory();
            factory.LoadEnvironmentVariables();
        }
    }
}