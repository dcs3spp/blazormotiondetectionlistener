using WebApp.Testing.Functional.Abstractions;
using WebApp.FunctionalTests.Data;
using WebApp.FunctionalTests.Server;


namespace WebApp.FunctionalTests.Fixtures
{
    /// <summary>
    /// Derived from base class structure to add fixture data, a motion detection event
    /// </summary>
    public class WebAppFixture : BlazorServerFixture<WebAppHost, BlazorServerPage>
    {
        /// <summary>
        /// Motion detection event meta-data loaded from json held resource stream
        /// Allows access to POCO object and MQTT message ready to publish
        /// </summary>
        /// <remarks>
        /// By storing the motion detection event in the test fixture 
        /// it is only being initialised once
        /// </remarks>
        public MotionDetectionEvent Data { get; private set; }

        /// <summary>
        /// Initialise motion detection POCO and MQTT message from json stored in
        /// resource stream
        /// </summary>
        public WebAppFixture() : base()
        {
            Data = new MotionDetectionEvent();
        }
    }
}
