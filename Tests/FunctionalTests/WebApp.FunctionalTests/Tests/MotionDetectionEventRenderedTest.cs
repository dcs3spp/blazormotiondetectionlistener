using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Autofac;
using Bunit;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Xunit;

using WebApp.Data;
using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.FunctionalTests.Fixtures;
using WebApp.Realtime.SignalR.Client;
using WebApp.Repository;
using WebApp.Repository.Contracts;
using WebApp.Testing.Mocks.Blazor;

using WebApp.Testing.Functional.Abstractions;
using WebApp.FunctionalTests.Server;


namespace WebApp.FunctionalTests
{
    public class MotionDetectionEventRenderedTest : BlazorServerTest<WebAppHost, BlazorServerPage, WebAppFixture>
    {
        #region Test lifecycle
        /// <summary>
        /// Let the base test class initialise service dependencies for the Page Under Test (PUT)
        /// via <see cref="CreateServiceDependencies"/>
        /// </summary>
        public MotionDetectionEventRenderedTest(WebAppFixture fixture) : base(fixture) { }

        /// <summary>
        /// Resolve page dependencies from host and add into Bunit test context
        /// </summary>
        /// <remarks>
        /// Blazor server uses RemoteNavigationManager
        /// https://github.com/dotnet/aspnetcore/tree/47a299160522100cdfa716926bbc8f6318c7a2da/src/Components/Server/src/Circuits
        /// as a subclassed <see cref="NavigationManager"/> instance.
        /// This also implements <see cref="IHostEnvironmentNavigationManager"/> interface to initialise baseUri and uri
        ///
        /// Unfortunately if we add BlazorServerPages, RazorPages and Logging dependencies to the Bunit Component Under Test (CUT), 
        /// and then add IConfiguration from the self host, there is an error thrown that RemoteNavigationManager failed to initialise.
        ///
        /// Unfortunately RemoteNavigationManager is an internal class, so we cannot inject it into CUT service dependencies or
        /// resolve it from self-host and cast as RemoteNavigationManager to enable calling Initialise method.
        /// Have tried resolving NavigationManager instance from self host and adding it to the CUT service dependencies
        /// The initialisation exception still occurs.
        ///
        /// For this reason, currently adding a mocked <see cref="NavigationManager"/> to initialise baseUrl and Uri unless
        /// anyone knows how to use an instance resolved from the self host container???
        /// <remarks>
        protected override IServiceCollection CreatePageDependencies()
        {
            var dependencies = new ServiceCollection();
            var serverUrl = _Fixture.Host.ServerUrl.ToString();

            dependencies.AddSingleton<IHubProxyFactory>(_Fixture.Host.BaseScope.Resolve<IHubProxyFactory>());
            dependencies.AddScoped<ILogger<MotionDetectionConverter>>(sp => _Fixture.Host.BaseScope.Resolve<ILogger<MotionDetectionConverter>>());
            dependencies.AddScoped<ILogger<WebApp.Pages.Index>>(sp => _Fixture.Host.BaseScope.Resolve<ILogger<WebApp.Pages.Index>>());
            dependencies.AddScoped<ILogger<MotionInfoConverter>>(sp => _Fixture.Host.BaseScope.Resolve<ILogger<MotionInfoConverter>>());
            dependencies.AddScoped<ILogger<JsonVisitor>>(sp => _Fixture.Host.BaseScope.Resolve<ILogger<JsonVisitor>>());
            dependencies.AddScoped<ILogger<MotionDetectionRepository>>(sp => _Fixture.Host.BaseScope.Resolve<ILogger<MotionDetectionRepository>>());
            dependencies.AddScoped<IMotionDetectionRepository>(sp => _Fixture.Host.BaseScope.Resolve<IMotionDetectionRepository>());
            dependencies.AddScoped<NavigationManager>(sp => new MockNavigationManager(serverUrl, serverUrl));

            return dependencies;
        }
        #endregion

        #region Tests
        [Fact]
        public async Task MotionDetectionEvent_Renders_Image_On_Dashboard()
        {
            const short WaitTimeSeconds = 15;

            // render the dashboard
            var cut = _Page.PageContext.RenderComponent<WebApp.Pages.Index>();

            // trigger mqtt event to start producer
            await _Fixture.Host.MqttClient.PublishAsync(this._Fixture.Data.Message);

            // give signalR on page some time to receive the motion detection notification
            var item = await WaitForMotionDetection(WaitTimeSeconds, cut.Instance);

            // assert that associated image is in S3 storage
            await AssertImageWasUploadedToS3Storage(item.Details.Img);

            // assert that event is rendered on dashboard page
            cut.WaitForAssertion(() =>
            {
                var cardContent = cut.Find("[class=mat-card-content]");

                Assert.Contains(item.Group, cardContent.Children[0].TextContent.Trim());
                Assert.Contains(item.MonitorId, cardContent.Children[1].TextContent.Trim());
                Assert.Contains(item.TimeReceivedUTC.ToString(), cardContent.NextElementSibling.TextContent.Trim());
            });
        }

        [Fact]
        public async Task ApiController_Downloads_Image()
        {
            const string ObjectName = "objectName";
            const string TestData = "test";

            byte[] Payload = System.Text.Encoding.UTF8.GetBytes(TestData);

            using (var byteStream = new MemoryStream(Payload))
            using (var stream = new BufferedStream(byteStream))
            {
                await _Fixture.Host.S3Client.UploadAsync(stream, ObjectName);
                var client = _Fixture.Host.HttpClientFactory.CreateClient("webappTest");
                client.BaseAddress = _Fixture.Host.ServerUrl;
                var response = await client.GetAsync(ApiRoutes.Get.Image(ObjectName));
                response.EnsureSuccessStatusCode();

                string result = await response.Content.ReadAsStringAsync();

                Assert.Equal(TestData, result);
            }
        }

        [Fact]
        public async Task WebApp_ApiController_DownloadImage_Throws_NotFound_For_Unknown_Image()
        {
            const string ObjectName = "unrecognised";

            var client = _Fixture.Host.HttpClientFactory.CreateClient("webappTest");
            client.BaseAddress = _Fixture.Host.ServerUrl;

            var response = await client.GetAsync(ApiRoutes.Get.Image(ObjectName));
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
        #endregion

        #region Test Helpers

        private MotionDetection AssertPageRepositoryReceivedMotionDetectionEvent()
        {
            const short expectedEventCount = 1;

            MotionDetection item = null;
            var repository = _Page.PageContext.Services.GetService<IMotionDetectionRepository>();
            var iterator = repository.GetEnumerator();

            var count = 0;
            while (iterator.MoveNext())
            {
                count++;
                item = iterator.Current as WebApp.Data.MotionDetection;
            }

            Assert.Equal(expectedEventCount, count);
            Assert.Equal(_Fixture.Data.MotionDetection.Group, item.Group);
            Assert.Equal(_Fixture.Data.MotionDetection.MonitorId, item.MonitorId);
            Assert.Equal(_Fixture.Data.MotionDetection.Plug, item.Plug);
            Assert.NotEqual(_Fixture.Data.MotionDetection.Details.Img, item.Details.Img);
            Assert.Equal(_Fixture.Data.MotionDetection.Details.ImgHeight, item.Details.ImgHeight);
            Assert.Equal(_Fixture.Data.MotionDetection.Details.ImgWidth, item.Details.ImgWidth);
            Assert.Equal(_Fixture.Data.MotionDetection.Details.Matrices, item.Details.Matrices);
            Assert.Equal(_Fixture.Data.MotionDetection.Details.Name, item.Details.Name);
            Assert.Equal(_Fixture.Data.MotionDetection.Details.Plug, item.Details.Plug);
            Assert.Equal(_Fixture.Data.MotionDetection.Details.Reason, item.Details.Reason);

            return item;
        }

        private async Task AssertImageWasUploadedToS3Storage(string img)
        {
            using (var bufferedImg = await _Fixture.Host.S3Client.DownloadObject(img))
            using (var memory = new System.IO.MemoryStream())
            {
                await bufferedImg.CopyToAsync(memory);
                Assert.Equal(Convert.FromBase64String(_Fixture.Data.MotionDetection.Details.Img), memory.ToArray());
            }
        }

        private async Task<MotionDetection> WaitForMotionDetection(short timeoutSeconds, WebApp.Pages.Index page)
        {
            const short ExpectedCount = 1;

            var cts = new CancellationTokenSource(TimeSpan.FromSeconds(timeoutSeconds));

            var task = Task.Run(() => WaitForMotionDetection(cts.Token, ExpectedCount, page));
            if (await Task.WhenAny(task, Task.Delay(TimeSpan.FromSeconds(timeoutSeconds), cts.Token)) == task)
            {
                Console.WriteLine($"Successfully acquired a motion detection from the repository within timeout period of {timeoutSeconds}");
                return await task;
            }
            else
            {
                throw new TimeoutException();
            }
        }

        private async Task<MotionDetection> WaitForMotionDetection(CancellationToken token, short expectedCount, WebApp.Pages.Index page)
        {
            MotionDetection item = null;

            var count = 0;

            while (!token.IsCancellationRequested && count != expectedCount)
            {
                // var repository = _Page.PageContext.Services.GetService<IMotionDetectionRepository>();
                var repository = page.Repository;
                var iterator = repository.GetEnumerator();

                while (iterator.MoveNext())
                {
                    count++;
                    item = iterator.Current as WebApp.Data.MotionDetection;
                }

                await Task.Delay(1000, token); // Thread.Sleep(1000);
            }

            return item;
        }
        #endregion
    }
}
