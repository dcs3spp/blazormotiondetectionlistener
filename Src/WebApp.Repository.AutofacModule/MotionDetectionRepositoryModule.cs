﻿using System.Reflection;

using Autofac;

using WebApp.Data.Serializers.AutofacModule;


namespace WebApp.Repository.AutofacModule
{
    /**
        This module is dependent on WebApp.Data.Serializers.AutofacModule
        This is the entry point module that will be used in the blazor app
     */
    public class MotionDetectionRepositoryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly repository = typeof(MotionDetectionRepository).GetTypeInfo().Assembly;

            builder.RegisterModule(new SerializerModule());

            builder.RegisterAssemblyTypes(repository)
                   .AsImplementedInterfaces();
        }
    }
}
