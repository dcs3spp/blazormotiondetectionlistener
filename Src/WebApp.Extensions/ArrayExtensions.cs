﻿using System;


namespace WebApp.Extensions
{
    public static class ArrayExtensions
    {
        /// <summary>
        /// Extension method for determining if an array is null or empty
        /// </summary>
        /// <param name="array">The array to check</param>
        /// <returns>True if array null or empty, otherwise false</returns>
        public static bool IsNullOrEmpty<T>(this T[] array)
        {
            return array == null || array.Length == 0;
        }
    }
}
