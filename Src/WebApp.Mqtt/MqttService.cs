using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Contracts;


namespace WebApp.Mqtt
{
    public class MqttService : IMqttService, IMqttClientConnectedHandler, IConnectingFailedHandler, IDisposable
    {
        #region Properties
        public IManagedMqttClient Client { get; }
        public Func<MqttApplicationMessageReceivedEventArgs, Task> MessageReceived { get; set; }
        #endregion

        #region State
        private MqttConfig _Config { get; }
        private bool _Disposed { get; set; }
        private IManagedMqttClientOptions _MqttOptions { get; }
        private ILogger<MqttService> _Logger { get; }
        #endregion


        #region Constructor
        /// <summary>Create MQTTnet managed client configured with options from config file</summary>
        /// <param name="config">Options from config file</param>
        /// <param name="factory">Factory used to create MQTTnet managed client</param>
        /// <param name="logger">Logger instance</param>
        public MqttService(
            IOptions<MqttConfig> config,
            IMQTTClientFactory factory,
            ILogger<MqttService> logger
        )
        {
            _Config = config.Value ?? throw new ArgumentNullException(nameof(config));
            _Logger = logger ?? throw new ArgumentNullException(nameof(logger));

            var result = factory.CreateMQTTnetManagedClient(_Config);
            Client = result.client;
            _MqttOptions = result.options;

            Client.ConnectingFailedHandler = this;
            Client.UseConnectedHandler(this.HandleConnectedAsync);
            Client.UseDisconnectedHandler(this.HandleDisconnectedAsync);
            Client.UseApplicationMessageReceivedHandler(this.OnMessageReceived);

            _Logger.LogInformation($"Mqtt Settings :: mqtt://{config.Value.UserName}:****@{config.Value.Host}:{config.Value.Port}");
            _Logger.LogInformation($"Mqtt Topic :: {_Config.Topic}");
        }
        #endregion


        #region IManagedMqttClient event handlers
        public async Task HandleConnectingFailedAsync(ManagedProcessFailedEventArgs eventArgs)
        {
            _Logger.LogError(eventArgs.Exception, "Failed to connect to MQTT Broker");

            await Task.CompletedTask;
        }

        public async Task HandleConnectedAsync(MqttClientConnectedEventArgs eventArgs)
        {
            _Logger.LogInformation("MQTT managed client connected");

            await Task.CompletedTask;
        }

        public async Task HandleDisconnectedAsync(MqttClientDisconnectedEventArgs eventArgs)
        {
            _Logger.LogInformation("Mqtt managed client disconnected");

            await Task.CompletedTask;
        }

        protected virtual async Task OnMessageReceived(MqttApplicationMessageReceivedEventArgs args)
        {
            var handler = MessageReceived;
            await handler?.Invoke(args);
        }
        #endregion


        #region MqttClient interface
        /// <summary>Subscribe to topic filter and start listening for messages</summary>
        public async Task StartAsync()
        {
            var filters = new List<MqttTopicFilter>()
                {
                    new MqttTopicFilterBuilder().WithTopic(_Config.Topic).Build()
                };
            await Client.SubscribeAsync(filters.ToArray());
            await Client.StartAsync(_MqttOptions);
        }

        /// <summary>
        /// Stop publishing, clear message queue and stop trying to maintain a connection
        /// </summary>
        public async Task StopAsync()
        {
            await Client.StopAsync();
        }
        #endregion


        #region Dispose

        /// <summary>
        /// Dispose of the MQTTnet managed client instance. If the managed client was configured with Tls
        /// then dispose and remove list of certificates from options
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_Disposed)
            {
                if (disposing)
                {
                    _Logger.LogInformation("Disposing Mqtt Client");
                    Client.Dispose();
#if !WINDOWS_UWP
                    if (_Config.Tls != null)
                    {
                        _Logger.LogInformation("Disposing Mqtt Certificates");
                        DisposeCertificates(_MqttOptions.ClientOptions.ChannelOptions.TlsOptions.Certificates);
                    }
#endif
                }

                _Disposed = true;
            }
        }

        /// <summary>Dispose and remove each certificate from list</summary>
        /// <param name="certificates">List containing certificates</param>
        private void DisposeCertificates(IList<X509Certificate> certificates)
        {
            while (certificates.Count > 0)
            {
                X509Certificate cert = certificates[0];
                certificates.RemoveAt(0);
                cert.Dispose();
            }
        }

        ~MqttService()
        {
            Dispose(false);
        }
        #endregion
    }
}