using System;
using System.Net.Security;

using Microsoft.Extensions.Logging;
using MQTTnet;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Contracts;


namespace WebApp.Mqtt.Factory
{
    public class MQTTClientFactory : IMQTTClientFactory
    {
        private ILogger<MQTTClientFactory> _Logger { get; set; }
        public MQTTClientFactory(ILogger<MQTTClientFactory> logger)
        {
            _Logger = logger;
        }

        /// <summary>
        /// Factory method to create an MQTTnet managed client with/without Tls depending on config 
        /// </summary>
        /// <param name="config">Options object from appsettings config file</param>
        /// <exception cref="System.ArgumentNullException">Throws when config parameter is null</exception>
        /// <returns>
        /// A Tuple consisting of the <see cref="IManagedMqttClient"/> client and <see cref="IManagedMqttClientOptions"/> options
        /// </returns>
        public (IManagedMqttClient client, IManagedMqttClientOptions options) CreateMQTTnetManagedClient(
            MqttConfig config)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            if (config.Tls != null)
                return this.CreateMQTTnetManagedClientTls(config, new MQTTnetManagedClientTlsOptionsBuilder());
            else
                return this.CreateMQTTnetManagedClient(config, new MQTTnetManagedClientOptionsBuilder());
        }

        /// <summary>
        /// Factory method to create an MQTTnet managed client withput Tls
        /// </summary>
        /// <param name="config">Options object from appsettings config file</param>
        /// <param name="builder">Builder to build MQTTnet options using appsettings config file</param>
        /// <exception cref="System.ArgumentNullException">Throws when a parameter is null</exception>
        /// <returns>
        /// A Tuple consisting of the <see cref="IManagedMqttClient"/> client and <see cref="IManagedMqttClientOptions"/> options
        /// </returns>
        public (IManagedMqttClient client, IManagedMqttClientOptions options) CreateMQTTnetManagedClient(
            MqttConfig config,
            IMQTTnetManagedClientOptionsBuilder builder)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            if (builder == null)
                throw new ArgumentNullException(nameof(builder));

            IMQTTnetManagedClientOptionsBuilder opts = builder.WithOptions(config);

            return (new MqttFactory().CreateManagedMqttClient(), opts.Build());
        }

        /// <summary>
        /// Factory method to create an TLS MQTTnet managed client with Tls
        /// </summary>
        /// <param name="config">Options object from appsettings config file</param>
        /// <param name="builder">Builder to build MQTTnet TLS options using appsettings config file</param>
        /// <exception cref="System.ArgumentNullException">Throws when a parameter is null</exception>
        /// <returns>
        /// A Tuple consisting of the <see cref="IManagedMqttClient"/> client and 
        /// <see cref="IManagedMqttClientOptions"/> options
        /// </returns>
        public (IManagedMqttClient client, IManagedMqttClientOptions options) CreateMQTTnetManagedClientTls(
            MqttConfig config,
            IMQTTnetManagedClientTlsOptionsBuilder builder)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            if (config.Tls == null)
                throw new ArgumentNullException(nameof(config.Tls));

            if (builder == null)
                throw new ArgumentNullException(nameof(builder));

            Func<MqttClientCertificateValidationCallbackContext, bool> certValidationHandler = (context) =>
            {
                _Logger.LogInformation($"SSL POLICY ERRORS {context.SslPolicyErrors.ToString()}");

                switch (context.SslPolicyErrors)
                {
                    case SslPolicyErrors.RemoteCertificateChainErrors:
                        {
                            foreach (var status in context.Chain.ChainStatus)
                            {
                                _Logger.LogInformation($"Status {status.Status}");
                                _Logger.LogInformation($"Status {status.StatusInformation}");
                            }
                            break;
                        }
                }
                return true;
            };
            builder = builder.WithOptions(config, certValidationHandler, new X509CertificateWrapper());

            MqttFactory factory = new MqttFactory();
            return (factory.CreateManagedMqttClient(), builder.Build());
        }
    }
}