using System.Security.Cryptography.X509Certificates;

using WebApp.Mqtt.Contracts;


namespace WebApp.Mqtt.Factory
{
    public class X509CertificateWrapper : IX509CertificateWrapper
    {
        public virtual X509Certificate CreateFromCertFile(string path)
        {
            return X509Certificate.CreateFromCertFile(path);
        }
    }
}