using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Contracts;


namespace WebApp.Mqtt.Factory
{
    /// <summary>
    /// Class that builds <see cref="IManagedMqttClientOptions"/> from <see cref="MqttConfig"/>
    /// with Tls configured
    /// </summary>
    public class MQTTnetManagedClientTlsOptionsBuilder : IMQTTnetManagedClientTlsOptionsBuilder
    {
        private ManagedMqttClientOptionsBuilder _managedClientBuilder = new ManagedMqttClientOptionsBuilder();
        private MqttClientOptionsBuilder _clientBuilder = new MqttClientOptionsBuilder();


        /// <summary>Add TLS options to existing configured client options</summary>
        /// <param name="config">Options from appsettings config file</param>
        /// <param name="certValidationHandler">Handler for determining if certificate is valid/></param>
        /// <param name="x509Factory=">Wrapper for X509Certificate to increase testablity</param>
        /// <exception cref="System.ArgumentNullException">Thrown when any parameter is null</exception>
        /// <returns>
        /// <see cref="IMQTTnetManagedClientOptionsBuilder"/> with TLS
        /// options populated from <see cref="MqttConfig"/>
        /// </returns>
        /// <example>
        /// This sample shows how to call the <see cref="WithOptions"/> method.
        /// <code>
        /// MQTTnetManagedClientOptionsBuilder builder = new MQTTManagedClientOptionsBuilder()
        /// builder.WithOtions(config, (context)=>return true, new X509CertificateWrapper()).Build()
        /// </code>
        /// </example>
        public IMQTTnetManagedClientTlsOptionsBuilder WithOptions(
            MqttConfig config,
            Func<MqttClientCertificateValidationCallbackContext, bool> certValidationHandler,
            IX509CertificateWrapper x509Factory)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            if (certValidationHandler == null)
                throw new ArgumentNullException(nameof(certValidationHandler));

            if (x509Factory == null)
                throw new ArgumentNullException(nameof(x509Factory));

            _clientBuilder.WithClientId(Guid.NewGuid().ToString())
                                    .WithCredentials(config.UserName, config.Password)
                                    .WithTcpServer(config.Host, config.Port);

            _managedClientBuilder.WithAutoReconnectDelay(TimeSpan.FromSeconds(5));

            var caCert = x509Factory.CreateFromCertFile(config.Tls.CACerts);

            _clientBuilder.WithTls(o =>
            {
                o.UseTls = true;
                o.AllowUntrustedCertificates = true;
                o.SslProtocol = SslProtocols.Tls12;
#if WINDOWS_UWP
                o.Certificates = new List<byte[]> { caCert.Export(X509ContentType.Cert) };
#else
                o.Certificates = new List<X509Certificate> { caCert };
#endif
                o.CertificateValidationHandler = certValidationHandler;
            });
            return this;
        }

        public IManagedMqttClientOptions Build()
        {
            return _managedClientBuilder.WithClientOptions(
                _clientBuilder.Build()
            )
            .Build();
        }
    }
}
