using System;

using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;
using WebApp.Mqtt.Contracts;


namespace WebApp.Mqtt.Factory
{
    /// <summary>
    /// Class that builds <see cref="IManagedMqttClientOptions"/> from <see cref="MqttConfig"/>
    /// </summary>
    public class MQTTnetManagedClientOptionsBuilder : IMQTTnetManagedClientOptionsBuilder
    {
        private ManagedMqttClientOptionsBuilder _managedClientBuilder = new ManagedMqttClientOptionsBuilder();
        private MqttClientOptionsBuilder _clientBuilder = new MqttClientOptionsBuilder();


        /// <summary>Build MQTTnet TLS client from <see cref="MqttConfig"/></summary>
        /// <param name="config"><see cref="MqttConfig"/> config instance</param>
        /// <returns>Builder instance with options updated</returns>
        public IMQTTnetManagedClientOptionsBuilder WithOptions(MqttConfig config)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            _clientBuilder.WithClientId(Guid.NewGuid().ToString())
                        .WithCredentials(config.UserName, config.Password)
                        .WithTcpServer(config.Host, config.Port);

            _managedClientBuilder.WithAutoReconnectDelay(TimeSpan.FromSeconds(5));

            return this;
        }

        /// <summary>Build MQTTnet Managed client options from config</summary>
        /// <returns><see cref="IManagedMqttClientOptions"/></returns>
        public IManagedMqttClientOptions Build()
        {
            return _managedClientBuilder.WithClientOptions(
                _clientBuilder.Build()
            )
            .Build();
        }
    }
}
