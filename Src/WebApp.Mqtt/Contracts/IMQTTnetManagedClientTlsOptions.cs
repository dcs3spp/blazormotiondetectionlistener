using System;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;


namespace WebApp.Mqtt.Contracts
{
    /// <summary>
    /// Builder to create <see cref="IManagedMqttTlsClientOptions"/> from <see cref="MqttConfig"/>
    /// for the MQTTnet library
    /// </summary>
    public interface IMQTTnetManagedClientTlsOptionsBuilder
    {
        /// <summary>Add TLS options to existing configured client options</summary>
        /// <param name="config">Options from appsettings config file</param>
        /// <param name="certValidationHandler">Handler for determining if certificate is valid/></param>
        /// <param name="x509Factory">Interface for X509Certificate wrapper to increase testablity</param>
        /// <returns>
        /// <see cref="IMQTTnetManagedClientTlsOptionsBuilder"/> with TLS
        /// options populated from <see cref="MqttConfig"/>
        /// </returns>
        IMQTTnetManagedClientTlsOptionsBuilder WithOptions(
            MqttConfig config,
            Func<MqttClientCertificateValidationCallbackContext, bool> certValidationHandler,
            IX509CertificateWrapper x509Factory
        );

        /// <summary>
        /// Return the <see cref="IManagedMqttClientOptions"/> managed
        /// by the builder.
        /// </summary>
        IManagedMqttClientOptions Build();
    }
}
