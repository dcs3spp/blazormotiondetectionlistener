using System;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;


namespace WebApp.Mqtt.Contracts
{
    /// <summary>
    /// Builder to create <see cref="IManagedMqttClientOptions"/> from <see cref="MqttConfig"/>
    /// for the MQTTnet library
    /// </summary>
    public interface IMQTTnetManagedClientOptionsBuilder
    {
        /// <summary>Populate standard options from MqttConfig</summary>
        /// <param name="config">Options from appsettings config file</param>
        /// <returns>
        /// <see cref="IMQTTnetManagedClientOptionsBuilder"/> with standard
        /// options populated from <see cref="MqttConfig"/>
        /// </returns>
        IMQTTnetManagedClientOptionsBuilder WithOptions(MqttConfig config);


        /// <summary>
        /// Return the <see cref="IManagedMqttClientOptions"/> managed
        /// by the builder.
        /// </summary>
        IManagedMqttClientOptions Build();
    }
}
