using MQTTnet.Extensions.ManagedClient;

using WebApp.Mqtt.Config;


namespace WebApp.Mqtt.Contracts
{

    /// <summary>
    /// Abstract factory to allow create MQTT clients for a variety of libraries
    /// </summary>
    public interface IMQTTClientFactory
    {
        /// <summary>
        /// Factory method to create an MQTTnet managed client with/without Tls depending on config 
        /// </summary>
        /// <param name="config">Config POCO object from appsettings config file</param>
        /// <returns>
        /// A Tuple consisting of the <see cref="IManagedMqttClient"/> client and <see cref="IManagedMqttClientOptions"/> options
        /// </returns>
        (IManagedMqttClient client, IManagedMqttClientOptions options) CreateMQTTnetManagedClient(
            MqttConfig config);

        /// <summary>
        /// Factory method to create an MQTTnet managed client 
        /// </summary>
        /// <param name="config">Options object from appsettings config file</param>
        /// <param name="builder">Builder to build MQTTnet options using appsettings config file</param>
        /// <returns>
        /// A Tuple consisting of the <see cref="IManagedMqttClient"/> client and <see cref="IManagedMqttClientOptions"/> options
        /// </returns>
        (IManagedMqttClient client, IManagedMqttClientOptions options) CreateMQTTnetManagedClient(
            MqttConfig config,
            IMQTTnetManagedClientOptionsBuilder builder);

        /// <summary>
        /// Factory method to create a TLS MQTTnet managed client 
        /// </summary>
        /// <param name="config">Options object from appsettings config file</param>
        /// <param name="builder">Builder to build MQTTnet TLS options using appsettings config file</param>
        /// <returns>
        /// A Tuple consisting of the <see cref="IManagedMqttClient"/> client and <see cref="IManagedMqttClientOptions"/> options
        /// </returns>
        (IManagedMqttClient client, IManagedMqttClientOptions options) CreateMQTTnetManagedClientTls(
                   MqttConfig config,
                   IMQTTnetManagedClientTlsOptionsBuilder builder);
    }
}
