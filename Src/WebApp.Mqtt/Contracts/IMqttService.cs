using System;
using System.Threading.Tasks;

using MQTTnet;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Extensions.ManagedClient;


namespace WebApp.Mqtt.Contracts
{
    public interface IMqttService : IDisposable
    {
        Func<MqttApplicationMessageReceivedEventArgs, Task> MessageReceived { get; set; }
        IManagedMqttClient Client { get; }
        Task HandleConnectingFailedAsync(ManagedProcessFailedEventArgs eventArgs);
        Task HandleConnectedAsync(MqttClientConnectedEventArgs eventArgs);
        Task HandleDisconnectedAsync(MqttClientDisconnectedEventArgs eventArgs);
        Task StartAsync();
        Task StopAsync();
    }
}
