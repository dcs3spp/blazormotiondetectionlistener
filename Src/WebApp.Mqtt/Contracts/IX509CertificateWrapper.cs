using System.Security.Cryptography.X509Certificates;


namespace WebApp.Mqtt.Contracts
{
    public interface IX509CertificateWrapper
    {
        /// <summary>Create a <see cref="X509Certificate"/>from file</summary>
        /// <param name="path">File path for certificate</param>
        /// <returns><see cref="X509Certificate"/> from file</returns>
        X509Certificate CreateFromCertFile(string path);
    }
}
