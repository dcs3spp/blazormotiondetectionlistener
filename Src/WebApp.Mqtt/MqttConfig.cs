﻿namespace WebApp.Mqtt.Config
{
    public class MqttTls
    {
        public string CACerts { get; set; }
    }

    public class MqttConfig
    {
        public const string SectionName = "MqttSettings";


        public string Host { get; set; }
        public int Port { get; set; }
        public string Topic { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public MqttTls Tls { get; set; }
    }
}
