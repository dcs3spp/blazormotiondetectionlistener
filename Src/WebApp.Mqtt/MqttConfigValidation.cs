using Microsoft.Extensions.Options;


namespace WebApp.Mqtt.Config
{
    public class MqttConfigValidation : IValidateOptions<MqttConfig>
    {
        public ValidateOptionsResult Validate(string name, MqttConfig options)
        {
            if (options is null)
                return ValidateOptionsResult.Fail("Mqtt configuration object is null.");

            if (string.IsNullOrEmpty(options.Host))
                return ValidateOptionsResult.Fail($"Property {nameof(options.Host)} must have a value specified");

            if (string.IsNullOrEmpty(options.Password))
                return ValidateOptionsResult.Fail($"Property {nameof(options.Password)} must have a value specified");

            if (string.IsNullOrEmpty(options.Topic))
                return ValidateOptionsResult.Fail($"Property {nameof(options.Topic)} must have a value specified");

            if (string.IsNullOrEmpty(options.UserName))
                return ValidateOptionsResult.Fail($"Property {nameof(options.UserName)} must have a value specified");

            if (options.Tls != null)
            {
                if (string.IsNullOrEmpty(options.Tls.CACerts))
                    return ValidateOptionsResult.Fail($"Property {nameof(options.Tls.CACerts)} must have a value specified");
            }

            return ValidateOptionsResult.Success;
        }
    }
}
