using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MQTTnet;

using WebApp.Extensions;
using WebApp.Kafka.Contracts;
using WebApp.Mqtt.Contracts;
using WebApp.S3.Contracts;


namespace WebApp.BackgroundServices
{
    /// <summary>
    /// A simple MQTT-Kafka Bridge that publishes messages received from /shinobi/+/+/trigger to a configured Kafka topic:
    /// 1. Start Mqtt Service to listen for object detection JSON messages for configured camera topics <see cref="IMqttService" />
    /// 2. Extract image from JSON payload and upload to S3 storage with GUID object name <see cref="IS3Service" />
    /// 3. Update JSON payload so that Details.Img property refers to the GUID for uploaded snapshot image
    /// 4. Prepare Kafka message by prepending magic byte 0 and a schema id
    /// 5. Publish message to a configured Kafka Topic <see cref="IProducerService" />. Each message is keyed by it's
    ///    corresponding MQTT topic.
    /// </summary>
    /// <remarks>
    /// Another Background Service <see cref="WebApp.Kafka.ConsumerService" /> subscribes to messages received from
    /// the configured Kafka topic <see cref="WebApp.Kafka.Config.KafkaConfig" />, extracting payloads with keys 
    /// that matches a preconfigured list. Filtered messages are forwarded to a signalR endpoint that Blazor Server Pages
    /// subscribe to for rendering snapshot images to clients.
    /// 
    /// MqttService is started and stopped by this Background Service.
    /// 
    /// A docker-compose stack has been made available to start an mqtt broker and kafka cluster in development environments
    /// Certificates will need to be added in Docker/Mqtt/Certs/[localCA.crt, server.crt, server.key]
    ///
    /// In production environments a Kafka cluster would be available from Azure HDInsight 
    /// (https://docs.microsoft.com/en-us/azure/hdinsight/hdinsight-overview). Mqtt broker would be served by Azure IoTHub
    /// (https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-mqtt-support). SignalR would be hosted by Azure SignalR
    /// (https://azure.microsoft.com/en-us/services/signalr-service/)
    /// </remarks>
    public class MqttKafkaBridge : BackgroundService, IDisposable
    {
        private const int DelayTime = 1000;


        private readonly ILogger<MqttKafkaBridge> _logger;

        private IMqttService _mqttService;
        IProducerService<string, byte[]> _producer;
        private IS3Service _s3Service;


        /// <summary>
        /// Initialise dependencies for:
        /// <see cref="IMqttService" />, <see cref="IS3Service" />, <see cref="IProducerService" />
        /// Subscribe to MessageReceived action of <see cref="IMqttService" />
        /// </summary>
        public MqttKafkaBridge(
            IS3Service s3Service,
            IMqttService mqttService,
            IProducerService<string, byte[]> producer,
            ILogger<MqttKafkaBridge> logger
        )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mqttService = mqttService ?? throw new ArgumentNullException(nameof(mqttService));
            _producer = producer ?? throw new ArgumentNullException(nameof(producer));
            _s3Service = s3Service ?? throw new ArgumentNullException(nameof(mqttService));

            _mqttService.MessageReceived = OnMqttMessageReceivedAsync;
        }

        /// <summary>
        /// Wait until service receives cancellation token and stop the mqtt client
        /// Dispose method will then dispose resources of mqtt client
        /// </summary>
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            new Thread(() => _ = StartConsumerLoop(stoppingToken)).Start();

            return Task.CompletedTask;
        }

        private Task StartConsumerLoop(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            try
            {
                _mqttService.StartAsync();

                while (!stoppingToken.IsCancellationRequested)
                {
                    Task.Delay(DelayTime, stoppingToken);
                }
            }
            finally
            {
                _mqttService.StopAsync();
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Extract snapshot image and upload to S3 storage and forward message to Kafka topic.
        /// The Kafka message does not contain the snapshot image, but a reference to the id of the 
        /// image uploaded to S3 storage.
        /// </summary>
        /// <remarks>
        /// Forward messages to kafka topic:
        /// {
        ///     Key: <MQTT Topic>
        ///     Payload: <payload with reference to image in S3 storage>
        /// }
        /// </remarks>
        public Task OnMqttMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs eventArgs)
        {
            return ProcessMqttMessage(eventArgs);
        }

        private async Task ProcessMqttMessage(MqttApplicationMessageReceivedEventArgs eventArgs)
        {
            try
            {
                string topic = eventArgs.ApplicationMessage.Topic;
                if (!string.IsNullOrWhiteSpace(topic))
                {
                    if (!eventArgs.ApplicationMessage.Payload.IsNullOrEmpty<byte>())
                    {
                        string payload = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload);

                        byte[] updatedPayload = await _s3Service.UploadImage(payload, Guid.NewGuid().ToString());
                        byte[] bytePayload = this._producer.PrepareMessage(updatedPayload, 5);

                        _producer.Produce(topic, bytePayload, 10);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                throw ex;
            }

            await Task.CompletedTask;
        }


        /// <summary>
        /// Trigger disposal of the MqttService and call base class dispose
        /// </summary>
        public override void Dispose()
        {
            try
            {
                _mqttService.Dispose();
            }
            finally
            {
                base.Dispose();
            }
        }
    }
}
