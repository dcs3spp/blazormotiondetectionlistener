namespace WebApp.S3.Config
{
    public class S3Config
    {
        public const string SectionName = "S3Settings";

        public string AccessKey { get; set; }
        public string Bucket { get; set; }
        public string Endpoint { get; set; }
        public string SecretKey { get; set; }
    }
}