using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Minio;
using Minio.DataModel;

using WebApp.S3.Contracts;


namespace WebApp.S3
{
    /// <summary>
    /// Delegate for creating an instance of <see cref="MinioClientWrapper"/>
    /// </summary>
    public delegate IMinioClient MinioFactory(string endPoint, string accessKey, string password);

    /// <summary>Wrapper class for <see cref="MinioClient"></summary>
    /// <remarks>Makes <see cref="MinioClient"/> mockable</remarks>
    public class MinioClientWrapper : IMinioClient
    {
        private IObjectOperations _MinioClient { get; set; }


        /// <summary>
        /// Initialise client with instance of <see cref="IObjectOperations"/>
        /// <param name="client">Minio client</param>
        /// </summary>
        public MinioClientWrapper(IObjectOperations client)
        {
            _MinioClient = client;
        }

        /// <summary>
        /// Wrapper for <see cref="MinioClient"/>
        /// </summary>
        /// <param name="endPoint">Minio endpoint</param>
        /// <param name="accessKey">Minio accessKey</param>
        /// <param name="password">Minio password</param>
        /// <exception cref="ArgumentNullException">Thrown when any parameter is null</exception>
        public MinioClientWrapper(string endPoint, string accessKey, string password)
        {
            if (endPoint == null)
                throw new ArgumentNullException(nameof(endPoint));

            if (accessKey == null)
                throw new ArgumentNullException(nameof(accessKey));

            if (password == null)
                throw new ArgumentNullException(nameof(password));

            _MinioClient = new MinioClient(endPoint, accessKey, password);
        }

        public async virtual Task GetObjectAsync(
            string bucket,
            string objectName,
            Action<Stream> cb,
            ServerSideEncryption sse = null,
            CancellationToken cancellationToken = default
        )
        {
            await _MinioClient.GetObjectAsync(bucket, objectName, cb, sse, cancellationToken);
        }

        public async virtual Task PutObjectAsync(
            string bucket,
            string objectName,
            Stream data,
            long size,
            string contentType = null,
            Dictionary<string, string> metaData = null,
            ServerSideEncryption sse = null,
            CancellationToken cancellationToken = default)
        {
            await _MinioClient.PutObjectAsync(
                bucket,
                objectName,
                data,
                size,
                contentType,
                metaData,
                sse,
                cancellationToken
            );
        }
    }
}