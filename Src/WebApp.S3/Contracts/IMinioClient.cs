using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Minio.DataModel;


namespace WebApp.S3.Contracts
{
    public interface IMinioClient
    {
        Task GetObjectAsync(
            string bucket,
            string objectName,
            Action<Stream> cb,
            ServerSideEncryption sse = null,
            CancellationToken cancellationToken = default
        );

        Task PutObjectAsync(
            string bucket,
            string objectName,
            Stream data,
            long size,
            string contentType = null,
            Dictionary<string, string> metaData = null,
            ServerSideEncryption sse = null,
            CancellationToken cancellationToken = default
        );
    }
}