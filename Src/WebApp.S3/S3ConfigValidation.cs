using System.Text.RegularExpressions;

using Microsoft.Extensions.Options;


namespace WebApp.S3.Config
{
    public class S3ConfigValidation : IValidateOptions<S3Config>
    {
        private const string S3Pattern = "^([a-z]|(d(?!d{0,2}.d{1,3}.d{1,3}.d{1,3})))([a-zd]|(.(?!(.|-)))|(-(?!.))){1,61}[a-zd.]$";
        public ValidateOptionsResult Validate(string name, S3Config options)
        {
            if (options is null)
                return ValidateOptionsResult.Fail("S3 configuration object is null.");

            if (string.IsNullOrEmpty(options.AccessKey))
                return ValidateOptionsResult.Fail($"Property {nameof(options.AccessKey)} must have a value specified");

            if (string.IsNullOrEmpty(options.Bucket))
                return ValidateOptionsResult.Fail($"Property {nameof(options.Bucket)} must have a value specified");

            if (string.IsNullOrEmpty(options.Endpoint))
                return ValidateOptionsResult.Fail($"Property {nameof(options.Endpoint)} must have a value specified");

            if (string.IsNullOrEmpty(options.SecretKey))
                return ValidateOptionsResult.Fail($"Property {nameof(options.SecretKey)} must have a value specified");

            var regEx = new Regex(S3Pattern);
            if (!regEx.IsMatch(options.Bucket))
                return ValidateOptionsResult.Fail($"Property {nameof(options.Bucket)} failed S3 bucket name policy");

            return ValidateOptionsResult.Success;
        }
    }
}
