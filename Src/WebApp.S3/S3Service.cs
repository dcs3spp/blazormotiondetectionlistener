﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Minio;
using Minio.Exceptions;
using Newtonsoft.Json.Linq;
using WebApp.S3.Config;
using WebApp.S3.Contracts;


namespace WebApp.S3
{
    public class S3Service : IS3Service, IDisposable
    {
        const string JpegContentType = "image/jpeg";

        private bool _Disposed { get; set; }
        private ILogger<S3Service> _Logger { get; set; }
        private IMinioClient _MinioClient { get; set; }
        private S3Config _Config { get; set; }


        /// <summary>
        /// <param name="config">S3 config from appsettings</param>
        /// <param name="minioFactory">
        /// Function to create a <see cref="MinioClient"/>. Accepts the following parameters:
        /// 1. endpoint: string
        /// 2. username: string
        /// 3. password: string
        /// </param>
        /// <param name="logger">Logger instance</param>
        /// </summary>
        public S3Service(
            IOptions<S3Config> config,
            MinioFactory minioFactory,
            ILogger<S3Service> logger
        )
        {
            if (minioFactory == null)
                throw new ArgumentNullException(nameof(minioFactory));

            if (config == null)
                throw new ArgumentNullException(nameof(config));

            _Config = config.Value;
            _Disposed = false;
            _Logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _MinioClient = minioFactory(_Config.Endpoint, _Config.AccessKey, _Config.SecretKey);

            _Logger.LogInformation($"Minio client created for endpoint {_Config.Endpoint}");
            _Logger.LogInformation($"minio://{config.Value.AccessKey}:{config.Value.SecretKey}@{config.Value.Endpoint}");
        }

        /// <summary>
        /// Upload the json payload to S3 storage. The 'img' property is modified to be a GUID
        /// that represents the resulting object name in S3 storage.
        /// </summary>
        /// <param name="json">Json mqtt message</param>
        /// <param name="guid">guid for identifying image</param>
        /// <param name="initalBufferSize">Buffer size (bytes), defaults to 1024</param>
        /// <returns>Modified bytes payload with 'img' property set to guid</returns>
        public async Task<byte[]> UploadImage(string json, string guid, int initialBufferSize = 1024)
        {
            // Use NewtonSoft Json here, it allows us to modify the DOM, System.Text.Json cannot do
            // this at the time of writing
            _Logger.LogInformation($"Loading json into JSON DOM and updating 'img' property with key {guid}");
            JObject rootElement = JObject.Parse(json);
            JObject details = (JObject)rootElement["details"];

            _Logger.LogInformation("Extracting UTF8 bytes from base64");
            byte[] fromBase64 = Convert.FromBase64String(details["img"].Value<string>());
            details["img"] = guid; // write the string to the JSON DOM here

            // Upload the image to S3 storage
            using (BufferedStream buffer = new BufferedStream(new MemoryStream(fromBase64), initialBufferSize))
            using (BinaryWriter writer = new BinaryWriter(buffer, Encoding.UTF8))
            {
                buffer.Flush();
                _Logger.LogInformation($"Updated JSON payload with img: {guid}, now uploading {buffer.Length / 1024.0 / 1024.0} MB to S3 storage");
                buffer.Position = 0;
                await UploadAsync(buffer, guid); // this manages converting to jpeg
            }

            _Logger.LogInformation("Converting modified payload back to UTF8 bytes for Kafka processing");
            return Encoding.UTF8.GetBytes(rootElement.ToString());
        }


        public async Task UploadAsync(BufferedStream byteStream, string objectName)
        {
            // validate args are not null
            if (byteStream == null)
            {
                throw new ArgumentNullException(nameof(byteStream));
            }

            if (objectName == null)
            {
                throw new ArgumentNullException(nameof(objectName));
            }

            // Try and upload, let minio handle the validation
            try
            {
                await _MinioClient.PutObjectAsync(_Config.Bucket, objectName, byteStream, byteStream.Length, JpegContentType);
            }
            catch (MinioException e)
            {
                _Logger.LogError(e, "Error occurred while uploading to S3 storage");
                throw e;
            }
        }

        /// <summary>
        /// Try and retrieve an object from minio client.
        /// It is the clients responsiblity to close the buffered stream
        /// </summary>
        /// <param name="objectName">Name of object to download</param>
        /// <Throws>Exception if failed to stat the object</Throws>
        public async Task<BufferedStream> DownloadObject(string objectName)
        {
            if (objectName == null)
                throw new ArgumentNullException(nameof(objectName));

            BufferedStream buffered = new BufferedStream(new MemoryStream(), 1024);

            try
            {
                // Get input stream to have content of 'my-objectname' from 'my-bucketname'
                await _MinioClient.GetObjectAsync(_Config.Bucket, objectName,
                                                 (stream) =>
                                                 {
                                                     stream.CopyTo(buffered);
                                                     buffered.Flush();
                                                     buffered.Position = 0;
                                                 });
            }
            catch (MinioException e)
            {
                _Logger.LogError(e, "Error occured while connection to S3 storage => {}", e.Message);
                throw e;
            }

            _Logger.LogInformation($"Downloaded {buffered.Length / 1024.0 / 1024.0} MB from S3 storage");

            return buffered;
        }


        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_Disposed)
            {
                if (disposing)
                {
                    _Logger.LogInformation("Disposing of resources");
                }

                _Disposed = true;
            }
        }

        ~S3Service()
        {
            Dispose(false);
        }
    }
}
