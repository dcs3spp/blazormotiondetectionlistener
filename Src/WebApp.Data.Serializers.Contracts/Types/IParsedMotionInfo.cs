using System.Collections.Generic;


namespace WebApp.Data.Serializers.Contracts
{
    public interface IParsedMotionInfo : IVisitorElement<MotionInfo>
    {
        #region Properties
        string img { get; set; }
        int imgHeight { get; set; }
        int imgWidth { get; set; }
        IList<MotionLocation> matrices { get; set; }
        string name { get; set; }
        string plug { get; set; }
        string reason { get; set; }
        long time { get; set; }
        #endregion
    }
}