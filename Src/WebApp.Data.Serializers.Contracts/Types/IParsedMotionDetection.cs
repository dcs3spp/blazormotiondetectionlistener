namespace WebApp.Data.Serializers.Contracts
{
    public interface IParsedMotionDetection : IVisitorElement<MotionDetection>
    {
        #region attributes
        MotionInfo details { get; set; }
        string group { get; set; }
        string monitorId { get; set; }
        string plug { get; set; }
        long time { get; set; }
        #endregion
    }
}