namespace WebApp.Data.Serializers.Contracts
{
    public interface IParsedLocation : IVisitorElement<MotionLocation>
    {
        #region Properties
        double confidence { get; set; }
        double height { get; set; }
        string tag { get; set; }
        double width { get; set; }
        double x { get; set; }
        double y { get; set; }
        #endregion
    }
}