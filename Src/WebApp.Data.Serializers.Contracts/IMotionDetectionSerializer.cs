using System.IO;
using System.Threading.Tasks;

namespace WebApp.Data.Serializers.Contracts
{
    public interface IMotionDetectionSerializer<T>
    {
        Task<T> FromJSONAsync(Stream stream);

        T FromJSON(Stream stream);
    }
}
