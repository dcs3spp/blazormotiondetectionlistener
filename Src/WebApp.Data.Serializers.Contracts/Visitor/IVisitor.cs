using System.Text.Json;


namespace WebApp.Data.Serializers.Contracts
{
    internal interface IVisitor<ReaderType>
    {
        MotionDetection DeserializeMotionDetection(ReaderType reader);
        MotionInfo DeserializeMotionInfo(ReaderType reader);
        MotionLocation DeserializeMotionLocation(ReaderType reader);
    }

    public interface IVisitor
    {
        void DeserializeMotionDetection(IParsedMotionDetection target, ref Utf8JsonReader reader, JsonSerializerOptions options);
        void DeserializeMotionInfo(IParsedMotionInfo target, ref Utf8JsonReader reader, JsonSerializerOptions options);
        void DeserializeMotionLocation(IParsedLocation target, ref Utf8JsonReader reader, JsonSerializerOptions options);
    }
}
