using System.Text.Json;

namespace WebApp.Data.Serializers.Contracts
{
    public interface IVisitorElement<ModelType>
    {
        void accept(IVisitor visitor, ref Utf8JsonReader reader, JsonSerializerOptions options);
        ModelType ToModel();
    }
}
