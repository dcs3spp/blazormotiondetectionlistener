using System.IO;
using System.Text.Json;
using System.Threading.Tasks;


namespace WebApp.Data.Serializers.Contracts
{
    public interface IJsonSerializerWrapper
    {
        Task<T> DeserializeAsync<T>(Stream stream, JsonSerializerOptions options);
        T Deserialize<T>(string json, JsonSerializerOptions options);

        T Deserialize<T>(ref Utf8JsonReader reader, JsonSerializerOptions options);
    }
}
