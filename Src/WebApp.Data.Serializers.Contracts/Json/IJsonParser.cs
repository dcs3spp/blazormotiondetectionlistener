using System;
using System.Text.Json;


namespace WebApp.Data.Serializers.Contracts
{
    public interface IJsonParser
    {
        string ReadProperty(ref Utf8JsonReader reader, string propertyName);
        string ReadStringProperty(ref Utf8JsonReader reader, string propertyName);
        int ReadIntProperty(ref Utf8JsonReader reader, string propertyName);
        long ReadLongProperty(ref Utf8JsonReader reader, string propertyName);
        double ReadDoubleProperty(ref Utf8JsonReader reader, string propertyName);
        DateTimeOffset ReadDateTimeOffsetProperty(ref Utf8JsonReader reader, string propertyName);
        void ThrowFailedConversion(string propertyName);
        void ThrowUnrecognisedProperty(string propertyName);
    }
}
