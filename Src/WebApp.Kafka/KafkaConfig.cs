using System.Collections.Generic;

using Confluent.Kafka;


namespace WebApp.Kafka.Config
{
    public class KafkaTopic
    {
        /// <summary>
        /// Create default partition count as 3 and replication count as 1
        /// </summary>
        public KafkaTopic()
        {
            PartitionCount = 3;
            ReplicationCount = 1;
        }

        public string Name { get; set; }
        public int PartitionCount { get; set; }
        public short ReplicationCount { get; set; }
    }

    public class KafkaConfig
    {
        private const char ListSeparatorChar = ',';
        public const string SectionName = "KafkaSettings";

        public ConsumerConfig Consumer { get; set; }
        public ProducerConfig Producer { get; set; }
        public HashSet<string> MqttCameraTopics { get; set; }
        public KafkaTopic Topic { get; set; }


        /// <summary>Create a set of brokers from Consumer and Producer BootstrapServer properties</summary>
        /// <returns>Return a HashSet<string> from Consumer and Producer brokers</returns>
        public HashSet<string> GetBrokers()
        {
            HashSet<string> brokers = new HashSet<string>();

            foreach (var item in Consumer.BootstrapServers.Split(ListSeparatorChar)) brokers.Add(item);
            foreach (var item in Producer.BootstrapServers.Split(ListSeparatorChar)) brokers.Add(item);

            return brokers;
        }
    }
}
