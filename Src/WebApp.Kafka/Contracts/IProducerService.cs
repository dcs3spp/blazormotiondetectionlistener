﻿namespace WebApp.Kafka.Contracts
{
    public interface IProducerService<Key, Value>
    {
        void Produce(Key key, Value value, int flushTimeSpan);
        byte[] PrepareMessage(byte[] payload, int schemaId, int initialBufferSize = 1024);
    }
}
