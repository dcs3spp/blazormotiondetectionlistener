using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Extensions.Options;


namespace WebApp.Kafka.Config
{
    /// <summary>Validates Kafka configuration</summary>
    /// <remarks>
    ///     "KafkaSettings": {
    ///         "Consumer": {
    ///             "BootstrapServers": "127.0.0.1:9092",
    ///             "GroupId": "consumer-group"
    ///         },
    ///         "Producer": {
    ///             "BootstrapServers": "127.0.0.1:9092",
    ///             "LingerMs": "5"
    ///         },
    ///         "MqttCameraTopics": ["/shinobi/group/monitor/trigger"],
    ///         "Topic": {
    ///             "Name": "eventbus"
    ///             "ReplicationCount": 1
    ///             "PartitionCount": 3
    ///         }
    ///     }
    ///
    /// ReplicationCount and PartitionCount are optional. When ommitted:
    /// PartitionCount = 3 and ReplicationCount = 1
    /// </remarks>
    public class KafkaConfigValidation : IValidateOptions<KafkaConfig>
    {
        private const string ListSeparator = ",";
        private const char ListSeparatorChar = ',';

        /// <summary>
        /// Validate KafkaConfig from appsettings
        /// </summary>
        /// <param name="name">Config name</param>
        /// <param name="options">Kafka config settings object</param>
        /// <returns>
        /// ValidateOptionsResult.Success if valid.ValidateOptionsResult.Fail if invalid
        /// </returns>
        public ValidateOptionsResult Validate(string name, KafkaConfig options)
        {
            if (options is null)
                return ValidateOptionsResult.Fail("Kafka configuration object is null.");

            if (options.Consumer is null)
                return ValidateOptionsResult.Fail($"Subsection {nameof(options.Consumer)} must have a value specified");

            if (options.Producer is null)
                return ValidateOptionsResult.Fail($"Subsection {nameof(options.Producer)} must have a value specified");

            if (options.MqttCameraTopics is null || options.MqttCameraTopics.Count == 0)
                return ValidateOptionsResult.Fail($"Property {nameof(options.MqttCameraTopics)} must specify list of camera topics");

            if (options.Topic is null)
                return ValidateOptionsResult.Fail($"Subsection {nameof(options.Topic)} must have a value specified");

            if (string.IsNullOrEmpty(options.Topic.Name))
                return ValidateOptionsResult.Fail($"Property {nameof(options.Topic.Name)} must have a value specified");

            if (string.IsNullOrEmpty(options.Producer.BootstrapServers))
                return ValidateOptionsResult.Fail($"Property {nameof(options.Producer.BootstrapServers)} must be a list of servers 'host[:port], host[:port]'");

            if (string.IsNullOrEmpty(options.Consumer.BootstrapServers))
                return ValidateOptionsResult.Fail($"Property {nameof(options.Consumer.BootstrapServers)} must be a list of servers 'host[:port], host[:port]'");

            if (string.IsNullOrEmpty(options.Consumer.GroupId))
                return ValidateOptionsResult.Fail($"Property {nameof(options.Consumer.GroupId)} must have a value specified");

            var result = ValidateBootstrapServers(options);
            if (result.errors.Count > 0)
                return ValidateOptionsResult.Fail($"Invalid bootstrap servers => {string.Join(ListSeparator, result.errors)}");

            if (options.Topic.ReplicationCount > result.brokers.Count)
                return ValidateOptionsResult.Fail($"Insufficient brokers to allow replication count of {options.Topic.ReplicationCount}");

            return ValidateOptionsResult.Success;
        }


        /// <summary>
        /// Enumerate bootstrap servers that violate format host|ip[:port]
        /// </summary>
        /// <param name="config">Kafka config from appsettings</param>
        /// <returns>Enumerated invalid brokers from consumer and producer config</returns>
        private (HashSet<string> brokers, IList<string> errors) ValidateBootstrapServers(KafkaConfig config)
        {
            HashSet<string> brokers = config.GetBrokers();

            var filteredResult = from broker in brokers
                                 where !IsValidBroker(broker)
                                 select broker;

            return (brokers, filteredResult.ToList<string>());
        }


        /// <summary>
        /// Check string format as host|ip[:port]
        /// </summary>
        /// <param name="broker"></param>
        /// <returns>True if valid. False when null or not in expected format</return>
        private bool IsValidBroker(string broker)
        {
            if (string.IsNullOrEmpty(broker))
                return false;

            try
            {
                const string schema = "net.tcp://";

                Uri obj = new Uri(string.Format($"{schema}{broker}"));
                return true;
            }
            catch (UriFormatException)
            {
                return false;
            }
        }
    }
}
