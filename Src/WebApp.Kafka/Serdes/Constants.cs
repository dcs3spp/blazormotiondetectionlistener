namespace WebApp.Kafka.SchemaRegistry.Serdes
{
    internal static class Constants
    {
        /// <summary>
        ///     Magic byte that identifies a message with Confluent Platform framing.
        /// </summary>
        public const byte MagicByte = 0;
    }
}
