using System;
using System.IO;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;
using Confluent.Kafka;

using WebApp.Data.Serializers.Contracts;


namespace WebApp.Kafka.SchemaRegistry.Serdes
{
    /// <summary>
    ///     (async) JSON deserializer.
    /// </summary>
    /// <remarks>
    ///     Serialization format:
    ///       byte 0:           A magic byte that identifies this as a message with
    ///                         Confluent Platform framing.
    ///       bytes 1-4:        Unique global id of the JSON schema associated with
    ///                         the data (as registered in Confluent Schema Registry),
    ///                         big endian.
    ///       following bytes:  The JSON data (utf8)
    ///
    ///     Internally, uses System.Text.Json for deserialization. Currently,
    ///     no explicit validation of the data is done against the
    ///     schema stored in Schema Registry.
    ///
    ///     Note: Off-the-shelf libraries do not yet exist to enable
    ///     integration of System.Text.Json and JSON Schema, so this
    ///     is not yet supported by the deserializer.
    ///     Investigating looking at external libraries for this if possible
    /// </remarks>
    public class JsonDeserializer<T> : IAsyncDeserializer<T> where T : class
    {
        private readonly int headerSize = sizeof(int) + sizeof(byte);

        private IMotionDetectionSerializer<T> Serializer { get; set; }
        private ILogger<JsonDeserializer<T>> Logger { get; }

        /// <summary>
        ///     Initialize a new JsonDeserializer instance.
        /// </summary>
        /// <param name="serializer">
        ///     Interface for serializing using System.Text.Json
        ///     <see cref = "WebApp.Data.Serializers.Contracts.IMotionDetectionSerializer<T>" />).
        /// </param>
        public JsonDeserializer(IMotionDetectionSerializer<T> serializer, ILogger<JsonDeserializer<T>> logger)
        {
            Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            Serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));

            Logger.LogInformation("Kafka Json Deserializer Constructed");
        }

        /// <summary>
        ///     Deserialize an object of type <typeparamref name="T"/>
        ///     from a byte array.
        /// </summary>
        /// <param name="data">
        ///     The raw byte data to deserialize.
        /// </param>
        /// <param name="isNull">
        ///     True if this is a null value.
        /// </param>
        /// <param name="context">
        ///     Context relevant to the deserialize operation.
        /// </param>
        /// <returns>
        ///     A <see cref="System.Threading.Tasks.Task" /> that completes
        ///     with the deserialized value.
        /// </returns>
        public Task<T> DeserializeAsync(ReadOnlyMemory<byte> data, bool isNull, SerializationContext context)
        {
            Logger.LogInformation("Deserializing kafka data");

            if (isNull)
            {
                Logger.LogInformation("Deserializing is returning null");
                return Task.FromResult<T>(null);
            }

            try
            {
                var array = data.ToArray();

                if (array.Length < 5)
                {
                    Logger.LogCritical("Byte array length is < 5 for deserializing");
                    throw new InvalidDataException($"Expecting data framing of length 5 bytes or more but total data size is {array.Length} bytes");
                }

                if (array[0] != Constants.MagicByte)
                {
                    Logger.LogCritical("Expected first byte to be magic byte 0");
                    throw new InvalidDataException($"Expecting message {context.Component.ToString()} with Confluent Schema Registry framing. Magic byte was {array[0]}, expecting {Constants.MagicByte}");
                }

                // A schema is not required to deserialize json messages.
                // TODO: add validation capability.

                using (var stream = new MemoryStream(array, headerSize, array.Length - headerSize))
                {
                    return Task.FromResult<T>(Serializer.FromJSON(stream));
                }
            }
            catch (AggregateException e)
            {
                throw e.InnerException;
            }
        }
    }
}
