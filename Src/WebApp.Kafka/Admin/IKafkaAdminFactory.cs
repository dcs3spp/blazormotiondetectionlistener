using Confluent.Kafka;


namespace WebApp.Kafka.Admin
{
    public interface IKafkaAdminFactory
    {
        IAdminClient CreateAdminClient();
    }
}