
using System;

using Confluent.Kafka;
using Microsoft.Extensions.Options;

using WebApp.Kafka.Config;


namespace WebApp.Kafka.Admin
{
    /// <summary>Factory for creating a <see cref="IAdminClient"/> from config</summary>
    /// <remarks>
    /// The decision was made to use a factory class rather than Autofac delegates
    /// since using this library (https://github.com/thomaslevesque/Extensions.Hosting.AsyncInitialization)
    /// in Program.cs to asynchronously initialise Kafka Cluster with topic stored in config file.
    /// The library uses Microsoft's Dependency injection mechanism.
    /// </remarks>
    public class KafkaAdminFactory : IKafkaAdminFactory
    {
        /// <summary>
        /// <see cref= "KafkaConfig"/>Config refeencing bootstrap servers
        /// </summary>
        private KafkaConfig _Config { get; }

        /// <summary>
        /// Construct a factory to create a confluent kafka <see cref="IAdminClient"/>
        /// instance using a provided configuration
        /// </summary>
        /// <param name=""><see cref="IOptions"/>IOptions instance containing <see cref="KafkaConfig"/> instance</param>
        public KafkaAdminFactory(IOptions<KafkaConfig> options)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            _Config = options.Value;
        }

        /// <summary>
        /// Create a <see cref="IAdminClient"/> instance from config
        /// </summary>
        public IAdminClient CreateAdminClient()
        {
            var adminClientBuilder = new AdminClientBuilder(
                new AdminClientConfig()
                {
                    BootstrapServers = _Config.Consumer.BootstrapServers
                }
            );

            return adminClientBuilder.Build();
        }
    }
}
