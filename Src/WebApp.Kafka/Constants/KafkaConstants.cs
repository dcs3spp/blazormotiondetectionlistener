namespace WebApp.Kafka.Constants
{
    public static class KafkaConstants
    {
        public const int DefaultFlushTimeSeconds = 10;
    }
}