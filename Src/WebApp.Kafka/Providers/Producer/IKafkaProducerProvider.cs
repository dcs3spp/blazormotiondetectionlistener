using Microsoft.Extensions.Logging;

using WebApp.Kafka.Config;


namespace WebApp.Kafka.ProducerProvider
{
    public interface IKafkaProducerProvider
    {
        KafkaConfig Config { get; }

        IKafkaProducerAggregate<TKey, TValue> CreateProducerAggregate<TKey, TValue>(ILogger logger);
    }
}