using System;

using Confluent.Kafka;


namespace WebApp.Kafka.ProducerProvider
{
    public interface IKafkaProducerAggregate<TKey, TValue>
    {
        IProducer<TKey, TValue> Producer { get; }
        Action<DeliveryReport<TKey, TValue>> DeliveryReportAction { get; }
        String Topic { get; }
    }
}