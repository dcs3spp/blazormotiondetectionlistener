using System;

using Confluent.Kafka;


namespace WebApp.Kafka.ProducerProvider
{
    public class KafkaProducerAggregate<TKey, TValue> : IKafkaProducerAggregate<TKey, TValue>
    {
        public IProducer<TKey, TValue> Producer { get; }
        public Action<DeliveryReport<TKey, TValue>> DeliveryReportAction { get; }
        public String Topic { get; }

        public KafkaProducerAggregate(
            IProducer<TKey, TValue> producer,
            Action<DeliveryReport<TKey, TValue>> deliveryAction,
            string topic
        )
        {
            Producer = producer ?? throw new ArgumentNullException(nameof(producer));
            DeliveryReportAction = deliveryAction ?? throw new ArgumentNullException(nameof(deliveryAction));
            Topic = topic ?? throw new ArgumentNullException(nameof(topic));
        }
    }
}