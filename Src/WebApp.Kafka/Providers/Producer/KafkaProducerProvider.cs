using System;

using Confluent.Kafka;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using WebApp.Kafka.Config;


namespace WebApp.Kafka.ProducerProvider
{
    public class KafkaProducerProvider : IKafkaProducerProvider
    {
        public KafkaConfig Config { get; }


        public KafkaProducerProvider(IOptions<KafkaConfig> config)
        {
            Config = config.Value;
        }


        public IKafkaProducerAggregate<TKey, TValue> CreateProducerAggregate<TKey, TValue>(ILogger logger)
        {
            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            IProducer<TKey, TValue> producer = new ProducerBuilder<TKey, TValue>(Config.Producer).Build();
            Action<DeliveryReport<TKey, TValue>> action = delegate (DeliveryReport<TKey, TValue> report)
            {
                if (!report.Error.IsError)
                {
                    logger.LogInformation($"Delivered message to {report.TopicPartitionOffset}");
                }
                else
                {
                    logger.LogError($"Delivery Error: {report.Error.Reason}");
                }
            };

            return new KafkaProducerAggregate<TKey, TValue>(producer, action, Config.Topic.Name);
        }
    }
}
