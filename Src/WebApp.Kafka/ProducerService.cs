using System;
using System.IO;
using System.Net;
using System.Text;

using Confluent.Kafka;
using Microsoft.Extensions.Logging;

using WebApp.Kafka.Constants;
using WebApp.Kafka.Contracts;
using WebApp.Kafka.ProducerProvider;


namespace WebApp.Kafka
{
    public class ProducerService<Key, Value> : IProducerService<Key, Value>, IDisposable
    {
        private const int DefaultFlushTimeSpan = 10;

        private bool Disposed { get; set; }
        private IKafkaProducerAggregate<Key, Value> ProducerAggregate { get; }
        private ILogger Logger { get; set; }


        public ProducerService(
            IKafkaProducerProvider provider,
            ILogger<ProducerService<Key, Value>> logger
        )
        {
            if (provider == null)
                throw new ArgumentNullException(nameof(provider));
            Logger = logger ?? throw new ArgumentNullException(nameof(logger));


            ProducerAggregate = provider.CreateProducerAggregate<Key, Value>(Logger);

            Disposed = false;

            Logger.LogInformation("ProducerService constructor called");
        }

        /// <summary>
        /// Modify message payload for Kafka Confluent compatibility:
        /// Prepend magic byte 0 to head of message
        /// Prepend a 4 byte schema id to allow JSON schema validation
        /// </summary>
        /// <param name="payload">Message payload represented as bytes</param>
        /// <param name="schemaId">Schema id</param>
        /// <param name="initialBufferSize">Buffer size, defaults to 1024</param>
        /// <returns>Kafka compliant payload with magic byte and schema id prepended</returns>
        public byte[] PrepareMessage(byte[] payload, int schemaId, int initialBufferSize = 1024)
        {
            byte[] byteArr;

            const byte magicByte = 0;

            using (MemoryStream stream = new MemoryStream(initialBufferSize))
            using (BinaryWriter writer = new BinaryWriter(stream, Encoding.UTF8))
            {
                writer.Write(magicByte);
                writer.Write(IPAddress.HostToNetworkOrder(schemaId));
                writer.Write(payload);

                writer.Flush();

                byteArr = stream.ToArray();
            }

            return byteArr;
        }

        public void Produce(Key key, Value value, int flushTimeSpan = KafkaConstants.DefaultFlushTimeSeconds)
        {
            ProducerAggregate.Producer.Produce(
                ProducerAggregate.Topic,
                new Message<Key, Value> { Key = key, Value = value },
                ProducerAggregate.DeliveryReportAction
            );

            Logger.LogInformation("Produce topic : {}, key : {}, value : {}", ProducerAggregate.Topic, key.ToString(), value.ToString());
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!Disposed)
            {
                if (disposing)
                {
                    Logger.LogInformation("Flushing remaining messages to produce...");
                    ProducerAggregate.Producer.Flush(TimeSpan.FromSeconds(KafkaConstants.DefaultFlushTimeSeconds));
                    Logger.LogInformation("Disposing Kafka producer...");
                    ProducerAggregate.Producer.Dispose();
                }

                Disposed = true;
            }
        }

        ~ProducerService()
        {
            Dispose(false);
        }
    }
}
