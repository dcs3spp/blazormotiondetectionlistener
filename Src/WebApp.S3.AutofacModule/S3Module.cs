﻿using System.Reflection;

using Autofac;
using Minio;
using Microsoft.Extensions.Options;

using WebApp.S3.Contracts;
using WebApp.S3.Config;


namespace WebApp.S3.AutofacModule
{
    public class S3Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly repository = typeof(S3Config).GetTypeInfo().Assembly;

            builder.Register<MinioFactory>(context =>
                 {
                     return (string endpoint, string accessKey, string password) =>
                     {
                         var minio = new MinioClientWrapper(endpoint, accessKey, password);
                         return minio;
                     };
                 })
                 .SingleInstance();

            builder.RegisterType<S3ConfigValidation>()
                .As<IValidateOptions<S3Config>>()
                .SingleInstance();

            builder.RegisterType<S3Service>()
                .As<IS3Service>()
                .SingleInstance();
        }
    }
}

