using System;


namespace WebApp.Data
{
    public abstract class BasePOCO<T> : IEquatable<T> where T : class
    {
        #region IEquatable<T> Members
        public abstract bool Equals(T other);
        #endregion

        #region Object Equality
        public abstract override bool Equals(object obj);
        public abstract override int GetHashCode();
        #endregion
    }
}