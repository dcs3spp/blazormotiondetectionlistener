using System;

/**
 *                          {
 *                              group: 'myGroup',
 *                              time: 840,
 *                              monitorId: 'myMonitorId',
 *                              plug: TensorFlow-WithFiltering-And-MQTT,
 *                              details: {
 *                                  plug: TensorFlow-WithFiltering-And-MQTT,
 *                                  name: 'Tensorflow',
 *                                  reason: 'object',
 *                                  matrices: [{
 *                                      "x": 2.313079833984375,
 *                                      "y": 1.0182666778564453,
 *                                      "width": 373.25050354003906,
 *                                      "height": 476.9341278076172,
 *                                      "tag": "person",
 *                                      "confidence": 0.7375929355621338
 *                                  }],
 *                                  img: 'base64',
 *                                  imgHeight: 64,
 *                                  imgWidth: 48,
 *                                  time: 840
 *                              }
 *                          }
 */


namespace WebApp.Data
{
    public sealed class MotionDetection : BasePOCO<MotionDetection>, IEquatable<MotionDetection>
    {
        public string Group { get; }
        public long Time { get; }
        public string MonitorId { get; }
        public string Plug { get; }
        public MotionInfo Details { get; }
        public DateTimeOffset TimeReceivedUTC { get; }


        public MotionDetection(string group, long time, string monitorId, string plug, MotionInfo details)
        {
            TimeReceivedUTC = DateTimeOffset.UtcNow;

            Group = group ?? throw new ArgumentNullException(nameof(group));
            Time = time;
            MonitorId = monitorId ?? throw new ArgumentNullException(nameof(monitorId));
            Plug = plug ?? throw new ArgumentNullException(nameof(plug));

            if (details is null)
                throw new ArgumentNullException(nameof(details));

            Details = new MotionInfo(details);
        }

        public MotionDetection(MotionDetection other)
        {
            if (other is null)
                throw new ArgumentNullException(nameof(other));

            TimeReceivedUTC = DateTimeOffset.UtcNow;

            Group = other.Group;
            Time = other.Time;
            MonitorId = other.MonitorId;
            Plug = other.Plug; ;
            Details = new MotionInfo(other.Details);
        }

        public override bool Equals(MotionDetection other)
        {
            if (ReferenceEquals(other, null))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (!(Group == other.Group) || !(Time == other.Time) || !(MonitorId == other.MonitorId) || !(Plug == other.Plug) || !(Details.Equals(other.Details)))
            {
                return false;
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            MotionDetection other = obj as MotionDetection;

            if (other == null)
                return false;
            else
                return Equals(other);
        }

        public static bool operator ==(MotionDetection x, MotionDetection y)
        {
            if (((object)x) == null || ((object)y) == null)
                return Object.Equals(x, y);

            return x.Equals(y);
        }

        public static bool operator !=(MotionDetection x, MotionDetection y)
        {
            if (((object)x) == null || ((object)y) == null)
                return !Object.Equals(x, y);
            return !(x.Equals(y));
        }

        public override int GetHashCode()
        {
            unchecked
            {
                const int Prime = 397;
                int result = 37;

                result *= Prime;
                result += Group.GetHashCode();

                result *= Prime;
                result += MonitorId.GetHashCode();

                result *= Prime;
                result += Time.GetHashCode();

                return result;
            }
        }

        public override string ToString()
        {
            return string.Format(
                "\ndetails: {0}\ngroup: {1}\nmonitorId: {2}\nplugin: {3}\ntime: {4}\n",
                Details,
                Group,
                MonitorId,
                Plug,
                Time);
        }
    }
}
