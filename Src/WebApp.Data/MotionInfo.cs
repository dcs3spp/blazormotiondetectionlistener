using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

/**
 *                          {
 *                              group: 'myGroup',
 *                              time: 840,
 *                              monitorId: 'myMonitorId',
 *                              plug: TensorFlow-WithFiltering-And-MQTT,
 *                              details: {
 *                                  plug: TensorFlow-WithFiltering-And-MQTT,
 *                                  name: 'Tensorflow',
 *                                  reason: 'object',
 *                                  matrices: [{
 *                                      "x": 2.313079833984375,
 *                                      "y": 1.0182666778564453,
 *                                      "width": 373.25050354003906,
 *                                      "height": 476.9341278076172,
 *                                      "tag": "person",
 *                                      "confidence": 0.7375929355621338
 *                                  }],
 *                                  img: 'base64',
 *                                  imgHeight: 64,
 *                                  imgWidth: 48,
 *                                  time: 840
 *                              }
 *                          }
 */

namespace WebApp.Data
{
    public sealed class MotionInfo : BasePOCO<MotionInfo>, IEquatable<MotionInfo>
    {
        public string Plug { get; }
        public string Name { get; }
        public string Reason { get; }
        public ReadOnlyCollection<MotionLocation> Matrices { get; }
        public string Img { get; }
        public int ImgHeight { get; }
        public int ImgWidth { get; }
        public long Time { get; }

        public MotionInfo(string plug, string name, string reason, IList<MotionLocation> matrices, string img, int imgHeight, int imgWidth, long time)
        {
            Plug = plug ?? throw new ArgumentNullException(nameof(plug));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Reason = reason ?? throw new ArgumentNullException(nameof(reason));
            Img = img ?? throw new ArgumentNullException(nameof(img));
            ImgHeight = imgHeight;
            ImgWidth = imgWidth;
            Time = time;

            if (matrices is null)
                throw new ArgumentNullException(nameof(matrices));
            Matrices = matrices.Select(item => new MotionLocation(item)).ToList().AsReadOnly();
        }

        public MotionInfo(MotionInfo other)
        {
            if (other is null)
                throw new ArgumentNullException(nameof(other));

            Img = other.Img;
            ImgHeight = other.ImgHeight;
            ImgWidth = other.ImgWidth;
            Matrices = other.Matrices.Select(item => new MotionLocation(item)).ToList().AsReadOnly();
            Name = other.Name;
            Plug = other.Plug;
            Reason = other.Reason;
            Time = other.Time;
        }


        public override bool Equals(MotionInfo other)
        {
            if (ReferenceEquals(other, null))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (!(Plug == other.Plug)
                || !(Name == other.Name)
                || !(Reason == other.Reason)
                || !(Img == other.Img)
                || !(ImgWidth == other.ImgWidth)
                || !(ImgHeight == other.ImgHeight)
                || !(Matrices.SequenceEqual(other.Matrices))
                || !(Time == other.Time))
            {
                return false;
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            MotionInfo other = obj as MotionInfo;

            if (other == null)
                return false;
            else
                return Equals(other);
        }

        public static bool operator ==(MotionInfo x, MotionInfo y)
        {
            if (((object)x) == null || ((object)y) == null)
                return Object.Equals(x, y);

            return x.Equals(y);
        }

        public static bool operator !=(MotionInfo x, MotionInfo y)
        {
            if (((object)x) == null || ((object)y) == null)
                return !Object.Equals(x, y);
            return !(x.Equals(y));
        }

        public override int GetHashCode()
        {
            unchecked
            {
                const int Prime = 397;
                int result = 37;

                result *= Prime;
                result += Time.GetHashCode();

                return result;
            }
        }

        public override string ToString()
        {
            string matricesString = "\n" + string.Join("\n", Matrices.Select(item => item.ToString()).ToArray());

            return string.Format
                ("\nimg: {0}\nimgHeight: {1:0}\nimgWidth: {2:0}\nmatrices:\n {3}\nname: {4}\nplug: {5}\nreason: {6}\ntime: {7}\n",
                Img,
                ImgHeight,
                ImgWidth,
                matricesString,
                Name,
                Plug,
                Reason,
                Time
            );
        }
    }
}
