using System;


/**
 *                          {
 *                              group: 'myGroup',
 *                              time: 2020-04-24T13:49:16+00:00,
 *                              monitorId: 'myMonitorId',
 *                              plug: TensorFlow-WithFiltering-And-MQTT,
 *                              details: {
 *                                  plug: TensorFlow-WithFiltering-And-MQTT,
 *                                  name: 'Tensorflow',
 *                                  reason: 'object',
 *                                  matrices: [{
 *                                      "x": 2.313079833984375,
 *                                      "y": 1.0182666778564453,
 *                                      "width": 373.25050354003906,
 *                                      "height": 476.9341278076172,
 *                                      "tag": "person",
 *                                      "confidence": 0.7375929355621338
 *                                  }],
 *                                  img: 'base64',
 *                                  imgHeight: 64,
 *                                  imgWidth: 48,
 *                                  time: 2020-04-24T13:49:16+00:00
 *                              }
 *                          }
 */

namespace WebApp.Data
{
    public sealed class MotionLocation : BasePOCO<MotionLocation>, IEquatable<MotionLocation>
    {
        public double X { get; }
        public double Y { get; }
        public double Width { get; }
        public double Height { get; }
        public string Tag { get; }
        public double Confidence { get; }

        public MotionLocation(double x, double y, double width, double height, string tag, double confidence)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            Tag = tag ?? throw new ArgumentNullException(nameof(tag));
            Confidence = confidence;
        }

        public MotionLocation(MotionLocation other)
        {
            if (other is null)
                throw new ArgumentNullException(nameof(other));

            X = other.X;
            Y = other.Y;
            Width = other.Width;
            Height = other.Height;
            Tag = other.Tag;
            Confidence = other.Confidence;
        }

        public override bool Equals(MotionLocation other)
        {
            if (ReferenceEquals(other, null))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (!(X == other.X)
                || !(Y == other.Y)
                || !(Width == other.Width)
                || !(Height == other.Height)
                || !(Tag == other.Tag)
                || !(Confidence == other.Confidence))
            {
                return false;
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            MotionLocation other = obj as MotionLocation;

            if (other == null)
                return false;
            else
                return Equals(other);
        }

        public static bool operator ==(MotionLocation x, MotionLocation y)
        {
            if (((object)x) == null || ((object)y) == null)
                return Object.Equals(x, y);

            return x.Equals(y);
        }

        public static bool operator !=(MotionLocation x, MotionLocation y)
        {
            if (((object)x) == null || ((object)y) == null)
                return !Object.Equals(x, y);
            return !(x.Equals(y));
        }

        public override int GetHashCode()
        {
            return new { Confidence, Height, Tag, Width, X, Y }.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format(
                "x: {0}\ny: {1}\nwidth: {2}\nheight: {3}\ntag: {4}\nconfidence: {5}",
                X,
                Y,
                Width,
                Height,
                Tag,
                Confidence
            );
        }
    }
}