using System.IO;
using System.Threading.Tasks;


namespace WebApp.S3.Contracts
{
    public interface IS3Service
    {
        Task UploadAsync(BufferedStream byteStream, string objectName);
        Task<byte[]> UploadImage(string payload, string guid, int initialBufferSize = 1024);
        Task<BufferedStream> DownloadObject(string objectName);
    }
}
