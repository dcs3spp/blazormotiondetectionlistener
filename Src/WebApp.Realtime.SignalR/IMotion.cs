using System.Threading.Tasks;

using WebApp.Data;


namespace WebApp.Realtime.SignalR
{
    /// <summary>
    /// SignalR clients must implement this interface
    /// </summary>
    public interface IMotion
    {
        /// <summary>Receive notification of detection</summary>
        /// <param name="detection">Motion detection details</param>
        Task ReceiveMotionDetection(MotionDetection detection);
    }
}
