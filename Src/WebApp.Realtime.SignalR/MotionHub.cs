using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;


namespace WebApp.Realtime.SignalR
{
    /// <summary>signalR hub with handlers for client connection and disconnection</summary>
    /// <remarks>
    /// Provides endpoints, available for the clients to call.
    /// </remarks>
    public class MotionHub : Hub<IMotion>
    {
        public ILogger<MotionHub> Logger { get; set; }

        public MotionHub(ILogger<MotionHub> logger) : base()
        {
            Logger = logger;
        }


        public override async Task OnConnectedAsync()
        {
            Logger.LogInformation($"MotionHub accepted connection => connection ID={Context.ConnectionId} : user={Context.User.Identity.Name}");
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            if (exception != null)
            {
                Logger.LogInformation($"MotionHub disconnected connection ID={Context.ConnectionId} : user={Context.User.Identity.Name} : exception={exception.Message}");
            }
            else
            {
                Logger.LogInformation($"MotionHub disconnected connection ID={Context.ConnectionId} : user={Context.User.Identity.Name}");
            }

            await base.OnDisconnectedAsync(exception);
        }
    }
}
