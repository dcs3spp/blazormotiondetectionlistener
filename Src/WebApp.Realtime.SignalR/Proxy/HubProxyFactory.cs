using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

/**
 * Hub Proxy for ASP.NET Core signalR Client adapted from
 * https://github.com/AndersMalmgren/SignalR.EventAggregatorProxy/blob/b2c194df616d8bb319256ccdf551a92ab2655027/SignalR.EventAggregatorProxy.Client.DotNetCore/Bootstrap/Factories/HubProxyFactory.cs
 * 
 * Microsoft's initial stance is for developers to implement a wrapper interface:
 * - https://github.com/dotnet/aspnetcore/issues/14924
 * - https://github.com/dotnet/aspnetcore/issues/8133
 * This allows for increased testability.
 */
namespace WebApp.Realtime.SignalR.Client
{
    public class HubProxyFactory : IHubProxyFactory
    {
        public IHubProxy Create(string hubUrl, JsonSerializerOptions jsonOptions)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl(hubUrl)
                .ConfigureLogging(logging =>
                {
                    logging.AddConsole();
                    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Information);
                    logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Information);
                })
                .AddJsonProtocol(options =>
                {
                    options.PayloadSerializerOptions = jsonOptions;
                })
                .Build();

            return new HubProxy(connection);
        }
    }

    public class HubProxy : IHubProxy
    {
        private readonly object _eventLock = new object();

        public HubConnectionState State { get; }

        #region Closed event handler
        event Func<Exception, Task> IHubProxy.Closed
        {
            add
            {
                lock (_eventLock)
                {
                    connection.Closed += value;
                }
            }
            remove
            {
                lock (_eventLock)
                {
                    connection.Closed -= value;
                }
            }
        }
        #endregion

        private readonly HubConnection connection;

        public HubProxy(HubConnection connection)
        {
            this.connection = connection;
        }

        public IDisposable On<T>(string methodName, Action<T> handler)
        {
            return connection.On(methodName, handler);
        }

        public Task StartAsync(CancellationToken cancellationToken = default)
        {
            return connection.StartAsync();
        }

        public Task StopAsync(CancellationToken cancellationToken = default)
        {
            return connection.StopAsync();
        }

        public Task DisposeAsync()
        {
            return connection.DisposeAsync();
        }
    }
}
