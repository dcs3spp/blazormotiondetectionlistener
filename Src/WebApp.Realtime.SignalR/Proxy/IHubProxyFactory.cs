using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;



namespace WebApp.Realtime.SignalR.Client
{
    public interface IHubProxyFactory
    {
        IHubProxy Create(string hubUrl, JsonSerializerOptions jsonOptions);
    }

    public interface IHubProxy
    {
        event Func<Exception, Task> Closed;
        HubConnectionState State { get; }
        IDisposable On<T>(string methodName, Action<T> handler);

        Task StartAsync(CancellationToken cancellationToken = default);
        Task StopAsync(CancellationToken cancellationToken = default);
        Task DisposeAsync();
    }
}
