﻿using Autofac;
using System.Linq;
using System.Reflection;
using System.Text.Json.Serialization;

using WebApp.Data.Serializers.Contracts;


namespace WebApp.Data.Serializers.AutofacModule
{
    public class SerializerModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly serializers = typeof(MotionDetectionSerializer<>).GetTypeInfo().Assembly;

            builder.RegisterAssemblyTypes(serializers)
                   .AsImplementedInterfaces();

            builder.RegisterGeneric(typeof(MotionDetectionSerializer<>))
                .As(typeof(IMotionDetectionSerializer<>));

            builder.RegisterAssemblyTypes(serializers)
                .Where(t => t.IsSubclassOf(typeof(JsonConverter)))
                .As<JsonConverter>();
        }
    }
}
