﻿using System.Reflection;

using Autofac;
using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;
using Microsoft.Extensions.Options;

using WebApp.Data;
using WebApp.Kafka.Config;
using WebApp.Kafka.Contracts;
using WebApp.Kafka.ProducerProvider;
using WebApp.S3.AutofacModule;


namespace WebApp.Kafka.AutofacModule
{
    public class KafkaModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly serializers = typeof(ConsumerService).GetTypeInfo().Assembly;

            builder.RegisterModule(new S3Module());

            // register a factory method for creating a Kafka consumer instance
            builder.Register<ConsumerFactory>(context =>
            {
                return (KafkaConfig config, IAsyncDeserializer<MotionDetection> serializer) =>
                    {
                        var consumerBuilder = new ConsumerBuilder<string, MotionDetection>(config.Consumer);
                        consumerBuilder.SetValueDeserializer(serializer.AsSyncOverAsync());
                        return consumerBuilder.Build();
                    };
            }).SingleInstance();

            // register a provider for creating instance of ProducerService
            builder.RegisterType<KafkaProducerProvider>()
                .As(typeof(IKafkaProducerProvider))
                .SingleInstance();

            // register KafkaConfig options validation
            builder.RegisterType<KafkaConfigValidation>()
                .As(typeof(IValidateOptions<KafkaConfig>))
                .SingleInstance();

            builder.RegisterGeneric(typeof(ProducerService<,>))
                .As(typeof(IProducerService<,>))
                .SingleInstance();

            builder.RegisterGeneric(typeof(WebApp.Kafka.SchemaRegistry.Serdes.JsonDeserializer<>))
                .As(typeof(IAsyncDeserializer<>))
                .SingleInstance();

            builder.RegisterType<ConsumerService>()
                .AsSelf()
                .SingleInstance();
        }
    }
}
