﻿using System.Reflection;

using Autofac;
using Microsoft.Extensions.Options;

using WebApp.Mqtt.Contracts;
using WebApp.Mqtt.Config;
using WebApp.Mqtt.Factory;


namespace WebApp.Mqtt.AutofacModule
{
    public class MqttModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly repository = typeof(MqttConfig).GetTypeInfo().Assembly;

            builder.RegisterType<MQTTClientFactory>()
                .As<IMQTTClientFactory>()
                .SingleInstance();

            builder.RegisterType<MqttService>()
                .As<IMqttService>()
                .SingleInstance();

            builder.RegisterType<MqttConfigValidation>()
                .As<IValidateOptions<MqttConfig>>()
                .SingleInstance();
        }
    }
}

