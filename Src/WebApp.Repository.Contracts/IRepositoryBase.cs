﻿using System.IO;
using System.Threading.Tasks;


namespace WebApp.Repository.Contracts
{
    public interface IRepositoryBase<T>
    {
        Task<T> FromJSONAsync(Stream stream);
        T FromJSON(Stream stream);

        void AddItem(T item);
    }
}
