using System.Collections;
using WebApp.Data;

namespace WebApp.Repository.Contracts
{
    public interface IMotionDetectionRepository : IRepositoryBase<MotionDetection>
    {
        IEnumerator GetEnumerator();
    }
}
