﻿using System;
using System.Collections;
using System.IO;
using System.Threading.Tasks;

using Cyotek.Collections.Generic;

using Microsoft.Extensions.Logging;

using WebApp.Repository.Contracts;
using WebApp.Data;
using WebApp.Data.Serializers.Contracts;


namespace WebApp.Repository
{
    public sealed class MotionDetectionRepository : IMotionDetectionRepository, IEnumerable
    {
        private CircularBuffer<MotionDetection> _Buffer;
        private ILogger<MotionDetectionRepository> _Logger;
        private IMotionDetectionSerializer<MotionDetection> _Serializer { get; set; }


        public MotionDetectionRepository(IMotionDetectionSerializer<MotionDetection> serializer, ILogger<MotionDetectionRepository> logger, int capacity = 10)
        {
            _Buffer = new CircularBuffer<MotionDetection>(capacity);
            _Logger = logger;
            _Serializer = serializer;
        }

        public async Task<MotionDetection> FromJSONAsync(Stream stream)
        {
            if (stream is null)
                throw new ArgumentNullException(nameof(stream));

            MotionDetection detection = await _Serializer.FromJSONAsync(stream);
            _Buffer.Put(detection);

            return detection;
        }

        public MotionDetection FromJSON(Stream stream)
        {
            if (stream is null)
                throw new ArgumentNullException(nameof(stream));

            MotionDetection detection = _Serializer.FromJSON(stream);
            _Buffer.Put(detection);

            return detection;
        }

        public void AddItem(MotionDetection item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _Buffer.Put(item);
        }

        public IEnumerator GetEnumerator()
        {
            foreach (MotionDetection item in _Buffer)
            {
                yield return item;
            }
        }
    }
}
