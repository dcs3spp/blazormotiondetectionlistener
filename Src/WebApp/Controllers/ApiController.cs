using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using WebApp.S3.Contracts;


namespace WebApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        private readonly IS3Service _s3Service;
        private readonly ILogger<ApiController> _logger;
        public ApiController(ILogger<ApiController> logger, IS3Service service)
        {
            _logger = logger;
            _s3Service = service;
        }

        [HttpGet("{objectName}", Name = "DownloadImage")]
        [ProducesResponseType(typeof(FileStreamResult), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> DownloadImageAsync(string objectName)
        {
            const string ContentType = "image/jpeg";

            BufferedStream buffered;

            try
            {
                _logger.LogInformation($"Trying to retrieve image associated with id {objectName} from S3 storage");
                buffered = await _s3Service.DownloadObject(objectName);

                _logger.LogInformation($"Controller downloaded {buffered.Length / 1024.0 / 1024.0} MB from S3 service");
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occured while connection to S3 storage => {}", e.Message);
                return NotFound();
            }

            var result = new FileStreamResult(buffered, ContentType); // Let FileStreamResult dispose of the underlying stream for us
            result.FileDownloadName = objectName;

            _logger.LogInformation("Retrieved image from S3");
            return result;
        }
    }
}
