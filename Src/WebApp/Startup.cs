using System;
using System.Reflection;

using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using WebApp.BackgroundServices;
using WebApp.Data;
using WebApp.Data.Serializers.Converters;
using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Kafka;
using WebApp.Kafka.Admin;
using WebApp.Kafka.AutofacModule;
using WebApp.Kafka.Config;
using WebApp.Mqtt.AutofacModule;
using WebApp.Mqtt.Config;
using WebApp.Realtime.SignalR;
using WebApp.Realtime.SignalR.Client;
using WebApp.Repository.AutofacModule;
using WebApp.S3.Config;


namespace WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();

            services
                .AddCustomConfiguration(Configuration)
                .AddBackgroundServices()
                .AddLogging()
                .AddCustomSignalR()
                .AddKafkaAdminService();
        }


        // ConfigureContainer is where you can register things directly
        // with Autofac. This runs after ConfigureServices so the things
        // here will override registrations made in ConfigureServices.
        // Don't build the container; that gets done for you by the factory.
        public virtual void ConfigureContainer(ContainerBuilder builder)
        {
            // Register your own things directly with Autofac here. Don't
            // call builder.Populate(), that happens in AutofacServiceProviderFactory
            // for you.
            builder.RegisterModule(new MqttModule());
            builder.RegisterModule(new MotionDetectionRepositoryModule());
            builder.RegisterModule(new KafkaModule());
            builder.RegisterAssemblyTypes(typeof(MotionDetection).GetTypeInfo().Assembly);
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<MotionHub>("/motionhub");
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }

    static class CustomExtensionsMethods
    {
        /// <summary>
        /// Create background services to:
        /// 1: Mqtt->Kafka Bridge for object detections
        /// 2: Consume Kafka object detections and forward to signalR
        /// </summary>
        /// <param name="services">Service collection</param>
        public static IServiceCollection AddBackgroundServices(this IServiceCollection services)
        {
            services.AddHostedService<MqttKafkaBridge>();
            services.AddHostedService<ConsumerService>();

            return services;
        }

        public static IServiceCollection AddCustomConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            if (!configuration.GetSection(S3Config.SectionName).Exists())
            {
                throw new InvalidOperationException($"Failed to locate section {S3Config.SectionName} in config file");
            }
            services.Configure<S3Config>(options => configuration.GetSection(S3Config.SectionName).Bind(options));

            if (!configuration.GetSection(MqttConfig.SectionName).Exists())
            {
                throw new InvalidOperationException($"Failed to locate section {MqttConfig.SectionName} in config file");
            }
            services.Configure<MqttConfig>(options => configuration.GetSection(MqttConfig.SectionName).Bind(options));

            if (!configuration.GetSection(KafkaConfig.SectionName).Exists())
            {
                throw new InvalidOperationException($"Failed to locate section {KafkaConfig.SectionName} in config file");
            }
            services.Configure<KafkaConfig>(options => configuration.GetSection(KafkaConfig.SectionName).Bind(options));

            return services;
        }

        public static IServiceCollection AddCustomSignalR(this IServiceCollection services)
        {
            var sp = services.BuildServiceProvider();

            var loggerMD = sp.GetService<ILogger<MotionDetectionConverter>>();
            var loggerMI = sp.GetService<ILogger<MotionInfoConverter>>();
            var loggerJV = sp.GetService<ILogger<JsonVisitor>>();

            services.AddSignalR(o => o.EnableDetailedErrors = true)
               .AddJsonProtocol(options =>
               {
                   options.PayloadSerializerOptions = JsonConvertersFactory.CreateDefaultJsonConverters(loggerMD, loggerMI, loggerJV);
               });

            services.AddSingleton<IHubProxyFactory, HubProxyFactory>();

            return services;
        }

        public static IServiceCollection AddKafkaAdminService(this IServiceCollection services)
        {
            var sp = services.BuildServiceProvider();

            services.AddSingleton<IKafkaAdminFactory, KafkaAdminFactory>();
            services.AddAsyncInitializer<KafkaAdminService>();

            return services;
        }
    }
}

