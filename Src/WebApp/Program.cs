
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Autofac.Extensions.DependencyInjection;
using MessagePack;


namespace WebApp
{
    public class Program
    {
        /// <summary>
        /// Create Kafka topic from config on Kafka Cluster here before 
        /// ASp.NET Core Server starts. This allows fine grained control
        /// of limiting auto creation of a specific topic, rather than
        /// automically creating all topics on confluent Kafka cluster
        ///
        /// Considered doing this from the Startup.Configure method but that is synchronous and
        /// Confluent's library creation methods are asynchronous.
        /// 
        /// Sadly, running async initialisation task(s) cleanly before ASP.NET Core starts is still 
        /// an issue in ASP.NET Core 5, https://github.com/aspnet/Hosting/issues/1085 
        ///
        /// StartupFilters are synchronous. Could use IHostedService as suggested here,
        /// https://andrewlock.net/running-async-tasks-on-app-startup-in-asp-net-core-3/.
        /// However this causes blocking problems when used with Microsoft's TestServer 
        /// https://github.com/dotnet/aspnetcore/issues/6395, which I experienced in this project.
        /// 
        /// The decision was made to use a small helper library
        /// https://github.com/thomaslevesque/Extensions.Hosting.AsyncInitialization to facilitate
        /// cleanly representing a pre-start asynchronous task that implements an interface
        /// </summary>
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            await host.InitAsync();
            await host.RunAsync();

            /* see https://docs.microsoft.com/en-us/aspnet/core/signalr/messagepackhubprotocol?view=aspnetcore-3.1#configure-messagepack-on-the-server-1 */
            MessagePackSecurity.Active = MessagePackSecurity.UntrustedData;
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddConsole();
                    logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Information);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
