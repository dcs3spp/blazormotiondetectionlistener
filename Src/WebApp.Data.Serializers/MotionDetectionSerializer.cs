using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

using WebApp.Data.Serializers.Contracts;


namespace WebApp.Data.Serializers
{
    /// <summary>
    /// Class to deserialize json into a MotionDetection object
    /// Deserialization is performed to class instance properties 
    /// using CamelCase property naming policy.
    /// </summary>
    public class MotionDetectionSerializer<T> : IMotionDetectionSerializer<T>
    {
        private JsonSerializerOptions SerializationOptions { get; }
        private IJsonSerializerWrapper JsonSerializerWrapper { get; }


        /// <summary>
        /// Construct a serializer to deserialize json into a MotionDetection object's
        /// corresponding CamelCase properties.
        /// </summary>
        public MotionDetectionSerializer(IList<JsonConverter> converters, IJsonSerializerWrapper jsonSerializer)
        {
            SerializationOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            foreach (JsonConverter converter in converters)
            {
                SerializationOptions.Converters.Add(converter);
            }

            JsonSerializerWrapper = jsonSerializer;
        }


        /// <summary>
        /// Reads a json stream and deserializes into a MotionDetection object.
        /// The stream is read to completion unless an exception is thrown.
        /// </summary>
        public async Task<T> FromJSONAsync(Stream stream)
        {
            if (stream is null)
            {
                throw new ArgumentNullException(nameof(Stream));
            }

            return await JsonSerializerWrapper.DeserializeAsync<T>(stream, SerializationOptions);
        }

        /// <summary>
        /// Reads a json stream and deserializes into a MotionDetection object.
        /// The underlying stream is closed after the operation completes or an exception is thrown.
        /// The stream is read until completion.
        /// </summary>
        public T FromJSON(Stream stream)
        {
            if (stream is null)
            {
                throw new ArgumentNullException(nameof(Stream));
            }

            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                return JsonSerializerWrapper.Deserialize<T>(reader.ReadToEnd(), SerializationOptions);
            }
        }
    }
}