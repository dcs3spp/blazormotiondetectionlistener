using System;
using System.Text.Json;

using WebApp.Data.Serializers.Contracts;


namespace WebApp.Data.Serializers.Json
{
    public class JsonParser : IJsonParser
    {
        public JsonParser()
        { }

        public string ReadProperty(ref Utf8JsonReader reader, string propertyName)
        {
            reader.Read();

            if (reader.TokenType != JsonTokenType.PropertyName)
            {
                throw new JsonException(string.Format(@"Failed to read property {0}, encountered token {1}", propertyName, reader.TokenType));
            }

            var propName = reader.GetString();

            if (propName != propertyName)
            {
                throw new JsonException(string.Format("Unrecognised property name. Expected {0} but read {1}", propertyName, propName));
            }

            return propName;
        }

        public string ReadStringProperty(ref Utf8JsonReader reader, string propertyName)
        {
            var propName = ReadProperty(ref reader, propertyName);

            reader.Read();

            var val = reader.GetString();

            return val;
        }
        public virtual long ReadLongProperty(ref Utf8JsonReader reader, string propertyName)
        {
            var propName = ReadProperty(ref reader, propertyName);

            reader.Read();

            long val = default;

            try
            {
                if (!reader.TryGetInt64(out val))
                {
                    throw new JsonException(string.Format("Failed to read property {0} as long", propertyName));
                }
            }
            catch (InvalidOperationException io)
            {
                throw new JsonException(string.Format("Property {0}, is not a number", propertyName), io);
            }

            return val;
        }
        public virtual int ReadIntProperty(ref Utf8JsonReader reader, string propertyName)
        {
            var propName = ReadProperty(ref reader, propertyName);

            reader.Read();

            int val = default;

            try
            {
                if (!reader.TryGetInt32(out val))
                {
                    throw new JsonException(string.Format("Failed to read property {0} as integer", propertyName));
                }
            }
            catch (InvalidOperationException io)
            {
                throw new JsonException(string.Format("Property {0}, is not a number", propertyName), io);
            }

            return val;
        }

        public virtual double ReadDoubleProperty(ref Utf8JsonReader reader, string propertyName)
        {
            var propName = ReadProperty(ref reader, propertyName);

            reader.Read();

            double val = default;

            try
            {
                if (!reader.TryGetDouble(out val))
                {
                    throw new JsonException(string.Format("Failed to read property {0} as double", propertyName));
                }
            }
            catch (InvalidOperationException io)
            {
                throw new JsonException(string.Format("Property {0} is not a number", propertyName), io);
            }

            return val;
        }

        public virtual DateTimeOffset ReadDateTimeOffsetProperty(ref Utf8JsonReader reader, string propertyName)
        {
            var propName = ReadProperty(ref reader, propertyName);

            reader.Read();

            DateTimeOffset time = default(DateTimeOffset);
            try
            {
                if (!reader.TryGetDateTimeOffset(out time))
                {
                    throw new JsonException(string.Format("Failed to read property {0} as date with time offset", propertyName));
                }
            }
            catch (InvalidOperationException io)
            {
                throw new JsonException(string.Format("Property {0} is not a date time string", propertyName), io);
            }

            return time;
        }


        public virtual void ThrowFailedConversion(string propertyName)
        {
            throw new JsonException(string.Format("Failed to convert {0:s} property while deserializing", propertyName));
        }

        public virtual void ThrowUnrecognisedProperty(string propertyName)
        {
            throw new JsonException(string.Format("Unrecognised property => {0:s}", propertyName));
        }
    }
}