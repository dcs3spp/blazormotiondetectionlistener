using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

using WebApp.Data.Serializers.Contracts;


namespace WebApp.Data.Serializers.Json
{
    /// <summary>
    /// Wrapper for static JsonSerializer.Deserialize methods, since Moq cannot mock
    /// static class methods such as the JsonSerializer.Deserialize methods provided
    /// by the third party library System.Text.Json.
    ///
    /// A better solution is to use a free version of something like Microsoft Fakes to 
    /// produce a 'shimmed' return value of JsonSerializer.Deserialize. For third-party libraries
    /// it appears reasonable to use shims. 
    /// 
    /// Sadly, at the time of writing there appears to be lack of support for this in 
    /// .NET Core. So until something better is provided by the community have had to
    /// produce an asbtraction for the static Deserialize method provided by JsonSerializer
    /// and then inject the wrapper in the constructor.

    /// There are alternative priced solutions out there for doing this for those who
    /// wish to go down that route.
    /// </summary>
    public class JsonSerializerWrapper : IJsonSerializerWrapper
    {
        public virtual async Task<T> DeserializeAsync<T>(Stream stream, JsonSerializerOptions options)
        {
            return await JsonSerializer.DeserializeAsync<T>(stream, options);
        }

        public virtual T Deserialize<T>(string json, JsonSerializerOptions options)
        {
            return JsonSerializer.Deserialize<T>(json, options);
        }

        public virtual T Deserialize<T>(ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            return JsonSerializer.Deserialize<T>(ref reader, options);
        }
    }
}