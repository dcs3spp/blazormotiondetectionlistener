using System;
using System.Text.Json;
using System.Text.Json.Serialization;

using Microsoft.Extensions.Logging;

using WebApp.Data.Serializers.Contracts;


namespace WebApp.Data.Serializers.Converters
{
    public sealed class MotionInfoConverter : JsonConverter<MotionInfo>
    {
        private readonly ILogger _log;
        private readonly IVisitorElement<MotionInfo> _parseModel;
        private IVisitor _visitor;

        public MotionInfoConverter(
            IVisitor visitor,
            IVisitorElement<MotionInfo> parseModel,
            ILogger<MotionInfoConverter> logger)
        {
            _log = logger ?? throw new ArgumentNullException();
            _parseModel = parseModel ?? throw new ArgumentNullException();
            _visitor = visitor ?? throw new ArgumentNullException(nameof(logger));
        }

        public override MotionInfo Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            _parseModel.accept(_visitor, ref reader, options);

            _log.LogTrace("\n{0}\n", _parseModel);

            return _parseModel.ToModel();
        }

        public override void Write(Utf8JsonWriter writer, MotionInfo motionDetection, JsonSerializerOptions options) =>
                throw new NotImplementedException("Serialization is not implemented for MotionInfo objects");
    }
}

