using System;
using System.Text.Json;
using System.Text.Json.Serialization;

using Microsoft.Extensions.Logging;

using WebApp.Data.Serializers.Contracts;


namespace WebApp.Data.Serializers.Converters
{
    public sealed class MotionDetectionConverter : JsonConverter<MotionDetection>
    {
        private readonly ILogger _log;
        private readonly IVisitorElement<MotionDetection> _parseModel;
        private IVisitor _visitor;

        public MotionDetectionConverter(
            IVisitor visitor,
            IVisitorElement<MotionDetection> parseModel,
            ILogger<MotionDetectionConverter> logger)
        {
            _log = logger ?? throw new ArgumentNullException(nameof(logger));
            _visitor = visitor ?? throw new ArgumentNullException(nameof(visitor));
            _parseModel = parseModel ?? throw new ArgumentNullException(nameof(parseModel));
        }

        public override MotionDetection Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            _parseModel.accept(_visitor, ref reader, options);

            _log.LogTrace("\n{0}\n", _parseModel);

            return _parseModel.ToModel();
        }

        public override void Write(Utf8JsonWriter writer, MotionDetection motionDetection, JsonSerializerOptions options)
        {

            writer.WriteStartObject();
            writer.WriteString(WriteProperty(nameof(motionDetection.Group), options.PropertyNamingPolicy), motionDetection.Group);
            writer.WriteNumber(WriteProperty(nameof(motionDetection.Time), options.PropertyNamingPolicy), motionDetection.Time);
            writer.WriteString(WriteProperty(nameof(motionDetection.MonitorId), options.PropertyNamingPolicy), motionDetection.MonitorId);
            writer.WriteString(WriteProperty(nameof(motionDetection.Plug), options.PropertyNamingPolicy), motionDetection.Plug);

            writer.WriteStartObject(WriteProperty(nameof(motionDetection.Details), options.PropertyNamingPolicy));
            writer.WriteString(WriteProperty(nameof(motionDetection.Details.Plug), options.PropertyNamingPolicy), motionDetection.Details.Plug);
            writer.WriteString(WriteProperty(nameof(motionDetection.Details.Name), options.PropertyNamingPolicy), motionDetection.Details.Name);
            writer.WriteString(WriteProperty(nameof(motionDetection.Details.Reason), options.PropertyNamingPolicy), motionDetection.Details.Reason);

            writer.WriteStartArray(WriteProperty(nameof(motionDetection.Details.Matrices), options.PropertyNamingPolicy));
            foreach (var location in motionDetection.Details.Matrices)
            {
                writer.WriteStartObject();
                writer.WriteNumber(WriteProperty(nameof(location.X), options.PropertyNamingPolicy), location.X);
                writer.WriteNumber(WriteProperty(nameof(location.Y), options.PropertyNamingPolicy), location.Y);
                writer.WriteNumber(WriteProperty(nameof(location.Width), options.PropertyNamingPolicy), location.Width);
                writer.WriteNumber(WriteProperty(nameof(location.Height), options.PropertyNamingPolicy), location.Height);
                writer.WriteString(WriteProperty(nameof(location.Tag), options.PropertyNamingPolicy), location.Tag);
                writer.WriteNumber(WriteProperty(nameof(location.Confidence), options.PropertyNamingPolicy), location.Confidence);
                writer.WriteEndObject();
            }
            writer.WriteEndArray();
            writer.WriteString(WriteProperty(nameof(motionDetection.Details.Img), options.PropertyNamingPolicy), motionDetection.Details.Img);
            writer.WriteNumber(WriteProperty(nameof(motionDetection.Details.ImgHeight), options.PropertyNamingPolicy), motionDetection.Details.ImgHeight);
            writer.WriteNumber(WriteProperty(nameof(motionDetection.Details.ImgWidth), options.PropertyNamingPolicy), motionDetection.Details.ImgWidth);
            writer.WriteNumber(WriteProperty(nameof(motionDetection.Details.Time), options.PropertyNamingPolicy), motionDetection.Details.Time);
            writer.WriteEndObject();

            writer.WriteEndObject();
        }

        private string WriteProperty(string propertyName, JsonNamingPolicy policy)
        {
            if (policy == null)
                return propertyName;

            return policy.ConvertName(propertyName);
        }
    }
}
