using System.Text.Json;

using Microsoft.Extensions.Logging;

using WebApp.Data.Serializers.Converters.Visitors;
using WebApp.Data.Serializers.Json;


namespace WebApp.Data.Serializers.Converters
{
    public class JsonConvertersFactory
    {
        ///<summary>
        /// Creates default JSON serialiser options:
        /// Uses camelCase naming policy
        /// Contains Json Converters for MotionDetection and MotionInfo
        /// </summary>
        /// <param name="loggerMotionDetection">Logger for use in MotionDetection converter</param>
        /// <param name="loggerInfo">Logger for use in MotionInfo converter</param>
        /// <param name="loggerVisitor">Logger for use in JsonVisitor</param>
        /// <remarks>
        /// Considered injecting ILoggerFactory to allow factory method to generate
        /// but true DI comes from injecting ILogger dependencies required
        /// </remarks>
        public static JsonSerializerOptions CreateDefaultJsonConverters(
            ILogger<MotionDetectionConverter> loggerMotionDetection,
            ILogger<MotionInfoConverter> loggerInfo,
            ILogger<JsonVisitor> loggerVisitor
        )

        {
            JsonSerializerWrapper jsonSerializerWrapper = new JsonSerializerWrapper();

            var parsedModel = new ParsedMotionDetection();
            var parsedInfoModel = new ParsedMotionInfo();
            var parser = new JsonParser();
            var visitor = new JsonVisitor(jsonSerializerWrapper, parser, loggerVisitor);

            JsonSerializerOptions serializeOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            serializeOptions.Converters.Add(new MotionDetectionConverter(visitor, parsedModel, loggerMotionDetection));
            serializeOptions.Converters.Add(new MotionInfoConverter(visitor, parsedInfoModel, loggerInfo));

            return serializeOptions;
        }
    }
}
