using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

using WebApp.Data.Serializers.Contracts;


/**
 *                          {
 *                              group: 'myGroup',
 *                              time: 2020-04-24T13:49:16+00:00,
 *                              monitorId: 'myMonitorId',
 *                              plug: TensorFlow-WithFiltering-And-MQTT,
 *                              details: {
 *                                  plug: TensorFlow-WithFiltering-And-MQTT,
 *                                  name: 'Tensorflow',
 *                                  reason: 'object',
 *                                  matrices: [{
 *                                      "x": 2.313079833984375,
 *                                      "y": 1.0182666778564453,
 *                                      "width": 373.25050354003906,
 *                                      "height": 476.9341278076172,
 *                                      "tag": "person",
 *                                      "confidence": 0.7375929355621338
 *                                  }],
 *                                  img: 'base64',
 *                                  imgHeight: 64,
 *                                  imgWidth: 48,
 *                                  time: 2020-04-24T13:49:16+00:00
 *                              }
 *                          }
 */

namespace WebApp.Data.Serializers.Converters.Visitors
{
    public class ParsedLocation : IParsedLocation
    {
        #region Constants
        private const string Header = "Parsed Location\n---------------\n";
        #endregion

        #region Attributes
        public double confidence { get; set; }
        public double height { get; set; }
        public string tag { get; set; }
        public double width { get; set; }
        public double x { get; set; }
        public double y { get; set; }
        #endregion

        #region IVisitorElement interface
        public void accept(IVisitor visitor, ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            visitor.DeserializeMotionLocation(this, ref reader, options);
        }
        public MotionLocation ToModel()
        {
            return new MotionLocation(x, y, width, height, tag, confidence);
        }
        #endregion

        #region Object overrides
        public override string ToString()
        {
            return string.Format
                ("\n{0}\nx: {1}\ny: {2}\nwidth: {3}\nheight: {4}\ntag: {5}\nconfidence: {6}\n",
                Header,
                x,
                y,
                width,
                height,
                tag,
                confidence
            );
        }
        #endregion
    }

    public class ParsedMotionInfo : IParsedMotionInfo
    {
        #region Constants
        private const string Header = "Parsed Motion Info\n------------------\n";
        #endregion

        #region Attributes
        public string img { get; set; }
        public int imgHeight { get; set; }
        public int imgWidth { get; set; }
        public IList<MotionLocation> matrices { get; set; }
        public string name { get; set; }
        public string plug { get; set; }
        public string reason { get; set; }
        public long time { get; set; }
        #endregion

        #region IVisitorElement interface
        public void accept(IVisitor visitor, ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            visitor.DeserializeMotionInfo(this, ref reader, options);
        }

        public MotionInfo ToModel()
        {
            return new MotionInfo(plug, name, reason, matrices, img, imgHeight, imgWidth, time);
        }
        #endregion

        #region Object overrides
        public override string ToString()
        {
            string matricesString = string.Join("\n", matrices.Select(item => item.ToString()).ToArray());

            return string.Format
                ("{0}\nimg: {1}\nimgHeight: {2:0}\nimgWidth: {3:0}\nmatrices:\n {4}\nname: {5}\nplug: {6}\nreason: {7}\ntime: {8}",
                Header,
                img,
                imgHeight,
                imgWidth,
                matricesString,
                name,
                plug,
                reason,
                time
            );
        }
        #endregion
    }

    public class ParsedMotionDetection : IParsedMotionDetection
    {
        #region Constants
        private const string Header = "Parsed Motion Detection\n-----------------------\n";
        #endregion

        #region Properties
        public MotionInfo details { get; set; }
        public string group { get; set; }
        public string monitorId { get; set; }
        public string plug { get; set; }
        public long time { get; set; }
        #endregion

        #region IVisitorElement interface
        public void accept(IVisitor visitor, ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            visitor.DeserializeMotionDetection(this, ref reader, options);
        }

        public MotionDetection ToModel()
        {
            return new MotionDetection(group, time, monitorId, plug, details);
        }
        #endregion

        #region Object overrides
        public override string ToString()
        {
            return string.Format("\n{0}\ndetails: {1}\ngroup: {2}\nmonitorId: {3}\nplugin: {4}\ntime: {5}\n", Header, details, group, monitorId, plug, time);
        }
        #endregion
    }
}
