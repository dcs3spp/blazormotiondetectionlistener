namespace WebApp.Data.Serializers.Converters.Visitors
{
    public static class MotionDetectionPropertyNames
    {
        public const string Details = "details";
        public const string Group = "group";
        public const string MonitorId = "monitorId";
        public const string Plug = "plug";
        public const string Time = "time";
    }

    public static class MotionInfoPropertyNames
    {
        public const string Img = "img";
        public const string ImgHeight = "imgHeight";
        public const string ImgWidth = "imgWidth";
        public const string Matrices = "matrices";
        public const string Name = "name";
        public const string Plug = "plug";
        public const string Reason = "reason";
        public const string Time = "time";
    }

    public static class MotionLocationPropertyNames
    {
        public const string Confidence = "confidence";
        public const string Height = "height";
        public const string Tag = "tag";
        public const string Width = "width";
        public const string X = "x";
        public const string Y = "y";
    }
}
