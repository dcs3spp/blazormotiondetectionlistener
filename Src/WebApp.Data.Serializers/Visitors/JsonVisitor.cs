using System;
using System.Collections.Generic;
using System.Text.Json;

using Microsoft.Extensions.Logging;

using WebApp.Data.Serializers.Contracts;


namespace WebApp.Data.Serializers.Converters.Visitors
{
    public class JsonVisitor : IVisitor
    {
        private ILogger _log;
        private IJsonSerializerWrapper _jsonSerializer;
        private IJsonParser _parser;

        #region Constructor
        public JsonVisitor(IJsonSerializerWrapper jsonSerializer, IJsonParser parser, ILogger<JsonVisitor> log)
        {
            _log = log ?? throw new ArgumentNullException(nameof(log));
            _jsonSerializer = jsonSerializer ?? throw new ArgumentNullException(nameof(jsonSerializer));
            _parser = parser ?? throw new ArgumentNullException(nameof(parser));
        }
        #endregion

        #region Visitor Methods
        public void DeserializeMotionDetection(IParsedMotionDetection target, ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            _log.LogTrace("Start TokenType is {}", reader.TokenType);

            if (reader.TokenType != JsonTokenType.StartObject)
                throw new NotSupportedException(string.Format("Expected StartObject token but received a {0} token", reader.TokenType.ToString()));

            target.group = _parser.ReadStringProperty(ref reader, MotionDetectionPropertyNames.Group);
            target.time = _parser.ReadLongProperty(ref reader, MotionDetectionPropertyNames.Time);
            target.monitorId = _parser.ReadStringProperty(ref reader, MotionDetectionPropertyNames.MonitorId);
            target.plug = _parser.ReadStringProperty(ref reader, MotionDetectionPropertyNames.Plug);
            target.details = ReadDetails(ref reader, options);

            reader.Read();

            _log.LogTrace("End TokenType is {}", reader.TokenType);

            if (reader.TokenType != JsonTokenType.EndObject)
                throw new NotSupportedException(string.Format("Expected EndObject token but received a {0} token", reader.TokenType.ToString()));

        }

        public void DeserializeMotionInfo(IParsedMotionInfo target, ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.StartObject)
                throw new NotSupportedException(string.Format("Expected StartObject token but received a {0} token", reader.TokenType.ToString()));

            target.plug = _parser.ReadStringProperty(ref reader, MotionInfoPropertyNames.Plug);
            target.name = _parser.ReadStringProperty(ref reader, MotionInfoPropertyNames.Name);
            target.reason = _parser.ReadStringProperty(ref reader, MotionInfoPropertyNames.Reason);
            target.matrices = ReadMatrices(ref reader, options);
            target.img = _parser.ReadStringProperty(ref reader, MotionInfoPropertyNames.Img);
            target.imgHeight = _parser.ReadIntProperty(ref reader, MotionInfoPropertyNames.ImgHeight);
            target.imgWidth = _parser.ReadIntProperty(ref reader, MotionInfoPropertyNames.ImgWidth);
            target.time = _parser.ReadLongProperty(ref reader, MotionInfoPropertyNames.Time);

            reader.Read();
            if (reader.TokenType != JsonTokenType.EndObject)
                throw new NotSupportedException(string.Format("Expected EndObject token but received a {0} token", reader.TokenType.ToString()));

            _log.LogTrace(string.Format("Completed Read operation for MotionInfo object, token type is now => {0}", reader.TokenType));
        }
        public void DeserializeMotionLocation(IParsedLocation target, ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.StartObject)
                throw new NotSupportedException(string.Format("Expected StartObject token but received a {0} token", reader.TokenType.ToString()));

            target.x = _parser.ReadDoubleProperty(ref reader, MotionLocationPropertyNames.X);
            target.y = _parser.ReadDoubleProperty(ref reader, MotionLocationPropertyNames.Y);
            target.width = _parser.ReadDoubleProperty(ref reader, MotionLocationPropertyNames.Width);
            target.height = _parser.ReadDoubleProperty(ref reader, MotionLocationPropertyNames.Height);
            target.tag = _parser.ReadStringProperty(ref reader, MotionLocationPropertyNames.Tag);
            target.confidence = _parser.ReadDoubleProperty(ref reader, MotionLocationPropertyNames.Confidence);

            reader.Read();
            if (reader.TokenType != JsonTokenType.EndObject)
                throw new NotSupportedException(string.Format("Expected EndObject token but received a {0} token", reader.TokenType.ToString()));

            _log.LogTrace("\n{0}\n", target);
        }

        #endregion

        private MotionInfo ReadDetails(ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            _parser.ReadProperty(ref reader, MotionDetectionPropertyNames.Details); //PropertyName here

            _log.LogTrace("Handing over to MotionInfoConverter, current toke type is :: {}", reader.TokenType);

            return _jsonSerializer.Deserialize<MotionInfo>(ref reader, options);
        }

        private List<MotionLocation> ReadMatrices(ref Utf8JsonReader reader, JsonSerializerOptions options)
        {
            List<MotionLocation> matrices = new List<MotionLocation>();

            _parser.ReadProperty(ref reader, MotionInfoPropertyNames.Matrices);

            reader.Read();
            if (reader.TokenType != JsonTokenType.StartArray)
                throw new JsonException();

            while (reader.Read() && reader.TokenType != JsonTokenType.EndArray)
            {
                ParsedLocation location = new ParsedLocation();
                location.accept(this, ref reader, options);
                matrices.Add(location.ToModel());
            }

            return matrices;
        }
    }
}
