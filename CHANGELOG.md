# Changelog
All notable changes to this project will be documented in this file.


## [v1.0.0]
### Added
- Motion Detection Blazor Server Pages Architecture with CI Pipeline
- CI Pipeline with build, test and release stages, [Issue#4](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/issues/4) and [Issue#6](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/issues/6)
- Generate test coverage report, [Issue#5](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/issues/5)
- Run Software Under Test (SUT) and external service dependencies within docker compose architecture environment to faciliate testing on remote CI server
- Additional Debian based image for docker tests

### Changed
- Tests run locally and within docker-compose stack via docker/docker-compose-test.yml
- Updated README.md to reflect project structure with docker usage and testing explained, [Issue#7](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/issues/7)
- Dependency packages updated in project files

### Fixed
- KafkaAdmin Service no longer runs as a background service alongside the Kafka Consumer and MqttKafkaBridge 
background services. This was exhibiting blocking behaviour on the CI server when using the ```WebApplicationFactory```
upon test server startup.
- WebApp.Repository project fixed to correctly specify target framework


## [v0.0.1]
### Added
- Motion Detection Blazor Server Pages Architecture 
