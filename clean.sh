#!/usr/bin/env sh

##
# Small utility script to clean TestResults from ./Tests folder
# In future, moving this as msbuild task
##
find ./Tests/ -path '*/TestResults/*' -delete
find ./Tests/ \( -type d -name "TestResults" \) -delete
rm -rf ./artifacts
