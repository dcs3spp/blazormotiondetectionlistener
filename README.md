<div align="center">
  
  ![Logo](https://images.unsplash.com/photo-1534357192615-81138dca8f00?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=240&q=80)
  
  <center><h1>Blazor Server Motion Detection Architecture</h1></center>
</div>

<br/>

<div align="center">

<b>Display Realtime Snapshot Images From Webcam Motion Detection</b>

[![GitHub license](https://img.shields.io/badge/license-GPL-blue.svg)](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/blob/master/LICENSE.txt)
[![pipeline status](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/badges/master/pipeline.svg)](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/pipelines)
[![coverage report](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/badges/master/coverage.svg)](https://dcs3spp.gitlab.io/blazormotiondetectionlistener/coverage/)

</div>


# 🌎 Overview

This repository provides a Blazor Server front end to display snapshot motion and object detection
images captured from the [Shinobi](https://gitlab.com/Shinobi-Systems/Shinobi) Tensorflow plugin. It is managed by a [CI pipeline](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/pipelines). The author extended the plugin to publish object detection images captured from a webcam stream onto an Mosquitto MQTT broker.

Further details of the plugin can be found [here](https://gitlab.com/cctv_web_cam/shinobi/objectdetection_mqtt_plugin/-/blob/master/README.md). A short video demonstration is available [here](https://youtu.be/L5P7aUF5XRs).

![Screenshot](https://i.ibb.co/zxVVTFQ/cam-screenshot.png)

# 🎬 Object Detection Demonstration Environment

Object detection is performed by an extended [Shinobi](https://gitlab.com/Shinobi-Systems/Shinobi) Tensorflow NodeJS plugin, extended to send snapshot images to an MQTT broker. The plugin has been configured to listen for object detections every 10 seconds. For further details visit:

- https://gitlab.com/cctv_web_cam/shinobi/objectdetection_mqtt_plugin
- https://gitlab.com/cctv_web_cam/shinobi/cambase

The plugin is running on Ubuntu Server 20.04 LTS, installed on an old reconditioned Intel(R) Core(TM) 2 Quad Core CPU - Q6600 @ 2.40GHz machine. This CPU is incompatible with the default Tensorflow library, so I had to compile the library [myself](https://gitlab.com/dcs3spp/myfiles/-/blob/master/COMPILING_LIBTENSORFLOW.md). The webcam is Wi-Fi based: Motorola MBP854CONNECT.

The Blazor Server Web interface has been tested with Chrome, Firefox, Opera and Safari 14.0 (15610.1.28.1.9, 15610). Safari versions <= 13.0.5 will not dispose the secondary signalR connection in the Blazor server page.

# 🏛 Architecture

An architectural overview is illustrated below:

<div align="center">

![Architecure](https://i.ibb.co/s1bSJsd/Architecture.png)

</div>

An object detection event is currently represented as a JSON message. In future, BSON and/or MessagePack serialization formats could be investigated to reduce message payload size.

Each camera device is represented by an MQTT topic of the format `shinobi/<group>/<monitor>/trigger`. The _group_ segment is a collective name for a group of camera devices, e.g. `front_house`. The _monitor_ segment is the identifier assigned to the camera device by the Shinobi administrator.

A simple MQTT->Kafka bridge subscribes to the MQTT broker to receive object detection events. An image contained within the message payload is uploaded to S3 storage. Subsequently, the image is stripped from the message payload and updated with a reference to the S3 image. The message is then forwarded to Kafka, keyed by the corresponding MQTT camera device topic.

A Background service subscribes to the Kakfa topic and consumes messages with a key from a configurable recognised set. Messages with matching keys are forwarded to the Blazor server page via signalR. This retrieves the motion detection event metadata and downloads the image snapshot from S3 storage for storage in a circular buffer repository. The webpage is then updated for displaying the motion detection details to the client.

# 🧰 Development Tools And Technologies

The following techologies are used for development of this project:

**Microsoft .NET Core**
- ASP.NET Core 3.1.8
- Blazor Server Pages
- .NET Core 3.1
- signalR

**Testing**
- Coverlet
- ReportGenerator
- Xunit

**Ci Pipelines**
- GitLab CI

**Docker**
- docker-compose


# 🐋 Docker

Docker-compose stacks are available in the `Docker` folder providing a replicable development environment for the following external service dependencies:

- Kafka
- Minio S3 storage
- Mosquitto MQTT Broker

There are two docker-compose stacks provided:

1. **External service dependencies (docker-compose.yml)**
2. **External service dependencies and WebApp (docker-compose-test.yml)**


## External Service Dependencies

In this scenario the WebApp runs on the local host and consumes external service dependencies hosted within a docker compose environment.

<div align="center">

![LocalArchitecture](https://i.ibb.co/3pMxNgX/dockerlocal.png)

</div>

An _.env_ file must be added in the Docker folder to store the following variables:

- MINIO_BUCKET
- MINIO_USER
- MINIO_PASSWORD- MQTT_PASSWORD
- MQTT_USER

Please refer to the sample .env file (_.env-sample_) within the _Docker_ subfolder.

Developers can store a CA certificate, server.key and server.crt for the Mosquitto MQTT Broker in the _Docker/Mqtt/Certs_ folder. These are not stored in the source control repository.

To start the external services provided by the docker-compose stack:

```bash
# from project root folder
cd Docker

# ensure there are no existing services runnning
docker-compose down

# prune any lingering containers from the stack
docker container prune -f

# start the docker-compose stack
docker-compose up
```

The Kafka broker and associated services are provided by the open source [lenses.io UI tools](https://github.com/lensesio/fast-data-dev) `landoop/fast-data-dev` image. Once started, visit _http://localhost:3030_ to view the topics, schemas etc. within a browser window. This is used for local development environments, allowing a local host client to connect to a Kafka service dependency hosted within docker-compose. The lensesio image was selected for this scenario since it encapsulate a web interface within a single docker image. This offers the advantage that there is no need to install additional software or docker images to view the Kafka cluster.


## External Services and WebApp

In this scenario, external service dependencies **and** the Software Under Test (SUT) are hosted within a docker-compose environment. This is intended for running unit, integration and functional tests within a CI pipeline at[gitlab.com](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/pipelines). This offers a replicable environment, i.e. it can be used on different machine environments and CI servers. The Software Under Test (SUT) can easily access external service dependencies while running on the CI server, since it exists within the same docker-compose network.

<div align="center">

![DockerArchitecture](https://i.ibb.co/r68Wwcb/dockerembedded.png)

</div>

System environment variables are automatically initialised from the _.env_ file in the Docker folder.

For running tests on the CI server, Confluent Kafka is used:

- [confluent platform](https://hub.docker.com/u/confluentinc/)
- [kafkastack-docker-compose](https://github.com/simplesteph/kafka-stack-docker-compose)

To start the tests provided by this docker-compose stack:

```bash
# from project root folder
cd Docker

# ensure there are no existing services runnning
docker-compose down

# prune any lingering containers from the stack
docker container prune -f

# start the docker-compose stack
docker-compose -f docker-compose-test.yml up
```

For further details, please refer to the section on _Testing_ within this document.


# 🗒 Configuration

Configuration for Mqtt, Kafka and S3 services is stored in `appsettings`.
The `appsettings.Development.json` file is used for development environments.
The `appsettings.json` file is used for production environments.

## Mqtt Configuration

The Mqtt Services is configured within the `MqttSettings` section. These settings
are used to configure the MQTT broker that the _MQTT->Kafka_ bridge listens to:

```json
"MqttSettings": {
  "Host": "katelios.local",
  "Port": 8883,
  "Topic": "shinobi/+/+/trigger",
  "Tls": {
    "CaCerts": "Certs/kateliosCA.pem"
  }
}
```

```json
"MqttSettings": {
    "Host": "localhost",
    "Port": 1883,
    "Topic": "shinobi/+/+/trigger"
  },
```

The configurable items are:

- **Host**: The host name or IP address of the MQTT server.
- **Port**: The port number of the MQTT server. Typically 1883 is the default for unsecured and 8883 is the default for secured.
- **Topic**: The topic that the MQTT-Kafka bridge monitors. This can be a wild card string, as listed in the example above, to allow motion detection notifications to be captured for many cameras. Within [Shinobi] a camera is identified by `shinobi/<group>/<monitor>/trigger`.
- **Tls**: Optionally, the _Tls_ subsection can be included to allow a secure connection to the MQTT broker. This contains a single property, _CaCerts_, that contains the path to the CA certificate of the MQTT broker. Developers can place their CA certificate within the _Certs_ subfolder of the _WebApp_ project. Certificates placed here are not committed into the source repository.
- **Credentials**: For details of configuring the credentials for the MQTT Broker, please refer to the section entitled, _Secrets Configuration_.

## Kafka Configuration

The Kafka Consumer and Producer is configured within the `KafkaSettings` section:

```json
"KafkaSettings": {
  "Consumer": {
    "BootstrapServers": "127.0.0.1:9092",
    "GroupId": "consumer-group"
  },
  "Producer": {
    "BootstrapServers": "127.0.0.1:9092",
    "LingerMs": "5"
  },
  "MqttCameraTopics": [
    "shinobi/RHSsYfiV6Z/xi5cncrNK6/trigger",
    "shinobi/group/monitor/trigger"
  ],
  "Topic": {
    "Name": "eventbus",
    "PartitionCount": 3,
    "ReplicationCount": 1
  }
}
```

The [confluent-kafka-dotnet](https://github.com/confluentinc/confluent-kafka-dotnet) docker image is used for local development. The `Consumer` and `Producer` sub-settings correspond to the configuration for Confluent Kafka consumer and producers respectively. For further details see:

- [Confluent Kafka Consumer Configuration](https://docs.confluent.io/current/installation/configuration/consumer-configs.html)
- [Confluent Kafka Producer Configuration](https://docs.confluent.io/current/installation/configuration/producer-configs.html)
- [Confluent-Kafka-Dotnet ConsumerConfig](https://github.com/confluentinc/confluent-kafka-dotnet/blob/6337d8a268da24bfdecbbde429053077ceeb2357/src/Confluent.Kafka/Config_gen.cs#L1049)
- [Confluent-Kafka-Dotnet ProducerConfig](https://github.com/confluentinc/confluent-kafka-dotnet/blob/6337d8a268da24bfdecbbde429053077ceeb2357/src/Confluent.Kafka/Config_gen.cs#L853)

The remaining configurable items are:

- **MqttCameraTopics**: A list of cameras that the Kafka consumer forwards motion detection messages to the signalR hub. Within [Shinobi](https://gitlab.com/Shinobi-Systems/Shinobi) a camera device is identified by `shinobi/<group>/<monitor>/trigger`. Each Kafka message is keyed by the camera device that it represents. Kafka messages that have a matching key with an item in the `MqttCameraTopics` configuration list are forwarded to the signalR hub on the server.
- **Topic**: Kafka topic that the Mqtt->Kafka bridge publishes incoming MQTT messages to. The consumer subscribes to this topic for forwarding to the signalR server hub for display on the dashboard web page. The topic subsection can have optional parameters for `ReplicationCount` and `PartitionCount`. If these parameters are omitted then the default replication count is 1 and the deault partition count is 3.

## S3 Configuration

The S3 Service is configured within the `S3Settings` section of settings, e.g:

```json
"S3Settings": {
    "Bucket": "images",
    "Endpoint": "localhost:9000"
  },
```

The configurable items are:

- **Bucket**: The name of the S3 storage bucket where snapshot images are uploaded to.
- **Endpoint**: The host and port number of the S3 server, in the format "hostname:port number".
- **Credentials**: Please refer to the _Secrets Configuration_ section for storing S3 credentials.

## Secrets Configuration

Within the `WebApp` project user-secrets have been setup using `dotnet user-secrets init`. These are stored for use in development environments only.

Developers need to add the following `user-secrets`:

```bash
# add MQTT Settings
dotnet user-secrets set "MqttSettings:UserName" "random_username"
dotnet user-secrets set "MqttSettings:Password" "random_password"

# add S3 Settings
dotnet user-secrets set "S3Settings:AccessKey" "random_username"
dotnet user-secrets set "S3Settings:SecretKey" "random_password"

# list secrets
dotnet user-secrets list
```

Production environments would use Azure Vault or environment variables for storing sensitive configuration details. The Azure vault aspect is not implemented yet. However environment variables are in use on the Gitlab CI pipelines of this repository.


## HTTPS Configuration

For development environments developers can use `dotnet dev-certs https` and `dotnet dev-certs https --trust`.

For production environments it is expected that a full production quality certificate is provided and configured appropriately on hosting webserver and/or reverse proxy.

# 🛠 Testing

Tests can be run locally or within a docker-compose environment.

## Local Tests

To run the local tests, start the docker-compose stack of external dependencies and then issue the dotnet test command.

```bash
# from the root directory change to the Docker folder and start the stack of local service
cd Docker
docker-compose -f docker-compose.yml up

# move to the root folder and start the tests
cd ../
dotnet test WebApp.sln
```

A test coverage report will be available within the _CoverageReports_ folder of the local repository.

Alternatively, run the `test (release)` task, defined within the _tasks.json_ _.vscode_ folder of this repository. 

## Run Tests Within Docker-Compose Environment

To run tests within a docker-compose environment a dedicated docker-compose stack has been provided within the _Docker_ folder of the project root. Tests run with an ASP.NET core environment set to _Docker_. The advantages of running tests within a docker-compose stack are:

- Facilitates integration with external dependencies, tests run within the same system environment.
- Transferrable environment, e.g. can be run locally or on CI server etc.

Issue the following command to run tests:

```bash
# from the root directory change to the Docker folder and start the stack of local service
cd Docker
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose -f docker-compose-test.yml up
```

The above command uses the following variables:

- COMPOSE_DOCKER_CLI=1: Docker-compose will use the docker build system. Alternatively set this environment variable in shell script, e.g. .bashrc
- DOCKER_BUILDKIT=1: Use docker buildkit for faster build performance. Alternatively, enable buildkit in the docker daemon file:

```json
{
  "debug": true,
  "experimental": true,
  "features": {
    "buildkit": true
  }
}
```

A test coverage report will be available within the _CoverageReports_ folder of the local repository. An _artifacts_ folder is also created. This contain [JUnit test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html) to allow gitlab.com to display which tests passed and failed when running an instance of the CI pipeline.

Alternatively, tests can be executed by running the `test (docker)` task, defined within the _tasks.json_ _.vscode_ folder of this repository.


# 🔬 Future Features

- Migrate to .NET Core 5
- Login, e.g. with OpenID Connect server such as IdentityServer4
- Control authorisation to receive notifications for specific camera devices for different users
- How to access corresponding video streams from Shinobi for display on dashboard??
- Migration to cloud with Azure Kubernetes + Azure Functions + Confluent Cloud


# 🧳 Other Projects

[Demo](https://www.youtube.com/watch?v=u1yRHivaANQ) of an application hosted within an Azure Kubernetes (AKS) cluster for education institutions to manage course and assessment information. Tested using PyUnit, PyTest and WebTest, within a Gitlab CI pipeline to deliver docker images into an Azure Kubernetes (AKS) cluster.

Checkout my other projects on [GitLab](https://gitlab.com/groups/sppears_grp/-/shared)
